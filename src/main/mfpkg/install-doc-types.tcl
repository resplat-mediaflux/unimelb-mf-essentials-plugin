#unimelb
if { [xvalue exists [asset.doc.namespace.exists :namespace unimelb]] == "false" } {
    asset.doc.namespace.create :namespace unimelb
}

asset.doc.type.update :create yes :type unimelb:licence-usage \
  :label "LicenceUsage" \
  :description "Used to track licence usage" \
  :definition < \
    :element -name "max-used" -type "integer" -index "true" -min-occurs "0" \
    < \
      :restriction -base "integer" \
      < \
        :minimum "0" \
      > \
      :attribute -name "date" -type "date" -index "true" \
      < \
        :restriction -base "date" \
        < \
          :time "false" \
        > \
      > \
    > \
   >
   
   
asset.doc.type.update :create yes :type unimelb:asset-prune \
  :label "Asset Prune" \
  :description "If set on an asset, this can be used to control asset pruning via scheduled jobs." \
  :definition < \
    :element -name ticket-id -type string -index true -min-occurs 0 -max-occurs 1 < \
 	   :description "Service Now ticket ID requesting this asset be automatically pruned and Accountable Resourcve Owner has authorized." \
    > \
    :element -name retain -type integer -index true -min-occurs 1 -max-occurs 1 < \
       :description "The number of asset versions to retain after the prune" \
       :restriction -base integer < :minimum 1 > \
    > \
	:element -name email -type email-address -min-occurs 0 < \
	   :description "An email address to notify when the asset is pruned." \
	> \
   >

# unimelb-admin
 if { [xvalue exists [asset.doc.namespace.exists :namespace unimelb-admin]] == "false" } {
    asset.doc.namespace.create :namespace unimelb-admin
}
   
asset.doc.type.update :create yes  :type unimelb-admin:user-account \
   :source-of-truth true \
   :label UserAccount \
   :description "Used to manage the disabling of accounts after an expiry time." \
   :definition < \
      :element -type date -name disable -min-occurs 0 -max-occurs 1 < \
        :description "The date after which the account should be disabled" \
        :restriction -base date < :time false > \
      > \
   >