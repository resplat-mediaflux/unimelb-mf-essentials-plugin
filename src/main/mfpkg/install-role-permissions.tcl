
# =========================================================================== #
# role namespace: unimelb
# =========================================================================== #
authorization.role.namespace.create :namespace unimelb :ifexists ignore

# =========================================================================== #
# role: unimelb:token-downloader
# =========================================================================== #
authorization.role.create :role unimelb:token-downloader :ifexists ignore
actor.grant :type role :name unimelb:token-downloader :perm < :resource -type service asset.query :access ACCESS >
actor.grant :type role :name unimelb:token-downloader :perm < :resource -type service asset.get :access ACCESS >
actor.grant :type role :name unimelb:token-downloader :perm < :resource -type service asset.content.get :access ACCESS >
actor.grant :type role :name unimelb:token-downloader :role -type role user 
