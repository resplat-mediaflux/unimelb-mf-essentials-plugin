# =========================================================================== #
# service: unimelb.asset.download.aterm.script.create
# =========================================================================== #
actor.grant :type plugin:service :name unimelb.asset.download.aterm.script.create :role -type role service-user
actor.grant :type plugin:service :name unimelb.asset.download.aterm.script.create :perm < :resource -type role:namespace unimelb: :access ADMINISTER >
actor.grant :type plugin:service :name unimelb.asset.download.aterm.script.create :role -type role unimelb:token-downloader

# =========================================================================== #
# service: unimelb.asset.download.shell.script.create
# =========================================================================== #
actor.grant :type plugin:service :name unimelb.asset.download.shell.script.create :role -type role service-user
actor.grant :type plugin:service :name unimelb.asset.download.shell.script.create :perm < :resource -type role:namespace unimelb: :access ADMINISTER >
actor.grant :type plugin:service :name unimelb.asset.download.shell.script.create :role -type role unimelb:token-downloader

# =========================================================================== #
# service: unimelb.asset.download.aterm.script.url.create
# =========================================================================== #
actor.grant :type plugin:service :name unimelb.asset.download.aterm.script.url.create :role -type role service-user
actor.grant :type plugin:service :name unimelb.asset.download.aterm.script.url.create :perm < :resource -type role:namespace unimelb: :access ADMINISTER >
actor.grant :type plugin:service :name unimelb.asset.download.aterm.script.url.create :role -type role unimelb:token-downloader

# =========================================================================== #
# service: unimelb.asset.download.shell.script.url.create
# =========================================================================== #
actor.grant :type plugin:service :name unimelb.asset.download.shell.script.url.create :role -type role service-user
actor.grant :type plugin:service :name unimelb.asset.download.shell.script.url.create :perm < :resource -type role:namespace unimelb: :access ADMINISTER >
actor.grant :type plugin:service :name unimelb.asset.download.shell.script.url.create :role -type role unimelb:token-downloader