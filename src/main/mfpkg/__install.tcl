
# install plugin module
source install-plugin-module.tcl

# create/update doc types
source install-doc-types.tcl

# set role permissions
source install-role-permissions.tcl

# set plugin service permissions
source install-service-permissions.tcl
