/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.util;


import java.util.Collection;
import java.util.Vector;

//import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;


public class NameSpaceUtil {




	/**
	 * Does the namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  assetNameSpaceExists (ServiceExecutor executor,  String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("asset.namespace.exists", dm.root());
		return r.booleanValue("exists");
	}


	static public long count (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		dm.add("levels", "infinity");
		XmlDoc.Element r = executor.execute("asset.namespace.count", dm.root());
		String t = r.value("count");
		return Long.parseLong(t);
	}
	
	/**
	 * Create the namespace (and all required parents)
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param store (can be null - inherit from parent)
	 * @param storePolicy (can be null - inherit from parent)
	 * @param visibleTo (may be null)
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  createNameSpace (ServiceExecutor executor, String nameSpace, 
			String store, String storePolicy, String visibleToRole) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",new String[]{"all", "true"},  nameSpace);
		if (store!=null && storePolicy!=null) {
			throw new Exception("You cannot give store and store policy");
		}
		if (store!=null) {
			dm.add("store", store);
		} 
		if (storePolicy!=null) {
			dm.add("store-policy", storePolicy);
		}
		if (visibleToRole!=null) {
			dm.push("visible-to");
			dm.add("actor", new String [] {"type","role"}, visibleToRole);
			dm.pop();
		}
		XmlDoc.Element r = executor.execute("asset.namespace.create", dm.root());
		return r.booleanValue("exists");
	}

	/**
	 * Return the description of a namespace
	 * 
	 * @param executor
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public XmlDoc.Element describe (ServerRoute sr, ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute(sr, "asset.namespace.describe", dm.root());
		return r;
	}

	
	

	/**
	 * Does the role namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  roleNameSpaceExists (ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("authorization.role.namespace.exists", dm.root());
		return r.booleanValue("exists");
	}


	/**
	 * Does the dictionary namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  dictionaryNameSpaceExists (ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("dictionary.namespace.exists", dm.root());
		return r.booleanValue("exists");
	}

	/**
	 * List child namespaces.
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param absolute returns the absolute namespace path, else relative to parent
	 * @return Always returns an object, may be zero length
	 * @throws Throwable
	 */
	static public Collection<String> listNameSpaces (ServiceExecutor executor, String nameSpace, Boolean absolute) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("asset.namespace.list", dm.root());
		String path = r.value("namespace/@path");
		//
		Vector<String> t = new Vector<String>();
		Collection<String> nss = r.values("namespace/namespace");
		if (nss==null) {
			return t;
		}
		//
		if (absolute) {
			if (nss!=null) {
				for (String ns : nss) {
					ns = path + "/" + ns;
					t.add(ns);
				}
			} 
		} else {
			t.addAll(nss);
		}
		return t;
	}


	/**
	 * Does the meta-data namespace exist ?
	 * 
	 * @param executor
	 * @param serverUUID - the server to execute on. Null for localhost
	 * @param nameSpace
	 * @return
	 * @throws Throwable
	 */
	static public Boolean  metaNameSpaceExists (ServiceExecutor executor,  String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("asset.doc.namespace.exists", dm.root());
		return r.booleanValue("exists");
	}




	/**
	 * Remove a specific document from an asset namespace
	 * 
	 * @param executor
	 * @param nameSpace
	 * @param mid The document id
	 * @throws Thjrowable
	 */
	static public void removeAssetNameSpaceMetaData (ServerRoute sr, ServiceExecutor executor, String nameSpace, String mid) throws Throwable {
		if (mid==null) {
			throw new Exception ("Namespace meta-data fragment id ('mid') not specified");
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add("mid", mid);
		executor.execute(sr, "asset.namespace.asset.meta.remove", dm.root());
	}

	/**
	 * Rename an asset namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @throws Throwable
	 */
	public static String renameAssetNameSpace (ServiceExecutor executor, ServerRoute sr, String oldNameSpacePath, String newName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		
		// Strip off any leading / from the new name
		int idx = newName.indexOf("/");
		String newName2 = newName;
		if (idx>=0) {
			newName2 = newName.substring(idx+1);
		}
		dm.add("name", newName2);
		dm.add("namespace", oldNameSpacePath);
		// Renames just the child part to the new name
		executor.execute(sr, "asset.namespace.rename", dm.root());
		
		// Find the new path. The silly service does not tell us.
		idx = oldNameSpacePath.lastIndexOf("/");
		if (idx<0) {
			return "/" + newName2;
		} else {
			return oldNameSpacePath.substring(0,idx+1) + newName2;
		}
	}

	/**
	 * Rename a document namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @param n  Number of assets to update per batch.  Null means 500
	 * @throws Throwable
	 */
	public static void renameDocumentNameSpace (ServerRoute sr, ServiceExecutor executor, String oldName, 
			String newName, Integer n) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("new-namespace", newName);
		dm.add("namespace", oldName);
		if (n!=null) dm.add("txn-size", n);
		executor.execute(sr, "asset.doc.namespace.rename", dm.root());
	}


	/**
	 * Rename a dictionary namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @throws Throwable
	 */
	public static void renameDictionaryNameSpace (ServerRoute sr, ServiceExecutor executor, String oldName, 
			String newName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", newName);
		dm.add("namespace", oldName);
		executor.execute(sr, "dictionary.namespace.rename", dm.root());
	}


	/**
	 * Rename a role namespace
	 * 
	 * @param executor
	 * @param oldName
	 * @param newName
	 * @throws Throwable
	 */
	public static void renameRoleNameSpace (ServerRoute sr, ServiceExecutor executor, String oldNameSpace, String newNameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", newNameSpace);
		dm.add("namespace", oldNameSpace);
		executor.execute(sr, "authorization.role.namespace.rename", dm.root());
	}



	/**
	 * Remove the ACLs for this actor.
	 * Inheritance is not changed
	 *  
	 * @param executor
	 * @param actor
	 * @param nameSpace
	 * @param resetInheritance
	 * @throws Throwable
	 */
	public static void revokeACL (ServiceExecutor executor, 
			XmlDoc.Element actor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add(actor);
		executor.execute("asset.namespace.acl.revoke", dm.root());
	}
	

	public static void revokeACL (ServiceExecutor executor, 
			String actor, String actorType, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add("actor", new String[] {"type", actorType}, actor);
		executor.execute("asset.namespace.acl.revoke", dm.root());
	}
	
	/**
	 * Reset namespace ACL inheritance back to 'only-if-no-acls'
	 * 
	 * @param executor
	 * @param namespace
	 * @throws Throwable
	 */
	public static void resetACLInheritance (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		dm.add("inherit", "only-if-no-acls");
		executor.execute("asset.namespace.acl.inherit.set", dm.root());
	}

	/**
	 * Get the depth of a namespace. "/projects" is depth 1
	 * "/projects/proj-<name>-<cid>" is depth 2
	 * 
	 * @param executor
	 * @return The depth. 
	 *     "/" depth 0
	 *     "/xyz" depth 1
	 *     "/xyz/abc" depth 2
	 * 
	 * @throws Throwable
	 */
	public static int assetNameSpaceDepth(ServiceExecutor executor, String nameSpace) throws Throwable {

		// Add "/" on the front for consistency
		String s1 = nameSpace.substring(0, 1);
		String ns = null;
		if (s1.equals("/")) {
			ns = nameSpace;
		} else {
			ns = "/" + nameSpace;
		}
		if (ns.equals("/")) {
			return 0;
		}

		// Find the depth. Because there is a leading "/", split of
		// "/projects" will return two parts {"","projects"} so we subtract 1 from the
		// array length
		String[] parts = ns.split("/");
		return (parts.length) - 1;
	}

}
