package unimelb.mf.essentials.plugin.util;

import java.io.InputStream;
import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;

public class MailUtils {

	public static void sendEmail (ServiceExecutor executor, String to, String cc, String bcc,
			String from, String subject, String body, boolean async) throws Throwable {
		Vector<String> tos = new Vector<String>();
		tos.add(to);
		//
		Vector<String> ccs = null;
		if (cc!=null) {
			ccs = new Vector<String>();
			ccs.add(cc);
		}
		//
		Vector<String> bccs = null;
		if (bcc!=null) {
			bccs = new Vector<String>();
			bccs.add(bcc);
		}

		sendEmail (executor, tos, ccs, bccs, from, subject, body, null, null, 0, null, async);
	}

	public static void sendEmail(ServiceExecutor executor, Collection<String> tos, Collection<String> ccs,
			Collection<String> bccs, String from, String subject, String body, String attachmentFileName,
			InputStream attachmentInputStream, long attachmentFileLength, String attachmentMimeType, 
			boolean async)
					throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String to : tos) {
			dm.add("to", to);
		}
		if (ccs != null) {
			for (String cc : ccs) {
				dm.add("cc", cc);
			}
		}
		if (bccs != null) {
			for (String bcc : bccs) {
				dm.add("bcc", bcc);
			}
		}
		dm.add("subject", subject);
		dm.add("body", body);
		dm.add("async", async);
		if (from!=null) {
			dm.add("from", from);
		}

		if (attachmentFileName != null || attachmentMimeType != null) {
			dm.push("attachment");
			if (attachmentFileName != null) {
				dm.add("name", attachmentFileName);
			}
			if (attachmentMimeType != null) {
				dm.add("type", attachmentMimeType);
			}
			dm.pop();
		}

		PluginService.Input input = null;
		if (attachmentInputStream != null) {
			input = new PluginService.Input(attachmentInputStream, attachmentFileLength, attachmentMimeType, null);
		}

		executor.execute("mail.send", dm.root(), input == null ? null : new PluginService.Inputs(input), null);
	}

}
