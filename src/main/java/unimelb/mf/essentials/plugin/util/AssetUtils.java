package unimelb.mf.essentials.plugin.util;

import java.util.Collection;

import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class AssetUtils {


	/**
	 * Count the number of assets in the collection and/or where predicate
	 * 
	 * @param executor
	 * @param collection
	 * @param where
	 * @return count
	 * @throws throwable
	 */
	public static String count (ServiceExecutor executor,
			String collection, String where) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (where!=null) {
			dm.add("where", where);
		}
		if (collection!=null) {
			dm.add("collection", collection);
		}
		dm.add("action", "count");
		XmlDoc.Element r = executor.execute("asset.count", dm.root());
		return r.value("total");
	}

	
	/**
	 * Count the number of assets in the collection and/or wheres predicate
	 * 
	 * @param executor
	 * @param collection
	 * @param where
	 * @return count
	 * @throws throwable
	 */
	public static String count (ServiceExecutor executor,
			String collection, Collection<String> wheres) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (wheres!=null) {
			for (String where : wheres) {
				dm.add("where", where);
			}
		}
		if (collection!=null) {
			dm.add("collection", collection);
		}
		dm.add("action", "count");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		return r.value("value");
	}
	
	
	/**
	 * Sum the content (bytes) of the assets in the where predicate
	 * 
	 * @param executor
	 * @param where
	 * @return count
	 * @throws throwable
	 */
	public static String sumContent (ServiceExecutor executor,
			String where) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (where!=null) {
			dm.add("where", where);
		}
		dm.add("action", "sum");
		dm.add("xpath", "content/size");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		return r.value("value");
	}
	


	/**
	 * Sum the content (bytes) of the assets in the where predicate
	 * 
	 * @param executor
	 * @param where
	 * @return count
	 * @throws throwable
	 */
	public static String sumContent (ServiceExecutor executor,
			Collection<String> wheres) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (wheres!=null) {
			for (String where : wheres) {
				dm.add("where", where);
			}
		}
		dm.add("action", "sum");
		dm.add("xpath", "content/size");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		return r.value("value");
	}

	
	/**
	 * Get the sum of the content size recursively for this local citable ID.
	 * 
	 * @param executor
	 * @param id
	 *            citable ID of interest
	 * @return XmlDoc.Element as returned by asset.query :action sum :xpath
	 *         content/size
	 * @throws throwable
	 */
	public static XmlDoc.Element contentSizeSum(ServiceExecutor executor,
			String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");

		//
		String query = "(cid starts with '" + id + "' or cid = '" + id + "')";
		dm.add("where", query);
		dm.add("action", "sum");
		dm.add("xpath", "content/size");
		dm.add("pdist", 0); // Force local
		return executor.execute("asset.query", dm.root());
	}


	/**
	 * Prune content versions leaving just the latest
	 * 
	 * @param executor
	 * @param id asset ID of interest
	 * @return XmlDoc.Element as returned by asset.query :action sum :xpath
	 *         content/size
	 * @throws throwable
	 */
	public static void pruneContent (ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		executor.execute("asset.prune", dm.root());

	}

	/**
	 * Returns the asset id for this local CID
	 * 
	 * @param cid
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String getId(ServiceExecutor executor, String cid)
			throws Throwable {

		XmlDocMaker doc = new XmlDocMaker("args");
		doc.add("cid", cid);
		doc.add("pdist", 0); // Force local
		XmlDoc.Element r = executor.execute("asset.get", doc.root());
		return r.value("asset/@id");

	}


	public static String getType (ServiceExecutor executor, String id, boolean isCID) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (isCID) {
			dm.add("cid", id);
		} else {
			dm.add("id", id);
		}
		XmlDoc.Element r = executor.execute("asset.get", dm.root());
		return r.value("asset/type");
	}






	/**
	 * DOes an asset exist on the local server?
	 * 
	 * @param executor
	 * @param id
	 * @param citeable Is the id a citeable ID 
	 * @return
	 * @throws Throwable
	 */
	public static boolean exists(ServiceExecutor executor, ServerRoute sr, String id,
			boolean citeable) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		if (citeable) {
			dm.add("cid", id);
		} else {
			dm.add("id", id);
		}
		XmlDoc.Element r = executor.execute(sr, "asset.exists", dm.root());
		return r.booleanValue("exists");
	}
	
	/**
	 * DOes an asset exist on the local server?
	 * 
	 * @param executor
	 * @param path - the path to the asset <namespace/name>  (must include leading /)
	 * @return the id if it exists.  Null if does not exist
	 * @throws Throwable
	 */
	public static String exists(ServiceExecutor executor, String path) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", "path="+path);
		XmlDoc.Element r = executor.execute("asset.exists", dm.root());
		if (r.booleanValue("exists")) {
			return r.value("exists/@id");
		} else {
			return null;
		}
	}


	
	/**
	 * DOes an asset namespace exist
	 * 
	 * @param executor
	 * @param namespace
	 * @return
	 * @throws Throwable
	 */
	public static boolean assetNameSpaceExists(ServiceExecutor executor, String namespace) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("asset.namespace.exists", dm.root());
		return r.booleanValue("exists");
	}
	
	/**
	 * DOes an asset namespace exist on the given server
	 * 
	 * @param executor
	 * @param namespace
	 * @return
	 * @throws Throwable
	 */
	public static boolean assetNameSpaceExists(ServiceExecutor executor, ServerRoute sr, String namespace) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute(sr, "asset.namespace.exists", dm.root());
		return r.booleanValue("exists");
	}
	
	

	
	public static String getNamespace(ServiceExecutor executor, String id,
			String proute) throws Throwable {

		ServerRoute sr = proute == null ? null : new ServerRoute(proute);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		XmlDoc.Element r = executor.execute(sr, "asset.get", dm.root());
		return r.value("asset/namespace");
	}

	public static boolean hasContent(ServiceExecutor executor, String id,
			String proute) throws Throwable {

		ServerRoute sr = proute == null ? null : new ServerRoute(proute);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		XmlDoc.Element r = executor.execute(sr, "asset.get", dm.root());
		if (r.element("asset/content") != null) {
			return true;
		}
		return false;
	}





	/**
	 * Get the asset's meta-data
	 * 
	 * @param executor
	 * @param cid Citable ID
	 * @param id Asset ID (give one of id or cid)
	 * @throws Throwable
	 */
	public static XmlDoc.Element getAsset (ServiceExecutor executor, ServerRoute sr, String cid, String id) throws Throwable {

		// Get meta-data in input object
		XmlDocMaker dm = new XmlDocMaker("args");
		if (cid!=null) {
			dm.add("cid", cid);
		} else if (id!=null) {
			dm.add("id", id);
		} else {
			throw new Exception("One of cid or id must be given");
		}

		return executor.execute(sr, "asset.get", dm.root());

	}



	/**
	 * Returns the asset content encapsulation mime type (content/type). Null if none.  
	 * 
	 * @param id
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String getContentMimeType (ServiceExecutor executor, String id) throws Throwable {

		XmlDocMaker doc = new XmlDocMaker("args");
		doc.add("id", id);
		doc.add("pdist", 0); // Force local
		XmlDoc.Element r = executor.execute("asset.get", doc.root());
		return r.value("asset/content/type");
	}
	

	/**
	 * Returns the asset content logical mime type (content/ltype). Null if none.  
	 * 
	 * @param id
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static String getContentLogicalMimeType (ServiceExecutor executor, String id) throws Throwable {

		XmlDocMaker doc = new XmlDocMaker("args");
		doc.add("id", id);
		doc.add("pdist", 0); // Force local
		XmlDoc.Element r = executor.execute("asset.get", doc.root());
		return r.value("asset/content/ltype");
	}
	
	/**
	 * Return version information on all versions
	 * 
	 * @param executor
	 * @param id
	 * @return
	 * @throws Throwable
	 */
	public static Collection<XmlDoc.Element> versions (ServiceExecutor executor, String id) throws Throwable {

		XmlDocMaker doc = new XmlDocMaker("args");
		doc.add("id", id);
		XmlDoc.Element r = executor.execute("asset.versions", doc.root());
		return r.elements("asset/version");
	}
	
}
