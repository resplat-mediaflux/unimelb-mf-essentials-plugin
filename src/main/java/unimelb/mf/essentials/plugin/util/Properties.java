/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.util;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class Properties {



	/**
	 * Returns value of application property
	 * 
	 * @param executor
	 * @param name
	 * @param app
	 * @return Will be null if does not exist
	 * @throws Throwable
	 */
	static public String getApplicationProperty (ServiceExecutor executor, String name, String app) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("property", new String[]{"app", app}, name);
		XmlDoc.Element r = executor.execute("application.property.exists", dm.root());
		if (r.booleanValue("exists")) {
			r = executor.execute("application.property.get", dm.root());
			return r.value("property");
		} else {
			return null;
		}
	}
	
	/**
	 * Get server property
	 * 
	 * @param executor
	 * @param name property name
	 * @return
	 * @throws Throwable
	 */
	public static String getServerProperty (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		XmlDoc.Element r = executor.execute("server.property.get", dm.root());
		return r.value("property");
	}
	

}
