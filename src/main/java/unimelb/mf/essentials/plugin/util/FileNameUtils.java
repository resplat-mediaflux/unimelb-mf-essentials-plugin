package unimelb.mf.essentials.plugin.util;

public class FileNameUtils {

    public static final String REGEX_ILLEGAL_CHARS = ".*[\\\\\\\\/\":*?<>|]+.*";

    public static final String REGEX_LEADING_SPACES = "^ +.*";

    public static final String REGEX_TRAILING_SPACES = ".* +$";

    public static final String REGEX_ILLEGAL_PATHS = ".*(/[^/]*[\\\\\":*?<>|]+[^/]*|/ +[^/]*|/[^/]* +(/|$))+.*";

    public static boolean isValidFileName(String name) {
        if (name == null || name.trim().isEmpty()) {
            return false;
        }
        return !(name.matches(REGEX_ILLEGAL_CHARS) || name.matches(REGEX_LEADING_SPACES) || name.matches(REGEX_TRAILING_SPACES));
    }
}
