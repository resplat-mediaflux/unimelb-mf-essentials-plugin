package unimelb.mf.essentials.plugin.util;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

/**
 * A collection of functions to aid in federation management
 * 
 * @author nebk
 * 
 */
public class PeerUtil {

	/**
	 * Construct the ServerRoute for a particular peer UUID
	 * 
	 * @param executor
	 * @param peer UUID
	 * @return
	 * @throws Throwable
	 */
	public static ServerRoute findPeerRoute (ServiceExecutor executor, String peer) throws Throwable {
		if (peer==null) return null;

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", peer);
		dm.add("check-peer-is-reachable", true);   // Forces recheck
		XmlDoc.Element r = executor.execute("server.peer.status", dm.root());
		if (r!=null) {
			String t = r.value("peer/@uuid");
			if (t==null) {
				throw new Exception("Could not determine route to peer : returned peer record = " + r);	
			}
			return new ServerRoute(t);
		} else {
			throw new Exception("Could not determine route to peer: returned peer record is null");
		}
	}
	
	/**
	 * Find the server at the end of the server route
	 * 
	 * @param proute
	 * @return
	 */
	public static String serverUUIDFromProute(String proute) {

		if (proute == null) {
			return PluginService.serverIdentityAsString();
		} else {
			if (proute.contains(".")) {
				String[] s = proute.split("\\.");
				return s[(s.length) - 1];
			} else {
				return proute;
			}
		}
	}
	
	/**
	 * FInd the server UUID from the peer name
	 * 
	 */
	public static String findPeerUUID (ServiceExecutor executor, String peer) throws Throwable {
		if (peer==null) return null;

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", peer);
		XmlDoc.Element r = executor.execute("server.peer.describe", dm.root());
		if (r!=null) {
			return r.value("peer/@uuid");
		} else {
			return null;
		}
	}
}
