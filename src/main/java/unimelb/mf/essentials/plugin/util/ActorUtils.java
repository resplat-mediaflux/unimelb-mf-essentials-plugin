package unimelb.mf.essentials.plugin.util;

import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;

public class ActorUtils {


	/**
	 * Return all known actor types
	 * 
	 * @param executor
	 * @return
	 * @throws Throwable
	 */
	public static Collection<String> types (ServiceExecutor executor) throws Throwable {
		XmlDoc.Element r = executor.execute("actor.type.list");
		return r.values("type");
	}

}
