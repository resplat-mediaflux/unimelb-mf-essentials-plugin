/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.util;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import unimelb.utils.PathUtils;

public class CollectionAssetUtil {

	/**
	 * Create collection asset with optional visibility role
	 * Makes any intermediarty parents if needed
	 * 
	 * @param sr ServerRoute to server. If null, local server
	 * @param executor
	 * @param path full path to the new collection asset including its name
	 * @param visibilityRole
	 * @param note A note to set in the mf-note meta-data
	 * @throws Throwable
	 */
	public static String create (ServerRoute sr, ServiceExecutor executor, 
			String path, String visibilityRole, String note) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("pid", new String[] {"create", "true"}, "path="+PathUtils.getParentPath(path));
		dm.add("name", PathUtils.getFileName(path));
		dm.add("collection", new String[] {"unique-name-index", "true"}, "true");
		if (visibilityRole!=null) {
			dm.push("visible-to");
			dm.add("actor", new String[] {"type", "role"}, visibilityRole);
			dm.pop();
		}
		if (note!=null) {
			dm.push("meta");
			dm.push("mf-note");
			dm.add("note", note);
			dm.pop();
			dm.pop();
		}
		XmlDoc.Element r = executor.execute(sr, "asset.create", dm.root());
		return r.value("id");
	}

	/**
	 * Fetch the asset ID of an collection asset from its path
	 * 
	 * @param executor
	 * @param path
	 * @parm throwIt If true and asset does not exist, throw excweption, else return null
	 * @return
	 * @throws Throwable
	 */
	public static String getAssetIDFromPath (ServiceExecutor executor, String path, Boolean throwIt) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", "path="+path);
		XmlDoc.Element r = executor.execute("asset.exists", dm.root());
		if (r.booleanValue("exists")) {
			return r.value("exists/@id");
		} else {
			if (throwIt) {
				throw new Exception("Asset collection with path '"+path+"' does not exist");
			} else {
				return null;
			}
		}
	}


	/**
	 * List direct child collection asset paths of the given parent collection.
	 * Does not list  assets that are not collections. This function is not
	 * recursive
	 * 
	 * @param executor
	 * @param path - the *path* of the parent collection asset
	 * @param absolute returns the absolute collection path, else relative to parent
	 * @return Always returns an object, may be zero length
	 * @throws Throwable
	 */
	static public Collection<String> listDirectChildCollectionPaths (ServiceExecutor executor, String path, Boolean absolute) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "(asset is collection) and (asset in collection '" + path + "')");
		dm.add("action", "get-path");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		Collection<String> paths = r.values("path");
		//
		Vector<String> t = new Vector<String>();
		if (paths==null) {
			return t;
		}
		//
		if (absolute) {
			t.addAll(paths);
		} else {
			for (String p : paths) {
				t.add(PathUtils.getFileName(p));
			}
		}
		return t;
	}

	/**
	 * List direct child collection asset IDs of the given parent collection.
	 * Does not list  assets that are not collections. This function is not
	 * recursive
	 * 
	 * @param executor
	 * @param id - the asset id of the parent collection asset
	 * @return Always returns an object, may be zero length
	 * @throws Throwable
	 */
	static public Collection<String> listDirectChildAssetIDs (ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", "(asset is collection) and (asset in collection " + id + ")");
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		Collection<String> ids = r.values("id");
		//
		Vector<String> t = new Vector<String>();
		if (ids==null) {
			return t;
		} 
		t.addAll(ids);
		return t;
	}


	/**
	 * Recursively list all  child asset IDs of the given parent 
	 * collection. Includes collection assets and non-collection assets
	 * 
	 * @param executor
	 * @param id - the asset ID of the parent collection. Don't use form path=<path> as does not
	 *             work in a collection query
	 * @return Always returns an object, may be zero length
	 * @throws Throwable
	 */
	static public Collection<String> listAllChildAssetIDs (ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("collection", id);
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		Collection<String> ids = r.values("id");
		//
		Vector<String> t = new Vector<String>();
		if (ids==null) {
			return t;
		}
		t.addAll(ids);
		return t;
	}


	/**
	 * Sum content for all assets in the collection
	 * 
	 * @param executor
	 * @param id - parent collection asset id. Don't use form path=<path> as does not
	 *             work in a collection query
	 * @param where - extra selection
	 * @return
	 * @throws Throwable
	 */
	static public XmlDoc.Element sumAssetContent (ServiceExecutor executor,  String id, String where) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("collection", id);;
		if (where!=null) {
			dm.add("where", where);
		}
		dm.add("action", "sum");
		dm.add("xpath", "content/size");
		dm.add("size", "infinity");
		return executor.execute("asset.query", dm.root());
	}





	/**
	 * Add a query to find plain (not themselves collection assets)
	 * assets only under the parent collection asset
	 * 
	 * @param id
	 * @param depth
	 * @param where
	 * @param dm
	 * @throws Throwable
	 */
	public static void makeQueryForPlainAssets (String id, String depth, 
			String where, XmlDocMaker dm) throws Throwable {
		if (depth==null) {
			dm.add("collection", id);
			String t = "not(asset is collection)";
			if (where==null) {
				dm.add("where", t);
			} else {
				dm.add("where", t + " and (" + where + ")");
			} 
		} else {
			String t = "not(asset is collection) and (asset in collection or subcollection of "+id+" to maximum depth " + depth + ")";
			if (where==null) {
				dm.add("where", t);
			} else {
				dm.add("where", t + " and ("+where + ")");
			}
		}
	}	


	/**
	 * Add a query to find all assets (collection and plain) under
	 * the parent collection asset
	 * 
	 * @param id
	 * @param depth
	 * @param where
	 * @param dm
	 * @throws Throwable
	 */
	public static void makeQueryForAllAssets (String id, String depth,
			String where, XmlDocMaker dm) throws Throwable {
		if (depth==null) {
			dm.add("collection", id);
			if (where!=null) {
				dm.add("where", where);
			}
		} else {
			String t = "(asset in collection or subcollection of "+id+" to maximum depth " + depth + ")";
			if (where==null) {
				dm.add("where", t);
			} else {
				dm.add("where", t+" and ("+where + ")");
			}
		}
	}
}