package unimelb.mf.essentials.plugin.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class Actor {

    public static XmlDoc.Element describe(ServiceExecutor executor, String actorType, String actorName)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", actorType);
        dm.add("name", actorName);
        return executor.execute("actor.describe", dm.root()).element("actor");
    }

    public static boolean haveRole(ServiceExecutor executor, String actorType, String actorName, String roleType,
            String roleName) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", actorType);
        dm.add("name", actorName);
        dm.add("role", new String[] { "type", roleType }, roleName);
        return executor.execute("actor.have", dm.root()).booleanValue("actor/role");
    }

    public static boolean haveRole(ServiceExecutor executor, String actorType, String actorName, String roleName)
            throws Throwable {
        return haveRole(executor, actorType, actorName, "role", roleName);
    }

    public static boolean havePerm(ServiceExecutor executor, String actorType, String actorName, String resourceType,
            String resourceName, String resourceAccess) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", actorType);
        dm.add("name", actorName);
        dm.push("perm");
        dm.add("resource", new String[] { "type", resourceType }, resourceName);
        dm.add("access", resourceAccess);
        dm.pop();
        return executor.execute("actor.have", dm.root()).booleanValue("actor/perm");
    }

    public static boolean isSystemAdministrator(ServiceExecutor executor, String actorType, String actorName)
            throws Throwable {
        return haveRole(executor, actorType, actorName, "role", Role.SYSTEM_ADMINISTRATOR_ROLE);
    }

    public static void grantRoles(ServiceExecutor executor, String actorType, String actorName,
            List<XmlDoc.Element> roleElements, Collection<String> roleNames) throws Throwable {
        grantRevokeRoles(executor, actorType, actorName, roleElements, roleNames, true);
    }

    public static void revokeRoles(ServiceExecutor executor, String actorType, String actorName,
            List<XmlDoc.Element> roleElements, Collection<String> roleNames) throws Throwable {
        grantRevokeRoles(executor, actorType, actorName, roleElements, roleNames, false);
    }

    public static void grantRoles(ServiceExecutor executor, String actorType, String actorName, String... roleNames)
            throws Throwable {
        grantRevokeRoles(executor, actorType, actorName, true, roleNames);
    }

    public static void revokeRoles(ServiceExecutor executor, String actorType, String actorName, String... roleNames)
            throws Throwable {
        grantRevokeRoles(executor, actorType, actorName, false, roleNames);
    }

    static void grantRevokeRoles(ServiceExecutor executor, String actorType, String actorName, boolean grant,
            String... roleNames) throws Throwable {
        if (roleNames == null || roleNames.length == 0) {
            return;
        }
        List<String> rs = new ArrayList<String>();
        for (String roleName : roleNames) {
            rs.add(roleName);
        }
        grantRevokeRoles(executor, actorType, actorName, null, rs, grant);
    }

    static void grantRevokeRoles(ServiceExecutor executor, String actorType, String actorName,
            List<XmlDoc.Element> roleElements, Collection<String> roleNames, boolean grant) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (roleElements != null) {
            for (XmlDoc.Element re : roleElements) {
                dm.add("role", new String[] { "type", re.stringValue("@type", "role") }, re.value());
            }
        }
        if (roleNames != null) {
            for (String roleName : roleNames) {
                dm.add("role", new String[] { "type", "role" }, roleName);
            }
        }
        dm.add("type", actorType);
        dm.add("name", actorName);
        executor.execute(grant ? "actor.grant" : "actor.revoke", dm.root());
    }

    public static void grantRole(ServiceExecutor executor, String actorType, String actorName, String roleType,
            String roleName) throws Throwable {
        grantRevokeRole(executor, actorType, actorName, roleType, roleName, true);
    }

    public static void revokeRole(ServiceExecutor executor, String actorType, String actorName, String roleType,
            String roleName) throws Throwable {
        grantRevokeRole(executor, actorType, actorName, roleType, roleName, false);
    }

    static void grantRevokeRole(ServiceExecutor executor, String actorType, String actorName, String roleType,
            String roleName, boolean grant) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("role", new String[] { "type", roleType }, roleName);
        dm.add("type", actorType);
        dm.add("name", actorName);
        executor.execute(grant ? "actor.grant" : "actor.revoke", dm.root());
    }

    public static void grantPerm(ServiceExecutor executor, String actorType, String actorName, String resourceType,
            String resourceName, String access) throws Throwable {
        grantRevokePerm(executor, actorType, actorName, resourceType, resourceName, access, true);
    }

    public static void revokePerm(ServiceExecutor executor, String actorType, String actorName, String resourceType,
            String resourceName, String access) throws Throwable {
        grantRevokePerm(executor, actorType, actorName, resourceType, resourceName, access, false);
    }

    static void grantRevokePerm(ServiceExecutor executor, String actorType, String actorName, String resourceType,
            String resourceName, String access, boolean grant) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", actorType);
        dm.add("name", actorName);
        dm.push("perm");
        dm.add("resource", new String[] { "type", resourceType }, resourceName);
        dm.add("access", access);
        dm.pop();
        executor.execute(grant ? "actor.grant" : "actor.revoke", dm.root());
    }

    public static class Self {

        public static boolean haveRole(ServiceExecutor executor, String roleType, String roleName) throws Throwable {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("role", new String[] { "type", roleType }, roleName);
            return executor.execute("actor.self.have", dm.root()).booleanValue("role");
        }

        public static boolean haveRole(ServiceExecutor executor, String roleName) throws Throwable {
            return haveRole(executor, "role", roleName);
        }

        public static boolean havePerm(ServiceExecutor executor, String resourceType, String resourceName,
                String resourceAccess) throws Throwable {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.push("perm");
            dm.add("resource", new String[] { "type", resourceType }, resourceName);
            dm.add("access", resourceAccess);
            dm.pop();
            return executor.execute("actor.self.have", dm.root()).booleanValue("perm");
        }

        public static boolean isSystemAdministrator(ServiceExecutor executor) throws Throwable {
            return haveRole(executor, "role", Role.SYSTEM_ADMINISTRATOR_ROLE);
        }
    }
}
