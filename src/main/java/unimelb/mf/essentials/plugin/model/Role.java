package unimelb.mf.essentials.plugin.model;

import java.util.Collection;
import java.util.LinkedHashSet;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;

public class Role {

	public static final String SYSTEM_ADMINISTRATOR_ROLE = "system-administrator";

	public static final String MODEL_USER_ROLE = "daris:pssd.model.user";
	public static final String MODEL_USER_ROLE_TITLE = "model-user";

	public static final String POWER_MODEL_USER_ROLE = "daris:pssd.model.power.user";
	public static final String POWER_MODEL_USER_ROLE_TITLE = "power-user";

	public static final String PROJECT_CREATOR_ROLE = "daris:pssd.project.create";
	public static final String PROJECT_CREATOR_ROLE_TITLE = "project-creator";

	public static final String SUBJECT_CREATOR_ROLE = "daris:pssd.subject.create";
	public static final String SUBJECT_CREATOR_ROLE_TITLE = "subject-creator";

	public static final String OBJECT_ADMIN_ROLE = "daris:pssd.object.admin";
	public static final String OBJECT_ADMIN_ROLE_TITLE = "object-admin";

	public static final String OBJECT_GUEST_ROLE = "daris:pssd.object.guest";
	public static final String OBJECT_GUEST_ROLE_TITLE = "object-guest";

	/**
	 * @deprecated
	 */
	public static final String R_SUBJECT_ADMIN_ROLE = "daris:pssd.r-subject.admin";

	/**
	 * @deprecated
	 */
	public static final String R_SUBJECT_GUEST_ROLE = "daris:pssd.r-subject.guest";

	public static final String DICOM_INGEST_ROLE = "daris:pssd.dicom-ingest";
	public static final String DICOM_INGEST_ROLE_TITLE = "dicom-ingest";

	public static Collection<String> rolesFromTitles(Collection<String> roleTitles) {
		Collection<String> roles = new LinkedHashSet<String>();
		if (roleTitles != null) {
			for (String roleTitle : roleTitles) {
				if (MODEL_USER_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(MODEL_USER_ROLE);
				} else if (POWER_MODEL_USER_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(POWER_MODEL_USER_ROLE);
				} else if (PROJECT_CREATOR_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(PROJECT_CREATOR_ROLE);
				} else if (SUBJECT_CREATOR_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(SUBJECT_CREATOR_ROLE);
				} else if (OBJECT_ADMIN_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(OBJECT_ADMIN_ROLE);
				} else if (OBJECT_GUEST_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(OBJECT_GUEST_ROLE);
				} else if (DICOM_INGEST_ROLE_TITLE.equalsIgnoreCase(roleTitle)) {
					roles.add(DICOM_INGEST_ROLE);
				}
			}
		}
		return roles.isEmpty() ? null : roles;
	}

	public static void destroy(ServiceExecutor executor, String role) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("role", role);
		executor.execute("authorization.role.destroy", dm.root());
	}

}
