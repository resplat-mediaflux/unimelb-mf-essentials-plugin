package unimelb.mf.essentials.plugin;

import arc.mf.plugin.ConfigurationResolver;
import arc.mf.plugin.PluginModule;
import arc.mf.plugin.PluginService;
import unimelb.mf.essentials.plugin.services.*;
import unimelb.mf.essentials.plugin.services.audit.*;
import unimelb.mf.essentials.plugin.services.user.SvcUserRevokeOnlyRole;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EssentialsPluginModule implements PluginModule {
    
    public static final String SERVICE_PREFIX = "unimelb.";

	private final List<PluginService> _services;

	public EssentialsPluginModule() throws Throwable {

		_services = new ArrayList<PluginService>();
		_services.add(new SvcActorsPermHave());
		_services.add(new SvcActorsPermContain());

		//
		_services.add(new SvcApplicationPropertyDescribe());
		
		//
		_services.add(new SvcAssetCollectionACLChildList());
		_services.add(new SvcAssetCollectionDestroy());
				
		//	
		_services.add(new SvcAssetContentAllHasStore());
		_services.add(new SvcAssetContentAllIsSingular());
		_services.add(new SvcAssetContentChecksumExport());
		_services.add(new SvcAssetContentCopyAllCreate());
		_services.add(new SvcAssetContentCopyAllRemove());
		_services.add(new SvcAssetContentCopiesValidate());
		_services.add(new SvcAssetContentCopiesValidatePiped());
		_services.add(new SvcAssetContentAccessLast());
		_services.add(new SvcAssetContentAtimeAnalyze());
		_services.add(new SvcAssetContentSizeSumAllVersions());
		_services.add(new SvcAssetContentTruncate());
		//
		_services.add(new SvcAssetContentValidate());
		_services.add(new SvcAssetContentValidateBasic());
		//
		_services.add(new SvcAssetContentZeroSizeFind());
		_services.add(new SvcAssetContentZeroSizeVersionOnePrune());
		//
		_services.add(new SvcAssetFindAndNotify());
		_services.add(new SvcAssetPathsCSVDump());
		_services.add(new SvcAssetProtocolUserList());
		_services.add(new SvcAssetPrune());
		_services.add(new SvcAssetReplicaNeedToReplicateAllVersions());
		_services.add(new SvcAssetReplicaNeedToReplicate());
		_services.add(new SvcAssetVersionsStimeSameCheck());
		_services.add(new SvcAssetMultiVersionFind());
		//
		_services.add(new SvcAssetProcessingQueueAllReset());
		_services.add(new SvcAssetProcessingQueueEntryReexecute());
		//
		_services.add(new SvcAssetCopy());
//		_services.add(new SvcAssetCopy0());
		_services.add(new SvcAssetDownloadAtermScriptCreate());
		_services.add(new SvcAssetDownloadAtermScriptUrlCreate());
		_services.add(new SvcAssetDownloadShellScriptCreate());
		_services.add(new SvcAssetDownloadShellScriptUrlCreate());

		_services.add(new SvcAssetNameValidate());
		_services.add(new SvcAssetNameSpaceNameValidate());
		_services.add(new SvcFileNameValidate());

		// Merge these when ticket 4017 addressed.
		_services.add(new SvcAssetIndexValidate());
		_services.add(new SvcAssetIndexValidateMulti());

		_services.add(new SvcAssetIngestRate());

		_services.add(new SvcAssetMetadataCSVDump());
		_services.add(new SvcAssetMetadataCSVExport());
		_services.add(new SvcAssetMetadataCSVSend());
		_services.add(new SvcAssetMetaDataRename());

		_services.add(new SvcAssetNamespaceAclInheritSet());
		_services.add(new SvcAssetNamespaceAclRemove());
		_services.add(new SvcAssetNameSpaceActorExclude());
		_services.add(new SvcAssetNameSpaceACLChildInheritanceList());
		_services.add(new SvcAssetNameSpaceACLChildList());
		_services.add(new SvcAssetNameSpaceChildCount());
		_services.add(new SvcAssetNameSpaceChildDestroy());
		_services.add(new SvcAssetNameSpaceChildFind());
		_services.add(new SvcAssetNameSpaceAssetDuplicateFind());
		_services.add(new SvcAssetNameSpaceChildSum());
//		_services.add(new SvcAssetNameSpaceCopy0());
		_services.add(new SvcAssetNameSpaceDestroy());
		_services.add(new SvcAssetNameSpaceDuplicates());
		_services.add(new SvcAssetNamespaceInaccessibleList());
		_services.add(new SvcAssetNameSpaceMetaDataCopy());
		_services.add(new SvcAssetNameSpaceMetaDataElementRename());
		_services.add(new SvcAssetNameSpaceMetaDataFind());
		_services.add(new SvcAssetNameSpaceMetaDataRemove());
		_services.add(new SvcAssetNameSpaceMetaDataRename());
		_services.add(new SvcAssetNameSpaceMetaDataTreeCopy());
		_services.add(new SvcAssetNameSpaceWhiteSpaceRemove());
		_services.add(new SvcAssetNamespaceQuotaUsedCheck());
		_services.add(new SvcAssetNamespaceStoreOverriddenList());
		_services.add(new SvcAssetNameSpaceAssetTrackingCreate());
		//
		_services.add(new SvcCollectionChildSum());
		_services.add(new SvcCollectionPopulateFromNamespace());
		
		_services.add(new SvcAssetNoteGenerate());

		_services.add(new SvcAssetReplicateCheck());
		_services.add(new SvcAssetReplicateDestroyedCheck());
		_services.add(new SvcAssetReplicateJobErrorNotify());
		_services.add(new SvcAssetReplicateTo());
		
		_services.add(new SvcAssetShareableAllInvalidDestroy());
		_services.add(new SvcAssetShareableAllDescribe());
		_services.add(new SvcAssetShareableExpiredDestroy());
		//
		_services.add(new SvcAssetStoreDescribe());
		//
		_services.add(new SvcAssetShareableDownloadCreate());
		_services.add(new SvcAssetNamespaceShareableDownloadCreate());
		
		_services.add(new SvcAssetWormCheck());
		_services.add(new SvcAssetWormStateCheck());
		//
		_services.add(new SvcAssetWormSet());
		_services.add(new SvcAssetWormStateSet());

		_services.add(new SvcAssetSizeDistribution());
		_services.add(new SvcAssetSymlinkMigrate());

		_services.add(new SvcAuditAssetAccessStat());
		_services.add(new SvcAuditUserActiveCount());
		_services.add(new SvcAuditUserActiveList());
		_services.add(new SvcAuditUserInactiveCount());
		_services.add(new SvcAuditUserInactiveList());
		_services.add(new SvcAuditUserLogonDescribe());
		_services.add(new SvcAuditUserLogonStat());

		_services.add(new SvcAutomationUserInteractionExpiredDestroy());
		_services.add(new SvcAuthenticationIntrusionDenyClear());
		
		_services.add(new SvcClusterErrorGet());

		_services.add(new SvcDataMoverTokenInstallerScriptGet());
		_services.add(new SvcDataMoverTokenInstallerScriptUrlGet());

		_services.add(new SvcIPAddressResolve());

		_services.add(new SvcLdapAVArchiveUserCsvImport());
		_services.add(new SvcLdapDomainSidGet());
		_services.add(new SvcLdapProviderHostUpdate());
		_services.add(new SvcLdapUserCsvImport());
		// This service has issues with the inherited AD domain
		//_services.add(new SvcLdapUserFind());
		//
		_services.add(new SvcLdapProviderHostProbe());

		_services.add(new SvcLicenceExpiryNotify());
		_services.add(new SvcLicenceUsage());
		_services.add(new SvcLicenceUsageDescribe());
		_services.add(new SvcNotificationSend());

		// These 4 Moved here from nig-essentials
		_services.add(new SvcReplicateCheck());
		_services.add(new SvcReplicatePathCheck());
		_services.add(new SvcReplicatePrimaryFindFromReplica());
		_services.add(new SvcReplicateSync());
		
		_services.add(new SvcSecureIdentityTokenCreate());
		_services.add(new SvcSecureIdentityTokenInvalidFind());
		//
		_services.add(new SvcServerAuditExport());
		_services.add(new SvcServerCertificateExpiryNotify());
		_services.add(new SvcServerAuditIPQuery());
		_services.add(new SvcServerPeerStatus());
		_services.add(new SvcServerClusterCheck());
		_services.add(new SvcServerDataBaseBackupAndMove());
		_services.add(new SvcServerAbortedFilesHandle());
		
		//
		_services.add(new SvcSMBSessionCount());
		
		//
		_services.add(new SvcAssetStoragePerformanceAnalyze());
		
		_services.add(new SvcServerThreadList());

		//
		_services.add(new SvcScheduleJobErrorNotify());
		_services.add(new SvcShoppingCartDescribe());
		_services.add(new SvcShoppingCartList());

		_services.add(new SvcSystemSessionList());

		_services.add(new SvcUserCount());
		_services.add(new SvcUserList());
		_services.add(new SvcUserLoginCount());

		_services.add(new SvcUserEmailExport());
		_services.add(new SvcUserEmailList());

		_services.add(new SvcUserMetadataCopy());
		_services.add(new SvcUserPermissionsCopy());
		_services.add(new SvcUserProfileCopy());
		_services.add(new SvcUserSettingsCopy());

		_services.add(new SvcUserGet());
		_services.add(new SvcUserSearch());
		_services.add(new SvcUserSearchLdap());
		_services.add(new SvcUserSearchLocal());

		_services.add(new SvcUserDescribe());
		_services.add(new SvcUserDisableSet());
		_services.add(new SvcUserDisableSetAll());
		_services.add(new SvcUserDisable());

		_services.add(new SvcUserRevokeOnlyRole());

		//
		_services.add(new SvcNetworkMonitor());
		//
		_services.add(new SvcDMExpiredDRDestroy());

	}

	public String description() {
		return "Essential plugin services.";
	}

	public void initialize(ConfigurationResolver conf) throws Throwable {

	}

	public Collection<PluginService> services() {
		return _services;
	}

	public void shutdown(ConfigurationResolver conf) throws Throwable {

	}

	public String vendor() {
		return "Research Computing Services, Business Services, The University of Melbourne";
	}

	public String version() {
		return "1.0.1";
	}

}
