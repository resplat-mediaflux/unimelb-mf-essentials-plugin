package unimelb.mf.essentials.plugin.services;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;

public class SvcServerThreadList extends PluginService {

	public static final String SERVICE_NAME = "unimelb.server.thread.list";

	private Interface _defn;

	public SvcServerThreadList() {
		_defn = new Interface();

		Interface.Element id = new Interface.Element("id", LongType.POSITIVE, "The id of the thread.", 0, 1);
		_defn.add(id);

		Interface.Element name = new Interface.Element("name", StringType.DEFAULT, "Thread name filter", 0, 100);
		name.add(new Interface.Attribute("exclusive", BooleanType.DEFAULT, "Defaults to false.", 0));
		name.add(new Interface.Attribute("op",
				new EnumType(new String[] { "contains", "equals", "starts-with", "ends-with", "matches" }),
				"Operator. If set to matches, the specified value of the name argument should be a regular expression. Defaults to contains.",
				0));
		_defn.add(name);

		Interface.Element group = new Interface.Element("group", StringType.DEFAULT, "Thread group filter", 0, 100);
		group.add(new Interface.Attribute("exclusive", BooleanType.DEFAULT, "Defaults to false.", 0));
		_defn.add(group);

		Interface.Element state = new Interface.Element("state",
				new EnumType(new String[] { "running", "waiting", "socket", "sleeping" }), "", 0, 4);
		_defn.add(state);

		Interface.Element priority = new Interface.Element("priority", new IntegerType(0, 100), "Priority filter", 0,
				2);
		priority.add(new Interface.Attribute("op", new EnumType(new String[] { "eq", "lt", "gt", "le", "ge", "ne" }),
				"Operator. Defaults to eq.", 0));
		_defn.add(priority);

		Interface.Element alive = new Interface.Element("alive", BooleanType.DEFAULT, "Thread is alive.", 0, 1);
		_defn.add(alive);

		Interface.Element context = new Interface.Element("context", StringType.DEFAULT, "Thread context filter", 0, 1);
		context.add(new Interface.Attribute("op", new EnumType(new String[] { "contains", "matches" }),
				"Operator. If set to matches, the specified value of the name argument should be a regular expression. Defaults to contains.",
				0));
		_defn.add(context);

		Interface.Element actorName = new Interface.Element("actor-name", StringType.DEFAULT,
				"The actor name of the context session.", 0, 1);
		_defn.add(actorName);

		Interface.Element session = new Interface.Element("session", LongType.POSITIVE,
				"The id of the session associated with the thread.", 0, 1);
		_defn.add(session);

		Interface.Element task = new Interface.Element("task", LongType.POSITIVE,
				"The id of the task associated with the thread.", 0, 1);
		_defn.add(task);

		Interface.Element hasTask = new Interface.Element("has-task", BooleanType.DEFAULT, "Thread is alive.", 0, 1);
		_defn.add(hasTask);

		Interface.Element service = new Interface.Element("service", StringType.DEFAULT, "The context service.", 0, 1);
		_defn.add(service);

		Interface.Element sourceIP = new Interface.Element("source-ip", StringType.DEFAULT,
				"The context source ip address.", 0, 1);
		_defn.add(sourceIP);

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "List server threads.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		ThreadFilter filter = new ThreadFilter(args);

		String dump = executor().execute("server.threads.get").value("dump");
		Set<ThreadInfo> threads = ThreadInfo.parseAll(dump);
		long total = 0;
		long count = 0;
		if (threads != null) {
			for (ThreadInfo thread : threads) {
				total++;
				if (thread.matches(filter)) {
					thread.save(w);
					count++;
				}
			}
		}
		w.add("count", new String[] { "total", Long.toString(total) }, count);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	private static class ThreadInfo implements Comparable<ThreadInfo> {

		static class ThreadContext {
			public final Long sessionId;
			public final String actorName;
			public final String serviceName;
			public final String sourceIP;

			private ThreadContext(Long sessionId, String actorName, String serviceName, String sourceIP) {
				this.sessionId = sessionId;
				this.actorName = actorName;
				this.serviceName = serviceName;
				this.sourceIP = sourceIP;
			}

			public void save(XmlWriter w) throws Throwable {
				w.push("context", new String[] { "actor", actorName, "session",
						sessionId == null ? null : Long.toString(sessionId) });
				if (serviceName != null) {
					w.add("service", serviceName);
				}
				if (sourceIP != null) {
					w.add("source-ip", sourceIP);
				}
				w.pop();
			}

			static ThreadContext parse(String context) {
				// @formatter:off
	            //           22-Oct-2021 14:44:06.730 [8.684 sec(s) ago]: service: local-admin:wilson [sid=166]: asset.index.validate :action  "status" 
				// called by: 22-Oct-2021 14:44:06.730 [8.684 sec(s) ago]: service: local-admin:wilson [sid=166]: service.execute :service -name "asset.index.validate" < :action  "status" > 
				// called by: 22-Oct-2021 14:44:06.730 [8.684 sec(s) ago]: http: from: /10.240.58.208, request: /__mflux_svc__ vars:  select headers: [Host=mediaflux-test.researchsoftware.unimelb.edu.au, User-Agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:93.0) Gecko/20100101 Firefox/93.0, Connection=keep-alive]
				// @formatter:on
				String str = context;
				String actorName = null;
				Long sessionId = null;
				String serviceName = null;
				String sourceIP = null;

				Pattern pattern = Pattern.compile("\\]: +service: +(.*) +\\[");
				Matcher matcher = pattern.matcher(str);
				if (matcher.find()) {
					actorName = matcher.group(1);
					str = str.substring(matcher.end());
				}

				pattern = Pattern.compile("sid=(\\d+)\\]: +");
				matcher = pattern.matcher(str);
				if (matcher.find()) {
					sessionId = Long.parseLong(matcher.group(1));
					str = str.substring(matcher.end());
					int idx = str.indexOf(' ');
					if (idx > 0) {
						serviceName = str.substring(0, idx);
					}
				}

				pattern = Pattern.compile("http: +from: +/([.0-9]+), ");
				matcher = pattern.matcher(str);
				if (matcher.find()) {
					sourceIP = matcher.group(1);
				}
				if (sessionId != null || actorName != null || serviceName != null || sourceIP != null) {
					return new ThreadContext(sessionId, actorName, serviceName, sourceIP);
				} else {
					return null;
				}
			}
		}

		public final long id;
		public final String name;
		public final int priority;
		public final String group;
		public final boolean alive;
		public final String state;
		public final String contextStr;
		public final ThreadContext context;
		public final String actorName;
		public final long sessionId;
		public final String serviceName;
		public final String sourceIP;
		public final List<Long> tasks;

		private ThreadInfo(long id, String name, int priority, String group, boolean alive, String state,
				String contextStr, ThreadContext context, String actorName, long sessionId, String serviceName,
				String sourceIP, List<Long> tasks) {
			this.id = id;
			this.name = name;
			this.priority = priority;
			this.group = group;
			this.alive = alive;
			this.state = state;
			this.contextStr = contextStr;
			this.context = context;
			this.actorName = actorName;
			this.sessionId = sessionId;
			this.serviceName = serviceName;
			this.sourceIP = sourceIP;
			this.tasks = tasks;
		}

		public int nbTasks() {
			if (this.tasks != null) {
				return this.tasks.size();
			}
			return 0;
		}

		@Override
		public int compareTo(ThreadInfo o) {

			// 1: priority
			int r = Integer.compare(this.priority, o.priority);
			if (r != 0) {
				return r;
			}

			// 2. group
			r = compare(this.group, o.group);
			if (r != 0) {
				return r;
			}

			// 3. name
			r = this.name.compareTo(o.name);
			if (r != 0) {
				return r;
			}

			// 4. actorName
			r = compare(this.actorName, o.actorName);
			if (r != 0) {
				return r;
			}

			// 5. sid
			r = Long.compare(this.sessionId, o.sessionId);
			if (r != 0) {
				return r;
			}

			// 6. service
			r = compare(this.serviceName, o.serviceName);
			if (r != 0) {
				return r;
			}

			// 7. ip
			r = compare(this.sourceIP, o.sourceIP);
			if (r != 0) {
				return r;
			}

			// 8. nbTasks
			r = Integer.compare(this.nbTasks(), o.nbTasks());
			if (r != 0) {
				return r;
			}

			return Long.compare(this.id, o.id);
		}

		public boolean matches(ThreadFilter filter) {
			if (filter.id != null) {
				if (this.id != filter.id) {
					return false;
				}
			}
			if (filter.priorities != null && !filter.priorities.isEmpty()) {
				if (!priorityMatchesAll(filter.priorities)) {
					return false;
				}
			}
			if (filter.incNames != null && !filter.incNames.isEmpty()) {
				if (!nameMatchesAny(filter.incNames)) {
					return false;
				}
			}
			if (filter.incGroups != null && !filter.incGroups.isEmpty()) {
				if (!groupMatchesAny(filter.incGroups)) {
					return false;
				}
			}
			if (filter.alive != null) {
				if (this.alive != filter.alive) {
					return false;
				}
			}
			if (filter.states != null && !filter.states.isEmpty()) {
				if (!filter.states.contains(this.state)) {
					return false;
				}
			}
			if (filter.context != null) {
				if (!filter.context.matches(this.contextStr)) {
					return false;
				}
			}
			if (filter.actorName != null) {
				if (!Objects.equals(this.actorName, filter.actorName)) {
					return false;
				}
			}
			if (filter.sessionId != null) {
				if (this.sessionId != filter.sessionId) {
					return false;
				}
			}
			if (filter.serviceName != null) {
				if (!Objects.equals(this.serviceName, filter.serviceName)) {
					return false;
				}
			}
			if (filter.sourceIP != null) {
				if (!Objects.equals(this.sourceIP, filter.sourceIP)) {
					return false;
				}
			}
			if (filter.hasTask != null) {
				boolean hasTask = this.tasks != null && !this.tasks.isEmpty();
				if (hasTask != filter.hasTask) {
					return false;
				}
			}
			if (filter.taskId != null) {
				if (this.tasks == null || this.tasks.isEmpty() || !this.tasks.contains(filter.taskId)) {
					return false;
				}
			}
			if (filter.excNames != null) {
				if (nameMatchesAny(filter.excNames)) {
					return false;
				}
			}
			if (filter.excGroups != null) {
				if (groupMatchesAny(filter.excGroups)) {
					return false;
				}
			}
			return true;
		}

		public boolean nameMatchesAny(List<ThreadFilter.NameFilter> nameFilters) {
			for (ThreadFilter.NameFilter filter : nameFilters) {
				if (filter.matches(this.name)) {
					return true;
				}
			}
			return false;
		}

		public boolean priorityMatchesAll(List<ThreadFilter.PriorityFilter> priorityFilters) {
			for (ThreadFilter.PriorityFilter filter : priorityFilters) {
				if (!filter.matches(this.priority)) {
					return false;
				}
			}
			return true;
		}

		public boolean groupMatchesAny(List<ThreadFilter.GroupFilter> groupFilters) {
			for (ThreadFilter.GroupFilter filter : groupFilters) {
				if (filter.matches(this.group)) {
					return true;
				}
			}
			return false;
		}

		public void save(XmlWriter w) throws Throwable {
			w.push("thread", new String[] { "id", Long.toString(id), "name", name, "state", state, "alive",
					Boolean.toString(alive), "group", group, "priority", Integer.toString(priority) });
			if (context != null) {
				context.save(w);
			}
			if (tasks != null) {
				for (Long task : tasks) {
					w.add("task", task);
				}
			}
			w.pop();
		}

		private static int compare(String a, String b) {
			if (a == null) {
				if (b != null) {
					return -1;
				} else {
					return 0;
				}
			} else {
				if (b == null) {
					return 1;
				} else {
					return a.compareTo(b);
				}
			}
		}

		public static ThreadInfo parse(String thread) throws Exception {

			long id = 0;
			String name = null;
			int priority = 0;
			String group = null;
			boolean alive = true;
			String state = null;
			String contextStr = null;
			ThreadContext context = null;
			String actorName = null;
			long sessionId = 0;
			String serviceName = null;
			String sourceIP = null;
			List<Long> tasks = new ArrayList<Long>();

			// @formatter:off
			// Thread[uuid=1247, id=5967, name="22912: Network Connection: http [port=8443]", alive=true, priority=5, group="main"]
			// @formatter:on

			int idx1 = thread.indexOf("Thread[");
			if (idx1 == -1) {
				throw new Exception("Failed to parse thread information.");
			}
			idx1 += 7;
			int idx2 = thread.indexOf("]\",", idx1);
			if (idx2 >= 0) {
				idx2 = thread.indexOf("]", idx2 + 3);
			} else {
				idx2 = thread.indexOf("]", idx1);
			}
			if (idx2 == -1) {
				throw new Exception("Failed to parse thread information.");
			}
			String s = thread.substring(idx1, idx2);
			String[] parts = s.split(",\\ *");
			for (String part : parts) {
				if (part.startsWith("id=")) {
					id = Long.parseLong(part.substring(3));
				} else if (part.startsWith("name=\"")) {
					name = part.substring(6, part.length() - 1);
				} else if (part.startsWith("alive=")) {
					alive = Boolean.parseBoolean(part.substring(6));
				} else if (part.startsWith("priority=")) {
					priority = Integer.parseInt(part.substring(9));
				} else if (part.startsWith("group=\"")) {
					group = part.substring(7, part.length() - 1);
				}
			}

			// @formatter:off
			// State: WAITING
			// @formatter:on
			idx2 += 1;
			idx1 = thread.indexOf("State: ", idx2);
			if (idx1 == -1) {
				throw new Exception("Failed to parse thread information.");
			}
			idx2 = thread.indexOf("\n", idx1 + 7);
			if (idx2 == -1) {
				throw new Exception("Failed to parse thread information.");
			}
			state = thread.substring(idx1 + 7, idx2).toLowerCase();

			// @formatter:off
			// ---- Execution Context ----
            //           22-Oct-2021 14:44:06.730 [8.684 sec(s) ago]: service: local-admin:wilson [sid=166]: asset.index.validate :action  "status" 
			// called by: 22-Oct-2021 14:44:06.730 [8.684 sec(s) ago]: service: local-admin:wilson [sid=166]: service.execute :service -name "asset.index.validate" < :action  "status" > 
			// called by: 22-Oct-2021 14:44:06.730 [8.684 sec(s) ago]: http: from: /10.240.58.208, request: /__mflux_svc__ vars:  select headers: [Host=mediaflux-test.researchsoftware.unimelb.edu.au, User-Agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:93.0) Gecko/20100101 Firefox/93.0, Connection=keep-alive]
			// ---------------------------
			// @formatter:on
			idx2 += 1;
			idx1 = thread.indexOf("---- Execution Context ----", idx2);
			if (idx1 >= 0) {
				idx1 += 27;
				idx2 = thread.indexOf("---------------------------", idx1);
				if (idx2 == -1) {
					throw new Exception("Failed to parse thread information.");
				}
				contextStr = thread.substring(idx1, idx2).replaceAll("^\n+", "").trim();
				if (!contextStr.isEmpty()) {
					context = ThreadContext.parse(contextStr);
				} else {
					contextStr = null;
				}
			} else {
				idx1 = idx2;
			}

			// @formatter:off
			// ---------- Tasks ----------
			// Task (id): 258
			// Task (id): 268
			// Task (id): 269
			// ---------------------------
			idx1 = thread.indexOf("---------- Tasks ----------", idx2);
			if (idx1 >= 0) {
				idx1 += 27;
				idx2 = thread.indexOf("---------------------------", idx1);
				if (idx2 == -1) {
					throw new Exception("Failed to parse thread information.");
				}
				String taskSection = thread.substring(idx1, idx2).replaceAll("^\n+", "").replaceAll("\n+$", ""); 
				String[] taskStrs = taskSection.split("\\ *Task \\(id\\):\\ *");
				for(String taskStr: taskStrs) {
					if(taskStr!=null) {
						taskStr = taskStr.trim();
						if(!taskStr.isEmpty()) {
							tasks.add(Long.parseLong(taskStr.trim()));
						}
					}
				}
			} else {
				idx1 = idx2;
			}
			return new ThreadInfo(id, name, priority, group, alive, state, contextStr, context, actorName, sessionId, serviceName, sourceIP, tasks);
		}
		
		public static SortedSet<ThreadInfo> parseAll(String threadDump) throws Throwable {
			String threadStart = "---------- Thread --------";
			int idx1 = threadDump.indexOf(threadStart);
			if(idx1>=0) {
				idx1 +=threadStart.length();
			} else {
				throw new Exception("Failed to parse threads dump.");
			}
			String summaryStart = "==============================================";
			int idx2 = threadDump.indexOf(summaryStart, idx1);
			if(idx2<0) {
				throw new Exception("Failed to parse threads dump.");
			}
			String[] ts = threadDump.substring(idx1, idx2).trim().split(threadStart);
			TreeSet<ThreadInfo> tis = new TreeSet<ThreadInfo>();
			for(String t : ts) {
				ThreadInfo ti = parse(t.trim());
				tis.add(ti);
			}
			return tis;
		}

	}
	
	static class ThreadFilter {
		static class NameFilter {
			public final boolean exclusive;
			public final String op; /* "contains", "equals", "starts-with", "ends-with", "matches" */
			public final String value;

			NameFilter(XmlDoc.Element name) throws Throwable {
				this.exclusive = name.booleanValue("@exclusive");
				this.op = name.stringValue("@op", "contains");
				this.value = name.value();
			}
			
			public boolean matches(String name) {
				if(name!=null) {
					if("contains".equalsIgnoreCase(this.op)) {
						return name.contains(this.value);
					} else if("equals".equalsIgnoreCase(this.op)) {
						return name.equals(this.value);
					} else if("starts-with".equalsIgnoreCase(this.op)) {
						return name.startsWith(this.value);
					} else if("ends-with".equalsIgnoreCase(this.op)) {
						return name.endsWith(this.value);
					} else if("matches".equalsIgnoreCase(this.value)) {
						return name.matches(this.value);
					}
				}
				return false;
			}
		}

		static class GroupFilter {
			public final boolean exclusive;
			public final String value;

			GroupFilter(XmlDoc.Element group) throws Throwable {
				this.exclusive = group.booleanValue("@exclusive", false);
				this.value = group.value();
			}
			
			public boolean matches(String group) {
				return group!=null && group.equals(this.value);
			}
		}

		static class PriorityFilter {
			public final String op; /* "eq", "lt", "gt", "le", "ge", "ne" */
			public final int value;

			PriorityFilter(XmlDoc.Element priority) throws Throwable {
				this.op = priority.stringValue("@op", "eq");
				this.value = Integer.parseInt(priority.value());
			}
			
			public boolean matches(Integer priority) {
				if(priority!=null) {
					if("eq".equalsIgnoreCase(this.op)) {
						return priority == this.value;
					} else if("lt".equalsIgnoreCase(this.op)) {
						return priority < this.value;
					}else if("gt".equalsIgnoreCase(this.op)) {
						return priority > this.value;
					}else if("le".equalsIgnoreCase(this.op)) {
						return priority <= this.value;
					}else if("ge".equalsIgnoreCase(this.op)) {
						return priority >= this.value;
					}else if("ne".equalsIgnoreCase(this.op)) {
						return priority != this.value;
					}
				}
				return false;
			}
		}

		static class ContextFilter {
			public final String op;
			public final String value;

			ContextFilter(XmlDoc.Element context) throws Throwable {
				this.op = context.stringValue("@op", "contains");
				this.value = context.value();
			}
			
			public boolean matches(String context) {
				if(context!=null) {
					if("contains".equalsIgnoreCase(this.op)) {
						return context.contains(this.value);
					} else if("matches".equalsIgnoreCase(this.op)) {
						return context.matches(this.value);
					} 
				}
				return false;
			}
		}

		public final Long id;
		public final List<NameFilter> incNames;
		public final List<NameFilter> excNames;
		public final List<PriorityFilter> priorities;
		public final List<GroupFilter> incGroups;
		public final List<GroupFilter> excGroups;
		public final Boolean alive;
		public final Set<String> states;
		public final ContextFilter context;
		public final String actorName;
		public final Long sessionId;
		public final String serviceName;
		public final String sourceIP;
		public final Boolean hasTask;
		public final Long taskId;

		ThreadFilter(XmlDoc.Element args) throws Throwable {

			this.id = args.longValue("id", null);
			this.incNames = new ArrayList<NameFilter>();
			this.excNames = new ArrayList<NameFilter>();
			List<XmlDoc.Element> names = args.elements("name");
			if (names != null && !names.isEmpty()) {
				for (XmlDoc.Element e : names) {
					boolean exclusive = e.booleanValue("@exclusive", false);
					NameFilter name = new NameFilter(e);
					if (exclusive) {
						this.excNames.add(name);
					} else {
						this.incNames.add(name);
					}
				}
			}

			this.priorities = new ArrayList<PriorityFilter>();
			List<XmlDoc.Element> pes = args.elements("priority");
			if (pes != null) {
				for (XmlDoc.Element pe : pes) {
					this.priorities.add(new PriorityFilter(pe));
				}
			}

			this.incGroups = new ArrayList<GroupFilter>();
			this.excGroups = new ArrayList<GroupFilter>();
			List<XmlDoc.Element> groups = args.elements("group");
			if (groups != null && !groups.isEmpty()) {
				for (XmlDoc.Element e : groups) {
					boolean exclusive = e.booleanValue("@exclusive", false);
					GroupFilter group = new GroupFilter(e);
					if (exclusive) {
						excGroups.add(group);
					} else {
						incGroups.add(group);
					}
				}
			}

			this.alive = args.booleanValue("alive", null);

			this.states = args.elementExists("state") ? new LinkedHashSet<String>(args.values("state")) : null;

			this.context = args.elementExists("context") ? new ContextFilter(args.element("context")) : null;
			
			this.actorName = args.value("actor-name");
			
			this.sessionId = args.longValue("session", null);
			
			this.serviceName = args.value("service");
			
			this.sourceIP = args.value("source-ip");
			
			this.hasTask = args.booleanValue("has-task", null);
			
			this.taskId = args.longValue("task", null);
		}
	}


}
