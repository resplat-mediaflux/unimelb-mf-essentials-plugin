package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
// TODO implement and test
public class SvcSFTPFileZillaSiteConfigurationGet extends PluginService {

    public static final String SERVICE_NAME = "unimelb.sftp.file-zilla.site.configuration.get";

    private Interface _defn;

    public SvcSFTPFileZillaSiteConfigurationGet() {
        _defn = new Interface();
        // TODO
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Generates SFTP site configuration for FileZilla.";
    }

    @Override
    public void execute(Element arg0, Inputs arg1, Outputs arg2, XmlWriter arg3) throws Throwable {
        // TODO Auto-generated method stub

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
