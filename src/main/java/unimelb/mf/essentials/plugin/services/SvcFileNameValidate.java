package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.*;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import unimelb.utils.XmlUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPOutputStream;

public class SvcFileNameValidate extends PluginService {

    public static final String SERVICE_NAME = "unimelb.file.name.validate";

    public static final int MAX_MESSAGE_LENGTH = 60000;
    private Interface _defn;

    public SvcFileNameValidate() {
        _defn = new Interface();
        _defn = new Interface();
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT, "Namespace path.", 1, 1));
        _defn.add(new Interface.Element("recursive", BooleanType.DEFAULT,
                "Validate the subnamespaces recursively. Defaults to true. ", 0, 1));
        _defn.add(new Interface.Element("compress", BooleanType.DEFAULT,
                "Compress the output CSV file to GZIP format. Defaults to false.", 0, 1));
        Interface.Element notify = new Interface.Element("notify", XmlDocType.DEFAULT,
                "Notify of the result via emails.", 0, 1);
        notify.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Recipient to receive the result.", 1, 10));
        _defn.add(notify);

        _defn.add(new Interface.Element("type", new EnumType(new String[]{"asset", "namespace", "all"}),
                "Type of search. Defaults to all.", 0, 1));

        Interface.Element filter = new Interface.Element("filter", StringType.DEFAULT,
                "Additional filter to select the assets. Ignored if search type is 'namespace'.", 0, 1);
        filter.add(new Interface.Attribute("primary", BooleanType.DEFAULT, "Append the filter to the primary where " +
                "clause of asset.query service. Defaults to false.", 0));
        _defn.add(filter);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Validate asset names and asset namespace names within the specified asset namespace";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String parentAnsPath = args.value("namespace");
        boolean recursive = args.booleanValue("recursive", true);

        String searchType = args.stringValue("type", "all");

        String filter = searchType.equals("namespace") ? null : args.value("filter");
        boolean filterPrimary = searchType.equals("namespace") ? false : args.booleanValue("where/@primary", false);


        Collection<String> notifyEmails = args.values("notify/email");
        boolean notify = notifyEmails != null && !notifyEmails.isEmpty();
        StringBuilder msg = notify ? new StringBuilder() : null;
        if (msg != null) {
            msg.append("---\n");
            msg.append(SERVICE_NAME).append("\n");
            msg.append(XmlUtils.toIndentedText(args, false, 4, 4));
            msg.append("---\n\n\n");
        }

        PluginService.Output output = outputs == null ? null : outputs.output(0);
        boolean compress = args.booleanValue("compress", false);
        String outputMimeType = compress ? "application/gzip" : "text/csv";
        String outputFileExt = compress ? "csv.gz" : "csv";
        File outputFile = (output == null && !notify) ? null : PluginTask.createTemporaryFile(outputFileExt);

        AtomicLong nbInvalidAssets = new AtomicLong(0);
        AtomicLong nbInvalidAssetNamespaces = new AtomicLong(0);

        try {
            validate(executor(), parentAnsPath, recursive, filter, filterPrimary, searchType, w, output, outputFile,
                    compress, notify, msg, nbInvalidAssetNamespaces, nbInvalidAssets);
            if (notify) {
                String subject = String.format("%s: %s", executor().execute("server.uuid").value("uuid"), SERVICE_NAME);
                String fileName = compress ? "invalid-names.csv.gz" : "invalid-names.csv";
                SvcAssetNameValidate.notify(executor(), subject, msg.toString(), outputFile, fileName, outputMimeType,
                        notifyEmails);
            }

            if (output != null) {
                output.setData(PluginTask.deleteOnCloseInputStream(outputFile), outputFile.length(), outputMimeType);
            } else {
                if (outputFile != null && outputFile.exists()) {
                    PluginTask.deleteTemporaryFile(outputFile);
                }
            }
        } catch (Throwable e) {
            if (outputFile != null && outputFile.exists()) {
                PluginTask.deleteTemporaryFile(outputFile);
            }
            throw e;
        }
    }

    public int maxNumberOfOutputs() {
        return 1;
    }

    public boolean canBeAborted() {
        return true;
    }

    private static void validate(ServiceExecutor executor, String parentAnsPath, boolean recursive, String filter,
                                 boolean filterPrimary, String searchType, XmlWriter w, PluginService.Output output,
                                 File outputFile, boolean compress, boolean notify, StringBuilder msg,
                                 AtomicLong nbInvalidAssetNamespaces, AtomicLong nbInvalidAssets) throws Throwable {
        PrintStream outputPS = (output == null && !notify) ? null : new PrintStream(
                compress ? new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile))) :
                        new BufferedOutputStream(new FileOutputStream(outputFile)));
        try {
            if (outputPS != null) {
                outputPS.println("Type,ID,Path,Name");
            }
            if (searchType.equalsIgnoreCase("asset") || searchType.equalsIgnoreCase("all")) {
                StringBuilder assetQuery = new StringBuilder();
                assetQuery.append("(namespace").append(recursive ? ">=" : "=").append(parentAnsPath).append(")");
                if (filter != null && filterPrimary) {
                    assetQuery.append("and (").append(filter).append(")");
                }
                SvcAssetNameValidate.validateAsset(executor, assetQuery.toString(),  filterPrimary ? null : filter, false,
                        new SvcAssetNameValidate.AssetHandler() {
                            @Override
                            public void handle(SvcAssetNameValidate.Asset asset) throws Throwable {
                                w.add("invalid",
                                        new String[]{"type", "asset", "id", asset.id, "version", Integer.toString(
                                                asset.version), "path", asset.path}, asset.name);
                                if (outputPS != null) {
                                    outputPS.printf("asset,%s,%d,\"%s\",\"%s\"%n", asset.id, asset.version, asset.path,
                                            asset.name);
                                }
                                if (msg != null) {
                                    if (msg.length() < MAX_MESSAGE_LENGTH) {
                                        msg.append(String.format(
                                                "    :invalid -type asset -id %s -version %d -path \"%s\" \"%s\"\n",
                                                asset.id, asset.version, asset.path, asset.name));
                                    }
                                }
                                nbInvalidAssets.getAndIncrement();
                            }
                        });
            }

            if (searchType.equalsIgnoreCase("namespace") || searchType.equalsIgnoreCase("all")) {
                SvcAssetNameSpaceNameValidate.validateAssetNameSpace(executor, parentAnsPath, recursive,
                        new SvcAssetNameSpaceNameValidate.AsssetNameSpaceHandler() {
                            @Override
                            public void handle(SvcAssetNameSpaceNameValidate.AssetNameSpace ans) throws Throwable {
                                w.add("invalid", new String[]{"type", "namespace", "path", ans.path, "id",
                                                Long.toString(ans.id)},
                                        ans.name);
                                if (outputPS != null) {
                                    outputPS.printf("namespace,%d,\"%s\",\"%s\"%n", ans.id, ans.path, ans.name);
                                }
                                if (msg != null) {
                                    if (msg.length() < MAX_MESSAGE_LENGTH) {
                                        msg.append(
                                                String.format(
                                                        "    :invalid -type namespace -id %s -path \"%s\" \"%s\"\n",
                                                        ans.id, ans.path, ans.name));
                                    }
                                }
                                nbInvalidAssetNamespaces.getAndIncrement();
                            }
                        });
            }
            long nbAssets = nbInvalidAssets.get();
            long nbAssetNamespaces = nbInvalidAssetNamespaces.get();
            long total = nbAssets + nbAssetNamespaces;
            if (nbAssets > 0) {
                w.add("count", new String[]{"type", "asset"}, nbAssets);
            }
            if (nbAssetNamespaces > 0) {
                w.add("count", new String[]{"type", "namespace"}, nbAssetNamespaces);
            }
            w.add("count", new String[]{"type", "all"}, total);
            if (msg != null) {
                if (msg.length() > +MAX_MESSAGE_LENGTH) {
                    msg.append("    ... ... ...\n");
                }
                if (nbAssets > 0) {
                    msg.append(String.format("    :count -type asset %d", nbAssets)).append("\n");
                }
                if (nbAssetNamespaces > 0) {
                    msg.append(String.format("    :count -type namespace %d", nbAssetNamespaces)).append("\n");
                }
                msg.append(String.format("    :count -type all %d", total)).append("\n");
            }
        } finally {
            if (outputPS != null) {
                outputPS.close();
            }
        }
    }
}
