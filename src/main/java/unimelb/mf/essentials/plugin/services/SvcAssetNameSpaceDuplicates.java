/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetNameSpaceDuplicates extends PluginService {


	private Interface _defn;

	public SvcAssetNameSpaceDuplicates() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("show", BooleanType.DEFAULT, "Show the actual duplicates (default false) as well as the summary.",
				0, 1));	}

	public String name() {
		return "unimelb.asset.namespace.duplicates";
	}

	public String description() {
		return "Service which iterates through the direct children namespaces of the specified namespace and compiles an asset duplicate report per child namespace.  The namespaces are sorted by duplicate size.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		Boolean show = args.booleanValue("show", false);

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor().execute("asset.namespace.list", dm.root());
		Collection<String> nss = r.values("namespace/namespace");

		ArrayList<XmlDoc.Element> results = new ArrayList<XmlDoc.Element>();
		Long sum = 0l;
		for (String ns : nss) {
			PluginTask.checkIfThreadTaskAborted();
			String absPath = namespace + "/" + ns;
			XmlDocMaker dm2 = new XmlDocMaker("args");
			if (findDuplicates (executor(), absPath, show, dm2)) {
				XmlDocMaker dm3 = new XmlDocMaker("args");
				dm3.push("namespace");
				dm3.add("path", absPath);
				dm3.addAll(dm2.root().elements());			
				dm3.pop();
				results.add(dm3.root());
				//
				String t = dm2.root().element("total-duplicated-size").value();
				Long total = Long.parseLong(t);
				sum += total;
			}
		}
		if (results.size()>0) {
			ArrayList<XmlDoc.Element> nsSorted = sortBySize(results);
			for (XmlDoc.Element ns : nsSorted) {
				w.addAll(ns.elements());
			}
		}

	
		w.add("total-duplicated-size", new String[] {"unit","bytes"}, sum);
	}

	private  ArrayList<XmlDoc.Element> sortBySize (ArrayList<XmlDoc.Element> projectResults) throws Throwable {
		if (projectResults == null || projectResults.isEmpty()) {
			return null;
		}
		ArrayList<XmlDoc.Element> list = new ArrayList<XmlDoc.Element>(projectResults);
		Collections.sort(list, new Comparator<XmlDoc.Element>() {

			@Override
			public int compare(XmlDoc.Element d1, XmlDoc.Element d2)   {

				Double d1d = null;
				try {
					// bytes
					String s = d1.value("namespace/total-duplicated-size");
					d1d = Double.parseDouble(s);
				} catch (Throwable e) {
				}

				Double d2d = null;
				try {
					// bytes
					String s = d2.value("namespace/total-duplicated-size");
					d2d = Double.parseDouble(s);
				} catch (Throwable e) {
				}
				// This will never happen but we have to handle the exception
				if (d1d==null || d2d==null) {
					return 0;
				}
				int retval = Double.compare(d1d, d2d);

				if(retval > 0) {
					return -1;
				} else if(retval < 0) {
					return 1;
				} else {
					return 0;
				}

			}
		});
		return list;
	}


	private Boolean findDuplicates (ServiceExecutor executor, String namespace,
			Boolean show, XmlDocMaker w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		// Restrict candidates and what it is compariong with
		// to the same where
		dm.add("where", "namespace>='"+namespace+"'");
		dm.add("duplicate-where", "namespace>='"+namespace+"'");
		XmlDoc.Element r = executor.execute("asset.duplicate.find", dm.root());
		// These elements are always returned
		Integer n = r.intValue("total-number-of-assets");
		if (n.equals(0)) {
			return false;
		}
		if (show) {
			w.addAll(r.elements("duplicates"));
		}

		w.add(r.element("total-number-of-assets"));
		w.add(r.element("total-number-of-duplicates"));
		w.add(r.element("total-duplicated-size"));
		return true;
	}


}
