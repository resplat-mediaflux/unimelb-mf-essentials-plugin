package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetReplicaNeedToReplicate extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.replica.need.to.replicate";

	private Interface _defn;

	public SvcAssetReplicaNeedToReplicate() {
		_defn = new Interface();

		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The path to the asset.", 1, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("peer", StringType.DEFAULT, "The name of the peer. If not specified, all discoverable peers will be used.", 0, 1));
		_defn.add(new Interface.Element("all-versions", BooleanType.DEFAULT, "Assess all versions (default=true). If false, just the latest version is assessed.", 0, 1));
		_defn.add(new Interface.Element("queue", StringType.DEFAULT, 
				"An asset processing queue (default is no queue) to which all found assets are added. In this way you can execute asset.query and pipe execution to this service and add assets to a queue.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Checks an asset for the need to replicate.  Returns nothing if it does not need to be replicated.  If it does, an attribute 'reasons' in the result will concatenate all reaons for all selected versions. Is a wrapper of unimelb.asset.replica.need.to.replicate.all-versions and asset.replica.need.to.replicate; the difference is these services always return a result, this one only returns a result for things that need to be replicated.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String id = args.value("id");
		String peer = args.value("peer");
		String queue = args.value("queue");
		Boolean allVersions = args.booleanValue("all-versions", true);
		//
		Boolean replicate = false;
		String reasons = "";
		String csize = null;
		String path = null;
		if (allVersions) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", id);
			if (peer!=null) {
				dm.add("peer", peer);
			}
			XmlDoc.Element r = executor().execute("unimelb.asset.replica.need.to.replicate.all-versions", dm.root());
			if (r!=null) {
				// Versions are ordered most recent to first
				Collection<XmlDoc.Element> assetVersions = r.elements("asset"); 
				if (assetVersions!=null) {
					Boolean firstTime = true;
					for (XmlDoc.Element assetVersion : assetVersions) {
						String version = assetVersion.value("@version");
						String required = assetVersion.value("required");
						if (!required.equals("no")) {
							replicate = true;
							String reason = assetVersion.value("reason");
							if (firstTime) {
								// We store these values for the most recent version
								// only.  The csize will be right enough for the purposes
								// of this service.
								path = assetVersion.value("path");
								csize = assetVersion.value("content-size");
							} else {
								firstTime = false;
							}

							// There probably is no reason if the result is "unknown"
							// but we are going to trigger a re-replication with the
							// reason string so we need one
							if (required.equals("unknown")) {
								reason = "unknown";
							} else if (required.equals("yes")) {
								// make sure we have a reason even if it's not very meaningful
								if (reason==null) {
									reason = "unknown";
								}
							}
							reasons += version+":"+reason+",";

							// Return as soon as we know we need to re-replicate
							if (required.equals("unknown")) {
								// Hard to know what to do, because perhaps the peer is down.
								// So be conservative and attempt to re-replicate (which does not hurt)
								reasons = reasons.substring(0,reasons.length()-1);
								break;
							} else if (required.equals("yes")) {
								reasons = reasons.substring(0,reasons.length()-1);
								break;
							}

						}
					}
				} 
			} 
		} else {
			// Last version only
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", id);
			if (peer!=null) { 
				dm.push("peer");
				dm.add("name", peer);
			}
			dm.pop();

			// The service returns "yes", "no" and "unknown" (e.g. peer is down)
			XmlDoc.Element r2 = executor().execute("asset.replica.need.to.replicate", dm.root());
			String required = r2.value("asset/required");
			reasons = r2.value("asset/reason");
			path = r2.value("asset/path");
			csize = r2.value("asset/content-size");

			if (!required.equals("no")) {
				replicate = true;	
				if (reasons==null) {
					reasons = "unknown";
				} 
			}
		}
		// Report
		if (replicate) {
			// csize from most recent version inspected
			w.add("id", new String[] {"reasons", reasons, 
					"csize", csize, "path", path}, id);
			
			// Add to queue
			if (queue!=null) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("name",queue);
				dm.add("id", id);
				executor().execute("asset.processing.queue.add", dm.root());
			}
		}
	}




	@Override
	public boolean canBeAborted() {
		return false;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
