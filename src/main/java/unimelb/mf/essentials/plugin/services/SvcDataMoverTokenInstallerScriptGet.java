package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.StringType;
import arc.streams.StreamCopy;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class SvcDataMoverTokenInstallerScriptGet extends PluginService {

	public static final String SERVICE_NAME = "unimelb.data.mover.token.installer.script.get";

	public static final String WINDOWS_SCRIPT_FILE_NAME = "dm-token-install.cmd";
	public static final String UNIX_SCRIPT_FILE_NAME = "dm-token-install.sh";

	public static final String WINDOWS_SCRIPT_TEMPLATE = "@echo off\n" + "\n" + "set TOKEN=@@TOKEN@@\n" + "set PS=^\n"
			+ "$TOKEN='%TOKEN%';^\n" + "$DMDIR=\\\"$HOME\\.Arcitecta\\DataMover\\\";^\n"
			+ "$DMCFG=\\\"$DMDIR\\settings.xml\\\";^\n" + "$SUCCESS=$False;^\n"
			+ "New-Item -ItemType Directory -Force $DMDIR;^\n"
			+ "if (-Not $(Test-Path -LiteralPath $DMCFG -PathType Leaf)) {^\n"
			+ "    Write-Host -NoNewline \\\"creating Mediaflux DataMover configuration file: $DMCFG ...\\\";^\n"
			+ "    Out-File -FilePath $DMCFG -Encoding ascii -InputObject '^<settings^>^<token^>%TOKEN%^</token^>^</settings^>';^\n"
			+ "    $SUCCESS=$?;^\n" + "    if ($SUCCESS) { Write-Host ' done.' } else { Write-Host ' failed.' }^\n"
			+ "} else {^\n" + "    [xml]$doc = Get-Content $DMCFG;^\n"
			+ "    $te = $doc.DocumentElement.SelectSingleNode('token');^\n" + "    if (-not $te) {^\n"
			+ "        $te = $doc.CreateElement('token');^\n" + "        $doc.DocumentElement.AppendChild($te);^\n"
			+ "    }^\n" + "    if ( $te.InnerText -ne $TOKEN) {^\n" + "        $te.InnerText = $TOKEN;^\n"
			+ "        Write-Host -NoNewline \\\"updating Mediaflux DataMover configuration file: $DMCFG ...\\\";^\n"
			+ "        Rename-Item $DMCFG settings.xml.$(Get-Date -Format yyyyMMddHHmmss);^\n"
			+ "        $doc.Save($DMCFG);^\n" + "        $SUCCESS=$?;^\n"
			+ "        if ($SUCCESS) { Write-Host ' done.' } else { Write-Host ' failed.' }^\n" + "    } else {^\n"
			+ "        Write-Host token: \\\"$TOKEN already exists in $DMCFG No change.\\\";^\n"
			+ "        $SUCCESS=$True^\n" + "    }^\n" + "}^\n" + "if ($SUCCESS) {^\n"
			+ "    Add-Type -AssemblyName PresentationCore,PresentationFramework;^\n"
			+ "    [System.Windows.MessageBox]::Show(\\\"Token: $TOKEN has been installed into Mediaflux Data Mover configuration file: $DMCFG. To load the token, please start or restart Mediaflux Data Mover.\\\", 'Mediaflux Data Mover Token Installer', [System.Windows.MessageBoxButton]::Ok, [System.Windows.MessageBoxImage]::Information)^\n"
			+ "}\n" + "powershell -NoProfile -Command \"%PS%\"\n" + "pause\n";
	public static final String UNIX_SCRIPT_TEMPLATE = "#!/bin/bash\n" + "\n" + "TOKEN=@@TOKEN@@\n"
			+ "DMDIR=${HOME}/.Arcitecta/DataMover\n" + "DMCFG=${DMDIR}/settings.xml\n" + "\n"
			+ "if [[ ! -d ${DMDIR} ]]; then\n" + "    echo -n \"creating directory: ${DMDIR} ... \"\n"
			+ "    mkdir -p ${DMDIR}\n" + "    echo \"done.\"\n" + "fi\n" + "\n" + "if [[ ! -f ${DMCFG} ]]; then\n"
			+ "    echo -n \"creating Mediaflux Data Mover configuration: ${DMCFG} ... \"\n"
			+ "    echo \"<settings><token>${TOKEN}</token></settings>\" > ${DMCFG}\n" + "    echo \"done.\"\n"
			+ "    exit 0\n" + "fi\n" + "\n" + "SETTINGS=$(cat $DMCFG)\n" + "if [[ $SETTINGS =~ ${TOKEN} ]]; then\n"
			+ "    echo \"token: ${TOKEN} already exists in Mediaflux Data Mover configuration file: ${DMCFG}\"\n"
			+ "    exit 0\n" + "elif [[ $SETTINGS =~ \"</token>\" ]]; then\n"
			+ "    SETTINGS=${SETTINGS/<token>[a-zA-Z0-9@]*<\\/token>/<token>${TOKEN}<\\/token>}\n"
			+ "elif [[ $SETTINGS =~ \"<token/>\" ]]; then\n"
			+ "    SETTINGS=${SETTINGS/<token\\/>/<token>${TOKEN}<\\/token>}\n" + "else\n"
			+ "    SETTINGS=${SETTINGS/<\\/settings>/<token>${TOKEN}<\\/token><\\/settings>}\n" + "fi\n" + "\n"
			+ "TIMESTAMP=$(date +%Y%m%d%H%M%S)\n" + "mv ${DMCFG} ${DMCFG}.${TIMESTAMP}\n" + "\n"
			+ "echo -n \"installing token: ${TOKEN} into Mediaflux Data Mover config file: ${DMCFG} ... \"\n"
			+ "echo ${SETTINGS} > ${DMCFG}\n" + "echo \"done.\"";

	private final Interface _defn;

	public SvcDataMoverTokenInstallerScriptGet() {
		_defn = new Interface();
		_defn.add(new Interface.Element("shareable-token", StringType.DEFAULT, "The shareable token to be installed.",
				0, 1));
		_defn.add(new Interface.Element("shareable-id", StringType.DEFAULT, "The shareable id.", 0, 1));
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public String description() {
		return "Generate installer scripts (Windows batch file and BASH script file) to install the specified token into Mediaflux Data Mover configuration file: settings.xml. The output file is a zip archive contains both scripts.";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String shareableToken = args.value("shareable-token");
		String shareableId = args.value("shareable-id");
		if (shareableId == null && shareableToken == null) {
			throw new IllegalArgumentException("Either shareable-id or shareable-token must be specified. Found none.");
		}
		if (shareableId != null && shareableToken != null) {
			throw new IllegalArgumentException("Either shareable-id or shareable-token must be specified. Found both.");
		}
		if (shareableToken == null) {
			shareableToken = executor()
					.execute("asset.shareable.describe",
							"<args><id>" + shareableId + "</id><get-token>true</get-token></args>", null, null)
					.value("shareable/token");
		}

		File tf = PluginTask.createTemporaryFile("dm-token-inst.zip");
		try {
			createDataMoverTokenInstallerScripts(shareableToken, tf);
			PluginService.Output output = outputs.output(0);
			output.setData(PluginTask.deleteOnCloseInputStream(tf), tf.length(), "application/zip");
		} catch (Throwable e) {
			PluginTask.deleteTemporaryFile(tf);
			throw e;
		}
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	@Override
	public int minNumberOfOutputs() {
		return 1;
	}

	public static void createDataMoverTokenInstallerScripts(String shareableToken, File outputZip) throws Throwable {
		String windowsScript = WINDOWS_SCRIPT_TEMPLATE.replace("@@TOKEN@@", shareableToken);
		String unixScript = UNIX_SCRIPT_TEMPLATE.replace("@@TOKEN@@", shareableToken);
		try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outputZip)))) {
			// add windows script to the zip file
			try (InputStream in = new ByteArrayInputStream(windowsScript.getBytes())) {
				zos.putNextEntry(new ZipEntry(WINDOWS_SCRIPT_FILE_NAME));
				StreamCopy.copy(in, zos);
				zos.closeEntry();
			}
			// add unix script to the zip file
			try (InputStream in = new ByteArrayInputStream(unixScript.getBytes())) {
				zos.putNextEntry(new ZipEntry(UNIX_SCRIPT_FILE_NAME));
				StreamCopy.copy(in, zos);
				zos.closeEntry();
			}
		}
	}
}
