/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

// TBD : read user resourcing asset as input instead of command line args,


import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcServerAbortedFilesHandle extends PluginService {
	private Interface _defn;		
	public SvcServerAbortedFilesHandle()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("store", StringType.DEFAULT, "The The store of interest", 1, 1));
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT, "List the results only (default true), take no actual file system action.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service to handle aborted files (in folder <store path>/aborted on the file system) after a server restart.  Files that cannot be matched with an asset are currently just left in place (to be deleted).  Files that have an asset and were moved successfully, but fail content validation are currently left associated with the asset.  Does not handle OIDs if there are multiple copies (disambiguated with suffice .N) because of wind-back process.  OIDs of this form will simply fail the lookup and the service will exit so they have to be handled manually. ";
	}

	public String name() {
		return "unimelb.server.aborted.files.handle";
	}

	public boolean canBeAborted() {
		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		// Args
		String store  = args.value("store");
		Boolean listOnly = args.booleanValue("list-only",  true);

		// Describe store
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", store);
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor().execute("asset.store.describe", dm.root());
		if (r==null) {
			return;
		}
		// Parse store meta-data
		String path = r.value("store/path");

		// Find aborted oids. The structure means we need to do
		// a recursive list
		String abortedPath = path + "/aborted";
		File fAborted = new File(abortedPath);
		if (!fAborted.exists()) {
			return;
		}
		
		// Continue as we have an aborted folder
		ArrayList<File> files = new ArrayList<File>();
		traverseDirectory (fAborted, files);
		if (files.size()==0) {
			return;
		}
		PluginTask.checkIfThreadTaskAborted();

		XmlDocMaker found = new XmlDocMaker("found");
		XmlDocMaker missing = new XmlDocMaker("missing");
		//
		Boolean foundSome = false;
		Boolean missingSome = false;
		for (File file : files) {
			PluginTask.checkIfThreadTaskAborted();
			String srcPath = file.getAbsolutePath();
			String oid = file.getName();

			// Lookup the oid
			dm = new XmlDocMaker("args");
			dm.add("id",new String[] {"store", store},  oid);
			PluginTask.checkIfThreadTaskAborted();
			r = executor().execute("asset.store.find.asset.from.id", dm.root());
			String assetID = r.value("id");
			
			// Handle 
			if (assetID!=null) {
				// We found the oid, so get the asset and it's path
				// so the file can be moved in place.
				XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, assetID);

				// Asset content path
				String s = asset.value("asset/content/url");
				int idx = s.indexOf(":") + 1;
				String destPath = s.substring(idx);        // Remove the leading <schema>:

				// Handle oid
				if (listOnly) {
					String command = "mv " + srcPath + " " + destPath;
					found.add("id", new String[] {"oid", oid, "command", command}, assetID);
				} else {

					// Try to move the file
					Boolean moveFailed = false;
					String moveError = null;
					PluginTask.checkIfThreadTaskAborted();
					try {
						moveFile (srcPath, destPath);
					} catch (Throwable t) {
						moveFailed = true;
						moveError = t.getMessage();
					}

					// Validate content
					PluginTask.checkIfThreadTaskAborted();
					if (!moveFailed) {
						dm = new XmlDocMaker("args");
						dm.add("id", assetID);
						dm.add("all-versions", true);
						dm.add("show-error-only", true);
						r = executor().execute ("unimelb.asset.content.validate.basic", dm.root()); 
						String status = r.value("asset/status");
						if (status==null) {
							status = "pass";
						}
						
						// Tell user move and validation status
						found.add("id", new String[] {"oid", oid, "source", srcPath, "dest", destPath, "move", "success", "validation-status", status}, assetID);
					} else {					
						// Tell user the move failed
						found.add("id", new String[] {"oid", oid, "source", srcPath, "dest", destPath, "move", "failed", "move-message", moveError}, assetID);
					}
				}
				foundSome = true;
			} else {
				// We don't have an asset for this OID. There is nothing to do
				// but destroy them. The permissions are read only so
				// we have to change the permissions or do through Unix layer
				if (listOnly) {
					missing.add("oid", oid);
				} else {
					// TBD change permissions and delete file
					missing.add("oid", oid);
				}
				missingSome = true;
			}
		}
		//
		if (foundSome) {
			w.push("found");
			w.addAll(found.root().elements());
			w.pop();
		}
		if (missingSome) {
			w.push("missing");
			w.addAll(missing.root().elements());
			w.pop();
		}
	}


	private void moveFile (String sourcePath, String destPath) throws Throwable {
		Path pSource = Paths.get(sourcePath);
		Path pDest = Paths.get(destPath);
		Files.move (pSource, pDest, StandardCopyOption.ATOMIC_MOVE);		
	}


	private  void traverseDirectory(File src, List<File> fileList) {
		for (File f : src.listFiles()) {

			if (f.isDirectory()) {
				traverseDirectory(f, fileList);
			}

			if (f.isFile()) {	               
				fileList.add(f);	                
			}
		}
	}
}
