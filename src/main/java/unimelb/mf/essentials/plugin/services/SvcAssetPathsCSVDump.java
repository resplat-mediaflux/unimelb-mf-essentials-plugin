package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.zip.GZIPOutputStream;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.UrlType;
import arc.streams.StreamCopy;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;
public class SvcAssetPathsCSVDump extends PluginService {
	public static final String SERVICE_NAME = "unimelb.asset.paths.csv.dump";
	private Interface _defn;
	public SvcAssetPathsCSVDump() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Selection query. Defaults to all assets.", 0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "Directory on the server to save the file. The file will be named asset-paths-YYYYMMDD-hhmmss.csv.gz", 1, 1));
		_defn.add(new Interface.Element("retain", IntegerType.DEFAULT, "How many files do we maintain in the export folder. If specified then that number of recent files will be maintained. If 0 is specified only the most recent export is retained. Defaults to infinite.", 0, 1));
		_defn.add(new Interface.Element("compress", BooleanType.DEFAULT, "Compress with single threaded gzip (default true). ", 0, 1));
	}
	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}
	@Override
	public Interface definition() {
		return _defn;
	}
	@Override
	public String description() {
		return "Export a csv file (with optional compression) to the server holding asset ID, path, type, csize, ctime and mtime.";
	}
	@Override
	public boolean canBeAborted() {

		return true;
	}
	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String where = args.value("where");
		String url = args.value("url");
		if (!url.toLowerCase().startsWith("file:")) {
			throw new IllegalArgumentException("Unsupported url: " + url + " Only file protocol is supported.");
		}
		String retain  = args.value("retain");
		Boolean compress = args.booleanValue("compress",  true);

		// Name file with date/time (YYYYMMDD-hhmmss)
		String path = url.substring(5);
		File outDir = new File(path);
		if (!outDir.exists()) {
			throw new IOException ("Path '" + path + "' does not exist");
		}
		if (!outDir.isDirectory()) {
			throw new IOException ("Path '" + path + "' is not a directory");
		}
		w.add("dir", outDir.getAbsolutePath());

		// Generate output file
		String today = DateUtil.todaysTime(2);
		File outputFile = null;
		if (compress) {
			outputFile = new File(path + "/asset-paths-" + today + ".csv.gz");
		} else {
			outputFile = new File(path + "/asset-paths-" + today + ".csv");
		}
		w.add("path", outputFile.getAbsolutePath());
		XmlDocMaker dm = new XmlDocMaker("args");
		if (where!=null) {
			dm.add("where", where);
		}
		dm.add("output-format", "csv");
		dm.add("size", "infinity");
		// We must handle multiple values.  FOr example, if the asset has content
		// copies then both of the URLs will get written correctly into the 
		// "content-URL" cell.  Thus we use 'get-values' not 'get-value'
		dm.add("action", "get-values");
		dm.add("xpath", new String[] { "ename", "id" }, "@id");
		dm.add("xpath", new String[] { "ename", "path" }, "path");
		dm.add("xpath", new String[] { "ename", "type" }, "type");
		dm.add("xpath", new String[] { "ename", "size-bytes" }, "content/size");
		dm.add("xpath", new String[] { "ename", "content-URL" }, "content/url");
		dm.add("xpath", new String[] { "ename", "ctime" }, "ctime");
		dm.add("xpath", new String[] { "ename", "mtime" }, "mtime");
		PluginService.Outputs outputs = new PluginService.Outputs(1);
		executor().execute("asset.query", dm.root(), null, outputs);

		// Save output to the file.
		PluginTask.checkIfThreadTaskAborted();
		PluginService.Output output = outputs.output(0);
		try {
			if (compress) {
				// Copy and compress (single threaded).
				gzip (output.stream(), outputFile);
			} else {
				// Copy
				StreamCopy.copy(output.stream(), outputFile);
			}
		} finally {
			output.close();
		}

		// Remove old exports
		PluginTask.checkIfThreadTaskAborted();
		if (retain!=null) {
			removeOldExports (executor(), outDir, retain, compress, w);
		}
	}


	private void removeOldExports (ServiceExecutor executor, File outDir,
			String retain, Boolean compress, XmlWriter w) throws Throwable {

		// Find files
		File[] listOfFiles = outDir.listFiles();
		if (listOfFiles==null) {
			return;
		}

		Integer nBackups = Integer.parseInt(retain);
		if (nBackups<=0) {
			throw new Exception ("You must retain at least one asset path export.");
		}

		// Convert to a List
		List<File> files = Arrays.asList(listOfFiles);
		Vector<File> files2 = new Vector<File>();

		// Sanity checks since we are going to delete files
		for (File file : files) {
			// Exclude directories - we don't delete those or check their naming
			if (!file.isDirectory()) {

				// Names of the form   asset-paths-YYYYMMDD-hhmmss.csv.gz
				// or asset-paths-YYYYMMDD-hhmmss.csv
				String name = file.getName();
				int n2 = name.length();
				if (compress) {
					if (n2!=34) {
						throw new IOException ("The file '" + name + "' is not the correct length [34]");
					}
					String suffix = name.substring(n2-7,n2);
					if (!suffix.equals(".csv.gz")) {
						throw new Exception ("The file '" + name + "' does not appear to be an expected asset paths export file (suffix should be .csv.gz");
					}
				} else {
					if (n2!=31) {
						throw new IOException ("The file '" + name + "' is not the correct length [31]");
					}
					String suffix = name.substring(n2-4,n2);
					if (!suffix.equals(".csv")) {
						throw new Exception ("The file '" + name + "' does not appear to be an expected asset paths export file (suffix should be .csv");
					}
				}
				String prefix = name.substring(0, 11);
				if (!prefix.equals("asset-paths")) {
					throw new Exception ("The file '" + name + "' does not appear to be an expected asset paths  export file. Root name is not 'paths'");
				}
				files2.add(file);
			}
		}

		if (files2.size()>0) {
			// Sort in reverse time order so oldest files at the end of the list
			Collections.sort(files2, new Comparator<File>() {
				@Override
				public int compare(File o1, File o2) {
					// Names of the form asset-paths-YYYYMMDD-hhmmss.csv.gz
					String n1 = o1.getName();
					String[]  parts1 = n1.split("-");
					String n2 = o2.getName();
					String[] parts2 = n2.split("-");

					String date1s = parts1[2].substring(0, 8);
					String date2s = parts2[2].substring(0, 8);
					Long date1 = Long.parseLong(date1s);
					Long date2 = Long.parseLong(date2s);
					if (date1<date2) {
						return 1;
					} else if (date1.equals(date2)) {
						Long time1 = Long.parseLong(parts1[3].substring(0, 6));
						Long time2 = Long.parseLong(parts2[3].substring(0, 6));

						if (time1<time2){
							return 1;
						} else if (time1.equals(time2)) {
							return 0;
						} else {
							return -1;
						}
					} else {
						return -1;
					}
				}
			});

			// Discard older files
			int cnt = 1;
			for (File file2 : files2) {
				if (cnt>nBackups) {
					w.add("file", new String[]{"destroyed", "true"}, file2.getAbsolutePath());
					file2.delete();
				} else {
					w.add("file", new String[]{"destroyed", "false"}, file2.getAbsolutePath());
				}
				cnt++;
			}	
		}
	}
	private  void gzip(InputStream is, File outFile) throws IOException {
		try (BufferedInputStream bis = new BufferedInputStream(is)) {
			try (FileOutputStream fos = new FileOutputStream(outFile);
					BufferedOutputStream bos = new BufferedOutputStream(fos);
					GZIPOutputStream gos = new GZIPOutputStream(bos)) {
				byte[] buffer = new byte[8192];
				int n;
				while ((n = bis.read(buffer)) != -1) {
					if (n > 0) {
						gos.write(buffer, 0, n);
					}
				}
				gos.finish();
			}
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}
}