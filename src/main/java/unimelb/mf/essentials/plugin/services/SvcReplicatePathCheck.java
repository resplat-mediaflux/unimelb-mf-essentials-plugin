package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.CollectionAssetUtil;
import unimelb.mf.essentials.plugin.util.PeerUtil;
import unimelb.utils.PathUtils;

public class SvcReplicatePathCheck extends PluginService {

	private class Asset {
		public String id_ = null; 
		public String path_ = null; 
		public String rid_ = null; 
		Asset(String id, String path, String rid) {
			id_ = id;
			path_ = path;
			rid_ = rid;
		}
	}


	private Interface _defn;

	public SvcReplicatePathCheck() {
		_defn = new Interface();
		_defn.add(new Interface.Element("peer",StringType.DEFAULT, "Name of peer that objects have been replicated to.", 1, 1));
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to restrict the selected assets on the local host. If unset, all assets are considered.", 0, 1));
		_defn.add(new Interface.Element("size",IntegerType.DEFAULT, "Limit the accumulation loop to this number of assets per iteration (if too large, the host may run out of virtual memory).  Defaults to 5000.", 0, 1));
		_defn.add(new Interface.Element("dst", StringType.DEFAULT, "The destination parent namespace path that assets will be replicated to  when replicating namespace trees. Our convention is to use the UUID of the primary server.", 0, 1));
		_defn.add(new Interface.Element("dst-collection", StringType.DEFAULT, "The destination parent collection asset path that assets will be replicated to when  replicating collection asset trees. Our convention is to use the UUID of the primary server + '_ca'. E.g. '1128_ca'.", 0, 1));
		_defn.add(new Interface.Element("move", BooleanType.DEFAULT, "If true, (default false) assets will actually be moved to the correct namespace (one at a time; not efficient) rather than just listed.", 0, 1));
		_defn.add(new Interface.Element("exclude-daris-proc", BooleanType.DEFAULT, "By default, processed DaRIS DataSets (ones for which (pssd-derivation/processed)='true' AND mf-dicom-series is absent) are included. Set to true to exclude these.", 0, 1));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT, "Turn on or off the use of indexes in the query. Defaults to true.", 0, 1));
		//
		Interface.Element list = new Interface.Element("list-paths", BooleanType.DEFAULT,
				"List the current and expected replica path for each asset which needs to move. Defaults to false.", 0, 1);
		list.add(new Interface.Attribute("all", BooleanType.DEFAULT,
				"If true (default false), and if checking just a single asset via the :id argument, list the paths whether the asset needs to move or not.", 0));
		_defn.add(list);
		//	
		_defn.add(new Interface.Element("total", BooleanType.DEFAULT, "If true (default false) and supplying a single :id (no :where), include the :total-checked output element .", 0, 1));
		//
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT, "Write some stuff in the log. Default to false.", 0, 1));
		_defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT, "Include soft destroyed assets (so don't include soft destroy selection in the where predicate. Default to false.", 0, 1));
		_defn.add(new Interface.Element("rep-inc", IntegerType.DEFAULT, "When debug is true, messages are written to the server log. This parameter specifies the increment to report that assets have been replicated.  Defaults to 1000. ", 0, 1));
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "Asset ID to check.  You can't give 'where' and 'id'", 0, 1));
	}
	public String name() {
		return "unimelb.replicate.path.check";
	}

	public String description() {
		return "Lists assets (both primaries, and replicas from other hosts) that have been replicated (if not replicate,  but for which the namespaces don't agree. Can optionally move them into the correct namespace (replica namespaces by our convention is prefixed by the primary server UUID). If  errors are reported see the mediaflux-server log file. Service output (see args. :list and :brief) includes :total-checked, :total-to-move (not returned  if none to move), :total-moved (not returned if move=false), :total-errors, :asset (asset paths if requested).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {


		// Init
		int[] idx = new int[]{1};
		int[] count = new int[]{0};
		Date date = new Date();
		String dateTime = date.toString();     // Just used to tag message in log file

		// Get inputs
		String where = args.value("where");
		String peer = args.value("peer");
		String size = args.stringValue("size", "5000");
		String dstNS = args.stringValue("dst");
		String dstCA = args.stringValue("dst-collection");
		if (dstNS==null && dstCA==null) {
			throw new Exception ("You must supply one of :dst or :dst-collection");
		}
		Boolean isCollection = false;
		String dst = dstNS;		
		if (dstCA!=null) {
			dst = dstCA;
			isCollection = true;
		}
		Boolean exclDaRISProc = args.booleanValue("exclude-daris-proc", false);
		Boolean useIndexes = args.booleanValue("use-indexes", true);
		Boolean dbg = args.booleanValue("debug", false);
		Boolean includeDestroyed = args.booleanValue("include-destroyed", false);
		Boolean showTotal = args.booleanValue("total", false);
		XmlDoc.Element listPaths = args.element("list-paths");

		Integer repInc = args.intValue("rep-inc", 1000);
		Boolean move = args.booleanValue("move", false);
		String assetID = args.value("id");
		if (where!=null && assetID!=null) {
			throw new Exception ("You cannot specify 'where' and 'id'");
		}

		// Find route to peer. Exception if can't reach and build in extra checks to make sure we are 
		// being very safe
		ServerRoute remoteSR = PeerUtil.findPeerRoute(executor(), peer);
		if (remoteSR==null) {
			throw new Exception("Failed to generated the ServerRoute for the remote host");
		}
		//
		String uuidLocal = serverUUID(executor(), null);
		if (remoteSR.target().equals(uuidLocal)) {
			throw new Exception ("Remote peer UUID appears to be the same as the local host (" + uuidLocal + ") - cannot proceed");
		}

		// If the user supplies a single asset ID then this allows this service to
		// then be called effectively via a query and pipe with multiple threads.
		// Take a short cut function for this case
		if (assetID!=null) {
			findAndMoveOneAsset (executor(), dateTime, assetID, peer, remoteSR,
					dst, isCollection, uuidLocal, move, listPaths, 
					showTotal, dbg, w);
			return;
		}


		// Iterate through cursor and build list of assets to move
		boolean more = true;
		Vector<XmlDoc.Element> assets = new Vector<XmlDoc.Element>();
		while (more) {
			more = find (executor(), dateTime, where, peer, remoteSR, dst, 
					isCollection, uuidLocal, size, 
					assets, exclDaRISProc, useIndexes, 
					dbg, includeDestroyed, idx, count, w);
			if (dbg) {
				log(dateTime, "unimelb.replicate.path.check : checking for abort \n");
			}
			PluginTask.checkIfThreadTaskAborted();
		}

		// Correct asset paths, one at a time
		w.add("total-checked", count[0]);
		w.add("total-to-move", assets.size());
		if (dbg) {
			log(dateTime, "   unimelb.replicate.path.check : total checked = " + count[0]);
			log(dateTime, "   unimelb.replicate.path.check : total to move = " + assets.size());
		}
		int nErr = 0;
		if (move) {
			if (dbg) {
				log(dateTime,"Starting move of " + assets.size() + " assets");
			}
			int c = 1;
			int nRep = 0;
			for (XmlDoc.Element asset : assets) {
				// Check for abort
				PluginTask.checkIfThreadTaskAborted();

				// Print out stuff
				if (dbg) {
					int rem = c % repInc; 
					if (c==1 || rem==1) {
						if (dbg) {
							log(dateTime, "unimelb.replicatenamespace.check: move asset # " + c);
						}
					}
				}
				//
				if (listPaths!=null && listPaths.booleanValue()) {
					w.push("asset");
					w.add("id", asset.value("primary/id"));
					w.add("path", asset.value("primary/path"));
					w.push("replica");
					w.add("current-path", asset.value("replica/path"));
					w.add("expected-path", asset.value("replica/expected-path"));
					w.pop();	
					w.pop();
				}

				// Move
				String replicaID = asset.value("replica/id");
				String newRemotePath = asset.value("replica/expected-path");
				if (replicaID==null || newRemotePath==null) {
					// I have seen this happen but I don't know why.
					throw new Exception ("Either the replica ID or path is null.  Something is wrong... Abandoning.");
				}

				try {
					// Move it
					moveAsset (remoteSR, executor(), isCollection, newRemotePath, replicaID);
					nRep++;
				} catch (Throwable t) {
					log(dateTime, "Failed to move remote asset " + replicaID + " with error " + t.getMessage());
					nErr++;
				}
				c++;
			}
			w.add("total-moved", nRep);
			w.add("total-errors", nErr);
			if (dbg) {
				log(dateTime, "   unimelb.replicate.path.check : total moved = " + nRep);
			}
		} else {
			for (XmlDoc.Element asset : assets) {
				if (listPaths!=null && listPaths.booleanValue()) {
					w.push("asset");
					w.add("id", asset.value("primary/id"));
					w.add("path", asset.value("primary/path"));
					w.push("replica");
					w.add("current-path", asset.value("replica/path"));
					w.add("expected-path", asset.value("replica/expected-path"));					w.pop();	
					w.pop();
				}
			}
		}
	}

	private void log (String dateTime, String message) {
		System.out.println(dateTime + " : " + message);
	}

	private String serverUUID(ServiceExecutor executor, String proute) throws Throwable {

		XmlDoc.Element r = executor.execute(new ServerRoute(proute), "server.uuid");
		return r.value("uuid");
	}

	/** 
	 * Build list of assets that are candidates to move
	 * 
	 */
	private boolean find (ServiceExecutor executor, String dateTime, String where, String peer, 
			ServerRoute sr, String dst, Boolean isCollection, String uuidLocal, String size, 
			Vector<XmlDoc.Element> assetList, Boolean exclDaRISProc, 
			Boolean useIndexes, Boolean dbg,
			Boolean includeDestroyed, int[] idx, int[] count, 
			XmlWriter w)	throws Throwable {

		int n0 = assetList.size();
		// Find local  assets  with the given query. We work through the cursor else
		// we may run out of memory
		if (dbg) log(dateTime, "unimelb.replicate.path.check : find assets on primary in chunk starting with idx = " + idx[0]);
		XmlDocMaker dm = new XmlDocMaker("args");
		if (exclDaRISProc) {
			// Drop processed DataSets from query
			if (where==null) where = "";
			// Need this complex expression as there the simpler generates a NULLP
			// (not(xpath(pssd-derivation/processed)='true') or (mf-dicom-series has value))
			where += " and ( (xpath(daris:pssd-derivation/processed)='false' or daris:pssd-derivation hasno value or daris:pssd-derivation/processed hasno value) or (mf-dicom-series has value) )";
		}
		if (includeDestroyed) {
			dm.add("include-destroyed", true);
		}
		if (where!=null) {
			dm.add("where", where);
		}

		dm.add("idx", idx[0]);
		dm.add("size", size);
		dm.add("pdist", 0);
		dm.add("action", "get-value");
		dm.add("xpath", new String[] {"ename", "id"}, "id");
		dm.add("xpath", new String[] {"ename", "rid"}, "rid");
		dm.add("use-indexes", useIndexes);
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) return false;  
		Collection<XmlDoc.Element> assets = r.elements("asset");
		if (assets==null) {
			if (dbg) {
				log(dateTime, "   unimelb.replicate.path.check : no assets found in query");
			}
			return false;
		}
		if (dbg) {
			log(dateTime, "   unimelb.replicate.path.check : found " + assets.size() + " assets in query.");
		}

		count[0] += assets.size();

		// Get the cursor and increment for next time
		XmlDoc.Element cursor = r.element("cursor");
		boolean more = !(cursor.booleanValue("total/@complete"));
		if (more) {
			Integer next = cursor.intValue("next");
			idx[0] = next;
		}

		// See if the replicas exist on the peer. 
		// Make a list of rids to find
		dm = new XmlDocMaker("args");	
		for (XmlDoc.Element asset : assets) {
			PluginTask.checkIfThreadTaskAborted();

			// Get the asset id, and the rid (asset may already be a replica from elsewhere)
			String id = asset.value("id");

			// If the asset is already a replica, its rid remains the same
			// when replicated to another peer
			String rid = asset.value("rid");    

			// If primary, set expected rid on remote peer.
			String rid2 = SvcReplicateCheck.setRID (id, rid, null, uuidLocal);
			dm.add("rid", rid2);
		}

		// Now check if they exist
		if (dbg) {
			log(dateTime, "   unimelb.replicate.path.check : checking if " + assets.size() + " assets exist on DR");
		}
		XmlDoc.Element r2 = executor.execute(sr, "asset.exists", dm.root());
		if (r2==null) {
			return more;
		}
		Collection<XmlDoc.Element> results = r2.elements("exists");
		if (results==null) {
			return more;
		}

		// Now check each replicated asset to see if its namespace is as expected.
		if (dbg) {
			log(dateTime, "   unimelb.replicate.path.check : iterate through " + results.size() + " results and build list for move.");
		}
		Integer n = null;
		for (XmlDoc.Element result : results) {
			PluginTask.checkIfThreadTaskAborted();


			// Fetch the rid 
			String rid = result.value("@rid");

			// If the primary asset is itself a replica (e.g. imported from
			// another collection), its rid will remain the same as the 
			// original asset and we can't extract is current primary asset
			// ID from the rid. 
			Boolean primaryIsAlreadyReplica = !(rid.startsWith(uuidLocal));


			if (dbg) {
				/*
				log(dateTime, "      unimelb.replicate.path.check : id, rid=" + primaryID + "," + rid);
				log(dateTime, "      unimelb.replicate.path.check : result.value=" + result.booleanValue());
				 */
			}


			if (result.booleanValue()) {

				// Get the local asset
				Asset asset = null;
				String primaryID = null;
				if (primaryIsAlreadyReplica) {
					// Fetch local asset by rid
					asset = setAssetDetails (executor, null, null, rid);
					primaryID = asset.id_;
				} else {
					String[] t = rid.split("\\.");			
					if (n==null) {
						n = t.length;           // They are all the same length
					}
					primaryID = t[n-1];
					asset = setAssetDetails (executor, null, primaryID, null);
				}
				String assetPath = asset.path_;

				// Get the remote asset via its rid
				Asset remoteAsset = setAssetDetails (executor, sr, null, rid);

				// FInd the namespace
				String remoteAssetPath = remoteAsset.path_;

				if (dbg) {
					log(dateTime, "      unimelb.replicate.path.check :paths=" + assetPath + ", " +remoteAssetPath);
				}
				String expectedRemoteAssetPath = dst + assetPath;
				if (!expectedRemoteAssetPath.equals(remoteAssetPath)) {
					dm = new XmlDocMaker("args");
					dm.push("asset");
					//
					dm.push("primary");
					dm.add("id", primaryID);
					dm.add("path", assetPath);
					dm.pop();
					//
					dm.push("replica");
					dm.add("path", remoteAssetPath);
					dm.add("expected-path", expectedRemoteAssetPath);
					dm.add("id", remoteAsset.id_);
					dm.add("rid", rid);
					dm.pop();
					//
					dm.pop();
					assetList.add(dm.root().element("asset"));	
				}

			}
		}
		if (dbg) {
			log(dateTime, "   unimelb.replicate.path.check : found " + (assetList.size()-n0) + " assets to move.");
		}
		//
		return more;
	}


	private void findAndMoveOneAsset (ServiceExecutor executor, String dateTime, 
			String id, String peer, ServerRoute sr, String dst, 
			Boolean isCollection, String uuidLocal, 
			Boolean move, XmlDoc.Element listPaths, 
			Boolean showTotal, Boolean dbg, XmlWriter w)	throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		if (dbg) {
			log(dateTime, "unimelb.replicate.path.check : find and move one asset");
		}

		// Get primary asset details that are needed
		Asset asset = setAssetDetails (executor, null, id, null);

		// If the asset is already a replica, its rid remains the same
		// when replicated to another peer
		String rid = asset.rid_;    

		// If primary, set expected rid on remote peer.
		String rid2 = SvcReplicateCheck.setRID (id, rid, null, uuidLocal);

		// See if the replica exists on the peer. 
		XmlDocMaker dm = new XmlDocMaker("args");	
		dm.add("rid", rid2);
		XmlDoc.Element result = executor.execute(sr, "asset.exists", dm.root());
		if (result==null) {
			return;
		}	
		if (!result.booleanValue("exists")) {
			return;
		}

		// Find the path of the primary
		String assetPath = asset.path_;

		// Get the remote asset by its rid
		Asset remoteAsset = setAssetDetails (executor, sr, id, rid2);

		// Find the remote path
		String remoteAssetID = remoteAsset.id_;
		String remoteAssetPath = remoteAsset.path_;
		String expectedRemoteAssetPath = dst + assetPath;

		// Parse listing options
		Boolean list = false;
		Boolean listAll = false;
		if (listPaths!=null) {
			list = listPaths.booleanValue();
			listAll = listPaths.booleanValue("@all",false);
		}
		//
		if (!expectedRemoteAssetPath.equals(remoteAssetPath)) {
			if (list) {
				w.push("asset");
				w.add("id", id);
				w.add("path", assetPath);
				w.push("replica");
				w.add("rid", rid2);
				w.add("current-path", remoteAssetPath);
				w.add("expected-path", expectedRemoteAssetPath);
				w.pop();	
				w.pop();				
			}
			if (showTotal) {
				w.add("total-checked", 1);
			}
			if (move) {
				try {
					moveAsset (sr, executor, isCollection, expectedRemoteAssetPath, remoteAssetID);
					w.add("total-moved", 1);  
				} catch (Throwable t) {
					log(dateTime, "Failed to move remote asset " + remoteAssetID + " with error " + t.getMessage());
					w.add("total-errors", 1);
				}
			} else {
				w.add("total-to-move", 1);
			}
		} else {
			if (list && listAll) {
				w.push("asset");
				w.add("id", id);
				w.add("path", assetPath);
				w.push("replica");
				w.add("rid", rid2);
				w.add("current-path", remoteAssetPath);
				w.add("expected-path", expectedRemoteAssetPath);
				w.pop();	
				w.pop();				
			}
			if (showTotal) {
				w.add("total-checked", 1);
			}
		}
	}

	/**
	 * Get some details from the asset, local or remote.
	 * 
	 * @param executor
	 * @param sr
	 * @param id 
	 * @param rid 
	 * @return
	 * @throws Throwable
	 */
	private Asset setAssetDetails (ServiceExecutor executor, ServerRoute sr, 
			String id, String rid) throws Throwable {
		// Avoid asset.get as it will have to call ActiveDirectory
		// to resolve actor IDs into names
		XmlDocMaker dm = new XmlDocMaker("args");
		if (id!=null && sr==null) {
			// Fetch local asset by ID
			dm.add("where", "id="+id);
		} else if (rid!=null && sr!=null) {
			// Fetch remote asset by RID
			dm.add("where", "rid='"+rid+"'");
		} else if (rid!=null && sr==null) {
			// Fetch local asset by RID
			dm.add("where", "rid='"+rid+"'");
		} else {
			throw new Exception("Internal error");
		}
		dm.add("action", "get-value");
		dm.add("xpath", new String[] {"ename", "id"}, "id");
		dm.add("xpath", new String[] {"ename", "path"}, "path");
		dm.add("xpath", new String[] {"ename", "rid"}, "rid");
		XmlDoc.Element r = executor().execute(sr, "asset.query", dm.root());
		Asset a = new Asset(r.value("asset/id"), 
				r.value("asset/path"),
				r.value("asset/rid"));
		return a;
	}


	/**
	 * Does not handle name clashes
	 * TBD replace with better native services
	 * See ticket 3899
	 * 
	 * @param sr
	 * @param executor
	 * @param isCollection
	 * @param path
	 * @param id
	 * @throws Throwable
	 */
	private void moveAsset (ServerRoute sr, ServiceExecutor executor, 
			Boolean isCollection, String path, String id) throws Throwable {

		// We have the full path on the DR, we just want the parent part
		// to see if a collection asset of that path exists
		String parent = PathUtils.getParentPath(path);

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		if (isCollection) {
			// The parent path that we want to move to may not exist
			// so create if needed.
			createParentCollections (executor, sr, parent);
			dm.add("to", "path="+parent);
		} else {
			dm.add("namespace", new String[] {"create", "true"}, parent);
		}
		executor().execute(sr, "asset.move", dm.root());
	}

	private void createParentCollections (ServiceExecutor executor, 
			ServerRoute sr, String path) throws Throwable {
		if (!AssetUtils.exists(executor, sr, "path="+path, false)) {
			// Makes any intermediary parents if needed
			CollectionAssetUtil.create(sr, executor, path, null, null);
		}	
	}

	/**
	 * Get the local asset's meta-data
	 * 
	 * @param executor
	 * @param rid 
	 * @throws Throwable
	 */
	public static XmlDoc.Element getLocalAssetByRID (ServiceExecutor executor, String rid) throws Throwable {

		// Get meta-data in input object
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", "rid="+rid);
		return executor.execute("asset.get", dm.root());

	}

}
