package unimelb.mf.essentials.plugin.services;

import java.io.InputStream;
import java.util.Date;
import java.util.EnumSet;

import arc.archive.ArchiveOutput;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import unimelb.mf.essentials.plugin.script.TargetOS;
import unimelb.mf.essentials.plugin.script.download.AssetDownloadAtermScriptWriter;
import unimelb.mf.essentials.plugin.script.download.AssetDownloadScriptSettings;

public class SvcAssetDownloadAtermScriptCreate extends SvcAssetDownloadScriptCreate {

    public static final String SERVICE_NAME = "unimelb.asset.download.aterm.script.create";

    public static final String DEFAULT_SCRIPT_FILE_NAME = "unimelb-mf-aterm-download";

    public SvcAssetDownloadAtermScriptCreate() {
        super();
        addToDefn(this.defn);
    }

    private static void addToDefn(Interface defn) {
        defn.add(new Interface.Element("ncsr", IntegerType.POSITIVE_ONE,
                "Number of concurrent server requests. Defaults to 1", 0, 1));
        defn.add(new Interface.Element("overwrite", BooleanType.DEFAULT,
                "Whether or not overwrite existing files. Defaults to false", 0, 1));
        defn.add(new Interface.Element("verbose", BooleanType.DEFAULT,
                "Whether or not display the files being downloaded. Defaults to false", 0, 1));
    }

    @Override
    public String description() {
        return "Generates a Unix/Windows shell script wrapper for aterm.jar. Java runtime is required.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    protected void addScriptFiles(EnumSet<TargetOS> targets, ArchiveOutput ao, AssetDownloadScriptSettings settings,
            Element args) throws Throwable {

        addAtermJar(ao);

        int ncsr = args.intValue("ncsr", 1);
        settings.addArg(AssetDownloadAtermScriptWriter.ARG_NCSR, ncsr);

        boolean overwrite = args.booleanValue("overwrite", false);
        settings.addArg(AssetDownloadAtermScriptWriter.ARG_OVERWRITE, overwrite);

        boolean verbose = args.booleanValue("verbose", false);
        settings.addArg(AssetDownloadAtermScriptWriter.ARG_VERBOSE, verbose);

        Date tokenExpiry = args.dateValue("token/to", null);
        settings.setTokenExpiry(tokenExpiry);

        for (TargetOS targetOS : targets) {
            String fileExt = targetOS == TargetOS.WINDOWS ? ".cmd" : ".sh";
            String fileName = settings.fileName();
            if (!fileName.endsWith(fileExt)) {
                fileName = fileName + fileExt;
            }
            ao.begin("text/plain", fileName, -1);
            try {
                AssetDownloadAtermScriptWriter w = AssetDownloadAtermScriptWriter.create(ao.stream(), false, targetOS,
                        settings);
                try {
                    w.process(executor());
                } finally {
                    w.close();
                }
            } finally {
                ao.end();
            }
        }
    }

    private void addAtermJar(ArchiveOutput ao) throws Throwable {
        PluginService.Outputs outputs = new PluginService.Outputs(1);
        executor().execute("system.aterm.get", (XmlDoc.Element) null, null, outputs);
        PluginService.Output output = outputs.output(0);
        InputStream in = output.stream();
        try {
            ao.add("application/java-archive", "aterm.jar", in, output.length());
        } finally {
            in.close();
            output.close();
        }
    }

    @Override
    protected final String defaultScriptFileName() {
        return DEFAULT_SCRIPT_FILE_NAME;
    }

    @Override
    protected String tokenApp() {
        // TODO
        // return AssetDownloadAtermScriptWriter.TOKEN_APP;
        return null;
    }

    @Override
    protected final String tokenTag() {
        return AssetDownloadAtermScriptWriter.TOKEN_TAG;
    }

}
