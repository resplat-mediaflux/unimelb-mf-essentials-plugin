package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.utils.DateUtil;

public class SvcAssetWormSet extends PluginService {


	private Interface _defn;


	public SvcAssetWormSet() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to restrict the selected assets on the local host. If unset, all assets are considered.", 0, 1));
		_defn.add(new Interface.Element("can-add-versions", BooleanType.DEFAULT,
				"Indicates whether new versions of metadata and content are allowed when in the WORM state. Defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("can-move", BooleanType.DEFAULT,
				" Indicates whether the collection, or asset(s) can be moved. Defaults to 'true'",
				0, 1));
		_defn.add(new Interface.Element("nb-threads", IntegerType.DEFAULT,
				"Number of threads to execute (via piped query). Defaults to 1.",
				0, 1));
		Interface.Element ie = new Interface.Element("email", StringType.DEFAULT, "Email address to send report to. Defaults to no email.", 0, 1);
		ie.add(new Interface.Attribute("subject", StringType.DEFAULT,
				"Subject of message. If not specified, a default is used.", 0));
		_defn.add(ie);
	}
	public String name() {
		return "unimelb.asset.worm.set";
	}

	public String description() {
		return "Sets assets into WORM state (no expiry, applies to all asset components). If it is already in WORM (any) state, no action is taken (this includes WORM state that is no longer active).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Get inputs
		String where = args.value("where");
		XmlDoc.Element email = args.element("email");
		Boolean canAddVersions = args.booleanValue("can-add-versions", false);
		Boolean canMove = args.booleanValue("can-move", true);
		String nbThreads = args.stringValue("nb-threads","1");

		// Do piped query
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where);
		dm.add("pipe-nb-threads", nbThreads);
		dm.add("pipe-generate-result-xml", true);
		dm.add("size", "infinity");
		dm.add("action","pipe");
		dm.push("service", new String[] {"name", "unimelb.asset.worm.state.set"});
		dm.add("can-add-versions", canAddVersions);
		dm.add("can-move", canMove);
		dm.add("show-new-only", true);
		dm.pop();
		PluginTask.checkIfThreadTaskAborted();
		String now = DateUtil.todaysTime();
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		int nWORM = 0;
		if (r!=null) {
			Collection<XmlDoc.Element> ids = r.elements("id");
			if (ids!=null) {
				w.addAll(ids);
				nWORM = ids.size();
			}
		}

		//
		if (email!=null && nWORM>0) {
			String subject = email.value("@subject");
			if (subject==null) {
				subject = "Assets set into WORM state";
			}
			String body = "Service unimelb.asset.worm.set was executed at " + now + " for query \n";
			body += "   " + where + "\n";
			body += "   " + nWORM + " assets were set into WORM state.";
			String from = Properties.getServerProperty(executor(), "mail.from");
			MailUtils.sendEmail(executor(), email.value(), null, null, from, subject, body, false);
		}
	}


}
