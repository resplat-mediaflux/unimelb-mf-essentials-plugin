package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.IntegerType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

public class SvcUserLoginCount extends PluginService {



    private Interface _defn;

    public SvcUserLoginCount() {
        _defn = new Interface();
        _defn.add(new Interface.Element("from", DateType.DEFAULT, "From date", 1, 1));
        _defn.add(new Interface.Element("to", DateType.DEFAULT, "To date.", 1, 1));
        _defn.add(new Interface.Element("size", IntegerType.DEFAULT, "cursor size (default 10000)", 0, 1));
    }

    public String name() {
        return "unimelb.user.login.count";
    }

    public String description() {
        return "Counts the number of distinct user logins in the specified date/time range.";
    }

    public Interface definition() {
        return _defn;
    }

    public Access access() {
        return ACCESS_ADMINISTER;
    }

    public int executeMode() {
        return EXECUTE_LOCAL;
    }

    public boolean canBeAborted() {
        return true;
    }

    @Override
    public int minNumberOfOutputs() {
        return 0;
    }

    @Override
    public int maxNumberOfOutputs() {
        return 0;
    }

    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        Date from = args.dateValue("from");
        Date to = args.dateValue("to");
        int size = args.intValue("size", 10000);

        countUsers (executor(), from, to, size, w);
    }



    private  void countUsers (ServiceExecutor executor, Date from, Date to,
                              int size, XmlWriter w) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("from", from);
        dm.add("to", to);
        dm.add("type", "user.logon");
        XmlDoc.Element r = executor.execute("audit.count", dm.root());
        Integer nEvents = r.intValue("count");
        w.add("nevents", nEvents);
        if (nEvents==0) {
            return;
        }

        // Iterate through events
        int idx = 1;
        int nHandled = 0;
        Boolean more = true;
        // String = user name
        // Integer = number logins
        HashMap<String,Integer> map = new HashMap<String,Integer>();
        while (more) {
            PluginService.checkIfThreadTaskAborted();
            dm = new XmlDocMaker("args");
            dm.add("from", from);
            dm.add("to", to);
            dm.add("type", "user.logon");
            dm.add("size", size);
            dm.add("idx", idx);
            // idx can be bigger than the max event index
            // So just keep iterating until nothing is returned
            r = executor.execute("audit.query", dm.root());
            if (r==null) {
                more = false;
            } else {
                Collection<XmlDoc.Element> actors = r.elements("event/actor");
                if (actors!=null) {
                    for (XmlDoc.Element actor : actors) {
                        String type =actor.value("@type");
                        String user = actor.value();
                        if (type.equals("user")) {
                            if (map.containsKey(user)) {
                                Integer n = map.get(user);
                                map.put(user, ++n);
                            } else {
                                map.put(user, 1);
                            }
                        }
                    }
                } else {
                    more = false;
                }
            }
            idx += size;
            nHandled += size;
            int nLeft = nEvents - nHandled;
            if (nLeft<=0) {
                more = false;
            } else {
                if (size>nLeft) {
                    size = nLeft;
                }
            }
        }
        w.add("nhandled", nHandled);

        // Count up users
        Integer nUsers = map.keySet().size();
        w.add("nusers", nUsers);
        for (String key : map.keySet()) {
            w.add("user", new String[] {"nlogins", ""+map.get(key)}, key);
        }
    }
}
