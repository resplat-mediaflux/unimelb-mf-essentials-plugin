package unimelb.mf.essentials.plugin.services;

import java.util.regex.Pattern;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.EssentialsPluginModule;
import unimelb.mf.plugin.util.AssetCopy;
import unimelb.mf.plugin.util.AssetCopy.IfExists;
import unimelb.mf.plugin.util.AssetCopy.Options;
import unimelb.mf.plugin.util.AssetNamespaceUtils;
import unimelb.mf.plugin.util.AssetUtils;
import unimelb.mf.plugin.util.UserUtils;
import unimelb.mf.plugin.util.collection.CollectionDetails;

public class SvcAssetCopy extends PluginService {

    public static final String SERVICE_NAME = EssentialsPluginModule.SERVICE_PREFIX + "asset.copy";

    public static final int MAX_WORKERS_USER = 8;

    public static final int MAX_WORKERS_ADMIN = 64;

    private final Interface _defn;

    public SvcAssetCopy() {

        _defn = new Interface();

        Interface.Element id = new Interface.Element("id", AssetType.DEFAULT,
                "The source asset id to copy from. If it is a collection asset, the whole collection will be copied.",
                0, 1);
        id.add(new Interface.Attribute("version",
                new StringType(Pattern.compile("^(latest|all|\\d+)$", Pattern.CASE_INSENSITIVE)),
                "Version(s) of the asset to copy. Can be 'latest', 'all', or the version number. Version number zero is equivalent to 'latest'. Defaults to 'latest'",
                0));
        _defn.add(id);

        _defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The source asset namespace.", 0, 1));

        _defn.add(new Interface.Element("drop-levels", new IntegerType(-1, Integer.MAX_VALUE),
                "The number of top levels of the asset/namespace path to drop when generating the output "
                        + "asset path. If the asset has path /a/b/c/d (4 levels) and 'drop-top-n-levels' is 2 and 'to' "
                        + "is /x/y then the copied asset path will be /x/y/c/d.  Use 0 to preserve the full input "
                        + "asset/namespace path. The special value of -1 (the default) means drop all leading levels "
                        + "so the new asset will just be created directly in 'to' (same behaviour as asset.move).",
                0, 1));

        Interface.Element to = new Interface.Element("to", StringType.DEFAULT,
                "The output parent path. Could be a collection asset or an asset namespace.", 1, 1);
        to.add(new Interface.Attribute("create", BooleanType.DEFAULT,
                "Create the output parent if it does not exist. Default to false.", 0));
        _defn.add(to);

        _defn.add(new Interface.Element("children-version", new EnumType(VersionSelect.stringValues()),
                "The version(s) to copy for the children assets. Defaults to " + VersionSelect.LATEST, 0, 1));

        _defn.add(new Interface.Element("if-exists", new EnumType(AssetCopy.IfExists.stringValues()),
                "Action to take if the asset already exists at the destination. Defaults to " + AssetCopy.IfExists.ERROR
                        + ".",
                0, 1));

        _defn.add(new Interface.Element("add-note", BooleanType.DEFAULT,
                "Save original asset id to " + "meta-data note. Defaults to false.", 0, 1));

        _defn.add(new Interface.Element("nb-workers", new IntegerType(1, MAX_WORKERS_ADMIN),
                "Number of worker threads to copy the data. Defaults to 1. System administrators can have up to "
                        + MAX_WORKERS_ADMIN + " workers. Non system administrators can have up to " + MAX_WORKERS_USER
                        + " workers.",
                0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Copy an asset (latest version or all versions) to a new location (either an asset collection or an "
                + "asset namespace). All ACLs are dropped.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        final int nbWorkers = args.intValue("nb-workers", 1);
        if (nbWorkers > MAX_WORKERS_USER && !UserUtils.hasSystemAdministratorRole(executor())) {
            throw new IllegalArgumentException(
                    "Non system-administrators are not allowed to have more than " + MAX_WORKERS_USER + " users.");
        }

        final String srcAssetId = args.value("id");
        final String srcAssetVersion = args.stringValue("id/@version", "latest");
        int srcAssetVersionNumber;
        if ("latest".equalsIgnoreCase(srcAssetVersion)) {
            srcAssetVersionNumber = AssetCopy.VERSION_LATEST;
        } else if ("all".equalsIgnoreCase(srcAssetVersion)) {
            srcAssetVersionNumber = AssetCopy.VERSION_ALL;
        } else {
            srcAssetVersionNumber = Integer.parseInt(srcAssetVersion);
        }
        String srcAssetNamespacePath = args.value("namespace");
        if (srcAssetId == null && srcAssetNamespacePath == null) {
            throw new IllegalArgumentException("Either id or namespace must be specified.");
        }

        final String toRootPath = args.value("to");
        final boolean create = args.booleanValue("to/@create", false);
        CollectionDetails toRootDir = CollectionDetails.get(executor(), toRootPath, null);
        if (!toRootDir.exists()) {
            if (create) {
                PluginTask.checkIfThreadTaskAborted();
                toRootDir = toRootDir.createIfNotExist(executor());
            } else {
                throw new Exception("The destination to be copied to: '" + toRootPath + "' does not exist.");
            }
        }

        final int dropTopNLevels = args.intValue("drop-levels", AssetCopy.DROP_LEVELS_ALL);
        final VersionSelect childrenVersion = VersionSelect.fromString(args.value("children-version"),
                VersionSelect.LATEST);
        final IfExists ifExists = IfExists.fromString(args.value("if-exists"), IfExists.ERROR);
        final boolean addNote = args.booleanValue("add-note", false);

        final Options options = new Options(nbWorkers, ifExists, childrenVersion.number, addNote);

        if (srcAssetId != null) {
            if (!AssetUtils.assetExists(executor(), srcAssetId)) {
                throw new IllegalArgumentException("Asset " + srcAssetId + " does not exist.");
            }
            PluginTask.checkIfThreadTaskAborted();
            AssetCopy.Result r = AssetCopy.copy(executor(), srcAssetId, srcAssetVersionNumber, dropTopNLevels,
                    toRootPath, toRootDir.isAssetNamespace(), options);
            r.save(w);
        }

        if (srcAssetNamespacePath != null) {
            if (!AssetNamespaceUtils.assetNamespaceExists(executor(), srcAssetNamespacePath)) {
                throw new IllegalArgumentException("Asset namespace '" + srcAssetNamespacePath + "' does not exist.");
            }
            PluginTask.checkIfThreadTaskAborted();
            AssetCopy.Result r = AssetCopy.copy(executor(), srcAssetNamespacePath, dropTopNLevels, toRootPath,
                    toRootDir.isAssetNamespace(), options);
            r.save(w);
        }
    }

    public static enum VersionSelect {
        LATEST(AssetCopy.VERSION_LATEST), ALL(AssetCopy.VERSION_ALL);

        public final int number;

        VersionSelect(int number) {
            this.number = number;
        }

        @Override
        public String toString() {
            return name().toLowerCase();
        }

        public static VersionSelect fromString(String s, VersionSelect defaultValue) {
            if (s != null) {
                VersionSelect[] vs = values();
                for (VersionSelect v : vs) {
                    if (v.toString().equalsIgnoreCase(s)) {
                        return v;
                    }
                }
            }
            return defaultValue;
        }

        public static String[] stringValues() {
            VersionSelect[] vs = values();
            String[] svs = new String[vs.length];
            for (int i = 0; i < vs.length; i++) {
                svs[i] = vs[i].toString();
            }
            return svs;
        }
    }
}
