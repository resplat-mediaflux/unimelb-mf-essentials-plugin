package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.utils.DateUtil;


/**
 * 
 * @author Neil Killeen
 *
 */
public class SvcAssetPrune extends PluginService {
	private Interface _defn;

	public SvcAssetPrune() {

		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "Asset ID to copy.", 0, 1));
		_defn.add(new Interface.Element("show-all", BooleanType.DEFAULT, "Return the result and generate a notification (optional), even if no actual pruning is done (default true).  If false, no result or notification is generated if no versions were actually pruned.", 0, 1));
	}

	public String name() {
		return "unimelb.asset.prune";
	}

	public String description() {
		return "Prunes an asset controlled by the meta-data unimelb:asset-prune. If this meta-data does not exist, the asset is not pruned. If the meta-data contains a notification email address that email address receives a notification of the action.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String id = args.value("id");
		Boolean showAll = args.booleanValue("show-all", true);

		// Get asset
		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);
		XmlDoc.Element meta = asset.element("asset/meta/unimelb:asset-prune");
		if (meta==null) {
			return;
		}

		// 'retain' is mandatory and restricted but make sanity check
		if (meta.value("retain")==null) {
			throw new Exception("Mandatory element unimelb:asset-prune/retain missing");
		}
		Integer nRetain = meta.intValue("retain");
		if (nRetain<1) {
			throw new Exception("Mandatory element unimelb:asset-prune/retain has illegal value");
		}
		// Prune
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		dm.add("retain", nRetain);
		XmlDoc.Element r = executor().execute("asset.prune", dm.root());
		XmlDoc.Element pruned = r.element("pruned");  // Always returned
		Integer nPruned = pruned.intValue();

		// Notify
		if (nPruned>0 || showAll) {
			w.add(pruned);
			Collection<String> emails = meta.values("email");
			if (emails!=null) {
				String today = DateUtil.todaysDate(2);
				String path = asset.value("asset/path");
				String from = Properties.getServerProperty(executor(), "mail.from");
				String body = "Dear colleague \n\n" +
						"You have previously indicated that older versions of the following asset \n" +
						"can be removed in order to keep them from growing too large.\n" +
						"See below for details for this asset \n\n" +
						"   date : " + today + "\n" +
						"   id : " + id + "\n" + 
						"   path : " + path + "\n" +
						"   versions pruned : " + ""+nPruned + "\n" +
						"   versions retained : " + ""+nRetain + "\n\n" +
						"regards \n Mediaflux Operations Team \n Research Computing Services \n The University of Melbourne";

				for (String email : emails) {
					MailUtils.sendEmail(executor(), email, null, null, from, "Pruned asset : " + path, body, false);
					w.add("notified", email);
				}
			}
		}
	}
}