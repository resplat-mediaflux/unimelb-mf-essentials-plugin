package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import arc.mf.plugin.*;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceMetaDataRename extends PluginService {

	private Interface _defn;

	public SvcAssetNameSpaceMetaDataRename() {
		_defn = new Interface();
		Interface.Element me = new Interface.Element("namespace", StringType.DEFAULT, "The namespace.", 1, 1);
		_defn.add(me);
		//
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "The Document Type name.", 1, 1));
		_defn.add(new Interface.Element("new-type", StringType.DEFAULT, "The new Document Type name.", 1, 1));
		_defn.add(new Interface.Element("mode",
				new EnumType(new String[] { "rename", "copy"}),
				"If 'copy', leaves the original meta-data document in place. If 'rename' (default), the original document is removed.", 0, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.metadata.rename";
	}

	public String description() {
		return "Specialised service to copy a namespace meta-data document to one of another name.  Both the source and destination document types must exist, and their definitions should be the same, apart from the document name (including document namespace). The method is to copy the document (changing its name) and then optionally remove the original. Use this to change the namespace of a document, say renaming from VicNode:Collection to ds:Collection.  If the new document already exists (by name), it won't be created again.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String nameSpace = args.value("namespace");
		String type = args.value("type");
		String typeNew = args.value("new-type");
		String mode = args.stringValue("mode", "rename");

		// Get namespace meta-data
		XmlDoc.Element nsMeta = NameSpaceUtil.describe(null, executor(), nameSpace);

		// Get the meta-data
		XmlDoc.Element meta = nsMeta.element("namespace/asset-meta");
		if(meta==null) {
			return;
		}

		// Get the documents
		Collection<XmlDoc.Element> docs = meta.elements(); 
		PluginTask.checkIfThreadTaskAborted();

		// Iterate through documents 
		if (docs!=null) {
			for (XmlDoc.Element doc : docs) {
				PluginTask.checkIfThreadTaskAborted();
				//
				String qn = doc.qname();
				String mid = doc.value("@id");

				// If we are interested in this document fragment handle it
				if (type.equals(qn)) {

					// Skip if pre-exists by name
					if (!docExistsByName(typeNew, docs)) {
						// Copy the document 
						XmlDoc.Element doc2 = doc.copy();

						// Modify the copy of the document by changing its name
						// and removing the id attribute
						doc2.setName(typeNew);
						doc2.removeAttribute("id");
						String[] t = qn.split(":");
						if (t.length==2) {
							doc2.removeAttribute("xmlns:" + t[0]);
						} else {
							doc2.removeAttribute("xmlns");
						}

						// Add that new modified meta-data document to the namespace
						XmlDocMaker dm = new XmlDocMaker("args");
						dm.add("namespace", nameSpace);
						dm.push("asset-meta");
						dm.add(doc2);
						dm.pop();
						executor().execute(null, "asset.namespace.asset.meta.add", dm.root());
						w.add("copied", new String[] {"from", type, "id", mid}, typeNew);

						// If doing 'rename' remove the old document
						if (mode.equals("rename")) {
							dm = new XmlDocMaker("args");
							dm.add("namespace", nameSpace);
							// The mid will not have changed because all we have 
							// done is add a fragment (which will get a new mid)
							dm.add("mid", mid);
							executor().execute(null, "asset.namespace.asset.meta.remove", dm.root());
							w.add("removed", new String[] {"id", mid}, type);
						}
					}
				}
			}
		}
	}
	
	private static Boolean docExistsByName (String name, Collection<XmlDoc.Element> docs) throws Throwable {
		for (XmlDoc.Element doc : docs) {
			String qn = doc.qname();
			if (qn.equals(name)) {
				return true;
			}
		}
		return false;
	}
}
