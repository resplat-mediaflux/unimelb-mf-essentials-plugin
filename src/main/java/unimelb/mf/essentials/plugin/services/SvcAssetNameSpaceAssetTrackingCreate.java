/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceAssetTrackingCreate extends PluginService {


	static public String NAMESPACE_ASSET_TYPE = "uom-ans";
	private Interface _defn;

	public SvcAssetNameSpaceAssetTrackingCreate() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "If true (default), descend down all child namespaces recursively.", 0, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.asset.tracking.create";
	}

	public String description() {
		return "Creates a ‘tracking’ asset in every child (recursion can be disabled) namespace (folder) that can be queried for (by name or type) as a proxy for the namespace (namespaces cannot be queried for). If the asset pre-exists, it will be skipped.  The asset is named <namespace>.ns where <namespace> is the child part of the namespace path.  The type of the asset is ‘uom-ans’. The full path of the namespace is stored in the meta-data element mf-name/name. For example, if the namespace in which an asset was created was called /a/b/c then the asset would be named ‘c.ns’, would have type uom-ans, and the meta-data element mf-name/name would have the value ‘/a/b/c’.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		Boolean recurse = args.booleanValue("recurse", true);
		createType (executor());
		create (executor(), namespace, recurse, w);
	}

	private void createType (ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("type", NAMESPACE_ASSET_TYPE);
		XmlDoc.Element r = executor.execute("type.exists", dm.root());
		if (r.booleanValue("exists")) {
			return;
		}
		//
		dm = new XmlDocMaker("args");
		dm.add("type", NAMESPACE_ASSET_TYPE);
		dm.add("description", "This type is used for assets which are located in namespaces. They have the name <namespace>.ns and are used as a proxy for querying on namespaqces");
		executor.execute("type.create", dm.root());
	}

	private void create (ServiceExecutor executor, String namespace, 
			Boolean recurse, XmlWriter w) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Create asset in this namespace
		createAsset (executor, namespace, w);
		if (!recurse) {
			return;
		}

		// Iterate/descend into  children namespaces
		Collection<String> nss = NameSpaceUtil.listNameSpaces(executor, namespace, true);
		if (nss==null) {
			return;
		}
		for (String ns : nss) {
			create (executor, ns, recurse, w);
		}
	}

	private void createAsset (ServiceExecutor executor, String namespace,
			XmlWriter w) throws Throwable {
		// Find the child part of the namespace path
		int idx = namespace.lastIndexOf("/");
		String relNS = namespace.substring(idx+1);

		// See if asset pre-exists
		String assetName = relNS + ".ns";
		String path = namespace + "/" + assetName;
		String id = AssetUtils.exists(executor, path);
		if (id==null) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", namespace);
			dm.add("type", NAMESPACE_ASSET_TYPE);
			dm.add("name", assetName);
			dm.push("meta");
			dm.push("mf-name");
			dm.add("name", namespace);
			dm.pop();
			dm.pop();
			XmlDoc.Element r = executor.execute("asset.create", dm.root());
			id = r.value("id");
			w.add("path", new String[] {"id", id, "exists", "false", "created", "true"}, path);
		} else
			w.add("path", new String[] {"id", id, "exists", "true", "created", "false"}, path);
	}
}
