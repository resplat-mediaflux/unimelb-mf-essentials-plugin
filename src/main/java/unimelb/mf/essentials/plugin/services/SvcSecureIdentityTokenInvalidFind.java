package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcSecureIdentityTokenInvalidFind extends PluginService {

	public static final String SERVICE_NAME = "unimelb.secure.identity.token.invalid.find";

	private Interface _defn;

	public SvcSecureIdentityTokenInvalidFind() {
		_defn = new Interface();
		_defn.add(new Interface.Element("destroy", BooleanType.DEFAULT,
				"Destroy the invalid tokens found. Defaults to false", 0, 1));
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT,
				"If any tokens were found, email the result to the specified recipient(s).", 0, 5));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Find the tokens whose owner accounts have been disabled or removed from LDAP domain.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(arc.xml.XmlDoc.Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		boolean destroy = args.booleanValue("destroy", false);
		Collection<String> emails = args.values("email");
		StringBuilder sb = (emails == null || emails.isEmpty()) ? null : beginEmailMessage();
		int count = 0;
		List<XmlDoc.Element> ies = describeAllIdentityTokens(executor());
		if (ies != null) {
			for (XmlDoc.Element ie : ies) {
				PluginTask.checkIfThreadTaskAborted();
				String ownerDomain = ie.value("owner/domain");
				String ownerUser = ie.value("owner/user");
				String ownerName = ie.value("owner/name");
				String ownerEmail = ie.value("owner/email");
				boolean ownerExists = userExists(executor(), ownerDomain, ownerUser);
				boolean ownerEnabled = ownerExists ? isUserEnabled(executor(), ownerDomain, ownerUser) : false;
				if (!ownerExists || !ownerEnabled) {
					String tokenId = ie.value("@id");
					String actorName = ie.value("actor");
					if (destroy) {
						String lifetime = ie.value("@lifetime");
						destroyIdentityToken(executor(), tokenId, lifetime);
					}
					w.push("identity", new String[] { "id", tokenId, "destroyed", Boolean.toString(destroy) });
					w.add("actor", new String[] { "type", "identity" }, actorName);
					w.push("owner", new String[] { "exists", Boolean.toString(ownerExists), "enabled",
							ownerExists ? Boolean.toString(ownerEnabled) : null });
					w.add("domain", ownerDomain);
					w.add("user", ownerUser);
					w.add("name", ownerName);
					w.add("email", ownerEmail);
					w.pop();
					w.pop();
					if (sb != null) {
						sb.append("<tr>");
						sb.append("<td>").append(tokenId).append("</td>");
						sb.append("<td>").append(actorName).append("</td>");
						sb.append("<td>").append(ownerDomain + ":" + ownerUser).append("</td>");
						sb.append("<td>").append(ownerExists ? "disabled user" : "removed user").append("</td>");
						sb.append("<td>").append(destroy ? "Yes" : "No").append("</td>");
						sb.append("</tr>");
					}
					count++;
				}
			}
		}
		w.add("count", count);
		if (sb != null && count > 0) {
			endEmailMessage(sb, count);
			sendEmails(executor(), emails, count, sb.toString());
			for (String email : emails) {
				w.add("sent-to", email);
			}
		}
	}

	private static List<XmlDoc.Element> describeAllIdentityTokens(ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("all-users", true);
		dm.add("size", "infinity");
		return executor.execute("secure.identity.token.describe", dm.root()).elements("identity");
	}

	private static boolean userExists(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		boolean domainExists = executor.execute("authentication.domain.exists", dm.root()).booleanValue("exists");
		if (!domainExists) {
			return false;
		}
		dm.add("user", user);
		return executor.execute("user.exists", dm.root()).booleanValue("exists");
	}

	private static boolean isUserEnabled(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("authentication.user.enabled", dm.root()).booleanValue("enabled");
	}

	private static void destroyIdentityToken(ServiceExecutor executor, String id, String lifetime) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", new String[] { "lifetime", lifetime }, id);
		executor.execute("secure.identity.token.destroy", dm.root());
	}

	private static StringBuilder beginEmailMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("<style>\n");
		sb.append("table, th, td { border: 1px solid black; }\n");
		sb.append("tr:nth-child(even) {background: #CCC}\n");
		sb.append("tr:nth-child(odd) {background: #FFF}\n");
		sb.append("</style>\n");
		sb.append("</head>\n");
		sb.append("<body>\n");
		sb.append("<h3>Secure Identity Tokens with Disabled or Removed Owners</h3><br>");
		sb.append("<table>");
		sb.append(
				"<thead><tr><th>ID</th><th>Actor Name</th><th>Owner</th><th>Reason</th><th>Destroyed</th></tr></thead>");
		sb.append("<tbody>");
		return sb;
	}

	private static void endEmailMessage(StringBuilder sb, int count) {
		sb.append("<tbody></table>");
		sb.append("<ul><li><b>Count:</b>").append(count).append("</li></ul>");
		sb.append("</body></html>");
	}

	private static void sendEmails(ServiceExecutor executor, Collection<String> emails, int nbInvalidTokens,
			String message) throws Throwable {
		String serverUUID = executor.execute("server.uuid").value("uuid");
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String email : emails) {
			dm.add("to", email);
		}
		dm.add("subject", "[" + serverUUID + "] Found " + nbInvalidTokens
				+ " secure identity tokens with removed/disabled owners");
		dm.add("body", new String[] { "type", "text/html" }, message);
		executor.execute("mail.send", dm.root());
	}

}
