package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;

public class SvcUserDisableSetAll extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.disable.set.all";

	private Interface _defn;

	public SvcUserDisableSetAll() {
		_defn = new Interface();
		_defn.add(new Interface.Element("domain",  StringType.DEFAULT, "The local authentication domain of interest.", 1, 1));
		_defn.add(new Interface.Element("date",  DateType.DEFAULT, "The date to disable the account on (will replace any existing)", 1, 1));
		_defn.add(new Interface.Element("report-only", BooleanType.DEFAULT, "Just generate a report (default true), don't actually set meta-data.", 0, 1));
		_defn.add(new Interface.Element("disabled-only", BooleanType.DEFAULT, "If true, only disabled accounts are considered.  By default (false), only enabled accounts are considered.", 0, 1));
		_defn.add(new Interface.Element("exclude", StringType.DEFAULT, "Exclude this user.", 0, Integer.MAX_VALUE));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Iterates through all enabled user accounts in the given local domain and for any that have no disable meta-data, either generates a report or sets meta-data specifying the date (specified in arguments) when the account should be disabled.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String domain = args.value("domain");
		Date disableDate = args.dateValue("date");
		Boolean reportOnly = args.booleanValue("report-only", true);
		Boolean disabledOnly = args.booleanValue("disabled-only", false);
		Collection<String> excludeUsers = args.values("exclude");

		// CHeck is not LDAP
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element r = executor().execute("authentication.domain.describe", dm.root());
		String protocol = r.value("domain/@protocol");
		if (protocol!=null) {
			w.add("protocol", protocol);
			if (protocol.equals("ldap")) {
				throw new Exception ("You cannot disable LDAP account users.");
			}
		}

		// Proceed and find all users in the domain
		dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("size", "infinity");
		r = executor().execute("user.describe", dm.root());
		if (r==null) {
			return;
		}

		// Check each user
		Collection<XmlDoc.Element> users = r.elements("user");
		for (XmlDoc.Element user : users) {
			checkUser (executor(), excludeUsers, user, reportOnly, disabledOnly, 
					disableDate, w);
		}


	}

	private void checkUser (ServiceExecutor executor, Collection<String> excludeUsers,
			XmlDoc.Element user, Boolean reportOnly, 
			Boolean disabledOnly, Date disableDate, XmlWriter w) throws Throwable {
		Boolean isEnabled = user.booleanValue("@enabled", true);
		String domain = user.value("@domain");
		String userName = user.value("@user");
		if (excludeUsers!=null && excludeUsers.contains(userName)) {
			return ;
		}
		//
		Date currentDisableDate = user.dateValue("asset/meta/unimelb-admin:user-account/disable");
		if (currentDisableDate==null) {
			if ( (isEnabled&&!disabledOnly) || (!isEnabled&&disabledOnly)) {
				if (reportOnly) {
					String disableDateStr = DateUtil.formatDate(disableDate, false, false);
					w.add("user", new String[] {"disable-date", disableDateStr}, domain+":"+userName);
				} else {
					SvcUserDisableSet.setDisable(executor, null, domain, userName, disableDate, false, w);
				}
			}
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}