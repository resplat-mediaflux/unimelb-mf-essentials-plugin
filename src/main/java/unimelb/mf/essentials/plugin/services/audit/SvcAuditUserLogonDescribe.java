package unimelb.mf.essentials.plugin.services.audit;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Date;
import java.util.List;

public class SvcAuditUserLogonDescribe extends PluginService {

    public static final String SERVICE_NAME = "unimelb.audit.user.logon.describe";

    public static final String EVENT_TYPE = "user.logon";

    public static final int STEP_SIZE = 1000;

    private final Interface _defn;

    public SvcAuditUserLogonDescribe() {
        _defn = new Interface();
        _defn.add(new Interface.Element("authority", StringType.DEFAULT,
                "The identity of the authority/repository where the user identity originates. If unspecified, then refers to a user in this repository.",
                0, 1));
        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain name",
                1, 1));
        _defn.add(new Interface.Element("user", StringType.DEFAULT, "The username", 1,
                1));

        Interface.Element count = new Interface.Element("count", XmlDocType.DEFAULT,
                "Count user logons within the specified time range.", 0, 10);
        count.add(new Interface.Element("from", DateType.DEFAULT, "Start date/time inclusive.",
                0, 1));
        count.add(new Interface.Element("to", DateType.DEFAULT, "End date/time exclusive.",
                0, 1));
        _defn.add(count);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Analyse user activities recorded in audit log.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String authority = args.value("authority");
        String domain = args.value("domain");
        String username = args.value("user");
        List<XmlDoc.Element> timeFrames = args.elements("count");

        String actorName = authority == null ? String.format("%s:%s", domain, username) :
                String.format("%s:%s:%s", authority, domain, username);

        PluginTask.checkIfThreadTaskAborted();
        Long lastLogonMillis = getLastLogonTime(executor(), actorName);
        String lastLongTime = lastLogonMillis == null ? null : DateTime.string(new Date(lastLogonMillis));
        if (lastLogonMillis != null) {
            w.add("last-logon", new String[]{"millisec", Long.toString(lastLogonMillis)}, lastLongTime);
        }

        PluginTask.checkIfThreadTaskAborted();
        Long firstLogonMillis = getFirstLogonTime(executor(), actorName);
        String firstLongTime = firstLogonMillis == null ? null : DateTime.string(new Date(firstLogonMillis));
        if (firstLogonMillis != null) {
            w.add("first-logon", new String[]{"millisec", Long.toString(firstLogonMillis)}, firstLongTime);
        }

        if (timeFrames != null) {
            for (XmlDoc.Element timeFrame : timeFrames) {
                String from = timeFrame.value("from");
                Date fromTime = timeFrame.dateValue("from", null);
                String to = timeFrame.value("to");
                Date toTime = timeFrame.dateValue("to", null);
                if (from != null || to != null) {
                    PluginTask.checkIfThreadTaskAborted();
                    long count = countUserLogons(executor(), actorName, from, to);
                    w.add("count", new String[]{"from", fromTime == null ? null : DateTime.string(fromTime), "to", toTime == null ? null : DateTime.string(toTime)}, count);
                }
            }
        }

        PluginTask.checkIfThreadTaskAborted();
        long total = countUserLogons(executor(), actorName, null, null);
        w.add("total", total);
    }


    public static Long getFirstLogonTime(ServiceExecutor executor, String actorName) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("actor", actorName);
        dm.add("type", EVENT_TYPE);
        dm.add("size", 1);
        PluginTask.checkIfThreadTaskAborted();
        return executor.execute("audit.query", dm.root()).longValue("event/time/@millisec", null);
    }

    public static Long getLastLogonTime(ServiceExecutor executor, String actorName) throws Throwable {
        return getLastLogonTime(executor, actorName, null, null);
    }

    public static Long getLastLogonTime(ServiceExecutor executor, String actorName, Date from, Date to) throws Throwable {
        XmlDoc.Element lastEvent = getLastEvent(executor, actorName, EVENT_TYPE, from == null ? null : DateTime.string(from), to == null ? null : DateTime.string(to));
        return lastEvent == null ? null : lastEvent.longValue("time/@millisec", null);
    }

    public static XmlDoc.Element getLastEvent(ServiceExecutor executor, String actorName, String eventType,
                                              String from, String to) throws Throwable {
        boolean complete = false;
        long idx = 1;
        XmlDoc.Element last = null;
        while (!complete) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("actor", actorName);
            dm.add("type", eventType);
            if (from != null) {
                dm.add("from", from);
            }
            if (to != null) {
                dm.add("to", to);
            }
            dm.add("size", STEP_SIZE);
            dm.add("idx", idx);
            PluginTask.checkIfThreadTaskAborted();
            XmlDoc.Element re = executor.execute("audit.query", dm.root());
            idx += STEP_SIZE;
            List<XmlDoc.Element> ees = re.elements("event");
            if (ees == null || ees.isEmpty()) {
                complete = true;
            } else {
                if (ees.size() < STEP_SIZE) {
                    complete = true;
                }
                last = ees.get(ees.size() - 1);
            }
        }
        return last;
    }

    public static long countUserLogons(ServiceExecutor executor, String actorName, String from, String to) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("actor", actorName);
        dm.add("type", EVENT_TYPE);
        if (from != null) {
            dm.add("from", from);
        }
        if (to != null) {
            dm.add("to", to);
        }

        PluginTask.checkIfThreadTaskAborted();
        return executor.execute("audit.count", dm.root()).longValue("count", 0);
    }

}
