package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.services.SvcUserSearch.UserEntry;

import java.util.*;

public class SvcUserSearchLocal extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.search.local";

	private Interface _defn;

	public SvcUserSearchLocal() {
		_defn = new Interface();
		SvcUserSearch.addArgs(_defn);
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Search for local user.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String name = args.value("name");
		String email = args.value("email");
		String domain = SvcUserSearch.resolveDomainName(executor(), args.value("domain"));
		Collection<String> excludeDomains = args.values("exclude-domain");
		boolean excludeDisabled = args.booleanValue("exclude-disabled", true);
		boolean permissions = args.booleanValue("permissions", true);
		int size = args.intValue("size", SvcUserSearch.DEFAULT_RESULT_SIZE);

		List<SvcUserSearch.UserEntry> users = new ArrayList<SvcUserSearch.UserEntry>();
		searchLocalUsers(executor(), domain, excludeDomains, name, email, excludeDisabled, permissions, users, size);

		SvcUserSearch.outputUsers(users, w);

	}

	static void searchLocalUsers(ServiceExecutor executor, String domain, Collection<String> excludeDomains,
			String name, String email, boolean excludeDisabled, boolean permissions, List<SvcUserSearch.UserEntry> users, int size)
			throws Throwable {
		if (name == null && email == null) {
			throw new IllegalArgumentException("Missing name or email argument.");
		}

		Set<String> domains = new LinkedHashSet<String>();
		if (domain != null) {
			if (!isLocalOrSamlDomain(executor, domain)) {
				throw new IllegalArgumentException("Domain: " + domain + " is not a local domain.");
			}
			if (excludeDomains != null && excludeDomains.contains(domain)) {
				throw new IllegalArgumentException("The specified domain: " + domain + " is also excluded.");
			}
			domains.add(domain);
		} else {
			addLocalOrSamlDomains(executor, domains, excludeDomains);
		}
		if (domains.isEmpty()) {
			throw new Exception("No local or plugin saml authentication domains are found.");
		}
		searchLocalUsers(executor, domains, name, email, excludeDisabled, permissions, users, size);
	}

	static void searchLocalUsers(ServiceExecutor executor, Collection<String> localDomains, String name, String email,
			boolean excludeDisabled, boolean permissions, List<SvcUserSearch.UserEntry> users, int size) throws Throwable {
		for (String domain : localDomains) {
			searchLocalUsers(executor, domain, name, email, excludeDisabled, permissions, users, size);
			if (users.size() >= size) {
				break;
			}
		}
	}

	private static void searchLocalUsers(ServiceExecutor executor, String domain, String name, String email,
			boolean excludeDisabled, boolean permissions, List<SvcUserSearch.UserEntry> users, int size)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("domain", domain);
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element re = executor.execute("user.describe", dm.root());
		List<XmlDoc.Element> ues = re.elements("user");
		if (ues != null) {
			for (XmlDoc.Element ue : ues) {
				XmlDoc.Element actor = null;
				if (permissions) {
					actor = executor.execute("actor.describe", "<args><type>user</type><name>" + ue.value("@domain")
							+ ":" + ue.value("@user") + "</name></args>", null, null).element("actor");
				}
				UserEntry u = new UserEntry(ue, null, actor == null ? null : actor.elements());
				if (u.enabled || !excludeDisabled) {
					if (name != null) {
						String n1 = u.name == null ? "" : u.name.trim().toLowerCase();
						String n2 = u.user.toLowerCase();
						String n = name.trim().toLowerCase();
						if (n1.contains(n) || n2.contains(n)) {
							users.add(u);
						}
					}
					if (email != null) {
						String e1 = u.email == null ? "" : u.email.trim().toLowerCase();
						String e = email.trim().toLowerCase();
						if (e1.contains(e)) {
							users.add(u);
						}
					}
				}
				if (users.size() >= size) {
					break;
				}
			}
		}
	}

	private static boolean isLocalOrSamlDomain(ServiceExecutor executor, String domain) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element de = executor.execute("authentication.domain.describe", dm.root()).element("domain");
		String type = de.value("@type");
		String protocol = de.value("@protocol");
		return "local".equalsIgnoreCase(type) || ("plugin".equalsIgnoreCase(type) && "saml".equalsIgnoreCase(protocol));
	}

	private static void addLocalOrSamlDomains(ServiceExecutor executor, Set<String> domains,
			Collection<String> excludeDomains) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element re = executor.execute("authentication.domain.list");
		List<XmlDoc.Element> des = re.elements("domain");
		if (des != null) {
			for (XmlDoc.Element de : des) {
				String domain = de.value();
				String type = de.value("@type");
				String protocol = de.value("@protocol");
				if (excludeDomains == null || !excludeDomains.contains(domain)) {
					if ("local".equalsIgnoreCase(type)) {
						domains.add(domain);
					} else if ("plugin".equalsIgnoreCase(type)) {
						if ("saml".equalsIgnoreCase(protocol)) {
							domains.add(domain);
						}
					}
				}
			}
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}

}
