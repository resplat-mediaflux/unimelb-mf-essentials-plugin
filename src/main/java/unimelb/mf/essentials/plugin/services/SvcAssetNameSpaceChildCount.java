/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceChildCount extends PluginService {


	private Interface _defn;
	private long _n = 0;

	public SvcAssetNameSpaceChildCount() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "Recurse down the whole namespace tree (the listing is simplified in that the specific ACL is not shown, just that it has one). Default is false (just inspect immediate children namespaces).", 0, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.child.count";
	}

	public String description() {
		return "Service to count the number of children namespaces. Can recurse down the full namespace tree.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		Boolean recurse = args.booleanValue("recurse", false);
		_n = 0;
		count (executor(), namespace, recurse, w);
		w.add("count", _n);
	}


	private void count (ServiceExecutor executor, String nameSpace, 
			Boolean recurse, XmlWriter w) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Iterate over children namespaces
		Collection<String> nss = NameSpaceUtil.listNameSpaces(executor, nameSpace, true);
		if (nss==null) {
			return;
		}
		for (String ns : nss) {
			_n++;
			PluginTask.checkIfThreadTaskAborted();
			
			// Descend into child namespace
			if (recurse) {
				count (executor, ns, true, w);
			}
		}
	}
}
