package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcLdapUserFind extends PluginService {

    public static final String SERVICE_NAME = "unimelb.ldap.user.find";

    private Interface _defn;

    public SvcLdapUserFind() {
        _defn = new Interface();
        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "The LDAP authentication domain (default 'unimelb').", 0, 1));
        _defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "The email address of the user.", 0, 1));
        Interface.Element name = new Interface.Element("name", StringType.DEFAULT, "The name of the user.", 0, 2);
        name.add(new Interface.Attribute("type", new EnumType(new String[] { "full", "first", "last" }),
                "Type of the name. Defaults to full", 1));
        _defn.add(name);

    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find user from LDAP authentication domain by email or name.";
    }

    @Override
    public void execute(Element args, Inputs i, Outputs o, XmlWriter w) throws Throwable {

        String domain = args.stringValue("domain", "unimelb");
        XmlDoc.Element de = executor()
                .execute("authentication.domain.describe", "<args><domain>" + domain + "</domain></args>", null, null)
                .element("domain");
        if (!"ldap".equalsIgnoreCase(de.value("@protocol"))) {
            throw new IllegalArgumentException("Domain: '" + domain + "' is not LDAP domain.");
        }
        String provider = de.value("provider");
        String path = de.value("userp");
        String uidAttrName = de.value("uid");

        boolean hasNameElement = args.elementExists("name");
        if (!args.elementExists("email") && !hasNameElement) {
            throw new IllegalArgumentException("Missing email or/and name arguments.");
        }
        if (args.count("name[@type='full']") > 1) {
            throw new IllegalArgumentException("Expects only one name[@type='full']. Found multiple.");
        }
        if (args.count("name[@type='first']") > 1) {
            throw new IllegalArgumentException("Expects only one name[@type='first']. Found multiple.");
        }
        if (args.count("name[@type='last']") > 1) {
            throw new IllegalArgumentException("Expects only one name[@type='last']. Found multiple.");
        }

        Set<String> filters = new LinkedHashSet<String>();
        String email = args.value("email");
        if (email != null) {
            String user = email.split("@")[0];
            filters.add("(|(" + uidAttrName + "=" + user + ")(mail=" + email + ")(displayName=" + user.replace('.', ' ')
                    + "))");
        }

        String fullName = args.value("name[@type='full']");
        String firstName = args.value("name[@type='first']");
        String lastName = args.value("name[@type='last']");
        if (fullName != null) {
            if (firstName != null) {
                throw new IllegalArgumentException(
                        "Unexpected name[@type='first'] because name[@type='full'] is specified.");
            }
            if (lastName != null) {
                throw new IllegalArgumentException(
                        "Unexpected name[@type='last'] because name[@type='full'] is specified.");
            }
            filters.add("displayName=" + fullName);
        } else {
            if (firstName != null) {
                filters.add("givenName=" + firstName);
            } else if (lastName != null) {
                filters.add("sn=" + lastName);
            }
        }

        PluginTask.checkIfThreadTaskAborted();
        Collection<String> users = ldapSearch(executor(), provider, path, filters, uidAttrName);
        int count = users == null ? 0 : users.size();
        if (count > 0) {
            for (String user : users) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element ue = describeUser(executor(), domain, user);
                w.add("user", new String[] { "domain", domain, "name", ue.value("name"), "email", ue.value("e-mail") },
                        user);
            }
        }
        w.add("found", count);
    }

    private static XmlDoc.Element describeUser(ServiceExecutor executor, String domain, String user) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("domain", domain);
        dm.add("user", user);
        return executor.execute("user.describe", dm.root()).element("user");
    }

    private static Collection<String> ldapSearch(ServiceExecutor executor, String provider, String path,
            Collection<String> filters, String uidAttrName) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("provider", provider);
        dm.add("path", path);
        if (filters.size() == 1) {
            dm.add("filter", filters.iterator().next());
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("(&");
            for (String f : filters) {
                sb.append("(").append(f).append(")");
            }
            sb.append(")");
        }
        return executor.execute("ldap.search", dm.root()).values("entry/" + uidAttrName);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
