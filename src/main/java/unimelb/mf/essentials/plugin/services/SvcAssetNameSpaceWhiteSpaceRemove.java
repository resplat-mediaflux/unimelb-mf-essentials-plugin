package unimelb.mf.essentials.plugin.services;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.utils.DateUtil;

public class SvcAssetNameSpaceWhiteSpaceRemove extends PluginService {


	private class NSC {
		public Boolean fixed_ = null;  // DId this namespace have its name fixed
		public String oldPath_ = null; // What was the old namespace (full) path
		public String newName_ = null; // What is the new (child part) name of the namesapce
		public Integer nAssetsFixed_ = null; // How many assets were fixed in this namespace
		NSC(Boolean fixed, String oldPath, String newName, Integer nAssetsFixed) {
			fixed_ = fixed;
			oldPath_ = oldPath;
			newName_ = newName;
			nAssetsFixed_ = nAssetsFixed;
		}
	}
	private Interface _defn;

	public SvcAssetNameSpaceWhiteSpaceRemove() {
		_defn = new Interface();
		//
		Interface.Element me = new Interface.Element("namespace", StringType.DEFAULT, "The parent namespace to start at (is subject to renaming).", 1, 1);
		_defn.add(me);
		me = new Interface.Element("include-assets", BooleanType.DEFAULT, "Include assets as well (default false). Brute force approach of iterating (no cursor iteration) through all assets per namespace. Could be enhanced to use a pipe if needed.", 0, 1);
		_defn.add(me);
		me = new Interface.Element("leading", BooleanType.DEFAULT, "Trim leading as well as trailing white space (default true).", 0, 1);
		_defn.add(me);
		me = new Interface.Element("recurse", BooleanType.DEFAULT, "Recurse down the namespace tree (default true).", 0, 1);
		_defn.add(me);
	}

	public String name() {
		return "unimelb.asset.namespace.whitespace.surrounding.remove";
	}

	public String description() {
		return "This service (optionally) recursively removes trailing (and optionally leading as well) white spaces in namespace names and optionally asset names in namespaces.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse arguments
		String from = args.value("namespace");
		String first = from.substring(0,1);
		if (!first.equals("/")) {
			from = "/" + from;
		}
		Boolean doAssets = args.booleanValue("include-assets", false);
		Boolean doLeading = args.booleanValue("leading", true);
		Boolean recurse = args.booleanValue("recurse", true);

		// Make a flat list of modified namespaces for reporting to the caller
		ArrayList<String> fixedNamespaces = new ArrayList<String>();

		// Fix this namespace and assets if needed
		NSC nsc = fixNamespace (executor(), doAssets, doLeading, from, getName(executor(), from));
		if (nsc.fixed_) {
			fixedNamespaces.add(nsc.oldPath_);
		}
		w.push("namespace", new String[] {"path", from, "fixed", nsc.fixed_.toString(), "nassets", ""+nsc.nAssetsFixed_});


		// Descend and recurse
		if (recurse) {
			fixChildrenNamespaces (executor(), doAssets, doLeading, fixedNamespaces, from, w);
		}
		w.pop();

		// List all fixed namespaces. The parent part of the path
		// may already have been fixed (white space removed). The
		// child part will be accurate.
		w.push("namespaces");
		for (String fixedNamespace : fixedNamespaces) {
			w.add("namespace", fixedNamespace);
		}
		w.pop();

	}

	private String getParentPath (String namespace) throws Throwable {
		int idx = namespace.lastIndexOf("/");
		if (idx==0) {
			// Special case where parent is "/"
			return "/";
		}
		return namespace.substring(0, idx);
	}


	private String getName (ServiceExecutor executor, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		XmlDoc.Element r = executor.execute("asset.namespace.describe", dm.root());
		return r.value("namespace/name");
	}

	/**
	 * 
	 * @param executor
	 * @param doAssets
	 * @param doLeading
	 * @param path  full path of namespace
	 * @param name  just the (child) name part of the namespace 
	 * @return
	 * @throws Throwable
	 */
	private NSC fixNamespace (ServiceExecutor executor, Boolean doAssets, Boolean doLeading, String path, String name) throws Throwable {
		// Don't try to handle special case of "/"
		String name2 = name;
		Boolean renamedNS = false;
		if (!path.equals("/")) {
			// See if has trailing white space
			name2 = trim(name, doLeading);
			if (!name2.equals(name)) {

				// Rename namespace	
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("namespace", path);
				dm.add("name", name2);
				executor.execute("asset.namespace.rename", dm.root());	
				renamedNS = true;
			}
		}
		
		// Update the path if the namespace was renamed
		String newPath = path;
		if (renamedNS) {
			// UPdate parent path
			String parent = getParentPath(path);
			newPath = parent + "/" + name2;
		
		    // Set some meta-data on the namespace for provenance
			Date date = new Date();
			String dates = DateUtil.formatDate(date, true, false);
			//
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", newPath);
			dm.push("asset-meta");
			dm.push("mf-note");
			dm.add("note", "Namespace renamed from '" + path + "' to '" + newPath + "' at " + dates);
			dm.pop();
			dm.pop();
			executor.execute("asset.namespace.asset.meta.add", dm.root());
		}


		// Rename any assets as required
		Integer nAssets = 0;
		if (doAssets) {
			nAssets = fixAssets (executor, doLeading, newPath);
		}

		//
		return new NSC(renamedNS, path, name2, nAssets);
	}


	private Integer fixAssets (ServiceExecutor executor, Boolean doLeading, String namespace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("where", "namespace="+namespace);
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) {
			return 0;
		}
		Collection<String> ids = r.values("id");
		if (ids==null) return 0;
		//
		Integer n = 0;
		for (String id : ids) {
			XmlDoc.Element meta = AssetUtils.getAsset(executor, null, null, id);
			String name = meta.value("asset/name");
			if (name!=null) {
				String name2 = trim(name, doLeading);
				if (!name2.equals(name)) {
					renameAsset (executor, id, name, name2);
					n++;
				}
			}
		}
		return n;
	}

	private void renameAsset (ServiceExecutor executor, String id, String oldName, String newName) throws Throwable {
		Date date = new Date();
		String dates = DateUtil.formatDate(date, true, false);
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		dm.add("name", newName);
		dm.push("meta");
		dm.push("mf-note");
		dm.add("note", "Asset renamed from '" + oldName + "' to '" + newName + "' at " + dates);
		dm.pop();
		dm.pop();
		executor.execute("asset.set", dm.root());
	}

	private void fixChildrenNamespaces (ServiceExecutor executor, Boolean doAssets, Boolean doLeading, List<String> fixedNamespaces,
			String parent, XmlWriter w) throws Throwable {

		PluginTask.checkIfThreadTaskAborted();

		// Find the child namespaces	
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", parent);
		XmlDoc.Element r = executor().execute("asset.namespace.list", dm.root());
		if (r==null) return;

		// There are children so continue
		XmlDoc.Element pathEl = r.element("namespace");
		String path = pathEl.value("@path");        // The parent path of the namespace
		Collection<String> nss = pathEl.values("namespace");
		if (nss==null) {
			return;
		}
		if (nss.size()==0) {
			return;
		}

		// Iterate through children namespaces
		for (String ns : nss) {
			PluginTask.checkIfThreadTaskAborted();

			String absPath = path + "/" + ns;
			// Fix this namespace name if needed
			NSC nsc = fixNamespace (executor, doAssets, doLeading, absPath, ns);
			w.push("namespace", new String[] {"path", absPath, "fixed", nsc.fixed_.toString(), "nassets", ""+nsc.nAssetsFixed_});

			// Correct abspath of this namespace as it may have changed
			if (nsc.fixed_) {
				absPath = path + "/" + nsc.newName_;
				fixedNamespaces.add(nsc.oldPath_);
			}
			// Descend into child
			fixChildrenNamespaces (executor(), doAssets, doLeading, fixedNamespaces, absPath, w);
			w.pop();
		}
		return;
	}

	private String trim (String str, Boolean doLeading) {
		if (doLeading) {
			return str.trim();
		} else {
			return str.replaceFirst("\\s++$", "");
		}
	}
}
