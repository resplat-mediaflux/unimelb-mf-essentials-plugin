package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcShoppingCartDescribe extends PluginService {

    public static final String SERVICE_NAME = "unimelb.shoppingcart.describe";

    private Interface _defn;

    public SvcShoppingCartDescribe() {
        _defn = new Interface();
        _defn.add(new Interface.Element("sid", LongType.POSITIVE_ONE,
                "The id of the shopping cart. If specified, owner and status filters will be ignored.", 0,
                Integer.MAX_VALUE));
        _defn.add(new Interface.Element("owner", StringType.DEFAULT, "The owner of the carts.", 0, 1));
        _defn.add(new Interface.Element("status",
                new EnumType(new String[] { "aborted", "assigned", "await processing", "data ready", "editable",
                        "error", "fulfilled", "processing", "rejected", "withdrawn" }),
                "The status of the carts.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Describe shopping carts.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        Collection<String> sids = args.values("sid");
        if (sids != null) {
            XmlDocMaker dm = new XmlDocMaker("args");
            for (String sid : sids) {
                dm.add("sid", sid);
            }
            XmlDoc.Element re = executor().execute("shopping.cart.describe", dm.root());
            w.add(re, false);
            w.add("count", sids.size());
        } else {
            String owner = args.value("owner");
            String status = args.value("status");
            boolean isSystemManager = "system:manager"
                    .equals(executor().execute("actor.self.describe").value("actor/@name"));

            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("size", "infinity");
            dm.add("list-all", isSystemManager);
            XmlDoc.Element re = executor().execute("shopping.cart.describe", dm.root());
            List<XmlDoc.Element> ces = re.elements("cart");
            long count = 0L;
            if (ces != null) {
                for (XmlDoc.Element ce : ces) {

                    String cartOwner = ce.value("owner");
                    if (owner != null && !owner.equals(cartOwner)) {
                        continue;
                    }
                    String cartStatus = ce.value("status");
                    if (status != null && !status.equals(cartStatus)) {
                        continue;
                    }
                    w.add(ce);
                    count++;
                }
            }
            w.add("count", count);
        }
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
