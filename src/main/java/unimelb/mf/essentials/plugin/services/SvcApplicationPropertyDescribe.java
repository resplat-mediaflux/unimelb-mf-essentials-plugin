package unimelb.mf.essentials.plugin.services;

import java.io.File;
import java.io.IOException;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;
import unimelb.utils.XmlUtils;

public class SvcApplicationPropertyDescribe extends PluginService {

	public static final String SERVICE_NAME = "unimelb.application.property.describe";

	private Interface _defn;

	public SvcApplicationPropertyDescribe() {

		_defn = new Interface();
		_defn.add(new Interface.Element("app", StringType.DEFAULT, "The application, if any. If not specified, describes all.", 0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, directory on the server to save the output as an XML file. The file will be named application-properties-YYYYMMDD-hhmmss.xml.  Of the form file:<path>", 0, 1));
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public String description() {
		return "Describes application properties and optional saves to a server side XML file.";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse
		String app = args.value("app");
		// Server-side URL
		String url = args.value("url");
		String urlPath = null;
		if (url!=null) {
		    if (!url.substring(0,5).equals("file:")) {
		    	throw new Exception ("url must be of the form 'file:<path>'");
		    }
			urlPath = url.substring(5);
			File outDir = new File(urlPath);
			if (!outDir.exists()) {
				throw new IOException ("Path '" + urlPath + "' does not exist");
			}
			if (!outDir.isDirectory()) {
				throw new IOException ("Path '" + urlPath + "' is not a directory");
			}
		}

		// Iterate through application properties
		XmlDocMaker dm = new XmlDocMaker("args");
		if (app!=null) {
			dm.add("app", app);
		}
		XmlDoc.Element r = executor().execute("application.property.describe", dm.root());
		if (r==null) {
			return;
		}

		// Outputs
		w.addAll(r.elements());
		
		
		// Write server side XML file
		if (url != null) {

			// Users
			String today = DateUtil.todaysTime(2);
			{
				String fileName = urlPath + "/application-properties-" + today + ".xml";
				File file = new File(fileName);
			    XmlUtils.writeServerSideXMLFile(r, file);
				w.add("file", fileName);
			}
		}	
	}

}
