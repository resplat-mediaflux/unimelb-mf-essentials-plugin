package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import arc.mf.plugin.*;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceMetaDataRemove extends PluginService {

	private Interface _defn;

	public SvcAssetNameSpaceMetaDataRemove() {
		_defn = new Interface();
		Interface.Element me = new Interface.Element("namespace", StringType.DEFAULT, "The namespace.", 1, 1);
		_defn.add(me);
		//
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "The Document Type name.", 1, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.metadata.remove";
	}

	public String description() {
		return "Specialised service to remove namespace meta-data documents specified by their type. All fragments of the given type are removed.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String nameSpace = args.value("namespace");
		String type = args.value("type");

		// Get namespace meta-data
		XmlDoc.Element nsMeta = NameSpaceUtil.describe(null, executor(), nameSpace);

		// Get the meta-data
		XmlDoc.Element meta = nsMeta.element("namespace/asset-meta");
		if(meta==null) {
			return;
		}

		// Get the documents
		Collection<XmlDoc.Element> docs = meta.elements(); 
		PluginTask.checkIfThreadTaskAborted();

		// Iterate through documents 
		if (docs!=null) {
			for (XmlDoc.Element doc : docs) {
				PluginTask.checkIfThreadTaskAborted();
				String qn = doc.qname();

				// If we are interested in this document fragment handle it
				if (type.equals(qn)) {
					String mid = doc.value("@id");
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("namespace", nameSpace);
					dm.add("mid", mid);
					executor().execute(null, "asset.namespace.asset.meta.remove", dm.root());
					w.add("removed", new String[] {"id", mid}, type);
				}
			}
		}
	}
}
