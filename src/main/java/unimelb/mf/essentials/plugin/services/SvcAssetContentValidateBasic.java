package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentValidateBasic extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.validate.basic";

	private Interface _defn;

	public SvcAssetContentValidateBasic() {
		_defn = new Interface();

		_defn.add(new Interface.Element("all-versions", BooleanType.DEFAULT,
				"If true, then validate the content for all versions (over-riding the -version attribute of :id). If false, then only validate the selected version. Defaults to false.",
				0,1));
		_defn.add(new Interface.Element("copy", IntegerType.DEFAULT,
				"The copy to check. If unspecified, then all copies are checked.", 0, Integer.MAX_VALUE));
		//
		Interface.Element id = new Interface.Element("id", AssetType.DEFAULT, "The path to the asset.", 1, Integer.MAX_VALUE);
		id.add(new Interface.Attribute("label", StringType.DEFAULT,
				"Selects the version given by the label. The overrides the 'version' argument.\nrestriction (string)",
				0));
		id.add(new Interface.Attribute("version", IntegerType.POSITIVE,
				"Version of the asset. A value of zero means the latest version. Defaults to latest.",
				0));
		_defn.add(id);
		// recover
		Interface.Element recover = new Interface.Element("recover", BooleanType.DEFAULT,
				"If invalid data is found, should it be recovered from valid copies. Defaults to false.", 0, 1);
		recover.add(new Interface.Attribute("if-no-copy",
				new EnumType(new String[] { "exception", "status", "notify" }),
				"The behaviour if there is no recoverable copy available. Defaults to 'exception' - throw an exception. 'status' will return the recovery status, and 'notify' will generate an error event on the notification object type 'asset' with name 'validate'",
				0));
		recover.add(new Interface.Attribute("notify-on-recover", BooleanType.DEFAULT,
				"If true and recovery occurred, generate warning notification object type 'asset' with name 'validate'. Defaults to true.",
				0));
		recover.add(new Interface.Attribute("pdist", IntegerType.POSITIVE,
				"If specified, and recovery cannot be performed from a local copy, then attempt to find a replica in any reachable federation (up to the specified peer distance 'pdist') and recover from there.",
				0));
		_defn.add(recover);
		// store
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"If specified, validate only content in this specified store(s).", 0, Integer.MAX_VALUE));	
		_defn.add(new Interface.Element("show-error-only", BooleanType.DEFAULT, "Show the results only if the asset has an error (non 'OK') state (default false).  This can be very useful when piping a query into this service to reduce possible output.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Thin wrapper for asset.content.validate adding the option :show-error-only to filter out asset/versions which have no issue.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		Boolean allVersions = args.booleanValue("all-versions", false);
		Collection<XmlDoc.Element> copy = args.elements("copy");
		Collection<XmlDoc.Element> ids = args.elements("id");
		XmlDoc.Element recover = args.element("recover");
		Collection<XmlDoc.Element> store = args.elements("store");
		Boolean showErrorOnly = args.booleanValue("show-error-only", false);
		// 
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("all-versions", allVersions);
		if (copy!=null) {
			dm.addAll(copy);
		}
		if (recover!=null) {
			dm.add(recover);
		}
		if (store!=null) {
			dm.addAll(store);
		}
		dm.addAll(ids);
		XmlDoc.Element r = executor().execute("asset.content.validate", dm.root());
		if (r==null) {
			return;
		}
		// Parse results
		PluginTask.checkIfThreadTaskAborted();
		Collection<XmlDoc.Element> assets = r.elements("asset");
		if (assets!=null) {
			for (XmlDoc.Element asset : assets) {
				PluginTask.checkIfThreadTaskAborted();
				if (asset!=null) {
					if (showErrorOnly) {
						String status = asset.value("status");
						if (status!=null && !status.equals("OK")) {
							w.add(asset);
						}
					} else {
						w.add(asset);
					}
				}
			}
		}
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
