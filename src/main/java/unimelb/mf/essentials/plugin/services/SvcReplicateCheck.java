package unimelb.mf.essentials.plugin.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.PeerUtil;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.mf.essentials.plugin.util.ServerDetails;
import unimelb.utils.DateUtil;
import unimelb.utils.FileSizeUtils;

public class SvcReplicateCheck extends PluginService {


	private Interface _defn;
	private Long totalSizeToReplicate_ = 0L;
	private int packSize_ = 1000;
	private Long timeToReplicateMilliSec_ = 0L;
	private Long totalChecked_ = 0L;
	private Long ITERATOR_SIZE = 1000L;

	private static final int DEFAULT_RETRY_TIMES = 120;
	private static final int DEFAULT_RETRY_INTERVAL = 1000; // 1 second
	private static final String SERVICE_NAME = "unimelb.replicate.check";


	public SvcReplicateCheck() {
		_defn = new Interface();
		Interface.Element ie = new Interface.Element("email", StringType.DEFAULT, "Email address to send report to. Defaults to no email.", 0, 1);
		ie.add(new Interface.Attribute("subject", StringType.DEFAULT,
				"Subject of message. If not specified, a default is used.", 0));
		_defn.add(ie);
		_defn.add(new Interface.Element("peer",StringType.DEFAULT, "Name of peer that objects have been replicated to.", 1, 1));
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to restrict the selected assets on the local host. If unset, all assets are considered. If there is more than one where clause, then the second and subsequent clauses are evaluated (linearly) against the result set of the first where clause (a post filter).", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("collection",AssetType.DEFAULT, 
				"If specified, then use this to specify selection of a collection asset (and all its children).  May be quicker than using a where clause. Can be used with a where clause.", 0, 1));		
		_defn.add(new Interface.Element("clevel",IntegerType.DEFAULT, "Level of compression. 0 is uncompressed, 9 is the highest level. Defaults to 0.", 0, 1));
		_defn.add(new Interface.Element("send-size",IntegerType.DEFAULT, "When all-in-one=false, when actually replicating assets, pack this many assets into the piped replication at a time. Defaults to " + packSize_, 0, 1));
		//
		_defn.add(new Interface.Element("dst", StringType.DEFAULT, "Destination namespace path  on receiving peer for when replicating namespace trees. You can only set dst or dst-collection, not both.  Not used for send-method 'find-and-send-queue'.", 0, 1));
		_defn.add(new Interface.Element("dst-collection", AssetType.DEFAULT, "Destination collection asset on receiving peer for when replicating LZR collection trees. Can be an asset ID or of the form path=<path>. You can only set dst or dst-collection, not both.   Not used for send-method 'find-and-send-queue'.", 0, 1));
		_defn.add(new Interface.Element("send", BooleanType.DEFAULT, "Actually send (replicate) the data. The default is false, which just checks. If send is true, you must set :dst or :dst-collection", 0, 1));
		//		
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT, "Turn on or off the use of indexes in the query. Defaults to true.", 0, 1));
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT, "Write some stuff in the log. Default to false.", 0, 1));
		_defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT, "Include soft destroyed assets (so don't include soft destroy selection in the where predicate. Default to false.", 0, 1));
		_defn.add(new Interface.Element("list", BooleanType.DEFAULT, "List all the IDs of assets and their sizes to be replicated and why.  Default to false.", 0, 1));
		_defn.add(new Interface.Element("nb-threads", IntegerType.DEFAULT, "Number of threads to use when replicating assets. Defaults to 1. ", 0, 1));
		_defn.add(new Interface.Element("related", IntegerType.DEFAULT, "Specifies the number of levels of related assets (primary relationship) to be replicated.  Has no impact on the find part of the service, just the replicatioh. Defaults to 0. Specify infinity to traverse all relationships.", 0, 1));
		_defn.add(new Interface.Element("versions", StringType.DEFAULT, "Replicate all ('all' - default) versions, or just the last version matched by the query ('match'). For versions already replicated, this is a null op.", 0, 1));
		_defn.add(new Interface.Element("collection-assets", BooleanType.DEFAULT, "When true (Default is false), sets these arguments of asset.replicate.to (when replicating) :collection-assets/{describe-queries,include-dynamic-members,include-static-members, Binclude-subcollections} to true.", 0, 1));
		_defn.add(new Interface.Element("restore-mismatched-content", BooleanType.DEFAULT, "If set to true (default false) then further comparison is made to determine if existing content is invalid and needs to be recovered.  Only activate if you believe the replica is corrupt.", 0, 1));
		_defn.add(new Interface.Element("format",
				new EnumType(
						new String[] { "aarv2", "aar", "tar", "tar.gz", "zip" }),
				"The format of the container used to transmit the data. 'zip' is ZIP format, 'tar' is Tar format (.tar) when no compression, or G-Zipped Tar (.tar.gz) when clevel>0, 'aarv2' is the non-concurrent Arcitecta format, 'aar' is the latest Arcitecta archive format (.aar) with file size limit of 2^63 per file. The default is 'aarv2'.", 0, 1));		
		//
		_defn.add(new Interface.Element("send-method",
				new EnumType(
						new String[] { "find-and-send-direct", "find-and-send-queue", "all-in-one"}),
				"The method to actually replicate the data. 'find-and-send-direct' (default) means find assets to replicate, then replicate them subsequently (will not scale [exceed memory] if huge numbers are found). 'find-and-send-queue' means put the found assets in an asset processing queue (element :queue) which must be pre-configured to replicate them; the assets are added to the queue as they are found (but only id :dst or dst:collection is set of course) and so it scales,  'all-in-one' finds and replicates all in one multi-threaded service call.  Scales right up but less introspection is available.", 0, 1));		
		_defn.add(new Interface.Element("queue", StringType.DEFAULT, "If arg. 'send-method 'is 'find-and-send-queue' this is the name of the replication asset processing queue to which assets are added.", 0, 1));
		//
		_defn.add(new Interface.Element("catch-exceptions", BooleanType.DEFAULT, "When all-in-one=false, if true (default false), catch excpetions to the asset.replicate.to pipe and then continue. That service call sends send-size assets in one go. We don't know where it was in the pipe when it failed.  The output will present the number of exceptions and the service will continue to the next tranch.  Reporting may be confused when exceptions occur.", 0, 1));
	}
	public String name() {
		return SERVICE_NAME;
	}

	public String description() {
		return "Lists or replicates assets that haven't been replicated to the remote peer (primary algorithm is to look for the replica by asset id). Filters out assets in staging or that have content but no checksum. Assets can also be actually replicated rather than just listed by specifying the destination root namespace.   allow-move is true and overwrite is set to false";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Init
		Date date = new Date();
		String dateTime = date.toString();     // Just used to tag message in log file

		// Get inputs
		String collection = args.value("collection");
		Collection<String> wheres = args.values("where");
		String peer = args.value("peer");
		Integer packSize = args.intValue("send-size", packSize_);
		String  dst = args.value("dst");
		String  dstCA = args.value("dst-collection");
		if (dst!=null && dstCA!=null) {
			throw new Exception("You cannot set both dst and dst-collection");
		}
		Boolean send = args.booleanValue("send",  false);
		XmlDoc.Element email = args.element("email");
		//
		Boolean useIndexes = args.booleanValue("use-indexes", true);
		Boolean dbg = args.booleanValue("debug", false);
		Boolean list = args.booleanValue("list", false);
		Boolean includeDestroyed = args.booleanValue("include-destroyed", false);
		Integer related = args.intValue("related", 0);
		String versions = args.stringValue("versions", "all");
		if (!versions.equals("match") && !versions.equals("all")) {
			throw new Exception ("versions must be 'match' or 'all'");
		}
		Boolean collectionAssets = args.booleanValue("collection-assets", false);
		Integer nbThreads = args.intValue("nb-threads", 1);
		Boolean restoreMismatchedContent = args.booleanValue("restore-mismatched-content",  false);
		Integer clevel = args.intValue("clevel", 0);
		String format = args.stringValue("format", "aarv2");
		Boolean catchExceptions = args.booleanValue("catch-exceptions", false);
		String sendMethod = args.stringValue("send-method", "find-and-send-direct");
		String queueName = args.value("queue");
		if (send) {
			if (sendMethod.equals("find-and-send-queue") && queueName==null) {
				throw new Exception("You must specify a queue name with the find-and-send-queue' method");
			} else {
				if (dst==null && dstCA==null) {
					throw new Exception ("You must supply :dst or dst-collection to actually replicate data.");
				}
			}
		}

		// Find route to peer. Exception if can't reach and build in extra checks to make sure we are 
		// being very safe
		ServerRoute srDR = PeerUtil.findPeerRoute(executor(), peer);
		if (srDR==null) {
			throw new Exception("Failed to generated the ServerRoute for the remote host");
		}
		String uuidLocal = serverUUID(executor(), null);
		if (srDR.target().equals(uuidLocal)) {
			throw new Exception ("Remote peer UUID appears to be the same as the local host (" + uuidLocal + ") - cannot proceed");
		}

		// Replicate as needed
		Vector<String> assetIDs = new Vector<String>();

		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.add("uuid-local", uuidLocal);
		totalSizeToReplicate_ = 0L;
		totalChecked_ = 0L;
		PluginLog logger = null;
		if (dbg) {
			logger = PluginLog.log(SERVICE_NAME);
		}

		Integer ret[] = {0,0,0,0,0,0};
		if (send  && sendMethod.equals("all-in-one")) {
			if (dbg) {
				log(logger, dateTime, "   starting new find and send");
			}
			// New function all in one. Much simpler. There are no
			// scale limitations with this approach.

			// Can only use one where
			if (wheres!=null && wheres.size()>1) {
				throw new Exception("The new function can only take a single where predicate");
			}
			String nAssets = AssetUtils.count(executor(), collection,  wheres);
			if (dbg) {
				log(logger, dateTime, "   count = "+nAssets);
			}
			ret = findAndSend (executor(), dst, dstCA,  peer, collection, wheres, collectionAssets, versions, 
					nbThreads, clevel,  dbg, format, dateTime, includeDestroyed, logger);	
			dmOut.add("total-checked", nAssets);
		} else {
			// Old find and send approach.  The find is still useful
			// as it's the only way to list the assets to be replicated
			// and the reasons why.
			//
			// It's also the way to put found assets in the replication
			// asset processing queue instead of replicating directly.
			//

			// FInd assets with nbThreads and make a list or add to the queue.
			// The list holds only the asset id, so it would need to be very large
			// to cause memory issues.
			Boolean addToQueue = send && sendMethod.equals("find-and-send-queue");
			Long n = findWithPipedQuery (executor(), dateTime, nbThreads, collection,
					wheres, peer, assetIDs, useIndexes, dbg,  list, includeDestroyed, 
					addToQueue, queueName, logger, dmOut);

			// Total checked is every asset checked, whether we succeeding in
			// contacting the DR or not
			dmOut.add("total-checked", totalChecked_);
			dmOut.add("total-to-replicate", n);
			if (totalSizeToReplicate_>0L) {
				String s = FileSizeUtils.toHumanReadable(totalSizeToReplicate_);
				dmOut.add("total-size-to-replicate", new String[] {"h", s, "unit", "bytes"}, totalSizeToReplicate_);
			}
			if (addToQueue) {
				dmOut.add("queue", new String[]{"assets-added", ""+n}, queueName);
			}
			if (dbg) {
				log(logger, dateTime, "   total checked = " + totalChecked_);
				log(logger, dateTime, "   total to replicate = " + assetIDs.size());
			}


			// Replicate with nbThreads  (but one asset at a time per thread)
			if (send && assetIDs.size()>0) {
				if (dbg) {
					log(logger, dateTime,"Starting replication of " + assetIDs.size() + " assets");
				}		

				if (sendMethod.equals("find-and-send-direct")) {
					// Actually replicate. If we added assets to the
					// replication queue that was done in the find stage.

					// Replicate the list (in packSize_ chunks) with nbThreads. I see sometimes the number reported replicated not
					// the total it should have replicated, but when I redo, there are no
					// new data to replicate.  So I have added the "nhandled" to assist diagnosis
					// This is the total number it has iterated through (should be "total to replicate")
					ret = replicate (executor(), assetIDs, peer, dst, dstCA, nbThreads, 
							packSize, related, includeDestroyed, versions, collectionAssets, dateTime, 
							restoreMismatchedContent, clevel,  logger, format, dbg,
							catchExceptions);

					if (totalSizeToReplicate_>0) {
						Double rate = totalSizeToReplicate_.doubleValue() / timeToReplicateMilliSec_.doubleValue() * 1000.0d; // bytes/sec
						double rate2 = rate * 8.0d / 1000000000.0d;         // Gbit/sec
						dmOut.push("transfer");
						dmOut.add("time-sec", timeToReplicateMilliSec_*1000l);
						dmOut.add("rate", new String[] {"unit", "bytes/sec"}, rate);
						dmOut.add("rate", new String[] {"unit", "Gbit/sec"}, rate2);
						dmOut.pop();
					}
				} 
			}
		}

		if (!sendMethod.equals("find-and-send-queue")) {
			if (dbg) {
				log(logger, dateTime, "   " + SERVICE_NAME + " : ret="+""+ret[0] + ","+ret[1] + "," + ret[2] + ", " + ret[3] + ","+ ret[4] + "," + ret[5]);

			}
			dmOut.add("sent", 
					new String[] {"replicated", ""+ret[3], "not-replicated", ""+ret[4], "failed", ""+ret[1], "exceptions", ""+ret[5]}, ""+ret[2]);
			if (dbg) {
				log(logger, dateTime, "   "+SERVICE_NAME+" : total sent = " + ret[2]);
			}
		}


		// Report to caller
		w.add(dmOut.root(), false);


		// Send email
		if (email!=null) {
			String msg = XmlPrintStream.outputToString(dmOut.root());
			String subject = email.value("@subject");
			if (subject==null) {
				subject = "Mediaflux Namespace Replication Summaries (UUID="+ServerDetails.serverUUID(executor())+", date=" + DateUtil.todaysDate(2) +")";
			}
			ArrayList<String> emails = new ArrayList<String>();
			emails.add(email.value());
			String from = Properties.getServerProperty(executor(), "mail.from");
			MailUtils.sendEmail(executor(), email.value(), null, null, from, subject, 
					msg, true);
		}
	}



	private static void addToQueue (ServiceExecutor executor, String queue,
			String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		dm.add("name", queue);
		executor.execute("asset.processing.queue.add", dm.root());
	}


	/**
	 * Iterate through our list of asset IDs in chunks (with a query) so that we can
	 * use a pipe to send assets in parallel
	 * 
	 * @param executor
	 * @param assetIDs
	 * @throws Throwable
	 */
	private Integer[] replicate (ServiceExecutor executor, Vector<String> assetIDs, String peer, 
			String dst, String dstCA,
			Integer nbThreads, Integer packSize, Integer related, Boolean includeDestroyed, String versions,
			Boolean collectionAssets, String dateTime, Boolean restoreMismatchedContent, 
			Integer clevel, PluginLog logger, String format, 
			Boolean dbg, Boolean catchExceptions) throws Throwable {

		int nAssets = assetIDs.size();
		if (dbg) {
			log(logger, dateTime, "   " + SERVICE_NAME + " - replicating " + nAssets + " assets");
		}
		int nHandled = 0;
		int idx = 0;
		int nTotalFailed = 0;
		int nTotalReplicated = 0;
		int nTotalNotReplicated = 0;
		int nTotalSent = 0;
		int nTotalExceptions = 0; // asset.replicate.to exception

		boolean more = true;
		long t0 = System.currentTimeMillis();
		while (more) {
			PluginTask.checkIfThreadTaskAborted();
			if (dbg) {
				log(logger, dateTime, "   " + SERVICE_NAME + ":replicate - replicating asset # = " + idx);
			}
			int nLeft = nAssets - nHandled;
			int nPack = packSize;
			if (nLeft<packSize) {
				nPack = nLeft;
			}
			if (dbg) {
				log(logger, dateTime, "   "+SERVICE_NAME+":replicate - nLeft = " + nLeft);
				log(logger, dateTime, "   "+SERVICE_NAME+":replicate - packSize = " + packSize);
				log(logger, dateTime, "   "+SERVICE_NAME+":replicate - nPack = " + nPack);
			}

			if (nPack>0) {
				// We will feed nPack assets at a time to asset.replicate.to
				Vector<String> ids = new Vector<String>();
				for (int i=0;i<nPack; i++) {
					String id = assetIDs.get(idx+i);
					ids.add(id);
				}

				// now replicate these nPack assets with nbThreads
				// If assets no longer exist (someone destroyed them since we made the list), the
				// pipe will look after this. Asset IDs that don't exist
				// will just be dropped by the pipe
				PluginTask.checkIfThreadTaskAborted();
				int[] ret = replicateAssets (executor, catchExceptions,
						peer, dst, dstCA, ids, nbThreads, related, 
						includeDestroyed, versions, collectionAssets,
						dbg, dateTime, 
						restoreMismatchedContent, clevel, format, logger);
				//
				nTotalSent += ret[0];
				nTotalFailed += ret[1];
				nTotalReplicated += ret[2];
				nTotalNotReplicated += ret[3];
				nTotalExceptions += ret[4];

				// See if we go again
				nHandled += nPack;
				idx += nPack;

				if (nHandled==nAssets) {
					more = false;
				} else if (nHandled>nAssets) {
					throw new Exception ("Algorithm failure");
				}
			} else {
				more = false;
			}
		}
		long t1 = System.currentTimeMillis();
		timeToReplicateMilliSec_ = t1 - t0;
		Integer[] RET = new Integer[6];
		RET[0] = nHandled;                  //  How many we requested to replicate. May include assets since destroyed
		RET[1] = nTotalFailed;              //  The number that failed in the pipe (so they don't contribute to the other success counts)
		RET[2] = nTotalSent;                //  The number asset.replicate.to actually tried to send
		RET[3] = nTotalReplicated;          //  The total actually restored/updated. -1 if not available
		RET[4] = nTotalNotReplicated;       //  THe number that were sent but not updated/restored. -1 if not available
		RET[5] = nTotalExceptions;          //  THe number of calls to asset.replicate.to that genertaed an exception
		return RET;
	}

	private int[] replicateAssets (ServiceExecutor executor, 
			Boolean catchExceptions, String peer, 
			String dst, String dstCA, Vector<String> ids, 
			Integer nbThreads, Integer related, Boolean includeDestroyed, String versions, 
			Boolean collectionAssets, Boolean dbg, String dateTime, 
			Boolean restoreMismatchedContent, Integer clevel,  
			String format, PluginLog logger) throws Throwable {
		// Replicate
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String id : ids) {
			dm.add("id", id);
		}
		dm.add("nb-threads", nbThreads);
		dm.add("format", format);
		dm.add("cmode", "push");
		dm.add("create-roles", "true");
		dm.add("keepacls", "true");
		setDest (dst, dstCA, dm);
		dm.push("peer");
		dm.add("name", peer);
		dm.pop();
		dm.add("related", related);
		dm.add("update-doc-types", true);
		dm.add("update-models", false);
		dm.add("allow-move", true);
		dm.add("content-must-have-checksum", true);     // Staged assets have no checksum
		dm.add("if-staging", "skip");                   // Default is error
		dm.add("locally-modified-only", false);         // Allows us to replicate foreign assets (that are already replicas on our primary)
		dm.add("versions", versions);
		dm.add("auto-name", true);
		dm.add("overwrite", false);
		// These 2 added in 4.9.033
		dm.add("restore-missing-content", true);
		if (restoreMismatchedContent) {
			dm.add("restore-mismatched-content", new String[] {"validate", "true"}, true);		
		}
		dm.add("clevel", clevel);
		//
		if (includeDestroyed) {
			dm.add("include-destroyed", true);
		}
		dm.add("include-components", false);
		if (collectionAssets) {
			dm.push("collection-assets");
			dm.add("describe-queries", true);
			dm.add("include-dynamic-members", true);
			dm.add("include-static-members", true);
			dm.add("include-subcollections", true);
			dm.pop();
		}


		// With this multi-threaded approach, we give up knowing which
		// specific assets failed, if errors do occur.  One would have to turn
		// on list = true to see which assets are still replication candidates
		int nToSend = 0;
		if (dbg) {
			nToSend = ids.size();
			log(logger, dateTime, "   "+SERVICE_NAME+":replicateAssets : number of assets in predicate ="+nToSend);
		}

		// Force a peer check with retries
		int nRetry = checkPeerStatus (executor, peer);
		if (dbg) {
			if (nRetry<0) {
				log (logger, dateTime, "   "+SERVICE_NAME+":replicateAssets - " + -1*nRetry + " peer checks were made but the peer was not established");
			} else {
				log (logger, dateTime, "   "+SERVICE_NAME+":replicateAssets - " + nRetry + " peer checks were made before the peer was established");
			}
		}
		// Do the replication
		int[] ret = new int[]{0, 0, 0, 0, 0};
		try {
			XmlDoc.Element r = executor.execute("asset.replicate.to", dm.root());
			if (r==null) {
				return ret;
			}
			// Find number of assets actually replicated. Because the pipe handles assets
			// that don't exist anymore, we can't rely on how many we think we are replicating
			// The returned count element has attribute 'updated'  which is the 
			// actual number of restored assets.  The count value is just the number
			// sent. 
			XmlDoc.Element count = r.element("peer/count");
			int nSent = count.intValue();
			int nReplicated = count.intValue("@updated");
			int nNotReplicated = nSent - nReplicated;


			// FInd any failures
			int nFailed = 0;
			if (r!=null) {
				XmlDoc.Element errors = r.element("errors");
				if (errors!=null) {
					nFailed = errors.intValue("fail");
				}
			}

			//
			ret[0] = nSent;
			ret[1] = nFailed;
			ret[2] = nReplicated;
			ret[3] = nNotReplicated;
			if (dbg) {
				int nd = nToSend - ret[0];
				int nd2 = nToSend - ret[2];
				if (nd != 0 || nd2 !=0) {
					log(logger, dateTime, "   "+SERVICE_NAME+":replicateAssets : mismatch - nToSend, nSent, nReplicated - " + nToSend + "," + ret[0] + "," + ret[2]);		
				} else {
					log(logger, dateTime, "   "+SERVICE_NAME+":replicateAssets : match - nToSend, nSent, nReplicated - " + nToSend + "," + ret[0] + "," + ret[2]);		
				}
				log(logger,dateTime,"");
			}
		} catch (Throwable t) {
			if (catchExceptions) {
				log(logger, dateTime, "   "+SERVICE_NAME+":replicateAssets : exception from asset.replicate.to : " + t.getMessage());	
				// All I can know is the service call generated an exception. I don't
				// know how many of the assets in the pipe failed
				ret[4]++;
			} else {
				throw new Exception(t);
			}
		}
		return ret;	
	}


	private void log (PluginLog logger, String dateTime, String message) {
		logger.add(PluginLog.INFORMATION,dateTime + " : " + message);
	}

	private String serverUUID(ServiceExecutor executor, String proute) throws Throwable {

		XmlDoc.Element r = executor.execute(new ServerRoute(proute), "server.uuid");
		return r.value("uuid");
	}


	/**
	 * In this approach we use a multi-threaded piped query
	 * for efficiency and an iterator for scalability
	 * 
	 */
	private Long findWithPipedQuery (ServiceExecutor executor, 
			String dateTime,  Integer nbThreads, String collection,
			Collection<String> wheres, String peer, 
			Vector<String> assetList,  
			Boolean useIndexes, Boolean dbg, Boolean list, 
			Boolean includeDestroyed, Boolean addToQueue,
			String queueName,
			PluginLog logger, XmlDocMaker w)	throws Throwable {

		if (dbg) {
			log(logger, dateTime, SERVICE_NAME+" : find assets on primary");
		}
		Long n = 0L;

		// Find all local  assets  that need to be replicated
		// for any version with a piped query. If too many assets are
		// found this will exceed the virtual memory when generating
		// the resultant asset ID  list that is subsequently replicated.
		// In this case, you can use the 
		// :all-in-one true approach of this service.
		XmlDocMaker dm = new XmlDocMaker("args");
		makeQueryPredicate (dm, wheres, includeDestroyed);
		if (collection!=null) {
			// asset.query won't find the collection asset
			// itself. So if you give say, a project asset
			// (which has not been replicated) in :collection
			// it will only create that as a stub (no ACLs)
			// so I have to force it to take the asset itself
			// as well as the collection, and there is only
			// one way to do that.
			dm.add("where", "asset in collection or subcollection of "+collection +
					        " or id="+collection);
		}
		dm.add("pdist", 0);
		dm.add("use-indexes", useIndexes);
		dm.add("action", "pipe");
		dm.add("pipe-nb-threads", nbThreads);
		dm.add("pipe-generate-result-xml", true);
		dm.add("size", "infinity");
		dm.add("as", "iterator");
		dm.push("service", new String[] {"name", "unimelb.asset.replica.need.to.replicate"});
		if (peer!=null) {
			dm.add("peer", peer);
		}
		// We really just need to look at the last version
		// and if it needs to replicate then the asset is out
		// of sync.  When we actually replicate, we can choose to
		// replicate all versions. We can't fix version holes
		//  in the replica versions compared with primary versions.
		dm.add("all-versions", false);
		dm.pop();
		if (dbg) {
			log(logger, dateTime, "Primary query = " + dm.root());
		}
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return 0L;  
		}		

		// Get the iterator ID and iterate through the results
		long iteratorID = r.longValue("iterator");
		boolean complete = false;
		dm.add("id", iteratorID);
		dm.add("size", ITERATOR_SIZE);
		try {
			while (!complete) {

				PluginTask.checkIfThreadTaskAborted();
				dm = new XmlDocMaker("args");
				dm.add("id", iteratorID);
				dm.add("size", ITERATOR_SIZE);
				XmlDoc.Element re = executor.execute("asset.query.iterate", dm.root());
				complete = re.booleanValue("iterated/@complete");
				Collection<XmlDoc.Element> ids = re.elements("id");
				if (ids!=null) {
					for (XmlDoc.Element id : ids) {
						n++;
						String path = id.value("@path");
						String csize = id.value("@csize");
						if (csize!=null) {
							long csizeI = Long.parseLong(csize);
							totalSizeToReplicate_ += csizeI;
						}
						//
						if (list) {
							w.add("id", new String[]{"reasons", id.value("@reasons"),
									"path-primary", path, "csize", csize},  id.value());

						}
						//
						if (addToQueue) {
							// This scales because we use the iterator
							addToQueue (executor, queueName, id.value());
						} else {
							// This does not scale because we are making a big
							// list in memory
							assetList.add(id.value());
						}
					}
				}
			} 
		} catch (Throwable t) {
			// make sure the iterator is destroyed.
			executor.execute("asset.query.iterator.destroy",
					"<args><ignore-missing>true</ignore-missing><id>" + iteratorID + "</id></args>", null, null);
			throw t;
		}

		// The only way to find out how many assets were checked is
		// to do that query as the pipe does not tell us.
		// Not too expensive
		// Force a peer check with retries
		int nRetry = checkPeerStatus (executor, peer);
		if (dbg) {
			if (nRetry<0) {
				log (logger, dateTime, "   "+SERVICE_NAME+":findWithPipedQuery - " + -1*nRetry + " peer checks were made but the peer was not established");
			} else {
				log (logger, dateTime, "   "+SERVICE_NAME+":findWithPipedQuery - " + nRetry + " peer checks were made before the peer was established");
			}
		}

		dm = new XmlDocMaker("args");
		makeQueryPredicate (dm, wheres, includeDestroyed);
		if (collection!=null) {
			dm.add("collection", collection);
		}
		dm.add("pdist", 0);
		dm.add("use-indexes", useIndexes);
		dm.add("size", "infinity");
		dm.add("action", "count");
		r = executor.execute("asset.query", dm.root());
		totalChecked_ = Long.parseLong(r.value("value"));
		//
		return n;
	}


	private void makeQueryPredicate (XmlDocMaker dm, Collection<String> wheres,
			Boolean includeDestroyed) throws Throwable {
		if (includeDestroyed) {
			if (wheres!=null) {
				boolean first = true;
				for (String where : wheres) {
					if (first)  {
						String where2 = where + " and ((asset has been destroyed) or (asset has not been destroyed))";
						first = false;
						dm.add("where", where2);						
					} else {
						dm.add("where", where);
					}
				}
			} else {
				dm.add("where", "((asset has been destroyed) or (asset has not been destroyed))");
			}
			dm.add("include-destroyed", true);			
		} else {
			if (wheres!=null) {
				for (String where : wheres) {
					dm.add("where", where);
				}
			}
		}
	}


	private Integer[]  findAndSend (ServiceExecutor executor, String dst,
			String dstCA, String peer, String collection,
			Collection<String> wheres, Boolean collectionAssets, String versions, 
			Integer nbThreads, Integer clevel,  Boolean debug, 
			String format, String dateTime, Boolean includeDestroyed,
			PluginLog logger) throws Throwable {	


		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("replication");

		// We can now do all checking, version iteration and sending directly
		// with asset.replicate.to
		XmlDocMaker dm = new XmlDocMaker("args");	
		dm.add("check-if-required", true); // Only tests latest version even if :versions all
		dm.add("content-must-have-checksum", true);     // Staged assets have no checksum
		dm.add("if-staging", "skip");                   // Default is error
		if (wheres!=null) {
			for (String where : wheres) {
				dm.add("where", where);
			}
		}
		if (collection!=null) {
			// Will include the collection asset itself
			dm.add("collection", collection);
		}
		dm.add("nb-threads", nbThreads);
		dm.add("format", format);
		dm.add("cmode", "push");
		//
		setDest (dst, dstCA, dm);
		dm.push("peer");
		dm.add("name", peer);
		dm.pop();
		dm.add("related", "0");
		dm.add("update-doc-types", true);
		dm.add("update-models", false);
		dm.add("allow-move", true);
		dm.add("locally-modified-only", false);         // Allows us to replicate foreign assets (that are already replicas on our primary)
		dm.add("versions", versions);
		dm.add("auto-name", true);
		dm.add("overwrite", false);
		dm.add("onerror", "report");     // Try and continue
		// These 2 added in 4.9.033
		// May like to add control for these
		dm.add("restore-missing-content", true);
		dm.add("restore-mismatched-content", new String[] {"validate", "true"}, false);		
		dm.add("clevel", clevel);
		//
		if (includeDestroyed) {
			dm.add("include-destroyed", false);
		}
		dm.add("include-components", false);
		if (collectionAssets) {
			dm.push("collection-assets");
			dm.add("describe-queries", true);
			dm.add("include-dynamic-members", true);
			dm.add("include-static-members", true);
			dm.add("include-subcollections", true);
			dm.pop();
		}

		// Force a peer check with retries
		// Force a peer check with retries
		int nRetry = checkPeerStatus (executor, peer);
		if (debug) {
			if (nRetry<0) {
				log (logger, dateTime, "   "+SERVICE_NAME+":findAndSend - " + -1*nRetry + " peer checks were made but the peer was not established");
			} else {
				log (logger, dateTime, "   "+SERVICE_NAME+":findAndSend - " + nRetry + " peer checks were made before the peer was established");
			}
		}

		// Do the replication		
		XmlDoc.Element r = executor.execute("asset.replicate.to", dm.root());

		// Parse result
		XmlDoc.Element count = r.element("peer/count");
		if (count!=null) {
			int nSent = count.intValue();
			int nReplicated = count.intValue("@updated");
			int nNotReplicated = nSent - nReplicated;
			int nFailed = 0;
			if (r!=null) {
				XmlDoc.Element errors = r.element("errors");
				if (errors!=null) {
					nFailed = errors.intValue("fail");
				}
			}
			//
			Integer[] RET = new Integer[6];
			RET[0] = -1;                   //  How many we requested to replicate - it's unknown
			RET[1] = nFailed;              //  The number that failed in the pipe (so they don't contribute to the other success counts)
			RET[2] = nSent;                //  The number asset.replicate.to actually tried to send
			RET[3] = nReplicated;          //  The total actually restored/updated. -1 if not available
			RET[4] = nNotReplicated;       //  THe number that were sent but not updated/restored. -1 if not available
			RET[5] = -1;                   //  THe number of calls to asset.replicate.to that generated an exception - it's unknown
			return RET;
		} else {
			Integer[] RET = new Integer[6];
			RET[0] = 0;              //  How many we requested to replicate - it's unknown
			RET[1] = 0;              //  The number that failed in the pipe (so they don't contribute to the other success counts)
			RET[2] = 0;              //  The number asset.replicate.to actually tried to send
			RET[3] = 0;              //  The total actually restored/updated. -1 if not available
			RET[4] = 0;              //  THe number that were sent but not updated/restored. -1 if not available
			RET[5] = 0;              //  THe number of calls to asset.replicate.to that generated an exception - it's unknown
			return RET;
		}
	}


	private void setDest (String dst, String dstCA, XmlDocMaker dm) throws Throwable {
		if (dstCA!=null) {
			dm.add("dst-collection", dstCA);
		} else {
			dm.add("dst", dst);
		}
	}


	// Iterate through all asset versions
	// If any have not been replicated, return true
	// returns null if does not need to replicate
	// else the returned string is a set of concatenated reasons
	// in the pattern <asset version>:<reason>,
	/*
	private String needToReplicateAllVersions (ServiceExecutor executor, String peerName,
			String id) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		XmlDoc.Element r = executor.execute("asset.versions", dm.root());
		Collection<String> versions = r.values("asset/version/@n");

		// Iterate over versions
		String allReasons = "";
		for (String version : versions) {
			XmlDocMaker dm2 = new XmlDocMaker("args");
			dm2.add("id", new String[] {"version", version}, id);
			dm2.push("peer");
			dm2.add("name", peerName);
			dm2.pop();
			// The service returns "yes", "no" and "unknown" (e.g. peer is down)
			XmlDoc.Element r2 = executor.execute("asset.replica.need.to.replicate", dm2.root());
			String req = r2.value("asset/required");

			// If the result is "yes" there should be a reason.
			if (!req.equals("no")) {
				String reason = r2.value("asset/reason");

				// There probably is no reason if the result is "unknown"
				// but we are going to trigger a re-replication with the
				// reason string so we need one
				if (req.equals("unknown")) {
					reason = "unknown";
				} else if (req.equals("yes")) {
					// make sure we have a reason even if it's not very meaningful
					if (reason==null) {
						reason = "unknown";
					}
				}
				allReasons += version+":"+reason+",";

				// Return as soon as we know we need to re-replicate
				if (req.equals("unknown")) {
					// Hard to know what to do, because perhaps the peer is down.
					// So be conservative and attempt to re-replicate (which does not hurt)
					return allReasons.substring(0,allReasons.length()-1);
				} else if (req.equals("yes")) {
					return allReasons.substring(0,allReasons.length()-1);
				}
			}
		}

		// We don't need to replicate
		return null;
	}
	 */

	/*
	private String needToReplicate (ServiceExecutor executor, String peerName,
			String id) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		dm.push("peer");
		dm.add("name", peerName);
		dm.pop();

		// The service returns "yes", "no" and "unknown" (e.g. peer is down)
		XmlDoc.Element r2 = executor.execute("asset.replica.need.to.replicate", dm.root());
		String req = r2.value("asset/required");
		String reason = r2.value("asset/reason");

		if (req.equals("no")) {
			return null;
		} else if (req.equals("unknown")) {
			return "unknown";
		} else if (req.equals("yes")) {
			if (reason==null) {
				return "unknown";
			} else {
				return reason;
			}
		} else {
			return null;
		}

	}
	 */

	/**
	 *  Set the expected RID on the DR server. If the asset is in a numbered schema
	 *     that has to be accounted for.
	 *     
	 * @param id
	 * @param rid
	 * @param schemaID
	 * @param uuidLocal
	 * @return
	 * @throws Throwable
	 */
	static public String setRID (String id, String rid, String schemaID, String uuidLocal) throws Throwable  {
		if (rid!=null) {
			return rid;
		}
		if (schemaID==null) {

			// No numbered schema (primary schema)
			return uuidLocal + "." + id;
		} else {
			// We are in a numbered schema
			return uuidLocal + "." + schemaID + "." + id;
		}
	}


	private int checkPeerStatus (ServiceExecutor executor, String peerName) throws Throwable {
		for (int i=0;i<DEFAULT_RETRY_TIMES;i++) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", peerName);
			dm.add("check-peer-is-reachable", true);
			XmlDoc.Element r = executor.execute("server.peer.status", dm.root());
			if (r!=null) {
				String status = r.value("peer/status");
				if (status!=null && status.equals("reachable")) {
					return i+1;
				}
			}
			Thread.sleep(DEFAULT_RETRY_INTERVAL);
		}
		return -1*DEFAULT_RETRY_TIMES;
	}

}
