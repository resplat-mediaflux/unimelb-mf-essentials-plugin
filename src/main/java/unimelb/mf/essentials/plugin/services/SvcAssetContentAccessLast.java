package unimelb.mf.essentials.plugin.services;


import java.util.Collection;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;


public class SvcAssetContentAccessLast extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.access.last";

	public static final int DEFAULT_BATCH_SIZE = 1000;
	private Interface _defn;

	public SvcAssetContentAccessLast() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Selection query.", 1, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT,
				"If true, then use available indexes. If false, then perform linear searching. Defaults to true.", 0,
				1));
		_defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT,
				"Include assets that have been marked as destroyed? Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("batch-size", IntegerType.POSITIVE_ONE,
				"Query result batch size. Defaults to " + DEFAULT_BATCH_SIZE + ".", 0, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Returns the most recent time of content access (and also most recent ctime and mtime)  for all of the assets selected by the query.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		// Parse
		Boolean useIndexes = args.booleanValue("use-indexes", null);
		Boolean includeDestroyed = args.booleanValue("include-destroyed", null);
		int size = args.intValue("batch-size", DEFAULT_BATCH_SIZE);

		// Query
		XmlDocMaker dm = new XmlDocMaker("args");
		if (args.elementExists("where")) {
			dm.addAll(args.elements("where"));
		}
		if (useIndexes != null) {
			dm.add("use-indexes", useIndexes);
		}
		if (includeDestroyed != null) {
			dm.add("include-destroyed", includeDestroyed);
		}
		dm.add("as", "iterator");
		dm.add("action", "get-values");
		dm.add("xpath", "ctime");
		dm.add("xpath", "mtime");
		dm.add("xpath", "content/atime");
		dm.add("size", "infinity");

		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return;
		}

		// Iterate
		long iteratorID = r.longValue("iterator");
		long nbAssets = 0;
		Date lastAccess = null;
		Date lastCTime = null;
		Date lastMTime = null;
		String lastAccessID = null;
		String lastCTimeID = null;
		String lastMTimeID = null;
		boolean complete = false;
		dm = new XmlDocMaker("args");
		dm.add("id", iteratorID);
		dm.add("size", size);
		try {
			while (!complete) {
				PluginTask.checkIfThreadTaskAborted();
				r = executor().execute("asset.query.iterate", dm.root());
				complete = r.booleanValue("iterated/@complete");
				Collection<XmlDoc.Element> res = r.elements("asset");
				if (res != null) {
					for (XmlDoc.Element re : res) {
						PluginTask.checkIfThreadTaskAborted();
						nbAssets++;
						List<Date> dates = re.dateValues("value");
						// 
						Date ctime = dates.get(0);
						Date mtime = dates.get(1);
						Date atime = null;
						if (dates.size()==3) {
							atime = dates.get(2);
						}

						if (atime!=null) {
							if (lastAccess==null) {
								lastAccess = atime;
								lastAccessID = re.value("@id");
							} else {
								if (atime.after(lastAccess)) {
									lastAccess = atime;
									lastAccessID = re.value("@id");
								}
							}
						}
						//
						if (lastCTime==null) {
							lastCTime = ctime;
							lastCTimeID = re.value("@id");
						} else {
							if (ctime.after(lastCTime)) {
								lastCTime = ctime;
								lastCTimeID = re.value("@id");
							}
						}
						//
						if (lastMTime==null) {
							lastMTime = mtime;
							lastMTimeID = re.value("@id");
						} else {
							if (mtime.after(lastMTime)) {
								lastMTime = mtime;
								lastMTimeID = re.value("@id");
							}
						}
					}
				}			
			}					
		} catch (Throwable e) {
			// make sure the iterator is destroyed.
			executor().execute("asset.query.iterator.destroy",
					"<args><ignore-missing>true</ignore-missing><id>" + iteratorID + "</id></args>", null, null);
			throw e;
		}		


		w.add("nassets", nbAssets);
		if (nbAssets>0) {
			if (lastAccess!=null) {
				w.add("last-content-access", new String[] {"id", lastAccessID}, lastAccess);
			} else {
				w.add("last-content-access",  "unknown");
			}
			w.add("last-asset-create", new String[] {"id", lastCTimeID}, lastCTime);
			w.add("last-asset-modification", new String[] {"id", lastMTimeID}, lastMTime);
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}


}