package unimelb.mf.essentials.plugin.services;

//import java.io.ByteArrayInputStream;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.ServerDetails;

public class SvcNetworkMonitor extends PluginService {

	public static final String SERVICE_NAME = "unimelb.network.monitor";

	private Interface _defn;

	public SvcNetworkMonitor() {
		_defn = new Interface();
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "The email address to notify.", 0, 1));
		_defn.add(new Interface.Element("port", IntegerType.DEFAULT, "The port to monitor", 1, 1));
		_defn.add(new Interface.Element("maxc", IntegerType.DEFAULT, 
				"If there are more connections than this value, email the output of server.threads.get", 1,1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Detects when the number of connections on a network service on the given port exceeds a threshold.  Sends the server threads to the given email address when this is true.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String email  = args.value("email");
		int port = args.intValue("port");
		int maxc = args.intValue("maxc");

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("port", port);
		XmlDoc.Element r = executor().execute("network.describe", dm.root());
		int nc = r.intValue("service/connections/active");
		if (nc>maxc) {
			r = executor().execute("server.threads.get");
			w.add(r);
			if (email!=null) {
				String uuid = ServerDetails.serverUUID(executor());
				String subject = "Connections alert for server UUID " + uuid + " on port " + port + " . Number exceeds " + maxc;
				String body= XmlPrintStream.outputToString(r);
				MailUtils.sendEmail(executor(), email, null, null, null, subject, body, false);
			}
		}
	}


	@Override
	public String name() {
		return SERVICE_NAME;
	}


	@Override
	public boolean canBeAborted() {
		return true;
	}

}
