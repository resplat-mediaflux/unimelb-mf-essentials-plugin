package unimelb.mf.essentials.plugin.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcLicenceExpiryNotify extends PluginService {

	public static final long MILLISECONDS_PER_DAY = 86400000L;
	public static final long MILLISECONDS_PER_WEEK = MILLISECONDS_PER_DAY * 7L;
	public static final long MILLISECONDS_PER_MONTH = MILLISECONDS_PER_DAY * 30L;

	public static final String SERVICE_NAME = "unimelb.licence.expiry.notify";

	private Interface _defn;

	public SvcLicenceExpiryNotify() {
		_defn = new Interface();
		Interface.Element before = new Interface.Element("before", IntegerType.POSITIVE_ONE,
				" The time before lincence expiry to send notification. Defaults to 30 days.", 0, 1);
		before.add(new Interface.Attribute("unit", new EnumType(new String[] { "day", "week", "month" }),
				"Time unit. Defaults to day.", 0));
		_defn.add(before);

		_defn.add(new Interface.Element("to", EmailAddressType.DEFAULT, "Recipients.", 1, 255));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Send notification to remind the server licence expiry.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		String serverUUID = executor().execute("server.uuid").value("uuid");

		long before = args.longValue("before", 30);
		String unit = args.stringValue("before/@unit", "day");
		Collection<String> to = args.values("to");

		long beforeMillis = 0;
		if ("month".equalsIgnoreCase(unit)) {
			beforeMillis = before * MILLISECONDS_PER_MONTH;
		} else if ("week".equalsIgnoreCase(unit)) {
			beforeMillis = before * MILLISECONDS_PER_WEEK;
		} else {
			beforeMillis = before * MILLISECONDS_PER_DAY;
		}

		List<Licence> licences = Licence.describe(executor());
		Date serverDate = executor().execute("server.date").dateValue("date");

		List<Licence> expired = new ArrayList<Licence>();
		Map<Licence, Long> expiring = new LinkedHashMap<Licence, Long>();

		for (Licence licence : licences) {
			long diffMillis = licence.expiry.getTime() - serverDate.getTime();
			if (diffMillis <= 0) {
				expired.add(licence);
			} else if (diffMillis < beforeMillis) {
				expiring.put(licence, diffMillis);
			}
		}

		if (!expired.isEmpty()) {
			String subject = String.format("[%s] WARNING: Mediaflux licence expired", serverUUID);
			StringBuilder sb = new StringBuilder();
			sb.append("\n");
			for (Licence el : expired) {
				sb.append(String.format("%s licence expired at %s", el.application,
						new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(el.expiry))).append("\n");
			}
			sb.append("\n");
			send(subject, sb.toString(), to);
		} else if (!expiring.isEmpty()) {
			long minDiff = Collections.min(expiring.values());
			long minDiffDays = minDiff / MILLISECONDS_PER_DAY;
			String subject = String.format("[%s] WARNING: Mediaflux licence will expire in %d days", serverUUID,
					minDiffDays);
			StringBuilder sb = new StringBuilder();
			sb.append("\n");
			expiring.forEach((lic, diff) -> {
				long diffDays = diff / MILLISECONDS_PER_DAY;
				sb.append(String.format("%s licence will expire in %d days at %s", lic.application, diffDays,
						new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(lic.expiry))).append("\n");
			});
			sb.append("\n");
			send(subject, sb.toString(), to);
		}
	}

	private void send(String subject, String body, Collection<String> recipients) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String to : recipients) {
			dm.add("to", to);
		}
		dm.add("subject", subject);
		dm.add("body", body);
		executor().execute("mail.send", dm.root());
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	private static class Licence {
		public final String application;
		public final Date expiry;

		Licence(XmlDoc.Element le) throws Throwable {
			this.application = le.value("application");
			this.expiry = le.dateValue("expiry");
		}

		public static List<Licence> describe(ServiceExecutor executor) throws Throwable {

			List<XmlDoc.Element> les = executor.execute("licence.describe").elements("licence");
			if (les != null && !les.isEmpty()) {
				List<Licence> ls = new ArrayList<Licence>(les.size());
				for (XmlDoc.Element le : les) {
					ls.add(new Licence(le));
				}
				return ls;
			}
			return null;
		}
	}

}
