package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetNamespaceQuotaUsedCheck extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.namespace.quota.used.check";

    private final Interface _def;

    public SvcAssetNamespaceQuotaUsedCheck() {
        _def = new Interface();
        _def.add(new Interface.Element("namespace", StringType.DEFAULT, "The asset namespace to check", 1, 1));
        _def.add(new Interface.Element("regenerate", BooleanType.DEFAULT, "Regenerate quota allocation if the value is invalid. Defaults to false.", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Check the quota/used values of the specified asset namespace and compare with total asset content size (for all versions). Optionally, regenerate the value if the quota/used value does not match total asset content size (all versions).";
    }

    @Override
    public Interface definition() {
        return _def;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        final String namespace = args.value("namespace");
        final boolean regenerate = args.booleanValue("regenerate", false);

        PluginTask.checkIfThreadTaskAborted();
        long totalContentSize = getTotalContentSize(executor(), namespace);

        PluginTask.checkIfThreadTaskAborted();
        Quota quota = getAssetNamespaceQuota(executor(), namespace);

        if (quota != null && totalContentSize != quota.used && regenerate) {
            PluginTask.checkIfThreadTaskAborted();
            regenerateQuota(executor(), namespace);
        }

        w.push("namespace", new String[]{"quota", Boolean.toString(quota != null),}, namespace);
        w.add("total-size", new String[]{"h", getHumanReadableDataSize(executor(), totalContentSize)}, totalContentSize);
        if (quota != null) {
            w.push("quota", new String[]{"overflow", Boolean.toString(totalContentSize > quota.allocation), "regenerate", regenerate && totalContentSize != quota.used ? "true" : null});
            w.add("allocation", new String[]{"h", getHumanReadableDataSize(executor(), quota.allocation)}, quota.allocation);
            w.add("used", new String[]{"match", Boolean.toString(totalContentSize == quota.used), "h", getHumanReadableDataSize(executor(), quota.used)}, quota.used);
            w.pop();
        }
        w.pop();
    }

    private static class Quota {
        public final long allocation;
        public final long used;

        Quota(XmlDoc.Element qe) throws Throwable {
            this.allocation = qe.longValue("allocation", 0);
            this.used = qe.longValue("used", 0);
        }

    }

    private static Quota getAssetNamespaceQuota(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespace);
        XmlDoc.Element qe = executor.execute("asset.namespace.describe", dm.root()).element("namespace/quota");
        if (qe.elementExists("allocation")) {
            return new Quota(qe);
        } else {
            return null;
        }
    }


    private static long getTotalContentSize(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + namespace + "'");
        dm.add("action", "sum");
        dm.add("xpath", "content/@total-size");
        return executor.execute("asset.query", dm.root()).longValue("value", 0);
    }

    private static void regenerateQuota(ServiceExecutor executor, String namespace) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespace);
        executor.execute("asset.namespace.quota.regenerate", dm.root());
    }

    private static String getHumanReadableDataSize(ServiceExecutor executor, long size) throws Throwable {
        PluginTask.checkIfThreadTaskAborted();
        return executor.execute("function.execute", String.format("<args><expression>data.size.format(%d)</expression></args>", size), null, null).value("result");
    }

}
