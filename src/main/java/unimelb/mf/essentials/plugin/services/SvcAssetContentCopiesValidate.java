package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.streams.SizedInputStream;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentCopiesValidate extends PluginService {

	public final String SERVICE_NAME = "unimelb.asset.content.copies.validate";

	private Interface _defn;

	public SvcAssetContentCopiesValidate() {
		_defn = new Interface();

		Interface.Element id = new Interface.Element("id", AssetType.DEFAULT, "The asset id.");
		id.add(new Interface.Attribute("label", StringType.DEFAULT,
				"Selects the version given by the label. The overrides the 'version' argument.", 0));
		id.add(new Interface.Attribute("version", IntegerType.POSITIVE,
				"Version of the asset. A value of zero means the latest version. Defaults to latest.", 0));
		_defn.add(id);

		_defn.add(new Interface.Element("all-versions", BooleanType.DEFAULT,
				"If true, then validate the content for all versions. If false, then only validate the selected version. Defaults to 'false'.",
				0, 1));
		_defn.add(new Interface.Element("copy", IntegerType.POSITIVE_ONE,
				"The copy to check. If unspecified, then all copies are checked.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"If specified, validate only content in this specified store(s)", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store-tag", StringType.DEFAULT,
				"If specified, then only check copies for stores with all of the specified tags.", 0,
				Integer.MAX_VALUE));

		_defn.add(new Interface.Element("must-match", BooleanType.DEFAULT,
				"If true, all of 'copy', 'store', 'store-tag' must match to select assets.  If false (default), assets will be selected if any of them match.",
				0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Validates the content copies in the file system stores. It independently (of standard Mediaflux services like asset.content.validate) reads the content and compares the check sum with that held in the data base.  This process also works for aggregated content (it makes use of the stored offsets and sizes).";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		List<AssetStore> allStores = AssetStore.getAll(executor());
		Map<String, String> allStoreTypes = AssetStore.getStoreTypes(allStores);

		Set<Integer> filterCopies = new LinkedHashSet<>();
		if (args.elementExists("copy")) {
			filterCopies.addAll(args.intValues("copy"));
		}
		Set<String> filterStores = new LinkedHashSet<>();
		if (args.elementExists("store")) {
			filterStores.addAll(args.values("store"));
		}
		Collection<String> filterStoreTags = args.values("store-tag");
		if (filterStoreTags != null && !filterStoreTags.isEmpty()) {
			AssetStore.findByTags(allStores, filterStoreTags, filterStores);
		}

		boolean mustMatch = args.booleanValue("must-match", false);

		boolean allVersions = args.booleanValue("all-versions", false);

		if (allVersions) {
			String id = args.value("id");
			// loop through versions
			int idx = 1;
			int size = 100;
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", id);
			dm.add("idx", idx);
			dm.add("size", size);
			dm.add("sort", "desc");
			XmlDoc.Element as = dm.root();
			boolean complete = false;
			while (!complete) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element re = executor().execute("asset.versions.get", as);
				List<XmlDoc.Element> aes = re.elements("asset/asset");
				if (aes != null && !aes.isEmpty()) {
					for (XmlDoc.Element ae : aes) {
						PluginTask.checkIfThreadTaskAborted();
						validateContentCopies(ae, filterStores, filterCopies, mustMatch, allStoreTypes, w);
					}
				}
				complete = re.booleanValue("cursor/total/@complete");
				idx += size;
				as.element("idx").setValue(idx);
			}
		} else {
			XmlDoc.Element ide = args.element("id");
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element ae = getAsset(executor(), ide);
			PluginTask.checkIfThreadTaskAborted();
			validateContentCopies(ae, filterStores, filterCopies, mustMatch, allStoreTypes, w);
		}
	}

	private static XmlDoc.Element getAsset(ServiceExecutor executor, XmlDoc.Element idElement) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add(idElement, true);
		return executor.execute("asset.get", dm.root()).element("asset");
	}

	private static void validateContentCopies(XmlDoc.Element ae, Set<String> filterStores, Set<Integer> filterCopies,
			boolean mustMatch, Map<String, String> allStoreTypes, XmlWriter w) throws Throwable {
		String assetId = ae.value("@id");
		int assetVersion = ae.intValue("@version");
		XmlDoc.Element contentElement = ae.element("content");
		if (contentElement == null) {
			// asset has no content
			return;
		}
		Content content = new Content(assetId, assetVersion, contentElement, allStoreTypes);
		content.validate(filterStores, filterCopies, mustMatch, w);
	}

	private static class Content {

		public final String assetId;
		public final int assetVersion;
		public final int numberOfCopies;
		@SuppressWarnings("unused")
		public final int primaryCopyId;
		public final long size;
		public final Long csum;
		private Map<Integer, Copy> _copies;

		private Content(String assetId, int assetVersion, XmlDoc.Element contentElement, Map<String, String> storeTypes)
				throws Throwable {
			this.assetId = assetId;
			this.assetVersion = assetVersion;
			this.numberOfCopies = contentElement.intValue("number-of-copies");
			this.primaryCopyId = contentElement.intValue("copy-id");
			this.size = contentElement.longValue("size");
			this.csum = contentElement.longValue("csum[@base='10']", null);
			this._copies = new LinkedHashMap<>();
			if (this.numberOfCopies > 1) {
				List<XmlDoc.Element> copyElements = contentElement.elements("copies/copy");
				if (copyElements != null && !copyElements.isEmpty()) {
					for (XmlDoc.Element copyElement : copyElements) {
						Copy copy = new Copy(this, copyElement, storeTypes);
						_copies.put(copy.copyId, copy);
					}
				}
			} else {
				Copy copy = new Copy(this, contentElement, storeTypes);
				_copies.put(copy.copyId, copy);
			}
		}

		public Map<Integer, Validity> validate(Set<String> filterStores, Set<Integer> filterCopies, boolean mustMatch) {

			Map<Integer, Validity> result = new TreeMap<>();
			for (Copy copy : _copies.values()) {
				Boolean matchStore = null;
				if (filterStores != null && !filterStores.isEmpty()) {
					matchStore = filterStores.contains(copy.storeName);
				}
				Boolean matchCopy = null;
				if (filterCopies != null && !filterCopies.isEmpty()) {
					matchCopy = filterCopies.contains(copy.copyId);
				}

				boolean match = false;
				if (matchStore == null && matchCopy == null) {
					match = true;
				} else if (matchStore == null && matchCopy != null) {
					match = matchCopy;
				} else if (matchStore != null && matchCopy == null) {
					match = matchStore;
				} else {
					if (mustMatch) {
						match = matchStore && matchCopy;
					} else {
						match = matchStore || matchCopy;
					}
				}
				if (match) {
					Validity validCopy = copy.validate();
					result.put(copy.copyId, validCopy);
				}
			}
			return result;
		}

		public void validate(Set<String> filterStores, Set<Integer> filterCopies, boolean mustMatch, XmlWriter w)
				throws Throwable {
			w.push("asset", new String[] { "id", this.assetId, "version", Integer.toString(this.assetVersion) });

			Map<Integer, Validity> valid = validate(filterStores, filterCopies, mustMatch);

			// number of validated copies.
			w.add("copies", new String[] { "total", Integer.toString(this.numberOfCopies) }, valid.size());

			if (!valid.isEmpty()) {
				Validity firstInvalid = null;
				Set<Integer> copyIdSet = valid.keySet();
				for (Integer copyId : copyIdSet) {
					Validity copyValid = valid.get(copyId);
					if (copyValid.status != Validity.Status.OK) {
						firstInvalid = copyValid;
						break;
					}
				}

				if (firstInvalid == null) {
					w.add("status", Validity.Status.OK.name());
				} else {
					// only print size and csum, when there's an error
					w.add("size", this.size);
					if (this.csum != null) {
						w.add("csum", new String[] { "base", "10" }, this.csum);
					}
					w.add("status", new String[] { "reason", firstInvalid.reason }, firstInvalid.status.name());
				}
				for (Integer copyId : copyIdSet) {
					Validity copyValid = valid.get(copyId);
					Copy copy = this._copies.get(copyId);
					w.push("copy",
							new String[] { "id", Integer.toString(copyId), "store", copy.storeName, "oid",
									Long.toString(copy.oid), "offset",
									copy.offset == null ? null : Long.toString(copy.offset) });
					w.add("status", new String[] { "reason", copyValid.reason }, copyValid.status.name());
					if (copyValid.status != Validity.Status.OK && copy.url != null) {
						// only print url when the copy is invalid.
						w.add("url", copy.url);
					}
					w.pop();
				}
			}

			w.pop();
		}

		private static class Copy {
			private final Content content;
			public final int copyId;
			public final String url;
			public final String storeName;
			public final String storeType;
			public final long oid;
			public final Long offset;

			private Copy(Content content, XmlDoc.Element ce, Map<String, String> storeTypes) throws Throwable {
				this.content = content;
				if (ce.nameEquals("copy")) {
					this.copyId = ce.intValue("@id");
				} else {
					// single copy
					assert ce.nameEquals("content") == true;
					this.copyId = ce.intValue("copy-id");
				}
				this.url = ce.value("url");
				this.storeName = ce.value("store");
				this.storeType = ce.stringValue("store/@type", storeTypes.get(storeName));
				this.oid = ce.longValue("store/@oid");
				this.offset = ce.longValue("store/@offset", null);
			}

			public Validity validate() {
				if (this.content.csum == null) {
					return new Validity(Validity.Status.error, "asset/content/csum is not set");
				}
				if (url == null || !url.startsWith("file")) {
					if (storeType == null || !"file-system".equals(storeType)) {
						return new Validity(Validity.Status.error, "unsupported store type: " + storeType);
					}
					return new Validity(Validity.Status.error, "missing or unsupported url");
				}
				Path file = Paths.get(url.substring(5));
				if (!Files.exists(file) || Files.isDirectory(file)) {
					return new Validity(Validity.Status.missing);
				}
				try {
					try (FileInputStream fis = new FileInputStream(file.toFile())) {
						if (this.offset != null) {
							if (this.offset > 0) {
								fis.getChannel().position(offset);
							}
							try (DataInputStream dis = new DataInputStream(fis)) {
								long offsetInFile = dis.readLong();
								if (offsetInFile != this.offset) {
									return new Validity(Validity.Status.mismatch, "mismatch offset value in file");
								}
								long lengthInFile = dis.readLong();
								if (lengthInFile != this.content.size) {
									return new Validity(Validity.Status.mismatch, "mismatch length value in file");
								}

								long copyCsum = calculateCRC32(dis, this.content.size);
								if (copyCsum == this.content.csum) {
									return new Validity(Validity.Status.OK);
								} else {
									return new Validity(Validity.Status.mismatch);
								}
							}
						} else {
							long copyCsum = calculateCRC32(fis, this.content.size);
							if (copyCsum == this.content.csum) {
								return new Validity(Validity.Status.OK);
							} else {
								return new Validity(Validity.Status.mismatch);
							}
						}
					}
				} catch (Throwable e) {
					return new Validity(Validity.Status.error, "failed to calculate csum");
				}
			}

			private static long calculateCRC32(InputStream in, long length) throws Throwable {
				PluginTask.checkIfThreadTaskAborted();
				CRC32 crc = new CRC32();
				byte[] buffer = new byte[1024];
				long totalBytesRead = 0;
				try (BufferedInputStream bis = new BufferedInputStream(in);
						SizedInputStream sis = new SizedInputStream(bis, length);
						CheckedInputStream cis = new CheckedInputStream(sis, crc)) {
					int bytesRead;
					while ((bytesRead = cis.read(buffer)) != -1) {
						PluginTask.checkIfThreadTaskAborted();
						totalBytesRead += bytesRead;
					}
				}
				if (totalBytesRead != length) {
					throw new IOException("Expects " + length + " bytes read " + totalBytesRead + " bytes");
				}
				return crc.getValue();
			}
		}

		private static class Validity {
			static enum Status {
				OK, mismatch, missing, error
			}

			public final Status status;
			public final String reason;

			public Validity(Status status, String reason) {
				this.status = status;
				this.reason = reason;
			}

			public Validity(Status status) {
				this(status, null);
			}
		}
	}

	private static class AssetStore {
		@SuppressWarnings("unused")
		public final long id;
		public final String name;
		public final String type;
		public final Collection<String> tags;

		AssetStore(XmlDoc.Element se) throws Throwable {
			this.id = se.longValue("@id");
			this.name = se.value("name");
			this.type = se.value("type");
			this.tags = se.values("tag");
		}

		public static List<AssetStore> getAll(ServiceExecutor executor) throws Throwable {
			List<XmlDoc.Element> ses = executor.execute("asset.store.list").elements("store");
			List<AssetStore> ss = new ArrayList<>(ses == null ? 0 : ses.size());
			for (XmlDoc.Element se : ses) {
				ss.add(new AssetStore(se));
			}
			return ss;
		}

		public static void findByTags(List<AssetStore> stores, Collection<String> tags, Set<String> matchedStoreNames) {
			for (String tag : tags) {
				for (AssetStore store : stores) {
					if (store.tags != null && store.tags.contains(tag)) {
						matchedStoreNames.add(store.name);
					}
				}
			}
		}

		public static Map<String, String> getStoreTypes(List<AssetStore> stores) {
			Map<String, String> storeTypes = new HashMap<>();
			for (AssetStore store : stores) {
				storeTypes.put(store.name, store.type);
			}
			return storeTypes;
		}
	}

}
