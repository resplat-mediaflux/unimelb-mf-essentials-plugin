package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetProcessingQueueAllReset extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.processing.queue.all.reset";

	private Interface _defn;

	public SvcAssetProcessingQueueAllReset() {
		_defn = new Interface();
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Resets the pointer to the start for all asset processing queues.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		reset (executor(), true, w);

	}

	public static void reset (ServiceExecutor executor, Boolean suspend, XmlWriter w) throws Throwable {
		XmlDoc.Element r = executor.execute("asset.processing.queue.list");
		if (r==null) {
			return;
		}
		Collection<String> qs = r.values("queue"); 
		if (qs==null) {
			return;
		}
		for (String q : qs ) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", q);
			executor.execute("asset.processing.queue.reset", dm.root());
			w.add("queue", new String[] {"reset", "true"}, q);
		}
	}



	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
