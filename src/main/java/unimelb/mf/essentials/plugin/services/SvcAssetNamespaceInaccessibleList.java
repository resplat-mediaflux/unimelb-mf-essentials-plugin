package unimelb.mf.essentials.plugin.services;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.plugin.util.AssetNamespaceUtils;

public class SvcAssetNamespaceInaccessibleList extends PluginService {
	public static final String SERVICE_NAME = "unimelb.asset.namespace.inaccessible.list";
	private final Interface _defn;

	public SvcAssetNamespaceInaccessibleList() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT,
				"the namespace to scan. If not specified, defaults to the root namespace to scan the whole system.", 0,
				1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Lists the inaccessible asset namespaces caused by name encoding issues.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String root = args.stringValue("namespace", "/");
		List<XmlDoc.Element> nes = listNamespace(executor(), root);
		if (nes != null) {
			for (XmlDoc.Element ne : nes) {
				checkNamespace(executor(), root, ne, w);
			}
		}
	}

	private static void checkNamespace(ServiceExecutor executor, String parentNamespacePath, XmlDoc.Element ne,
			XmlWriter w) throws Throwable {
		String id = ne.value("@id");
		String name = ne.value();
		String path = parentNamespacePath + "/" + name;
		PluginTask.checkIfThreadTaskAborted();
		if (!AssetNamespaceUtils.assetNamespaceExists(executor, path)) {
			w.add("namespace", new String[] { "id", id }, path);
		}
		boolean leaf = ne.booleanValue("@leaf");
		if (!leaf) {
			PluginTask.checkIfThreadTaskAborted();
			List<XmlDoc.Element> cnes = listNamespace(id, executor);
			if (cnes != null) {
				for (XmlDoc.Element cne : cnes) {
					checkNamespace(executor, path, cne, w);
				}
			}
		}
	}

	private static List<XmlDoc.Element> listNamespace(ServiceExecutor executor, String namespacePath) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespacePath);
		return executor.execute("asset.namespace.list", dm.root()).elements("namespace/namespace");
	}

	private static List<XmlDoc.Element> listNamespace(String namespaceId, ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", namespaceId);
		return executor.execute("asset.namespace.list", dm.root()).elements("namespace/namespace");
	}

}
