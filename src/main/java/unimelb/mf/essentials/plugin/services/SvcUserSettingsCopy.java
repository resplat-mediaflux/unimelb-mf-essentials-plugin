package unimelb.mf.essentials.plugin.services;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.atomic.AtomicOperation;
import arc.mf.plugin.atomic.AtomicTransaction;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcUserSettingsCopy extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.settings.copy";

	private Interface _defn;

	public SvcUserSettingsCopy() {
		_defn = new Interface();
		SvcUserProfileCopy.addToDefn(_defn);
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT, "Just list the user settings from  the 'from' source user account. No copying is done.   The default is false (copy).", 0, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Copy (replaces) user asset metadata from one to the other.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		XmlDoc.Element fromUser = args.element("from");
		final XmlDoc.Element toUser = args.element("to");
		final Boolean listOnly = args.booleanValue("list-only",  false);

		new AtomicTransaction(new AtomicOperation() {
			@Override
			public boolean execute(ServiceExecutor executor) throws Throwable {
				copyUserSettings(executor(), fromUser, toUser, listOnly, w);
				return false;
			}
		}).execute(executor());

	}

	static XmlDoc.Element getUserSettings(ServiceExecutor executor, Element user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add(user, false);
		return executor.execute("user.settings.get", dm.root());
	}

	static void copyUserSettings(ServiceExecutor executor, Element fromUser,  Element toUser, Boolean listOnly, XmlWriter w) throws Throwable {
		// Always return the user record, either with or without settings
		w.push("user", new String[] {"domain", fromUser.value("domain"), "user", fromUser.value("user")});
		
		// Get input settings
		XmlDoc.Element fromSettings = getUserSettings(executor, fromUser);
		List<XmlDoc.Element> ses = fromSettings.elements("settings");
		if (ses == null || ses.isEmpty()) {
			w.pop();
			return;
		}
		
		// Copy/list
		for (XmlDoc.Element se : ses) {
			if (se.hasSubElements()) {
				XmlDocMaker dm = new XmlDocMaker("args");
				if (toUser.elementExists("authority")) {
					dm.add(toUser.element("authority"));
				}
				dm.add(toUser.element("domain"));
				dm.add(toUser.element("user"));
				dm.add("app", se.value("@app"));
				dm.push("settings");
				dm.add(se, false);
				dm.pop();
				
				// Copy
				if (!listOnly) {
					executor.execute("user.settings.set", dm.root());
				}
				w.add(se);
			}
		}
		w.pop();
	}


@Override
public String name() {
	return SERVICE_NAME;
}

}