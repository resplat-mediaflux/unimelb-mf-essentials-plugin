package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.XmlUtils;

public class SvcAssetReplicateJobErrorNotify extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.replicate.job.error.notify";

    private Interface _defn;

    public SvcAssetReplicateJobErrorNotify() {
        _defn = new Interface();
        _defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "The recipient email addresses.", 1, 255));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Look for replicate job error and send email notifications.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        Collection<String> emails = args.values("email");

        XmlDoc.Element re = executor().execute("schedule.job.describe");

        List<XmlDoc.Element> jes = re.elements("job[@type='asset.replicate.to']");

        if (jes != null) {
            for (XmlDoc.Element je : jes) {
                if (je.elementExists("replicate/error")) {
                    sendEmails(executor(), je, emails);
                    w.add("job", new String[] { "error", "true", "notified", "true" }, je.value("@name"));
                }
            }
        }
    }

    static void sendEmails(ServiceExecutor executor, XmlDoc.Element je, Collection<String> emails) throws Throwable {
        String subject = subjectFor(executor, je);
        String bodyText = XmlUtils.toIndentedText(je);
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("subject", subject);
        dm.add("body", bodyText);
        for (String email : emails) {
            dm.add("to", email);
        }
        executor.execute("mail.send", dm.root());
    }

    static String subjectFor(ServiceExecutor executor, XmlDoc.Element je) throws Throwable {

        StringBuilder sb = new StringBuilder();
        sb.append("[").append(executor.execute("server.uuid").value("uuid")).append("] ");
        sb.append("Replicate Job '").append(je.value("@name")).append("' error ");
        sb.append("[").append(je.value("replicate/time")).append("]");
        return sb.toString();
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
