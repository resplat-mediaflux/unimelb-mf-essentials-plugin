package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.utils.XmlUtils;
import unimelb.xml.CollectionXmlWriter;
import unimelb.xml.IndentedStringBuilder;
import unimelb.xml.IndentedTextWriter;

public class SvcAssetIndexValidate extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.index.validate";

    public static final int DEFAULT_BATCH_SIZE = 100;

    public static final int MAX_EMAIL_BODY_SIZE = 64000;

    private Interface _defn;

    public SvcAssetIndexValidate() {
        _defn = new Interface();
        _defn.add(new Interface.Element("where", StringType.DEFAULT, "Selection query.", 0, Integer.MAX_VALUE));
        _defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT,
                "If true, then use available indexes. If false, then perform linear searching. Defaults to true.", 0,
                1));
        _defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT,
                "Include assets that have been marked as destroyed? Defaults to false.", 0, 1));
        _defn.add(new Interface.Element("batch-size", IntegerType.POSITIVE_ONE,
                "Query result batch size. Defaults to " + DEFAULT_BATCH_SIZE + ".", 0, 1));
        _defn.add(new Interface.Element("details", BooleanType.DEFAULT,
                "Include index error details of each asset. Defaults to false", 0, 1));
        _defn.add(new Interface.Element("recover", BooleanType.DEFAULT,
                "Recover the issue if possible. It will call asset.index.validate service again with action='recover'. Defaults to false, which will only report the assets with indexing issues.",
                0, 1));

        _defn.add(new Interface.Element("type", StringType.DEFAULT,
                "The non-element/attribute item. [ctime, mtime, dtime, stime, type, csize, ctype, mtext, ctext, atext, ntext, dtext, document, related, tags, tags.dictionary, labels, geoshape, name, model, content.ctime, content.copy.ctime, content.store, content.csum, content.time.acquisition.start, content.time.acquisition.end, time.series.name, time.series.type]",
                0, Integer.MAX_VALUE));

        _defn.add(new Interface.Element("email", EmailAddressType.DEFAULT,
                "Send result (in CSV format) to mail recipients", 0, Integer.MAX_VALUE));
        _defn.add(new Interface.Element("subject", StringType.DEFAULT, "Subject for email.", 0, 1));

    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Checks for index errors by calling asset.index.validate service and optionally recover the asset indexes.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        Boolean useIndexes = args.booleanValue("use-indexes", null);
        Boolean includeDestroyed = args.booleanValue("include-destroyed", null);
        boolean details = args.booleanValue("details", false);
        boolean recover = args.booleanValue("recover", false);
        List<XmlDoc.Element> types = args.elements("type");

        Collection<String> emails = args.values("email");
        boolean sendEmail = emails != null && !emails.isEmpty();
        String subject = args.value("subject");

        File txtFile = sendEmail ? PluginTask.createTemporaryFile("_" + SERVICE_NAME + "_result.txt") : null;
        StringBuilder emailBody = null;
        if (sendEmail) {
            emailBody = new StringBuilder();
            emailBody.append("Dear Administrator,\n\n");
            emailBody.append(String.format("Please see below for the result of service: \n\n> %s\n", SERVICE_NAME));
            emailBody.append(XmlUtils.toIndentedText(args, false, 4, 4));
            emailBody.append("\n\nRESULT:\n");
        }
        IndentedStringBuilder sb = sendEmail ? new IndentedStringBuilder(emailBody, 4, MAX_EMAIL_BODY_SIZE) : null;
        IndentedTextWriter tw = sendEmail ? new IndentedTextWriter(txtFile) : null;
        XmlWriter cw = sendEmail ? new CollectionXmlWriter(w, sb, tw) : w;

        try {

            int size = args.intValue("batch-size", DEFAULT_BATCH_SIZE);
            int idx = 1;

            XmlDocMaker dm = new XmlDocMaker("args");
            if (args.elementExists("where")) {
                dm.addAll(args.elements("where"));
            }
            if (useIndexes != null) {
                dm.add("use-indexes", useIndexes);
            }
            if (includeDestroyed != null) {
                dm.add("include-destroyed", includeDestroyed);
            }
            dm.add("idx", idx);
            dm.add("size", size);
            dm.add("action", "get-id");
            XmlDoc.Element queryArgs = dm.root();

            long nbAssets = 0;
            long nbAssetsWithIndexErrors = 0;
            boolean complete = false;
            while (!complete) {
                PluginTask.checkIfThreadTaskAborted();

                XmlDoc.Element re = executor().execute("asset.query", queryArgs);
                Collection<String> ids = re.values("id");
                if (ids != null) {
                    for (String id : ids) {
                        nbAssets++;
                        XmlDoc.Element e = validate(executor(), id, types);
                        if (e.intValue("index/errors/total", 0) > 0) {
                            nbAssetsWithIndexErrors++;
                            boolean recovered = false;
                            if (recover) {
                                recovered = recover(executor(), id);
                            }
                            cw.push("asset", new String[] { "id", id, "recovered", Boolean.toString(recovered) });
                            if (details) {
                                cw.add(e, false);
                                cw.add("recovered", recovered);
                            }
                            cw.pop();
                        }
                    }
                }
                idx += size;
                queryArgs.element("idx").setValue(Integer.toString(idx));
                complete = re.booleanValue("cursor/total/@complete");
            }
            cw.add("count", new String[] { "total", Long.toString(nbAssets) }, nbAssetsWithIndexErrors);

            if (sendEmail) {
                tw.close();
                if (sb.reachedMaxLength()) {
                    emailBody.append(
                            "\n\nNote: the above result is incomplete. See the attached .txt file for more details.\n");
                }
                sendEmail(executor(), subject, emails, emailBody.toString(), txtFile);
            }
        } finally {

            if (tw != null) {
                tw.close();
            }

            if (txtFile != null) {
                PluginTask.deleteTemporaryFile(txtFile);
            }
        }

    }

    private static void sendEmail(ServiceExecutor executor, String subject, Collection<String> recipients, String body,
            File resultCSVFile) throws Throwable {
        String serverUUID = executor.execute("server.uuid").value("uuid");
        String from = Properties.getServerProperty(executor, "mail.from");
        Date time = new Date();
        String attachmentFileName = String.format("%s_result_%s.txt", SERVICE_NAME,
                new SimpleDateFormat("yyyyMMddHHmmss").format(time));

        if (subject == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(serverUUID).append("] ");
            sb.append(SERVICE_NAME);
            sb.append(" [").append(new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(time)).append("]");
            subject = sb.toString();
        }

        InputStream attachmentInputStream = new BufferedInputStream(new FileInputStream(resultCSVFile));
        try {
            MailUtils.sendEmail(executor, recipients, null, null, from, subject, body, attachmentFileName,
                    attachmentInputStream, resultCSVFile.length(), "plain/text", false);
        } finally {
            attachmentInputStream.close();
        }
    }

    private static XmlDoc.Element validate(ServiceExecutor executor, String id, List<XmlDoc.Element> types)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", id);
        dm.add("action", "status");
        if (types != null && !types.isEmpty()) {
            dm.addAll(types);
        }
        return executor.execute("asset.index.validate", dm.root());
    }

    private static boolean recover(ServiceExecutor executor, String id) throws Throwable {

        // recover
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", id);
        dm.add("action", "recover");

        XmlDoc.Element args = dm.root();
        executor.execute("asset.index.validate", args);

        // validate again
        args.element("action").setValue("status");
        XmlDoc.Element re = executor.execute("asset.index.validate", args);

        boolean recovered = re.intValue("index/errors/total", 0) == 0;
        return recovered;

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
