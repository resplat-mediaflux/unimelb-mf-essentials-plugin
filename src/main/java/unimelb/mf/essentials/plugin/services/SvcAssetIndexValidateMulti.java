package unimelb.mf.essentials.plugin.services;


import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;


public class SvcAssetIndexValidateMulti extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.index.validate.multi";

	private Interface _defn;

	public SvcAssetIndexValidateMulti() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Selection query.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT,
				"If true, then use available indexes. If false, then perform linear searching. Defaults to true.", 0,
				1));
		_defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT,
				"Include assets that have been marked as destroyed? Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("action",
				new EnumType(
						new String[] { "status", "recover"}),
				"The action to perform. Defaults to recover. If recover no output can be returned.", 0, 1));

		_defn.add(new Interface.Element("type", StringType.DEFAULT,
				"The type of index to consider (default is all) [ctime, mtime, dtime, stime, type, csize, ctype, mtext, ctext, atext, ntext, dtext, document, related, tags, tags.dictionary, labels, geoshape, name, model, content.ctime, content.copy.ctime, content.store, content.csum, content.time.acquisition.start, content.time.acquisition.end, time.series.name, time.series.type]",
				0, Integer.MAX_VALUE));

		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT,
				"Send result (in CSV format) to mail recipients", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("subject", StringType.DEFAULT, "Subject for email.", 0, 1));
		_defn.add(new Interface.Element("nb-threads", IntegerType.DEFAULT, "Number of threads to use when replciating assets. Defaults to 1. ", 0, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Checks for index errors by calling asset.index.validate service and optionally recover the asset indexes.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		Boolean useIndexes = args.booleanValue("use-indexes", null);
		Boolean includeDestroyed = args.booleanValue("include-destroyed", null);
		List<XmlDoc.Element> types = args.elements("type");

		Collection<String> emails = args.values("email");
		boolean sendEmail = emails != null && !emails.isEmpty();
		String subject = args.value("subject");
		String action = args.stringValue("action","recover");
		Integer nbThreads = args.intValue("nb-threads", 1);
		//
		long nErrors = 0;
		PluginTask.checkIfThreadTaskAborted();

		XmlDocMaker dm = new XmlDocMaker("args");
		if (args.elementExists("where")) {
			dm.addAll(args.elements("where"));
		}
		if (useIndexes != null) {
			dm.add("use-indexes", useIndexes);
		}
		if (includeDestroyed != null) {
			dm.add("include-destroyed", includeDestroyed);
		}
		dm.add("pipe-generate-result-xml", true);
		dm.add("action", "pipe");
		dm.add("pipe-nb-threads", nbThreads);
		// See ticket 4017. When :action recover there is no output
		dm.push("service", new String[] {"name", "asset.index.validate"});
		dm.add("action", action);
		if (types != null && !types.isEmpty()) {
			dm.addAll(types);
		}
		dm.pop();

		// Execute multi-threaded piped query
		// Only :action status gives a return value
		// but the asset ID is not included. So all 
		// we can do is list the total number of
		// assets with an issue
		// Because you only get a result if there is an issue,
		// and there is rarely an indexing issue, we don't
		// need to iterate through the cursor to fetch results.
		// This should scale to large numbers of assets
		XmlDoc.Element re = executor().execute("asset.query", dm.root());
		Collection<XmlDoc.Element> indexes = re.elements("index");
		if (indexes != null) {
			for (XmlDoc.Element index : indexes) {
				Integer n = index.intValue("errors/total", 0);
				nErrors+= n;
			}
		}


		w.add("errors",  nErrors);

		if (sendEmail) {
			StringBuilder emailBody = null;
			if (sendEmail) {
				emailBody = new StringBuilder();
				emailBody.append("Dear Administrator,\n\n");
				emailBody.append(String.format("Please see below for the result of service: \n\n> %s\n", SERVICE_NAME));
				emailBody.append("There were " + nErrors + " indexing errors");
				emailBody.append("\n\nRegards Mediaflux");
			}

			sendEmail(executor(), subject, emails, emailBody.toString());
		}


	}

	private static void sendEmail(ServiceExecutor executor, String subject, Collection<String> recipients, String body) throws Throwable {
		String serverUUID = executor.execute("server.uuid").value("uuid");
		String from = Properties.getServerProperty(executor, "mail.from");
		Date time = new Date();
		String attachmentFileName = String.format("%s_result_%s.txt", SERVICE_NAME,
				new SimpleDateFormat("yyyyMMddHHmmss").format(time));

		if (subject == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("[").append(serverUUID).append("] ");
			sb.append(SERVICE_NAME);
			sb.append(" [").append(new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(time)).append("]");
			subject = sb.toString();
		}


		MailUtils.sendEmail(executor, recipients, null, null, from, subject, body, null, null, 0, null, false);
	}


	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

}
