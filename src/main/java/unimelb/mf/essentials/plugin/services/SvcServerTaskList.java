package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;

public class SvcServerTaskList extends PluginService {

	public static final String SERVICE_NAME = "unimelb.server.task.list";

	private Interface _defn;

	public SvcServerTaskList() {
		_defn = new Interface();
		_defn.add(new Interface.Element("state", StringType.DEFAULT, "List only the tasks in the specified state(s).",
				0, Integer.MAX_VALUE));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "List server tasks.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		// TODO Auto-generated method stub

	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
