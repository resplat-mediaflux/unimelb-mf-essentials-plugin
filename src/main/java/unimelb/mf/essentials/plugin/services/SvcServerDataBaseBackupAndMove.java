/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.zip.GZIPOutputStream;

import arc.mf.plugin.Exec;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.ServerDetails;
import unimelb.utils.DateUtil;


public class SvcServerDataBaseBackupAndMove extends PluginService {
	private Interface _defn;		
	static private String urlTmp = "/opt/mediaflux/volatile/tmp/";
	static private String urlFinal = "/opt/mediaflux/volatile/database-backup/";

	public SvcServerDataBaseBackupAndMove()  throws Throwable {
		_defn = new Interface();
		//	_defn.add(new Interface.Element("tmp", StringType.DEFAULT, "The temporary url (just specify the directory path) for the backup - must be accessible to the server.", 1, 1));
		//	_defn.add(new Interface.Element("url", StringType.DEFAULT, "The final url (just specify the directory path) for the output - must be accessible to the server.", 1, 1));
		_defn.add(new Interface.Element("retain", IntegerType.DEFAULT, "How many backup files do we maintain in the (final) backup folder. If specified then that number of recent files will be maintained. If 0 is specified only the most recent backup is retained. Defaults to infinite.", 0, 1));
		Interface.Element c = new Interface.Element("compress", BooleanType.DEFAULT, "Compress the exported file. Defaults to false.", 0, 1);
		c.add(new Interface.Attribute("pigz",  BooleanType.DEFAULT, "Use pigz (15 threads) to compress. The default is gzip (1 thread)", 0));
		_defn.add(c);
		_defn.add(new Interface.Element("suffix", StringType.DEFAULT, "Add this suffix string to the file name (default none).  The file will, by default be .dbb (uncompressed) or .dbb.gz (compressed). This suffix is in addition to these.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	public String description() {
		return "Service backs up the data base (to /opt/mediaflux/volatile/tmp, and when that backup is complete, moves it to the end point (/opt/mediaflux/volatile/database-backup. The database file name is constructed for you from the date and time. If discarding older files, the service expects only exported data base files in the final output directory. An exception (after export) will occur if this is not the case.";
	}

	public String name() {
		return "unimelb.server.database.backup.and.move";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		// Destination must exist
		File d = new File(urlFinal);
		if (d.exists()) {
			if (!d.isDirectory()) {
				throw new Exception ("The output path +'" + urlFinal + "' exists but is not a directory");
			}
		} else {
			throw new Exception ("The output path +'" + urlFinal + "' does not exist");
		}


		// XmlDoc.Element compress = args.element("level");
		String retain  = args.value("retain");
		String suffix  = args.value("suffix");
		XmlDoc.Element  compress  = args.element("compress");
		Boolean doCompress = compress!=null && compress.booleanValue();
		Boolean pigz = false;
		if (doCompress) {
			pigz = compress.booleanValue("@pigz", false);
		}

		// Get the date and time
		//  YYYYMMDDhhmmss.SSS
		String dateTime = DateUtil.todaysTime(2);

		String uuid = ServerDetails.serverUUID(executor());
		String hostName = InetAddress.getLocalHost().getHostName();
		String fileName = "scheduled-database-backup_"+uuid+"_"+hostName+"_"+dateTime + ".dbb";
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("url", urlTmp + fileName);

		// Do the uncompressed backup so that we don't lock the
		// process up with the compression (advice from Arcitecta) step
		dm.add("compress", new String[] {"level", "0"}, false); 
		PluginTask.checkIfThreadTaskAborted();
		executor().execute("server.database.backup", dm.root());

		// Now compress the backup post export
		String uncompressedPath = urlTmp + fileName;
		String tmpPath = uncompressedPath;
		if (doCompress) {
			PluginTask.checkIfThreadTaskAborted();

			// To use pigz there must be a symlink to the binary
			// in /opt/mediaflux/plugin/bin/pigz
			String compressedPath = uncompressedPath + ".gz";
			if (pigz) {
				// pigz will add .gz to the file name
				pigz (uncompressedPath);
			} else {			
				gzip (new File(uncompressedPath), new File(compressedPath));

				// Now remove the uncompressed file that gzip leaves behind
				File t = new File(uncompressedPath);
				t.delete();
			}
			//
			tmpPath = compressedPath;
		}

		// Now move the backup to the final location 
		dm = new XmlDocMaker("args");
		dm.add("file", tmpPath);
		String finalPath = null;
		if (doCompress) {
			finalPath = urlFinal + fileName + ".gz";
		} else {
			finalPath = urlFinal + fileName;
		}
		dm.add("to", finalPath);
		executor().execute("server.file.move", dm.root());

		// Now rename the file if there is a suffix. We leave this
		// until after the file is moved to its end point so it is quick
		if (suffix!=null) {
			dm = new XmlDocMaker("args");
			dm.add("file", finalPath);
			dm.add("to", finalPath += suffix);
			executor().execute("server.file.move", dm.root());
		}

		// Get list of files in the final location of the DB backups so we can remove old ones
		File folder = new File(urlFinal);
		File[] listOfFiles = folder.listFiles();

		// Convert to a List
		List<File> files = null;
		Vector<File> files2 = new Vector<File>();
		if (listOfFiles!=null) {
			files = Arrays.asList(listOfFiles);

			// Remove old backups
			if (retain!=null) {
				// Sanity checks since we are going to delete files
				for (File file : files) {
					// Exclude directories - we don't delete those or check their naming
					if (!file.isDirectory()) {

						// Names of the form   scheduled-database-backup-1128_n1-mf1-qh2.storage.unimelb.edu.au_20170813-133202.dbb
						String name = file.getName();
						int n2 = name.length();
						String suffix1 = name.substring(n2-4,n2);
						String suffix2 = name.substring(n2-7,n2);

						// Skip the file in this step if we are adding a suffix.
						// This is because the intent is that an asynchronous  
						// cron job will find files with the suffix
						// and then compress them and remove the suffix.
						// That's when they should be counted
						Boolean skip = suffix!=null && name.endsWith(suffix);
						if (!skip) {

							// This listing should not include any additional
							// added suffix. 
							if (!suffix1.equals(".dbb") && !suffix2.equals(".dbb.gz")) {
								throw new Exception ("The file '" + name + "' does not appear to be an expected Data Base export file (suffix should be .dbb or .dbb.gz");
							}
							String[] parts = name.split("_");
							if (parts.length!=4) {
								throw new Exception ("The file '" + name + "' does not appear to be an expected Data Base export file. Name should split into three parts on '_'");
							}
							String prefix = name.substring(0, 25);
							if (!prefix.equals("scheduled-database-backup")) {
								throw new Exception ("The file '" + name + "' does not appear to be an expected Data Base export file. Root name is not 'scheduled-database-backup'");
							}
							files2.add(file);
						}
					}
				}

				if (files2.size()>0) {
					// Sort in reverse time order so oldest files at the end of the list
					Collections.sort(files2, new Comparator<File>() {
						@Override
						public int compare(File o1, File o2) {
							// Names of the form   scheduled-database-backup_1128_n1-mf1-qh2.storage.unimelb.edu.au_20170813-133202.dbb
							// Names of the form   scheduled-database-backup_1128_n1-mf1-qh2.storage.unimelb.edu.au_20170813-133202.dbb.gz
							String n1 = o1.getName();
							String[]  parts1 = n1.split("_");
							String n2 = o2.getName();
							String[] parts2 = n2.split("_");

							String date1s = parts1[3].substring(0, 8);
							String date2s = parts2[3].substring(0, 8);
							Long date1 = Long.parseLong(date1s);
							Long date2 = Long.parseLong(date2s);
							if (date1<date2) {
								return 1;
							} else if (date1.equals(date2)) {
								Long time1 = Long.parseLong(parts1[3].substring(9, 15));
								Long time2 = Long.parseLong(parts2[3].substring(9, 15));

								if (time1<time2){
									return 1;
								} else if (time1.equals(time2)) {
									return 0;
								} else {
									return -1;
								}
							} else {
								return -1;
							}
						}
					});

					// Discard older files
					int cnt = 1;
					Integer nBackups = Integer.parseInt(retain);
					if (nBackups<=0) {
						throw new Exception ("You must retain at least one backup");
					}
					for (File file2 : files2) {
						if (cnt>nBackups) {
							w.add("file", new String[]{"destroyed", "true"}, file2.getName());
							file2.delete();
						} else {
							w.add("file", new String[]{"destroyed", "false"}, file2.getName());
						}
						cnt++;
					}	
				}
			} else {
				for (File file : files) {
					w.add("file", new String[]{"destroyed", "false"}, file.getName());
				}
			}
		}
	}

	private static String pigz (String path) throws Throwable {
		String t = Exec.exec("pigz","-p 15 " + path);
		return t;
	}

	private static void gzip(File inFile, File outFile) throws IOException {
		try (FileInputStream fis = new FileInputStream(inFile);
				BufferedInputStream bis = new BufferedInputStream(fis)) {
			try (FileOutputStream fos = new FileOutputStream(outFile);
					BufferedOutputStream bos = new BufferedOutputStream(fos);
					GZIPOutputStream gos = new GZIPOutputStream(bos)) {
				byte[] buffer = new byte[8192];
				int n;
				while ((n = bis.read(buffer)) != -1) {
					if (n > 0) {
						gos.write(buffer, 0, n);
					}
				}
				gos.finish();
			}
		}
	}
}
