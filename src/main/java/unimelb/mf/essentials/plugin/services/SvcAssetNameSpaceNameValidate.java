package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.FileNameUtils;
import unimelb.utils.XmlUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPOutputStream;

public class SvcAssetNameSpaceNameValidate extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.namespace.name.validate";

    public static final int MAX_MESSAGE_LENGTH = 60000;

    static class AssetNameSpace {
        public final String path;
        public final long id;
        public final String name;
        public final boolean leaf;

        public AssetNameSpace(String path, long id, String name, boolean leaf) {
            this.path = path;
            this.id = id;
            this.name = name;
            this.leaf = leaf;
        }

        public boolean hasValidName() {
            return "/".equals(this.path) ? true : FileNameUtils.isValidFileName(this.name);
        }
    }

    static interface AsssetNameSpaceHandler {
        void handle(AssetNameSpace ans) throws Throwable;
    }

    static interface AssetNameSpaceVisitor {
        void visit(AssetNameSpace ans) throws Throwable;
    }

    private Interface _defn;

    public SvcAssetNameSpaceNameValidate() {
        _defn = new Interface();
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT, "Namespace path.", 1, 1));
        _defn.add(new Interface.Element("recursive", BooleanType.DEFAULT,
                "Validate the subnamespaces recursively. Defaults to true. ", 0, 1));
        _defn.add(new Interface.Element("compress", BooleanType.DEFAULT,
                "Compress the output CSV file to GZIP format. Defaults to false.", 0, 1));
        Interface.Element notify = new Interface.Element("notify", XmlDocType.DEFAULT,
                "Notify of the result via emails.", 0, 1);
        notify.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Recipient to receive the result.", 1, 10));
        _defn.add(notify);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Validate the names of the specified asset namespace and its sub-namespaces.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String ns = args.value("namespace");
        boolean recursive = args.booleanValue("recursive", true);

        Collection<String> notifyEmails = args.values("notify/email");
        boolean notify = notifyEmails != null && !notifyEmails.isEmpty();
        StringBuilder msg = notify ? new StringBuilder() : null;
        if (msg != null) {
            msg.append("---\n");
            msg.append(SERVICE_NAME).append("\n");
            msg.append(XmlUtils.toIndentedText(args, false, 4, 4));
            msg.append("---\n\n\n");
        }

        PluginService.Output output = (outputs != null && outputs.size() > 0) ? outputs.output(0) : null;
        boolean compress = args.booleanValue("compress", false);
        String outputMimeType = compress ? "application/gzip" : "text/csv";
        String outputFileExt = compress ? "csv.gz" : "csv";
        File outputFile = (output == null && !notify) ? null : PluginTask.createTemporaryFile("." + outputFileExt);

        AtomicLong nbInvalid = new AtomicLong(0);

        try {
            validateAssetNameSpace(executor(), ns, recursive, w, output, outputFile, compress, notify, msg, nbInvalid);
            if (notify) {
                String subject = String.format("%s: %s", executor().execute("server.uuid").value("uuid"), SERVICE_NAME);
                String fileName = "invalid-asset-namespace-names." + outputFileExt;
                SvcAssetNameValidate.notify(executor(), subject, msg.toString(), outputFile, fileName, outputMimeType,
                        notifyEmails);
            }
            if (output != null) {
                output.setData(PluginTask.deleteOnCloseInputStream(outputFile), outputFile.length(), outputMimeType);
            } else {
                if (outputFile != null && outputFile.exists()) {
                    PluginTask.deleteTemporaryFile(outputFile);
                }
            }
        } catch (Throwable e) {
            if (outputFile != null && outputFile.exists()) {
                PluginTask.deleteTemporaryFile(outputFile);
            }
            throw e;
        }

    }

    public int maxNumberOfOutputs() {
        return 1;
    }

    public boolean canBeAborted() {
        return true;
    }

    private static void validateAssetNameSpace(ServiceExecutor executor, String parentAnsPath,
                                               boolean recursive, XmlWriter w, PluginService.Output output,
                                               File outputFile, boolean compress, boolean notify, StringBuilder msg,
                                               AtomicLong nbInvalid) throws Throwable {

        PrintStream outputPS = (output == null && !notify) ? null : new PrintStream(
                compress ? new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile))) :
                        new BufferedOutputStream(new FileOutputStream(outputFile)));
        try {
            if (outputPS != null) {
                outputPS.println("ID,Path,Name");
            }

            validateAssetNameSpace(executor, parentAnsPath, recursive, new AsssetNameSpaceHandler() {
                @Override
                public void handle(AssetNameSpace ans) throws Throwable {
                    w.add("invalid", new String[]{"path", ans.path, "id", Long.toString(ans.id)}, ans.name);
                    if (outputPS != null) {
                        outputPS.println(String.format("%d,\"%s\",\"%s\"", ans.id, ans.path, ans.name));
                    }
                    if (msg != null) {
                        if (msg.length() < MAX_MESSAGE_LENGTH) {
                            msg.append(
                                    String.format("    :invalid -id %s -path \"%s\" \"%s\"\n", ans.id, ans.path,
                                            ans.name));
                        }
                    }
                    nbInvalid.getAndIncrement();
                }
            });
            w.add("count", nbInvalid.get());
            if (msg != null) {
                if (msg.length() > +MAX_MESSAGE_LENGTH) {
                    msg.append("    ... ... ...\n");
                }
                msg.append(String.format("    :count %d", nbInvalid.get())).append("\n");
            }
        } finally {
            if (outputPS != null) {
                outputPS.close();
            }
        }
    }

    static void validateAssetNameSpace(ServiceExecutor executor, String parentAnsPath,
                                       boolean recursive, AsssetNameSpaceHandler nsh) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", parentAnsPath);
        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element parentAnse = executor.execute("asset.namespace.describe", dm.root()).element("namespace");
        AssetNameSpace parentAns = new AssetNameSpace(parentAnse.value("path"), parentAnse.longValue("@id"),
                parentAnse.value("name"), parentAnse.elementExists("namespace"));
        if (!parentAns.hasValidName()) {
            nsh.handle(parentAns);
        }
        if (recursive) {
            walkAssetNameSpaceTree(executor, parentAnsPath, ans -> {
                if (!ans.hasValidName()) {
                    nsh.handle(ans);
                }
            });
        }
    }


    private static void walkAssetNameSpaceTree(ServiceExecutor executor, String parentAnsPath,
                                               AssetNameSpaceVisitor nsv) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", parentAnsPath);
        dm.add("assets", false);
        PluginTask.checkIfThreadTaskAborted();
        List<XmlDoc.Element> anses = executor.execute("asset.namespace.list",
                dm.root()).elements("namespace/namespace");

        if (anses != null && !anses.isEmpty()) {
            for (XmlDoc.Element anse : anses) {
                long id = anse.longValue("@id");
                String name = anse.value();
                String path = String.join("/".equals(parentAnsPath) ? "" : "/", parentAnsPath, name);
                boolean leaf = anse.booleanValue("@leaf");
                AssetNameSpace ans = new AssetNameSpace(path, id, name, leaf);
                nsv.visit(ans);
                if (!ans.leaf) {
                    walkAssetNameSpaceTree(executor, ans.path, nsv);
                }
            }
        }
    }

}
