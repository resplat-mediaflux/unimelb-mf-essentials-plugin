package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceMetaDataFind extends PluginService {


	private Interface _defn;
	private long _ns = 0l;      // Number of namespaces with the specified meta-data
	private long _na = 0l;      // Number of namespaces with any meta-data
	private long _nt = 0l;      // Total number of namespaces traversed 


	public SvcAssetNameSpaceMetaDataFind() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The parent namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("nlevels", IntegerType.DEFAULT, "If recurse=true (ignored if false) only recurse this many levels, default infinity.", 0, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "Recurse down the whole namespace tree (the listing is simplified in that the specific ACL is not shown, just that it has one). Default is false (just inspect immediate children namespaces).", 0, 1));
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "The document type to look for. If not given, will find any type.", 0, 1));
		_defn.add(new Interface.Element("path", StringType.DEFAULT, "The top-level element path to look for in the given document type. If none, just looks for the document type.", 0, 1));
	}



	public String name() {
		return "unimelb.asset.namespace.metadata.find";
	}

	public String description() {
		return "Find all child namespaces of the given namespace that hold meta-data of the given type.  Can recurse down the tree if desired.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}



	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		String type = args.stringValue("type");
		String path = args.value("path");
		int nLevels = args.intValue("nlevels",-1);
		Boolean recurse = args.booleanValue("recurse", false);
		//
		_ns = 0;
		_na = 0;
		_nt = 0;
		Integer parentDepth = NameSpaceUtil.assetNameSpaceDepth(executor(), namespace);
		list (executor(), namespace, type, path, recurse, parentDepth, nLevels, w);	
		w.push("namespace-count");
		w.add("total-number-traversed", _nt);
		w.add("number-with-any-metadata", _na);
		w.add("number-with-specified-metadata", _ns);
		w.pop();
	}



	private void list (ServiceExecutor executor, String nameSpace, String type, String path,
			Boolean recurse, int parentDepth, int nLevels, XmlWriter w) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Iterate over children namespaces
		Collection<String> nss = NameSpaceUtil.listNameSpaces(executor, nameSpace, true);
		if (nss==null) {
			return;
		}
		for (String ns : nss) {
			_nt++;
			int depth = NameSpaceUtil.assetNameSpaceDepth(executor, ns);
			int diff = depth - parentDepth; 

			// If we have reached our maximum depth, bug out
			if (nLevels>0 && diff>nLevels) {
				return;
			} else {
				XmlDoc.Element meta = NameSpaceUtil.describe(null, executor(), ns);
				XmlDoc.Element assetMeta = meta.element("namespace/asset-meta");
				if (assetMeta!=null) {
					_na++;
					Collection<XmlDoc.Element> docs = assetMeta.elements();
					if (docs!=null) {
						Boolean some = false;
						for (XmlDoc.Element doc : docs) {
							String t =  doc.qname();
							if (type==null || (type!=null&&type.equals(t))) {
								some = true;
								if (path==null) {
									w.add("namespace", new String[] {"type", t}, meta.value("namespace/path"));
								} else {
									Collection<XmlDoc.Element> paths = doc.elements();
									if (paths!=null) {
										for (XmlDoc.Element path0 : paths) {
											String p = path0.qname();
											if (p.equals(path)) {
												w.add("namespace", new String[] {"type", t}, meta.value("namespace/path"));	
											}
										}
									}
								}
							}
						}
						if (some) {
							_ns++;
						}
					}	
				}
			} 

			// Descend into child namespace
			if (recurse) {
				list (executor, ns, type, path, recurse, parentDepth, nLevels, w);
			}
		}
	}
}
