/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;

public class SvcServerPeerStatus extends PluginService {
	private Interface _defn;		
	public SvcServerPeerStatus()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "Peer name.", 1, 1));
		_defn.add(new Interface.Element("to", IntegerType.DEFAULT, "Execute the server status repeatedly for this amount of time in minutes. Defaults to just one execution.", 0, 1));
		_defn.add(new Interface.Element("interval", IntegerType.DEFAULT, "The interval in seconds between executions.  Required if 'to' is set.", 0, 1));
		_defn.add(new Interface.Element("force-check", BooleanType.DEFAULT, "Force a reachability check now (default false).", 0, 1));
		_defn.add(new Interface.Element("fail-only", BooleanType.DEFAULT, "Only show failures (default true).", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service that returns 1 (peer available) or -1 (peer not available).  Forces a recheck of the peer.";
	}

	public String name() {
		return "unimelb.server.peer.status";
	}

	public boolean canBeAborted() {
		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		// Parse
		String name = args.value("name");
		Integer interval = args.intValue("interval",-1);
		Integer tod = args.intValue("to",-1);
		Boolean check = args.booleanValue("force-check",  false);
		Boolean failOnly = args.booleanValue("fail-only",  true);
		//
		Date now = new Date();
		Date to = null;
		Long intMS = 0l;
		if (tod>0) {
			to = DateUtil.addMinutes(now,tod);
			//
			intMS = interval * 1000l;
			if (intMS<=0) {
				throw new Exception("'interval' must be a positive integer.");
			}
			w.add("to", to);
		}

		// Execute
		Boolean more = true;
		while (more) {
			PluginTask.checkIfThreadTaskAborted();
			ping(executor(), name, check, failOnly, w);
			if (to==null) {
				// 1 execution only
				more = false;
			} else {
				now = new Date();
				if (now.after(to)) {
					more = false;
				}
				Thread.sleep(intMS);
			}
		}
	}

	private void ping (ServiceExecutor executor, String peerName, 
			Boolean check, Boolean failOnly, XmlWriter w) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", peerName);
		dm.add("check-peer-is-reachable", check);
		Date now = new Date();
		XmlDoc.Element r = executor().execute("server.peer.status", dm.root());
		String status = r.value("peer/status");
		String nowS = DateUtil.formatDate(now, true, false);
		if (status==null) {
			w.add("status", new String[] {"date", nowS}, "-1");
		} else if (status.equals("reachable")) {
			if (!failOnly) {
				w.add("status", new String[] {"date", nowS}, "1");
			}
		}  else {
			w.add("status", new String[] {"date", nowS}, "-1");
		}
	}
}
