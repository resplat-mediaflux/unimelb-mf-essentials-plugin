package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.FileSizeUtils;

public class SvcAssetNameSpaceChildSum extends PluginService {


	private Interface _defn;

	public SvcAssetNameSpaceChildSum() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 0, 1));
		_defn.add(new Interface.Element("children", BooleanType.DEFAULT, "If true, sums up recursively only the children namespaces and presents the results for each.  If false (default), just recursively sums the given namespace and presents that.", 0, 1));
	}
	public String name() {
		return "unimelb.asset.namespace.child.sum";
	}

	public String description() {
		return "Lists the sums of content in all child namespaces in the local server.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace = args.value("namespace");
		Boolean showChildren = args.booleanValue("children");

		if (showChildren) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("namespace", namespace);
			XmlDoc.Element r = executor().execute("asset.namespace.list", dm.root());
			if (r==null) return;
			Collection<String> nss = r.values("namespace/namespace");
			String path = r.value("namespace/@path");
			if (nss==null) return;
			w.push("namespace", new String[]{"path", namespace});
			for (String ns : nss) {
				PluginTask.checkIfThreadTaskAborted();
				String fullPath = path + "/" + ns;
				XmlDocMaker dm2 = new XmlDocMaker("args");
				dm2.add("where", "namespace>='" + fullPath + "'");
				dm2.add("action", "sum");
				dm2.add("xpath", "content/size");
				dm2.add("size", "infinity");
				dm2.add("pdist", 0);
				XmlDoc.Element r2 = executor().execute("asset.query", dm2.root());
				if (r2!=null) {
					String n = r2.value("value/@nbe");
					String s = r2.value("value");
					w.add("namespace", new String[]{"nbe", n, "sum-bytes", s}, ns);
				}
			}
			w.pop();
		} else {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("where", "namespace>='"+namespace+"'");
			dm.add("action", "count");
			dm.add("pdist", "0");
			dm.add("size", "infinity");
			XmlDoc.Element r = executor().execute("asset.query", dm.root());
			String nAssets = r.value("value");
			//
		    dm = new XmlDocMaker("args");
			dm.add("where", "namespace>='"+namespace+"'");
			dm.add("action", "sum");
			dm.add("xpath", "content/size");
			dm.add("pdist", "0");
			dm.add("size", "infinity");
			r = executor().execute("asset.query", dm.root());
			String nbe = r.value("value/@nbe");
			String bytesS = r.value("value");
			long bytes = 0l;
			if (bytesS!=null) {
			   bytes = Long.parseLong(bytesS);
			}			
			String human = FileSizeUtils.toHumanReadable(bytes);
			w.add("path", namespace);
			w.add("number-assets", nAssets);
			w.add("number-assets-with-content", nbe);
			w.push("sum");
			w.add("bytes", bytes);
			w.add("human", human);
			w.pop();
		}
	}
}
