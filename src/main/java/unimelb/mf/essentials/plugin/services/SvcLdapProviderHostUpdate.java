package unimelb.mf.essentials.plugin.services;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.NetUtils;

public class SvcLdapProviderHostUpdate extends PluginService {

	public static final String SERVICE_NAME = "unimelb.ldap.provider.host.update";

	private Interface _defn;

	public SvcLdapProviderHostUpdate() {
		_defn = new Interface();
		Interface.Element provider = new Interface.Element("provider", StringType.DEFAULT,
				"The name of the LDAP provider.", 1, Integer.MAX_VALUE);
		provider.add(new Interface.Attribute("domain", StringType.DEFAULT,
				"Domain name for DNS lookup. If not specified, use the provider's realm value if available.", 0));
		_defn.add(provider);

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Update LDAP provider hosts by looking up SRV record of the LDAP server. It calls ldap.provider.modify service to update the hosts.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		List<XmlDoc.Element> providers = args.elements("provider");
		for (XmlDoc.Element p : providers) {
			String provider = p.value();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("provider", provider);
			XmlDoc.Element pe = executor().execute("ldap.provider.describe", dm.root()).element("provider");
			String domain = p.stringValue("@domain", pe.value("realm").toLowerCase());
			int port = pe.intValue("port");
			List<String> hosts = null;
			if (port == NetUtils.GC_PORT || port == NetUtils.GCS_PORT) {
				hosts = NetUtils.getAliveGcHosts(domain, port);
			} else if (port == NetUtils.GC_PORT || port == NetUtils.GCS_PORT) {
				hosts = NetUtils.getAliveLdapHosts(domain, port);
			} else {
				// non-standard port???
				hosts = NetUtils.getAliveLdapHosts(domain, port);
			}
			if (hosts == null || hosts.isEmpty()) {
				// Unable to resolve LDAP hosts or the hosts are not alive.
				w.add("updated", new String[] { "provider", provider, "reason",
						"unable to resolve hosts from DNS SRV record or the hosts are not alive" }, false);
				continue;
			}

			// Update hosts.
			dm = new XmlDocMaker("args");
			dm.add("provider", provider);
			for (String host : hosts) {
				dm.add("host", host);
			}
			executor().execute("ldap.provider.modify", dm.root());
			w.add("updated", new String[] { "provider", provider }, true);
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
