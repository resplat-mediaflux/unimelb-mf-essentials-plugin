package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.utils.DateUtil;


public class SvcLicenceUsage extends PluginService {


	public static final String LICENCE_USAGE_ASSET_NAME = "LicenceUsage";
	public static final String LICENCE_USAGE_DOCTYPE =  "unimelb:licence-usage";
	public static final String MAX_VERSIONS = "10";    // Number of versions of asset


	private Interface _defn;

	public SvcLicenceUsage()   {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "Namespace to locate the tracking asset in.", 1, 1));
		_defn.add(new Interface.Element("date", StringType.DEFAULT, "A fake date for testing in the form dd-MMM-YYYY.", 0, 1));
		_defn.add(new Interface.Element("nused", IntegerType.DEFAULT, "A fake number of licenses used for testing.", 0, 1));
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "Name for asset. Defaults to 'LicenceUsage'.", 0, 1));
		_defn.add(new Interface.Element("actor", StringType.DEFAULT, "Ony measure licence usage for this actor. Defaults to all actors.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;

	}

	public String description() {
		return "Track licence usage.  An asset is created which tracks by date, the maximum number of licences used so far.   Schedule a job to run every 15 minutes to update this asset with this service.";
	}

	public String name() {
		return "unimelb.licence.usage";
	}

	public boolean canBeAborted() {
		return false;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}



	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String date  = args.value("date");
		if (date==null) date = DateUtil.todaysDate(2);
		//
		String nUsedS = args.value("nused");
		String nameSpace = args.value("namespace");
		//
		String name = args.stringValue("name", "LicenceUsage");
		String actor = args.value("actor");

		// FInd the licence usage asset
		if (name==null) {
			name = LICENCE_USAGE_ASSET_NAME;
		}
		String id = findLicenceUsageAsset(executor(), nameSpace, name);
		if (id==null) {
			id = createLicenceUsageAsset (executor(), nameSpace, name);
		}

		// Get the asset
		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);

		// Get the meta-data 
		XmlDoc.Element meta = asset.element("asset/meta/" + LICENCE_USAGE_DOCTYPE);

		// See if today is there already in the asset
		XmlDoc.Element usedToday = null;
		if (meta!=null) {
			usedToday = meta.element("max-used[@date='"+date+"']");
		}

		// FInd current license usage. If we need to filter by actor, use a
		// more fine-grained service.
		Integer nUsed = null;
		if (actor==null) {
			XmlDoc.Element r = executor().execute("licence.describe");
			Collection<XmlDoc.Element>licences = r.elements("licence");
			for (XmlDoc.Element licence : licences) {
				String application = licence.value("application");
				if (application.equalsIgnoreCase("MEDIAFLUX")) {
					String total = licence.value("total");
					String remaining = licence.value("remaining");
					if (nUsedS==null) {
						nUsed = Integer.parseInt(total) - Integer.parseInt(remaining);
					} else {
						nUsed = Integer.parseInt(nUsedS);
					}
				}
			}
		} else {
			XmlDoc.Element r = executor().execute("licence.holder.list");
			Collection<XmlDoc.Element> holders = r.elements("holder");
			nUsed = 0;
			for (XmlDoc.Element holder : holders) {
				String theActor = holder.value("@actor");
				if (theActor!=null) {
					if (theActor.equals(actor)) {
						nUsed++;		
					}
				}
			}
		}
		w.add("nused", nUsed);
		w.add("id", id);
		w.add("date", date);

		// Update licence usage asset
		Integer max = null;
		if (usedToday!=null) {
			Integer oldMax = Integer.parseInt(usedToday.value());
			max =  java.lang.Math.max(oldMax,nUsed);
			//
			w.add("max", new String[]{"old", ""+oldMax}, max);
			if (max>oldMax) {
				usedToday.setValue(max);

				// Update
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("id", id);
				dm.add("max-versions", MAX_VERSIONS);
				dm.push("meta");
				removeAttribute(meta, "id");
				dm.add(meta);
				dm.pop();
				executor().execute("asset.set", dm.root());
			}
		} else {
			max = nUsed;
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", id);
			dm.add("max-versions", MAX_VERSIONS);
			dm.push("meta");
			dm.push(LICENCE_USAGE_DOCTYPE);
			dm.add("max-used", new String[]{"date", date}, max);
			dm.pop();
			dm.pop();
			executor().execute("asset.set", dm.root());
			w.add("max", max);
		}
	}


	public static String findLicenceUsageAsset (ServiceExecutor executor, String nameSpace, 
			String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("pdist", "0");
		//
		String where = "namespace='" + nameSpace + 
				"' and name='" + name + "'";
		dm.add("where", where);
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) return null;
		return r.value("id");
	}


	private String createLicenceUsageAsset (ServiceExecutor executor, String nameSpace, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");

		dm.add("name",name);
		dm.add("namespace", nameSpace);
		XmlDoc.Element r = executor.execute("asset.create", dm.root());
		return r.value("id");
	}

	private void removeAttribute (XmlDoc.Element doc, String attributeName) throws Throwable {
		XmlDoc.Attribute attr = doc.attribute(attributeName);
		if (attr != null) {
			doc.remove(attr);
		}

	}

}
