/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


// TBD : read user resourcing asset as input instead of command line args,


import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcServerAuditIPQuery extends PluginService {
	private Interface _defn;		
	public SvcServerAuditIPQuery()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("IP", StringType.DEFAULT, "IP string to query for (starts with query).", 1, 1));
		_defn.add(new Interface.Element("from", DateType.DEFAULT, "Start date of query (defaults to first).", 0, 1));
		_defn.add(new Interface.Element("to", DateType.DEFAULT, "End date of query (defaults to last).", 0, 1));
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "Audit event type. Defaults to all types.", 0, Integer.MAX_VALUE));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service to query audit records by IP.";
	}

	public String name() {
		return "unimelb.server.audit.IP.query";
	}

	public boolean canBeAborted() {
		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		String IP  = args.value("IP");
		Collection<String> types  = args.values("type");
		Date from = args.dateValue("from");
		Date to = args.dateValue("to");

		XmlDocMaker dm = new XmlDocMaker("args");
		if (from!=null) {
			dm.add("from", from);
		}
		if (to!=null) {
			dm.add("to", to);
		}
		if (types!=null) {
			for (String type : types) {
				dm.add("type", type);
			}
		}
		XmlDoc.Element r = executor().execute("audit.query", dm.root());
		if (r==null) {
			return;
		}
		Collection<XmlDoc.Element> events = r.elements("event");
		for (XmlDoc.Element event : events) {
			String theIP = event.value("ip-address");
			if (theIP.startsWith(IP)) {
				w.add(event);
			}	
		} 
	}


}
