package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.CollectionAssetUtil;
import unimelb.utils.FileSizeUtils;

public class SvcCollectionChildSum extends PluginService {


	private Interface _defn;

	public SvcCollectionChildSum() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", StringType.DEFAULT, " The identity of a collection asset to sum content.", 1, 1));
		_defn.add(new Interface.Element("children", BooleanType.DEFAULT, "If true, sums up recursively only the children collections  and presents the results for each.  If false (default), just recursively sums the given collection asset and presents that.", 0, 1));
	}
	public String name() {
		return "unimelb.collection.child.sum";
	}

	public String description() {
		return "Lists the counts and content sum (only finds plain assets, not collection assets themselves) in the given collection asset.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String assetID = args.value("id");
		Boolean showChildren = args.booleanValue("children");

		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, assetID);
		w.push("collection", new String[]{"id", assetID, "path", asset.value("asset/path")});

		if (showChildren) {
			Collection<String> ids = CollectionAssetUtil.listDirectChildAssetIDs(executor(), assetID);
			for (String id : ids) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element r2 = CollectionAssetUtil.sumAssetContent(executor(), id, null);
				if (r2!=null) {
					String n = r2.value("value/@nbe");
					String bytesS = r2.value("value");
					XmlDoc.Element r = AssetUtils.getAsset(executor(), null, null, id);
					//
					long bytes = 0l;
					if (bytesS!=null) {
					   bytes = Long.parseLong(bytesS);
					} else {
						bytesS = "0";
					}
					String human = FileSizeUtils.toHumanReadable(bytes);
					w.add("collection", new String[]{"nbe", n, "sum-bytes", bytesS, "sum-bytes-human", human, "id", id}, r.value("asset/path"));
				}
			}
		} else {
			XmlDocMaker dm = new XmlDocMaker("args");
			CollectionAssetUtil.makeQueryForPlainAssets(assetID, null, null, dm);
			dm.add("action", "count");
			dm.add("pdist", "0");
			dm.add("size", "infinity");
			XmlDoc.Element r = executor().execute("asset.query", dm.root());
			String nAssets = r.value("value");
			//
		    dm = new XmlDocMaker("args");
			CollectionAssetUtil.makeQueryForPlainAssets(assetID, null, null, dm);
			dm.add("action", "sum");
			dm.add("xpath", "content/size");
			dm.add("pdist", "0");
			dm.add("size", "infinity");
			r = executor().execute("asset.query", dm.root());
			String nbe = r.value("value/@nbe");
			String bytesS = r.value("value");
			long bytes = 0l;
			if (bytesS!=null) {
			   bytes = Long.parseLong(bytesS);
			}			
			String human = FileSizeUtils.toHumanReadable(bytes);
			w.add("number-assets", nAssets);
			w.add("number-assets-with-content", nbe);
			w.push("sum");
			w.add("bytes", bytes);
			w.add("human", human);
		}
		w.pop();
	}
}
