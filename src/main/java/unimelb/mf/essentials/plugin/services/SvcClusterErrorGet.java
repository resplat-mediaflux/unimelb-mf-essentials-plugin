package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.*;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class SvcClusterErrorGet extends PluginService {

    public final String SERVICE_NAME = "unimelb.cluster.error.get";

    private static class ClusterNodeError {
        public final String uuid;
        public final long nbErrors;
        public final long nbExecutionsSinceError;
        public final String errorContext;
        public final String lastErrorMsg;
        public final long lastErrorId;
        public final long lastErrorTime;
        public final long cioReadBytes;
        public final long cioReadMilliSecs;

        public ClusterNodeError(XmlDoc.Element ne) throws Throwable {
            this.uuid = ne.value("@uuid");
            this.nbErrors = ne.longValue("errors/nb-errors", 0L);
            this.nbExecutionsSinceError = ne.longValue("errors/nb-executions-since-error", 0);
            this.errorContext = ne.value("errors/error-context");
            this.lastErrorMsg = ne.value("errors/last-error");
            long readBytes = -1;
            long readMilliSecs = -1;
            if (this.lastErrorMsg != null) {
                if (this.lastErrorMsg.contains("Clustered file write: I/O error")) {
                    int idx1 = lastErrorMsg.indexOf("reading from input stream after: ");
                    int idx2 = lastErrorMsg.indexOf(" read time: ");
                    int idx3 = lastErrorMsg.indexOf(" millisec(s)");
                    if (idx1 > 0 && idx2 > 0 && idx3 > 0) {
                        readBytes = Long.parseLong(lastErrorMsg.substring(idx1 + 33, idx2));
                        readMilliSecs = Long.parseLong(lastErrorMsg.substring(idx2 + 12, idx3));
                    }
                }
            }
            this.lastErrorId = ne.longValue("errors/last-error/@nb", -1);
            this.lastErrorTime = ne.longValue("errors/last-error-time/@time", -1);
            this.cioReadMilliSecs = readMilliSecs;
            this.cioReadBytes = readBytes;
        }


        public void saveXml(XmlWriter w) throws Throwable {

            w.push("node", new String[]{"uuid", this.uuid});

            w.add("nb-errors", this.nbErrors);

            if (this.lastErrorMsg != null) {
                w.add("nb-executions-since-error", this.nbExecutionsSinceError);
            }

            if (this.errorContext != null) {
                w.add("error-context", this.errorContext);
            }

            if (lastErrorMsg != null) {
                w.add("last-error", new String[]{
                        "nb", this.lastErrorId >= 0 ? Long.toString(lastErrorId) : null,
                        "time", this.lastErrorTime >= 0 ? DateTime.string(new Date(this.lastErrorTime)) : null,
                        "read-bytes", this.cioReadBytes >= 0 ? Long.toString(this.cioReadBytes) : null,
                        "read-time", this.cioReadMilliSecs >= 0 ? Long.toString(this.cioReadMilliSecs) : null,
                });
            }

            w.pop();
        }


        public static void saveHtmlTable(Collection<ClusterNodeError> errors, StringBuilder sb) {
            sb.append("<table>\n");
            sb.append("<thead>\n");
            sb.append("<tr><th>Node</th><th>Number of errors</th><th>Number of executions since last error</th><th>Last error</th></tr>");
            sb.append("</thead>\n");
            sb.append("<tbody>\n");
            for (ClusterNodeError e : errors) {
                e.saveHtmlTableRow(sb);
            }
            sb.append("</tbody>\n");
            sb.append("</table>\n");
        }

        public void saveHtmlTableRow(StringBuilder table) {
            table.append("<tr>");

            table.append("<td style=\"text-align:right\">").append(this.uuid).append("</td>");

            table.append("<td style=\"text-align:right\">").append(this.nbErrors).append("</td>");

            table.append("<td style=\"text-align:right\">");
            if (this.nbExecutionsSinceError > 0) {
                table.append(this.nbExecutionsSinceError);
            } else {
                table.append("&nbsp;");
            }
            table.append("</td>");

            if (this.lastErrorMsg != null) {
                int idx = this.lastErrorMsg.indexOf('\n');
                String firstLine = idx > 0 ? this.lastErrorMsg.substring(0, idx) : this.lastErrorMsg;
                table.append("<td>").append(String.format("[%s id=%s context=%s] %s", DateTime.string(new Date(this.lastErrorTime)), this.lastErrorId, this.errorContext, firstLine)).append("</td>");
            }

            table.append("</tr>\n");

        }


        public boolean matches(Collection<String> incFilters, Collection<String> excFilters) {
            if (this.lastErrorMsg == null) {
                return false;
            }
            if ((incFilters == null || incFilters.isEmpty()) && (excFilters == null || excFilters.isEmpty())) {
                return true;
            }
            boolean match;
            if (incFilters != null && !incFilters.isEmpty()) {
                match = false;
                for (String inc : incFilters) {
                    if (lastErrorMsg.contains(inc)) {
                        match = true;
                        break;
                    }
                }
                if (match && excFilters != null) {
                    for (String exc : excFilters) {
                        if (lastErrorMsg.contains(exc)) {
                            match = false;
                            break;
                        }
                    }
                }
            } else {
                match = true;
                for (String exc : excFilters) {
                    if (lastErrorMsg.contains(exc)) {
                        match = false;
                        break;
                    }
                }
            }
            return match;
        }

    }


    private final Interface _defn;

    public SvcClusterErrorGet() {
        _defn = new Interface();

        Interface.Element notify = new Interface.Element("notify", XmlDocType.DEFAULT, "send email notifications.", 0, 1);

        notify.add(new Interface.Attribute("ignore-if-no-error", BooleanType.DEFAULT, "Ignore if there is no (matched) error. Defaults to true.", 0));

        notify.add(new Interface.Element("to", EmailAddressType.DEFAULT, "Recipient's email address.", 1, 10));

        notify.add(new Interface.Element("contains", StringType.DEFAULT, "Inclusive filter. If specified, the notification will be sent only if the error message contains the specified string.", 0, 100));

        notify.add(new Interface.Element("contains-no", StringType.DEFAULT, "Exclusive filter. If specified, the notification will be sent only if the error message does not contain the specified string. This has higher order than inclusive filter.", 0, 100));

        _defn.add(notify);

        _defn.add(new Interface.Element("expected-nb-included-nodes", IntegerType.POSITIVE, "Expected number of included cluster nodes (including both controller and I/O nodes). If the actual number of included nodes is less than the specified number, it will report as en error. Defaults to 0, which does not check.", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Get the latest cluster error by calling cluster.describe service. Optinally send email notifications.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        int expectedNbIncludedNodes = args.intValue("expected-nb-included-nodes", 0);

        Collection<String> recipients = args.values("notify/to");
        Collection<String> contains = args.values("notify/contains");
        Collection<String> containsNo = args.values("notify/contains-no");
        boolean ignoreIfNoError = args.booleanValue("notify/@ignore-if-no-error", true);

        boolean notify = recipients != null && !recipients.isEmpty();

        XmlDoc.Element re = executor().execute("cluster.describe");
        int nbNodes = re.intValue("count");
        int nbIncludedNodes = re.count("node[@included='true']");
        int nbExcludedNodes = nbNodes - nbIncludedNodes;
        int nbMissingNodes = (expectedNbIncludedNodes > 0 && expectedNbIncludedNodes > nbIncludedNodes) ? (expectedNbIncludedNodes - nbIncludedNodes) : 0;

        List<XmlDoc.Element> nes = re.elements("node");
        List<ClusterNodeError> matchedErrors = new ArrayList<>();
        if (nes != null) {
            for (XmlDoc.Element ne : nes) {
                if (!ne.booleanValue("@controller", false) && ne.booleanValue("@included", false)) {
                    ClusterNodeError error = new ClusterNodeError(ne);
                    error.saveXml(w);
                    if (notify && error.matches(contains, containsNo)) {
                        matchedErrors.add(error);
                    }
                }
            }
        }
        long totalNbErrors = re.longValue("total-nb-errors", 0L);
        w.add("total-nb-errors", totalNbErrors);
        w.add("node-count",
                new String[]{
                        "excluded", Integer.toString(nbExcludedNodes),
                        "included", Integer.toString(nbIncludedNodes),
                        "expected-included", expectedNbIncludedNodes > 0 ? Integer.toString(expectedNbIncludedNodes) : null,
                        "missing-included", nbMissingNodes > 0 ? Integer.toString(nbMissingNodes) : null},
                nbNodes);


        if (notify) {
            if ((nbMissingNodes > 0 || !matchedErrors.isEmpty()) || !ignoreIfNoError) {
                notify(executor(), recipients, contains, containsNo, matchedErrors, totalNbErrors, expectedNbIncludedNodes, re);
                w.add("notified", true);
            } else {
                w.add("notified", false);
            }
        }
    }

    private static void notify(ServiceExecutor executor, Collection<String> recipients, Collection<String> contains, Collection<String> containsNo,
                               List<ClusterNodeError> matchedErrors, long totalNbErrors, int expectedNbIncludedNodes, XmlDoc.Element nodes) throws Throwable {
        String uuid = executor.execute("server.uuid").value("uuid");
        String time = DateTime.string(new Date());
        String subject = String.format("[%s] Cluster Error (%s)", uuid, time);
        StringBuilder body = new StringBuilder("<html>\n");

        body.append("<style>\n");
        body.append("body { font-family: Arial,helvetica,sans-serif; }\n");
        body.append("table, th, td { border: 1px solid black; }\n");
        body.append("</style>\n");

        List<XmlDoc.Element> nes = nodes.elements("node");
        if (nes != null && !nes.isEmpty()) {
            body.append("<h3>Cluster Nodes</h3>\n<hr><br>\n");
            body.append("<table>\n");
            body.append("<thead>\n");
            body.append("<tr><th>Node</th><th>Controller</th><th>Included</th><th>Number of errors</th></tr>");
            body.append("</thead>\n");
            body.append("<tbody>\n");
            for (XmlDoc.Element ne : nes) {
                body.append("<tr>");
                body.append("<td>").append(ne.value("@uuid")).append("</td>");
                body.append("<td>").append(ne.booleanValue("@controller", false)).append("</td>");
                body.append("<td>").append(ne.booleanValue("@included", false)).append("</td>");
                body.append("<td>").append(ne.longValue("errors/nb-errors", 0)).append("</td>");
                body.append("</tr>\n");
            }
            body.append("</tbody>\n");
            body.append("</table>\n");

            if (expectedNbIncludedNodes > 0) {
                int nbNodes = nodes.intValue("count");
                int nbIncludedNodes = nodes.count("node[@included='true']");
                int nbExcludedNodes = nbNodes - nbIncludedNodes;
                int nbMissingNodes = (expectedNbIncludedNodes > 0 && expectedNbIncludedNodes > nbIncludedNodes) ? (expectedNbIncludedNodes - nbIncludedNodes) : 0;
                body.append("<ul>\n");
                body.append("<li>Expected number of included nodes: ").append(expectedNbIncludedNodes).append("</li>\n");
                body.append("<li>Number of included nodes: ").append(nbIncludedNodes).append("</li>\n");
                if (nbExcludedNodes > 0) {
                    body.append("<li>Number of excluded nodes: ").append(nbExcludedNodes).append("</li>\n");
                }
                if (nbMissingNodes > 0) {
                    body.append("<li>Number of missing (included) nodes: ").append(nbMissingNodes).append("</li>\n");
                }
                body.append("</ul>\n");
            }
        }

        body.append("<br><br><h3>Cluster Errors</h3><hr><br>");
        body.append("<ul>\n");
        if (contains != null) {
            body.append("<li><b>Contains:</b> ").append(String.join(", ", contains)).append("</li>\n");
        }
        if (containsNo != null) {
            body.append("<li><b>Contains no:</b> ").append(String.join(", ", containsNo)).append("</li>\n");
        }
        body.append("<li><b>Total number of errors:</b> ").append(totalNbErrors).append("</li>\n");
        body.append("</ul>\n");

        if (!matchedErrors.isEmpty()) {
            body.append("<br><br>\n");
            ClusterNodeError.saveHtmlTable(matchedErrors, body);
        }
        body.append("</html>");

        XmlDocMaker dm = new XmlDocMaker("args");
        for (String email : recipients) {
            dm.add("to", email);
        }
        dm.add("subject", subject);
        dm.add("body", new String[]{"type", "text/html"}, body.toString());
        executor.execute("mail.send", dm.root());
    }
}
