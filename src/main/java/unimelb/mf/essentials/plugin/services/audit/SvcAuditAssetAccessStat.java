package unimelb.mf.essentials.plugin.services.audit;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.DateType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.FileSizeUtils;
import unimelb.utils.IPV4Utils;

import java.net.InetAddress;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SvcAuditAssetAccessStat extends PluginService {

    public static final String SERVICE_NAME = "unimelb.audit.asset.access.stat";

    private static int MAX_SET_SIZE = 10000;

    private Interface _defn;

    public SvcAuditAssetAccessStat() {
        _defn = new Interface();
        _defn.add(new Interface.Element("from", DateType.DEFAULT, "Start date/time inclusive.", 0, 1));
        _defn.add(new Interface.Element("to", DateType.DEFAULT, "End date/time inclusive.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Scan audit records with types of asset.created, asset.modified, asset.content.accessed, summarise to get an idea of the total/internal/external traffic.";
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        Date from = args.dateValue("from", null);
        Date to = args.dateValue("to", new Date());

        long accessedContentCountTotal = 0L;
        long accessedContentCountInternal = 0L;
        long accessedContentCountExternal = 0L;

        long accessedContentSizeTotal = 0L;
        long accessedContentSizeInternal = 0L;
        long accessedContentSizeExternal = 0L;

        long createdCountTotal = 0L;
        long createdCountInternal = 0L;
        long createdCountExternal = 0L;

        long createdSizeTotal = 0L;
        long createdSizeInternal = 0L;
        long createdSizeExternal = 0L;

        long modifiedCountTotal = 0L;
        long modifiedCountInternal = 0L;
        long modifiedCountExternal = 0L;

        long modifiedSizeTotal = 0L;
        long modifiedSizeInternal = 0L;
        long modifiedSizeExternal = 0L;

        boolean complete = false;
        long idx = 1;
        int size = 10000;
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "asset.accessed.content");
        dm.add("type", "asset.created");
        dm.add("type", "asset.modified");
        if (from != null) {
            dm.add("from", from);
        }
        if (to != null) {
            dm.add("to", to);
        }
        dm.add("idx", idx);
        dm.add("size", size);
        XmlDoc.Element queryArgs = dm.root();

        Set<String> knownInternalIPs = new HashSet<>();
        Set<String> knownExternalIPs = new HashSet<>();

        while (!complete) {
            PluginTask.checkIfThreadTaskAborted();
            queryArgs.element("idx").setValue(idx);
            XmlDoc.Element re = executor().execute("audit.query", queryArgs);
            List<XmlDoc.Element> ees = re.elements("event");
            if (ees != null && !ees.isEmpty()) {
                for (XmlDoc.Element ee : ees) {
                    String ipAddr = ee.value("ip-address");
                    boolean internalIP = ipAddr == null ? true : isUnimelbIPAddr(ipAddr, knownInternalIPs, knownExternalIPs);
                    long csize = ee.longValue("payload/size", 0);
                    String type = ee.value("@type");
                    if ("asset.accessed.content".equalsIgnoreCase(type)) {
                        accessedContentCountTotal++;
                        accessedContentSizeTotal += csize;
                        if (internalIP) {
                            accessedContentCountInternal++;
                            accessedContentSizeInternal += csize;
                        } else {
                            accessedContentCountExternal++;
                            accessedContentSizeExternal += csize;
                        }
                    } else if ("asset.created".equalsIgnoreCase(type)) {
                        createdCountTotal++;
                        createdSizeTotal += csize;
                        if (internalIP) {
                            createdCountInternal++;
                            createdSizeInternal += csize;
                        } else {
                            createdCountExternal++;
                            createdSizeExternal += csize;
                        }
                    } else if ("asset.modified".equalsIgnoreCase(type)) {
                        modifiedCountTotal++;
                        modifiedSizeTotal += csize;
                        if (internalIP) {
                            modifiedCountInternal++;
                            modifiedSizeInternal += csize;
                        } else {
                            modifiedCountExternal++;
                            modifiedSizeExternal += csize;
                        }
                    }
                }
                PluginTask.threadTaskProcessed(ees.size());
                idx += size;
            }
            complete = ees == null || ees.size() < size;
        }
        if (to != null || from != null) {
            w.push("time");
            if (from != null) {
                w.add("from", from);
            }
            if (to != null) {
                w.add("to", to);
            }
            w.pop();
        }
        w.push("asset-accessed-content");

        w.push("internal");
        w.add("count",
                new String[]{"percent",
                        String.format("%.2f",
                                accessedContentCountTotal == 0 ? 0.0
                                        : (double) accessedContentCountInternal / accessedContentCountTotal * 100)},
                accessedContentCountInternal);
        w.add("size",
                new String[]{"h", FileSizeUtils.toHumanReadable(accessedContentSizeInternal), "percent",
                        String.format("%.2f",
                                accessedContentSizeTotal == 0 ? 0.0
                                        : (double) accessedContentSizeInternal / accessedContentSizeTotal * 100)},
                accessedContentSizeInternal);
        w.pop();

        w.push("external");
        w.add("count",
                new String[]{"percent",
                        String.format("%.2f",
                                accessedContentCountTotal == 0 ? 0.0
                                        : (double) accessedContentCountExternal / accessedContentCountTotal * 100)},
                accessedContentCountExternal);
        w.add("size",
                new String[]{"h", FileSizeUtils.toHumanReadable(accessedContentSizeExternal), "percent",
                        String.format("%.2f",
                                accessedContentSizeTotal == 0 ? 0.0
                                        : (double) accessedContentSizeExternal / accessedContentSizeTotal * 100)},
                accessedContentSizeExternal);
        w.pop();

        w.push("total");
        w.add("count", accessedContentCountTotal);
        w.add("size", new String[]{"h", FileSizeUtils.toHumanReadable(accessedContentSizeTotal)},
                accessedContentSizeTotal);
        w.pop();

        w.pop();

        w.push("asset-created");

        w.push("internal");
        w.add("count", new String[]{"percent",
                        String.format("%.2f",
                                createdCountTotal == 0 ? 0.0 : (double) createdCountInternal / createdCountTotal * 100)},
                createdCountInternal);
        w.add("size",
                new String[]{"h", FileSizeUtils.toHumanReadable(createdSizeInternal), "percent",
                        String.format("%.2f",
                                createdSizeTotal == 0 ? 0.0 : (double) createdSizeInternal / createdSizeTotal * 100)},
                createdSizeInternal);
        w.pop();

        w.push("external");
        w.add("count", new String[]{"percent",
                        String.format("%.2f",
                                createdCountTotal == 0 ? 0.0 : (double) createdCountExternal / createdCountTotal * 100)},
                createdCountExternal);
        w.add("size",
                new String[]{"h", FileSizeUtils.toHumanReadable(createdSizeExternal), "percent",
                        String.format("%.2f",
                                createdSizeTotal == 0 ? 0.0 : (double) createdSizeExternal / createdSizeTotal * 100)},
                createdSizeExternal);
        w.pop();

        w.push("total");
        w.add("count", createdCountTotal);
        w.add("size", new String[]{"h", FileSizeUtils.toHumanReadable(createdSizeTotal)}, createdSizeTotal);
        w.pop();

        w.pop();

        w.push("asset-modified");

        w.push("internal");
        w.add("count", new String[]{"percent",
                        String.format("%.2f",
                                modifiedCountTotal == 0 ? 0.0 : (double) modifiedCountInternal / modifiedCountTotal * 100)},
                modifiedCountInternal);
        w.add("size", new String[]{"h", FileSizeUtils.toHumanReadable(modifiedSizeInternal), "percent",
                        String.format("%.2f",
                                modifiedSizeTotal == 0 ? 0.0 : (double) modifiedSizeInternal / modifiedSizeTotal * 100)},
                modifiedSizeInternal);
        w.pop();

        w.push("external");
        w.add("count", new String[]{"percent",
                        String.format("%.2f",
                                modifiedCountTotal == 0 ? 0.0 : (double) modifiedCountExternal / modifiedCountTotal * 100)},
                modifiedCountExternal);
        w.add("size", new String[]{"h", FileSizeUtils.toHumanReadable(modifiedSizeExternal), "percent",
                        String.format("%.2f",
                                modifiedSizeTotal == 0 ? 0.0 : (double) modifiedSizeExternal / modifiedSizeTotal * 100)},
                modifiedSizeExternal);
        w.pop();

        w.push("total");
        w.add("count", modifiedCountTotal);
        w.add("size", new String[]{"h", FileSizeUtils.toHumanReadable(modifiedSizeTotal)}, modifiedSizeTotal);
        w.pop();

        w.pop();
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    static boolean isUnimelbIPAddr(String ip, Set<String> knownInternalIPs, Set<String> knownExternalIPs) {
        try {
            // 128.250.0.0/16
            if (ip.startsWith("128.250.")) {
                return true;
            }
            // 10.0.0.0/8
            if (ip.startsWith("10.")) {
                return true;
            }
            // 172.16.0.0/12
            if (IPV4Utils.isInRange(ip, "172.16.0.0/12")) {
                return true;
            }
            if (knownInternalIPs.contains(ip)) {
                return true;
            }
            if (knownExternalIPs.contains(ip)) {
                return false;
            }
            InetAddress addr = InetAddress.getByName(ip);
            if (addr.isLoopbackAddress() || addr.isSiteLocalAddress()) {
                // loopback and private networks should be considered unimelb internal
                return true;
            }
            String hostName = addr.getHostName();
            boolean isInternal = hostName != null && hostName.endsWith("unimelb.edu.au");
            if (isInternal) {
                if (knownInternalIPs.size() < MAX_SET_SIZE) {
                    knownInternalIPs.add(ip);
                }
            } else {
                if (knownExternalIPs.size() < MAX_SET_SIZE) {
                    knownExternalIPs.add(ip);
                }
            }
            return isInternal;
        } catch (Throwable e) {
            PluginLog.log().add(PluginLog.ERROR, "failed to resolve host name for ip: " + ip, e);
            return false;
        }
    }

}
