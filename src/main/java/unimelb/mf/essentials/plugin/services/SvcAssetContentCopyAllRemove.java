package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentCopyAllRemove extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.copy.all.remove";

	private Interface _defn;

	public SvcAssetContentCopyAllRemove() {
		_defn = new Interface();

		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"The name of the store to remove content from. If specified, then the 'copy' argument is ignored.", 1, 1));
		_defn.add(new Interface.Element("reason", StringType.DEFAULT,
				"The reason for the removal to store with the asset.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Iterates through all asset versions and removes content from the given store. This service will throw an exception if you attempt to remove the last content copy.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String assetID = args.value("id");
		String store = args.value("store");
		String reason = args.value("reason");

		// Get the versions
		Collection<String> versions = executor()
				.execute("asset.versions", "<args><id>" + assetID + "</id></args>", null, null)
				.values("asset/version/@n");

		// Iterate through versions 
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("asset", new String[] { "id", assetID });
		Boolean some = false;           // SOmething from at least one version to show
		for (String version : versions) {
			PluginTask.checkIfThreadTaskAborted();

			// See if has content in this version
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", new String[] { "version", version}, assetID);
			XmlDoc.Element r = executor().execute("asset.get", dm.root());
			XmlDoc.Element content = r.element("asset/content");

			// Remove content
			if (content!=null) {
				// Sometimes when content is removed, the user can choose to leave 
				// evidence behind that the content was once there but has been removed
				XmlDoc.Element removed = content.element("removed");
				if (removed==null) {
					dm = new XmlDocMaker("args");
					dm.add("id", new String[] { "version", version}, assetID);
					dm.add("store", store);
					dm.add("action", "destroy");
					if (reason!=null) {
						dm.add("reason", reason);
					}
					dm.add("remove-completely", true);
					dm.add("remove-last-copy", false);   // Exception if you try to remive the last copy
					r = executor().execute("asset.content.remove", dm.root());
					String t = r.value("removed");
					// 0 means nothging was removed, I think!
					if (!t.equals("0")) {
						some = true;
						dmOut.add("version", new String[] {"store", store, "removed", t}, version);
					}
				}
			}
		}
		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}

	}



	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
