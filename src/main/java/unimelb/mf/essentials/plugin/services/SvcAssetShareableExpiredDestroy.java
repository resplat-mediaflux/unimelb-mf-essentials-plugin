package unimelb.mf.essentials.plugin.services;

import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;

public class SvcAssetShareableExpiredDestroy extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.shareable.expired.destroy";

	private Interface _defn;

	public SvcAssetShareableExpiredDestroy() {
		_defn = new Interface();
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT,
				"Instead of destroy, list the expired shareables in the service result. Defaults to false.", 0, 1));
		_defn.add(
				new Interface.Element("email", EmailAddressType.DEFAULT, "If any shareables are found, email the result to the recipient(s).", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_MODIFY;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Find and destroy expired asset shareables. This service is intended got end users to find and destroy their own shareables.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		boolean listOnly = args.booleanValue("list-only", false);
		String email = args.value("email");

		long idx = 1;
		long size = 100;
		boolean complete = false;
		int nbExpired = 0;
		int nbDownload = 0;
		int nbUpload = 0;
		StringBuilder sb = new StringBuilder();
		if (email != null) {
			sb.append("<html>");
			sb.append("<head>");
			sb.append("<style>");
			sb.append("table, th, td { border: 1px solid black; }");
			sb.append("</style>");
			sb.append("</head>");
			sb.append("<body>");
			sb.append(
					"<table><thead><tr><th>ID</th><th>Type</th><th>Name</th><th>Is Facility Shareable?</th><th>Expiry</th><th>Destroyed</th></tr></thead><tbody>");
		}
		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			List<XmlDoc.Element> ses = executor().execute("asset.shareable.describe",
					"<args><idx>" + idx + "</idx><size>" + size + "</size></args>", null, null).elements("shareable");
			int nbDestroyed = 0;
			if (ses != null && !ses.isEmpty()) {
				for (XmlDoc.Element se : ses) {
					PluginTask.checkIfThreadTaskAborted();
					Date validTo = se.dateValue("valid-to", null);
					boolean expired = validTo != null && validTo.before(new Date());
					if (expired) {
						nbExpired += 1;
						String type = se.value("@type");
						if ("download".equals(type)) {
							nbDownload += 1;
						}
						if ("upload".equals(type)) {
							nbUpload += 1;
						}
						boolean destroyed = false;
						if (!listOnly) {
							PluginTask.checkIfThreadTaskAborted();
							destroyShareable(executor(), se);
							destroyed = true;
							nbDestroyed += 1;
						}
						saveXml(se, destroyed, w);
						if (email != null) {
							saveHtml(se, destroyed, sb);
						}
					}
				}
				idx += (size - nbDestroyed);
			} else {
				complete = true;
			}
		}
		w.add("count", nbExpired);
		if (nbExpired > 0 && email != null) {
			sb.append("</tbody></table><br/>");
			sb.append("Expired Upload Shareables: <b>").append(nbUpload).append("</b><br/>");
			sb.append("Expired Download Shareables: <b>").append(nbDownload).append("</b><br/>");
			sb.append("Expired Total: <b>").append(nbExpired).append("</b><br/>");
			sb.append("</body></html>");
			sendEmail(executor(), email, sb.toString(), nbExpired, !listOnly);
			w.add("sent-to", email);
		}
	}

	static void destroyShareable(ServiceExecutor executor, XmlDoc.Element se) throws Throwable {
		String shareableId = se.value("@id");
		String type = se.value("@type");
		if ("download".equals(type) && se.elementExists("manifest")) {
			executor.execute("asset.shareable.download.manifest.destroy", "<args><id>" + shareableId + "</id></args>",
					null, null);
		}
		executor.execute("asset.shareable.destroy", "<args><id>" + shareableId + "</id></args>", null, null);
	}

	private static void saveXml(XmlDoc.Element se, boolean destroyed, XmlWriter w) throws Throwable {
		String id = se.value("@id");
		String type = se.value("@type");
		String name = se.value("name");
		String expiry = se.value("valid-to");
		String completionServiceName = se.value("completion-service/@name");
		boolean isFacilityShareable = completionServiceName != null
				&& completionServiceName.startsWith("vicnode.facility.shareable.");
		w.add("shareable", new String[] { "id", id, "type", type, "name", name, "facility",
				Boolean.toString(isFacilityShareable), "expiry", expiry, "destroyed", Boolean.toString(destroyed) });
	}

	private static void saveHtml(XmlDoc.Element se, boolean destroyed, StringBuilder sb) throws Throwable {
		String id = se.value("@id");
		String type = se.value("@type");
		String name = se.value("name");
		String expiry = se.value("valid-to");
		String completionServiceName = se.value("completion-service/@name");
		boolean isFacilityShareable = completionServiceName != null
				&& completionServiceName.startsWith("vicnode.facility.shareable.");
		sb.append("<tr>");
		sb.append("<td>").append(id).append("</td>");
		sb.append("<td>").append(type).append("</td>");
		sb.append("<td>").append(name == null ? "&nbsp;" : name).append("</td>");
		sb.append("<td>").append(isFacilityShareable ? "Yes" : "No").append("</td>");
		sb.append("<td>").append(expiry == null ? "&nbsp;" : expiry).append("</td>");
		sb.append("<td>").append(destroyed).append("</td>");
		sb.append("</tr>");
	}

	private static void sendEmail(ServiceExecutor executor, String email, String htmlMessage, int nbExpired,
			boolean destroyed) throws Throwable {
		String serverUUID = executor.execute("server.uuid").value("uuid");
		String subject = String.format("[%s] expired asset shareable%s %s (%d)", serverUUID, nbExpired > 1 ? "s" : "",
				destroyed ? "destroyed" : "found", nbExpired);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("to", email);
		dm.add("subject", subject);
		dm.add("body", new String[] { "type", "text/html" }, htmlMessage);
		executor.execute("mail.send", dm.root());
	}

}
