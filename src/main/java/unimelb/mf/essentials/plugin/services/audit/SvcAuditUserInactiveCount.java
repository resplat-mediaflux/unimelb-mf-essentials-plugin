package unimelb.mf.essentials.plugin.services.audit;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SvcAuditUserInactiveCount extends PluginService {

    public static final String SERVICE_NAME = "unimelb.audit.user.inactive.count";
    public static final String DEFAULT_INACTIVE_FROM = SvcAuditUserInactiveList.DEFAULT_INACTIVE_FROM;
    private final Interface _defn;

    public SvcAuditUserInactiveCount() {
        _defn = new Interface();
        SvcAuditUserInactiveList.addArgs(_defn);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }


    @Override
    public String description() {
        return "Count the users are inactive since the specified date (defaults to last 90 days).";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String inactiveFrom = args.stringValue("from", DEFAULT_INACTIVE_FROM);
        Date inactiveFromDate = DateTime.parse(inactiveFrom);

        String inactiveTo = args.stringValue("to", "now");
        Date inactiveToDate = DateTime.parse(inactiveTo);

        if (inactiveToDate.getTime() < inactiveFromDate.getTime()) {
            throw new IllegalArgumentException("Invalid from: " + inactiveFrom + ". Must be earlier then to: " + DateTime.string(inactiveToDate));
        }

        long inactiveDays = TimeUnit.DAYS.convert(Math.abs(inactiveToDate.getTime() - inactiveFromDate.getTime()),
                TimeUnit.MILLISECONDS);
        String role = args.value("role");
        String roleType = args.stringValue("role/@type", "role");
        Collection<String> domains = args.values("domain");
        boolean includeDisabled = args.booleanValue("include-disabled", false);

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("role", new String[]{"type", roleType}, role);
        dm.add("relationship", "either");

        PluginTask.checkIfThreadTaskAborted();
        Collection<String> actorNames = executor().execute("actors.granted", dm.root()).values("actor/@name");

        long count = 0;
        if (actorNames != null) {
            for (String actorName : actorNames) {
                String[] parts = actorName.split(":");
                String authority, domain, username;
                if (parts.length == 3) {
                    authority = parts[0];
                    domain = parts[1];
                    username = parts[2];
                } else if (parts.length == 2) {
                    authority = null;
                    domain = parts[0];
                    username = parts[1];
                } else {
                    throw new Exception("Failed to parse user name: " + actorName);
                }
                if (authority != null) {
                    // Currently, audit log does not support users from external authorities. Need to revisit.
                    // TODO revisit
                    continue;
                }
                if (domains != null && !domains.contains(domain)) {
                    continue;
                }
                if (!SvcAuditUserInactiveList.userExists(executor(), authority, domain, username)) {
                    continue;
                }
                boolean enabled = userEnabled(executor(), authority, domain, username);
                if (!includeDisabled && !enabled) {
                    continue;
                }
                if (!SvcAuditUserInactiveList.hasLogon(executor(), actorName, inactiveFromDate, inactiveToDate)) {
                    count++;
                }
            }
        }
        w.add("count", count);

        w.push("inactive");
        w.add("from", inactiveFromDate);
        w.add("days", inactiveDays);
        w.pop();

        w.add("role", new String[]{"type", roleType}, role);
    }

    boolean userEnabled(ServiceExecutor executor, String authority, String domain, String username) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (authority != null) {
            dm.add("authority", authority);
        }
        dm.add("domain", domain);
        dm.add("user", username);

        PluginTask.checkIfThreadTaskAborted();
        return executor.execute("authentication.user.enabled", dm.root()).booleanValue("enabled");
    }
}
