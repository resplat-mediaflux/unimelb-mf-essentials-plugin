package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Date;
import java.util.List;

public class SvcAuthenticationIntrusionDenyClear extends PluginService {

    public static final String SERVICE_NAME = "unimelb.authentication.intrusion.deny.clear";

    private Interface _defn;

    public SvcAuthenticationIntrusionDenyClear() {
        _defn = new Interface();
        _defn.add(new Interface.Element("from", DateType.DEFAULT, "Select the ip addresses denied after this time.", 0, 1));
        _defn.add(new Interface.Element("to", DateType.DEFAULT, "Select the ip addresses denied before this time.", 0, 1));
        _defn.add(new Interface.Element("all", BooleanType.DEFAULT, "If set to true, and 'from' and 'to' arguments are not specified, all addresses are removed from deny list. Defaults to false", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Remove the matching ip addresses from the deny list.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        Date from = args.dateValue("from", null);
        Date to = args.dateValue("to", null);
        boolean removeAll = args.booleanValue("all", false);

        List<XmlDoc.Element> des = executor().execute("authentication.intrusion.deny.describe").elements("deny");
        if (des != null) {
            for (XmlDoc.Element de : des) {
                String address = de.value("address");
                String ctime = de.value("ctime");
                Long ctimeMillisec = de.longValue("ctime/@millisec", null);
                if (from == null && to == null) {
                    if (removeAll) {
                        removeDenyAddress(executor(), address);
                        w.add("removed", new String[]{"ctime", ctime}, address);
                    }
                } else {
                    System.out.println("D1: " + ctime);
                    if (ctimeMillisec != null) {
                        System.out.println("D2: " + ctimeMillisec);
                        boolean matches = from == null || ctimeMillisec >= from.getTime();
                        if (to != null && ctimeMillisec > to.getTime()) {
                            System.out.println("D3: " + ctimeMillisec + " : " + to.getTime());
                            matches = false;
                        }
                        if (matches) {
                            System.out.println("D4: ");
                            removeDenyAddress(executor(), address);
                            w.add("removed", new String[]{"ctime", ctime}, address);
                        }
                        System.out.println("D5: ");
                    }
                }
            }
        }
    }

    private static void removeDenyAddress(ServiceExecutor executor, String address) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("address", address);
        executor.execute("authentication.intrusion.deny.remove", dm.root());
    }
}
