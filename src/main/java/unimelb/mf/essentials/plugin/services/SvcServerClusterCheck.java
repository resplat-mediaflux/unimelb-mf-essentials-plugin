/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;



import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.utils.DateUtil;


public class SvcServerClusterCheck extends PluginService {
	private Interface _defn;
	private static int nNodes = 2;

	public SvcServerClusterCheck()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Notify this email of the results (defaul none).",0,1)); 
		_defn.add(new Interface.Element("error-delta", IntegerType.DEFAULT, "If any of the cluster nodes have errors since this many hours ago report them (errors cannot be cleared).  Default is 3.",0,1)); 
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Checks that the cluster is functioning correctly (number of nodes at least 2, ping/io capabilities in place, errors incurred) and optionally sends a notification if not.";
	}

	public String name() {
		return "unimelb.server.cluster.check";
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String email = args.value("email");
		int errorDelta = args.intValue("error-delta",  3);
		XmlDoc.Element r = executor().execute("cluster.describe");
		Collection<XmlDoc.Element> nodes = r.elements("node");
		int nIO = 0;
		Boolean nodesIncluded = true;
		Boolean nodesOK = true;
		Date now = new Date();
		//
		int nErrors = 0;
		String nodesWithErrors = "";
		String nodesNotInCluster = "";
		String nodesWithoutCapabilities = "";
		for (XmlDoc.Element node : nodes) {
			String uuid = node.value("@uuid");
			w.push("node");
			w.add("uuid", uuid);
			Boolean  controller = node.booleanValue("@controller", false);
			if (controller) {
				w.add("controller", true);
			} else {
				Boolean isIncluded = node.booleanValue("@included");
				if (!isIncluded) {
					nodesIncluded = false;
					nodesNotInCluster += uuid + " ";
				}
				nIO++;
				w.add("io", true);
				Collection<XmlDoc.Element> capabilities = node.elements("capability"); 
				Boolean hasPing = false;
				Boolean hasIO = false;
				for (XmlDoc.Element capability : capabilities) {
					String op = capability.value("op");
					String resource  = capability.value("resource");
					if (op.equals("io")) {
						hasIO = true;
					} 
					if (op.equals("service") && resource.equals("server.ping")) {
						hasPing = true;
					}
				}
				w.add("has-ping", hasPing);	
				w.add("has-io", hasIO);
				if (!hasPing || !hasIO) {
					nodesOK = false;
					nodesWithoutCapabilities += uuid + " ";
				}

				// Errors. We can't clear, so just see if there any new ones in
				// the last given time period (these are rare)
				Collection<XmlDoc.Element> errors = node.elements("errors");
				if (errors!=null) {
					for (XmlDoc.Element error : errors) {
						Date errorDate = error.dateValue("last-error-time");
						long hourDiff = DateUtil.dateDifferenceInHours(now, errorDate);
						if (hourDiff<errorDelta) {
							nErrors++;
							w.add("has-errors", true);
							nodesWithErrors += uuid + " ";
						} else {
							w.add("has-errors", false);
						}
					}
				}
			}
			w.pop();
		}
		w.add("nio", nIO);
		w.add("nodes-included", nodesIncluded);
		w.add("nodes-ok", nodesOK);
		w.add("nerrors", nErrors);

		//
		if (email!=null) {
			String from = Properties.getServerProperty(executor(), "notification.from");
			String uuid = executor().execute("server.uuid").value("uuid");

			String subject = "The IO nodes of Mediaflux controller (uuid="+uuid+ ") have an issue";
			Boolean some = false;
			String body = "Dear Mediaflux ops\n\n" +
					"The Mediaflux controller with UUID " + uuid + "\n\n";
			if (nIO<nNodes) {
				// We expect at least 2 IO nodes.  Should really get this from a property.
				body += "Has only " + nIO + "cluster nodes available. \n";
				some = true;
			}
			if (!nodesIncluded) {
				body += "Has some IO nodes (" + nodesNotInCluster.trim() + ") that are not included in the cluster.\n";
				some = true;
			}
			if (!nodesOK) {
				body += "Has IO nodes(" + nodesWithoutCapabilities.trim() + ") for which not all expected capabilities (io & ping) are available.\n";
				some = true;
			}
			if (nErrors>0) {
				body += "Has IO nodes (" + nodesWithErrors.trim() + ") for which there are errors in the last " + errorDelta + " hours.\n";
				some = true;
			}

			// Send
			if (some) {
				body += "\nregards \nMediaflux";
				MailUtils.sendEmail(executor(), email, null, null, from, subject, body, false);
			}
		}
	}
}

