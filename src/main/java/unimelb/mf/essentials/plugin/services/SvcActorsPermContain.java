package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.PluginService.Interface.Element;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.ActorUtils;

public class SvcActorsPermContain extends PluginService {

	private Interface _defn;

	public SvcActorsPermContain() {

		_defn = new Interface();
		_defn.add(new Element("type", StringType.DEFAULT, "The type of actor to scrutinize. Defaults to all actor types (actor.type.list).", 0, 1));
		_defn.add(new Element("contains", StringType.DEFAULT, "If this string is contained in the permission resource value, report the permission.", 1, 1));
		_defn.add(new Element("domain", StringType.DEFAULT, "If an actor being assessed is a user, then only consider it if the domain is one specified here. Defaults to all domains.  You might like to exclude Active Directory domains.", 0, Integer.MAX_VALUE));
	}

	public String name() {
		return "unimelb.actors.perm.contain";
	}

	public String description() {
		return "Lists actors which have a direct permission for which the resource value contains the given string.  Brute force iteration, not efficient.  For user actors belonging to AD domains, the service actor.list will find ALL user actors bound to the AD domain, whether they hold mediaflux roles or not.  Once the actor is listed then it is described in a second iteration.  This is why the :domain argument is useful because it can be used to discard AD user actors before they are described, which is muich much faster. If you really need to do it for AD domains, you will have to be patient, as there are tens of thousands of entries per domain.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public boolean canBeAborted() {

		return true;
	}


	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse
		String contains = args.value("contains");
		Collection<String> domains = args.values("domain");
		String theType = args.value("type");
		//
		Vector<String> types = new Vector<String>();
		if (theType==null) {
			types.addAll(ActorUtils.types(executor()));
		} else {
			types.add(theType);
		}

		// Iterate over actor types
		for (String type : types) {
			PluginTask.checkIfThreadTaskAborted();
			w.push("type", new String[] {"name", type});
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("type", type);
			dm.add("size", "infinity");
			XmlDoc.Element r = executor().execute("actor.list", dm.root());
			PluginTask.checkIfThreadTaskAborted();

			// Iterate over actors for this type
			Collection<XmlDoc.Element> actors = r.elements("actor");
			if (actors!=null) {
				for (XmlDoc.Element actor : actors) {
					PluginTask.checkIfThreadTaskAborted();
					String name = actor.value();
					Boolean proceed = true;
					if (type.equals("user") && domains!=null) {
						String[] t = name.split(":"); // <authority>:domain:user
						String t2 = t[0];
						if (t.length==3) {
							t2 = t[1];
						}
						Boolean found = false;
						for (String domain : domains) {
							if (t2.equals(domain)) {
								found = true;
							}
						}
						proceed = found;
					}
					// Describe actor
					if (proceed) {
						PluginTask.checkIfThreadTaskAborted();
						dm = new XmlDocMaker("args");
						dm.add ("type", type);
						dm.add("name", name);
						r = executor().execute("actor.describe", dm.root());

						// Find permissions
						if (r!=null) {
							Collection<XmlDoc.Element> perms = r.elements("actor/perm");
							if (perms!=null) {
								Boolean some = false;
								XmlDocMaker dmOut = new XmlDocMaker("args");
								dmOut.push("actor", 
										new String[] {"id", r.value("actor/@id"),
												"type", r.value("actor/@type"), 
												"name", r.value("actor/@name")});

								for (XmlDoc.Element perm : perms) {
									String resource = perm.value("resource");
									if (resource.contains(contains)) {
										dmOut.add(perm);
										some = true;
									}
								}
								dmOut.pop();
								if (some) {
									w.push("actor", 
											new String[] {"id", r.value("actor/@id"),
													"type", r.value("actor/@type"), 
													"name", r.value("actor/@name")});

									w.addAll(dmOut.root().elements());
									w.pop();
								}
							}
						}
					}
				}
			}
			w.pop();
		}
	}
}