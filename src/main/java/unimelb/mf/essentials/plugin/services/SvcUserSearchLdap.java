package unimelb.mf.essentials.plugin.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.services.SvcUserSearch.UserEntry;

public class SvcUserSearchLdap extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.search.ldap";

	public static final String PROVIDER_STAFF = "unimelb";

	public static final String PROVIDER_STUDENT = "unimelb-student";

	public static final String ROOT_PATH_STAFF = "dc=unimelb,dc=edu,dc=au";

	public static final String ROOT_PATH_STUDENT = "dc=student,dc=unimelb,dc=edu,dc=au";

	public static final String PATH_STAFF = "ou=people,dc=unimelb,dc=edu,dc=au";

	public static final String PATH_SYSTEM_ACCOUNTS = "ou=business units,dc=unimelb,dc=edu,dc=au";

	public static final String PATH_STUDENTS = "ou=students,dc=student,dc=unimelb,dc=edu,dc=au";

	public static final String DOMAIN_STAFF = "unimelb";

	public static final String DOMAIN_STUDENT = "unimelb-student";

	public static final String UID_ATTR_NAME = "sAMAccountName";

	public static final String EMAIL_SUFFIX_STAFF = "@unimelb.edu.au";

	public static final String EMAIL_SUFFIX_STUDENT = "@student.unimelb.edu.au";

	private final Interface _defn;

	public SvcUserSearchLdap() {
		_defn = new Interface();
		SvcUserSearch.addArgs(_defn);
		SvcUserSearch.addEmployeeIdArg(_defn);
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Search for user in LDAP domains.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String name = args.value("name");
		String email = args.value("email");
		String domain = SvcUserSearch.resolveDomainName(executor(), args.value("domain"));
		String employeeId = args.value("employee-id");
		Collection<String> excludeDomains = args.values("exclude-domain");
		boolean excludeDisabled = args.booleanValue("exclude-disabled", true);
		boolean permissions = args.booleanValue("permissions", true);
		int size = args.intValue("size", SvcUserSearch.DEFAULT_RESULT_SIZE);

		List<SvcUserSearch.UserEntry> users = new ArrayList<>();
		searchLdapUsers(executor(), domain, excludeDomains, name, email, employeeId, excludeDisabled, permissions,
				users, size);

		SvcUserSearch.outputUsers(users, w);
	}

	static void searchLdapUsers(ServiceExecutor executor, String domain, Collection<String> excludeDomains, String name,
			String email, String employeeId, boolean excludeDisabled, boolean permissions, List<UserEntry> users,
			int size) throws Throwable {
		// validate required arguments
		if (name == null && email == null && employeeId == null) {
			throw new IllegalArgumentException("Missing name, email or employee-id argument.");
		}

		// validate domain argument
		if (domain != null) {
			if (!isLdapDomain(executor, domain)) {
				throw new IllegalArgumentException("Domain: " + domain + " is not a ldap domain.");
			}
			if (excludeDomains != null && excludeDomains.contains(domain)) {
				throw new IllegalArgumentException("The specified domain: " + domain + " is also excluded.");
			}
		}

		// validate email argument
		// NOTE: email not ends with unimelb.edu.au, such as florey.edu.au are valid.
//		if (email != null) {
//			if (!email.toLowerCase().endsWith(EMAIL_SUFFIX_STUDENT)
//					&& !email.toLowerCase().endsWith(EMAIL_SUFFIX_STAFF)) {
//				throw new IllegalArgumentException("Invalid email: " + email + ". Must end with " + EMAIL_SUFFIX_STAFF
//						+ " or " + EMAIL_SUFFIX_STUDENT);
//			}
//		}

		//
		if (domain != null) {
			if (domain.toLowerCase().contains("student")) {
				if (email != null && !email.toLowerCase().endsWith(EMAIL_SUFFIX_STUDENT)) {
					throw new IllegalArgumentException(
							"Domain and email arguments conflict. User in student domain should have " + email
									+ " ends with " + EMAIL_SUFFIX_STUDENT + ". Found " + email);
				}
				searchLdapStudentUsers(executor, domain, name, email, employeeId, excludeDisabled, permissions, users,
						size);
			} else {
				if (email != null && !email.toLowerCase().endsWith(EMAIL_SUFFIX_STAFF)) {
					throw new IllegalArgumentException(
							"Domain and email arguments conflict. User in staff domain should have " + email
									+ " ends with " + EMAIL_SUFFIX_STAFF + ". Found " + email);
				}
				searchLdapStaffUsers(executor, domain, name, email, employeeId, excludeDisabled, permissions, users,
						size);
			}
		} else {
			if (email != null) {
				if (email.toLowerCase().endsWith(EMAIL_SUFFIX_STUDENT)) {
					searchLdapStudentUsers(executor, domain, name, email, employeeId, excludeDisabled, permissions,
							users, size);
				} else {
					searchLdapStaffUsers(executor, domain, name, email, employeeId, excludeDisabled, permissions, users,
							size);
				}
			} else {
				searchLdapStaffUsers(executor, domain, name, email, employeeId, excludeDisabled, permissions, users,
						size);
				searchLdapStudentUsers(executor, domain, name, email, employeeId, excludeDisabled, permissions, users,
						size);
			}
		}
	}

	static void searchLdapStaffUsers(ServiceExecutor executor, String ldapDomain, String name, String email,
			String employeeId, boolean excludeDisabled, boolean permissions, List<UserEntry> users, int size)
			throws Throwable {
		if (ldapProviderExists(executor, PROVIDER_STAFF)) {
			searchLdapUsers(executor, PROVIDER_STAFF, ROOT_PATH_STAFF, ldapDomain, name, email, employeeId,
					excludeDisabled, permissions, users, size);
		}
	}

	static void searchLdapStudentUsers(ServiceExecutor executor, String ldapDomain, String name, String email,
			String employeeId, boolean excludeDisabled, boolean permissions, List<UserEntry> users, int size)
			throws Throwable {
		if (ldapProviderExists(executor, PROVIDER_STUDENT)) {
			searchLdapUsers(executor, PROVIDER_STUDENT, ROOT_PATH_STUDENT, ldapDomain, name, email, employeeId,
					excludeDisabled, permissions, users, size);
		}
	}

	private static boolean ldapProviderExists(ServiceExecutor executor, String provider) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("provider", provider);
		return executor.execute("ldap.provider.exists", dm.root()).booleanValue("exists");
	}

	static void searchLdapUsers(ServiceExecutor executor, String provider, String rootPath, String ldapDomain,
			String name, String email, String employeeId, boolean excludeDisabled, boolean permissions,
			List<UserEntry> users, int size) throws Throwable {

		List<XmlDoc.Element> ees = null;
		String exactQuery = constructLdapQuery(name, email, employeeId, true);
		ees = searchLdapUsers(executor, provider, rootPath, exactQuery, size);
		if (ees == null || ees.isEmpty()) {
			String wildcardQuery = constructLdapQuery(name, email, employeeId, false);
			ees = searchLdapUsers(executor, provider, rootPath, wildcardQuery, size);
		}
		if (ees != null) {
			for (XmlDoc.Element ee : ees) {
				String dn = ee.value("@dn").toLowerCase();
				String domain = dn.endsWith(PATH_STUDENTS) ? DOMAIN_STUDENT
						: ((dn.endsWith(PATH_STAFF) || dn.endsWith(PATH_SYSTEM_ACCOUNTS.toLowerCase())) ? DOMAIN_STAFF
								: null);
				if (domain == null) {
					// non-standard user
					continue;
				}
				if (ldapDomain == null || domain.equalsIgnoreCase(ldapDomain)) {
					UserEntry.Builder ub = SvcUserSearch.UserEntry.builder().setDomain(domain)
							.setUser(ee.value(UID_ATTR_NAME)).setEmail(ee.value("mail"))
							.setName(ee.value("displayName")).setEmployeeID(ee.value("employeeID"));
					String department = ee.value("department");
					if (department != null) {
						ub.addDepartment(department);
					}
					Collection<String> uomDepartments = ee.values("uOMDepartments");
					if (uomDepartments != null && !uomDepartments.isEmpty()) {
						ub.addDepartments(uomDepartments);
					}
					if (permissions) {
						XmlDoc.Element actor = executor.execute("actor.describe", "<args><type>user</type><name>"
								+ domain + ":" + ee.value(UID_ATTR_NAME) + "</name></args>", null, null)
								.element("actor");
						if (actor != null) {
							ub.setPermissions(actor.elements());
						}
					}
					UserEntry u = ub.build();
					users.add(u);
				}
			}
		}
	}

	private static List<XmlDoc.Element> searchLdapUsers(ServiceExecutor executor, String provider, String rootPath,
			String query, int size) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("provider", provider);
		dm.add("path", rootPath);
		dm.add("filter", query);
		dm.add("attribute", UID_ATTR_NAME);
		dm.add("attribute", "displayName");
		dm.add("attribute", "mail");
		dm.add("attribute", "department");
		dm.add("attribute", "uOMDepartments");
		dm.add("attribute", "employeeID");
		dm.add("size", size);

		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element re = executor.execute("ldap.search", dm.root());
		List<XmlDoc.Element> ees = re.elements("entry");
		return ees;
	}

	private static String constructLdapQuery(String name, String email, String employeeId, boolean exactMatch) {
		StringBuilder sb = new StringBuilder();
		sb.append("(&(objectClass=user)(|");
		if (name != null) {
			if (exactMatch) {
				if (name.matches("^[a-z0-9-_]+$")) {
					// username
					sb.append("(").append(UID_ATTR_NAME).append("=").append(name).append(")");
				} else {
					if (name.contains(" ")) {
						sb.append("(displayName=").append(name).append(")");
					} else {
						sb.append("(givenName=").append(name).append(")");
						sb.append("(sn=").append(name).append(")");
					}
				}
			} else {
				sb.append("(").append(UID_ATTR_NAME).append("=*").append(name).append("*)");
				if (name.contains(" ")) {
					sb.append("(displayName=*").append(name).append("*)");
				}
				sb.append("(givenName=*").append(name).append("*)");
				sb.append("(sn=*").append(name).append("*)");
			}
		}
		if (email != null) {
			String[] parts = email.split("@");
			if (exactMatch) {
				sb.append("(mail=").append(email).append(")");
				sb.append("(userPrincipalName=").append(email).append(")");
				sb.append("(proxyAddresses=").append(email).append(")");
				if (!parts[0].isEmpty()) {
					sb.append("(").append(UID_ATTR_NAME).append("=").append(parts[0]).append(")");
				}
			} else {
				sb.append("(mail=*").append(email).append("*)");
				sb.append("(proxyAddresses=*").append(email).append("*)");
				sb.append("(userPrincipalName=*").append(email).append("*)");
				if (!parts[0].isEmpty()) {
					sb.append("(").append(UID_ATTR_NAME).append("=*").append(parts[0]).append("*)");
					sb.append("(displayName=").append(parts[0].replace('.', ' ')).append(")");
				}
			}
		}
		if (employeeId != null) {
			sb.append("(employeeID=").append(employeeId).append(")");
		}
		sb.append("))");
		return sb.toString();
	}

	static boolean isLdapDomain(ServiceExecutor executor, String domain) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element de = executor.execute("authentication.domain.describe", dm.root()).element("domain");
		String type = de.value("@type");
		String protocol = de.value("@protocol");
		return "external".equalsIgnoreCase(type) && "ldap".equalsIgnoreCase(protocol);
	}

	static boolean isDomainAlias(ServiceExecutor executor, String domain, String alias) throws Throwable {
		if (domain.equals(alias)) {
			return true;
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element de = executor.execute("authentication.domain.describe", dm.root()).element("domain");
		Collection<String> aliases = de.values("aliases/alias");
		return aliases != null && aliases.contains(alias);
	}

	static boolean isStudentDomainAlias(ServiceExecutor executor, String domainAlias) throws Throwable {
		return isDomainAlias(executor, DOMAIN_STUDENT, domainAlias);
	}

	static boolean isStaffDoaminAlias(ServiceExecutor executor, String domainAlias) throws Throwable {
		return isDomainAlias(executor, DOMAIN_STAFF, domainAlias);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}

}
