package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import arc.mf.plugin.*;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceMetaDataElementRename extends PluginService {

	// TBD : Never tested with anything other than a top-level element path

	private Interface _defn;

	public SvcAssetNameSpaceMetaDataElementRename() {
		_defn = new Interface();
		Interface.Element me = new Interface.Element("namespace", StringType.DEFAULT, "The namespace.", 1, 1);
		_defn.add(me);
		//
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "The Document Type name.", 1, 1));

		me = new Interface.Element("from", StringType.DEFAULT, "The element name to copy from.", 1, 1);
		_defn.add(me);
		//
		me = new Interface.Element("to", StringType.DEFAULT, "The element name to copy to.", 1, 1);
		_defn.add(me);
	}

	public String name() {
		return "unimelb.asset.namespace.metadata.element.rename";
	}

	public String description() {
		return "Service to rename one namespace meta-data element (top-level; never tested with lower-level paths) to another. Both must exist in the definition.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String nameSpace = args.value("namespace");
		String type = args.value("type");
		String from = args.value("from");
		String to = args.value("to");

		// Get namespace meta-data
		XmlDoc.Element nsMeta = NameSpaceUtil.describe(null, executor(), nameSpace);


		// Find documents
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.push("asset-meta");
		//
		XmlDoc.Element meta = nsMeta.element("namespace/asset-meta");
		if(meta==null) {
			return;
		}
		Collection<XmlDoc.Element> docs = meta.elements(); 
		PluginTask.checkIfThreadTaskAborted();
		
		// Iterate through documents and look for the one we want
		if (docs!=null) {
			for (XmlDoc.Element doc : docs) {
				if (type.equals(doc.qname())) {	
					doc.renameElement(from, to);
					dm.add(doc);
					w.add("changed", doc.qname());
				} else {
					dm.add(doc);
					w.add("copied", doc.qname());
				}
			}
			dm.pop();
			executor().execute(null, "asset.namespace.asset.meta.set", dm.root());
		}
	}
}
