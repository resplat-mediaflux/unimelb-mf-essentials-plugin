package unimelb.mf.essentials.plugin.services;

import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcShoppingCartList extends PluginService {

    public static final String SERVICE_NAME = "unimelb.shoppingcart.list";

    private Interface _defn;

    public SvcShoppingCartList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("owner", StringType.DEFAULT, "The owner of the carts.", 0, 1));
        _defn.add(new Interface.Element("status",
                new EnumType(new String[] { "aborted", "assigned", "await processing", "data ready", "editable",
                        "error", "fulfilled", "processing", "rejected", "withdrawn" }),
                "The status of the carts.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "List shopping carts.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String owner = args.value("owner");
        String status = args.value("status");
        boolean isSystemManager = "system:manager"
                .equals(executor().execute("actor.self.describe").value("actor/@name"));

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("size", "infinity");
        dm.add("list-all", isSystemManager);
        XmlDoc.Element re = executor().execute("shopping.cart.describe", dm.root());
        List<XmlDoc.Element> ces = re.elements("cart");
        long count = 0L;
        if (ces != null) {
            for (XmlDoc.Element ce : ces) {

                String cartOwner = ce.value("owner");
                if (owner != null && !owner.equals(cartOwner)) {
                    continue;
                }
                String cartStatus = ce.value("status");
                if (status != null && !status.equals(cartStatus)) {
                    continue;
                }
                String cartId = ce.value("@id");
                w.add("cart", new String[] { "id", cartId, "owner", cartOwner, "status", cartStatus });
                count++;
            }
        }
        w.add("count", count);
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
