package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import arc.mf.plugin.*;
import arc.mf.plugin.PluginService.Interface;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcAssetMetaDataRename extends PluginService {

	// TBD : Never tested with anything other than a top-level element path

	private Interface _defn;

	public SvcAssetMetaDataRename() {
		_defn = new Interface();
		Interface.Element id = new Interface.Element("id", AssetType.DEFAULT, "The asset id.", 1, 1);
		_defn.add(id);
		//
		_defn.add(new Interface.Element("type", StringType.DEFAULT, "The Document Type name.", 1, 1));
		_defn.add(new Interface.Element("new-type", StringType.DEFAULT, "The new Document Type name.", 1, 1));
		_defn.add(new Interface.Element("mode",
				new EnumType(new String[] { "rename", "copy"}),
				"If 'copy', leaves the original meta-data document in place. If 'rename' (default), the original document is removed.", 0, 1));

	}

	public String name() {
		return "unimelb.asset.metadata.rename";
	}

	public String description() {
		return "Service to rename one asset meta-data document type to another. Both types must exist and should have the same definition apart from the name. The service does it by changing the name of the meta-data document, removing attributes and then replacing it.  Use this to change the namespace of a document, say renaming from test:mynote to DS:mynote";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String id = args.value("id");
		String type = args.value("type");
		String typeNew = args.value("new-type");
		String mode = args.stringValue("mode", "rename");

		// Get  asset
		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);

		// Find documents
		XmlDoc.Element meta = asset.element("asset/meta");

		if(meta==null) {
			return;
		}

		// Iterate through the documents
		Collection<XmlDoc.Element> docs = meta.elements(); 
		PluginTask.checkIfThreadTaskAborted();
		if (docs!=null) {
			for (XmlDoc.Element doc : docs) {
				String qn = doc.qname();		
				if (type.equals(qn)) {

					// Set new name and remove attributes
					doc.setName(typeNew);
					doc.removeAttribute("id");
					String[] t = qn.split(":");
					if (t.length==2) {
						doc.removeAttribute("xmlns:" + t[0]);
					} else {
						doc.removeAttribute("xmlns");
					}

					// Set			
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add ("id", id);
					dm.push("meta", new String[] {"action", "add"});
					dm.add(doc);
					dm.pop();
					executor().execute("asset.set",dm.root());
					w.add("copied",new String[] {"from", type}, typeNew);

					// Remove old
					if (mode.equals("rename")) {
						dm = new XmlDocMaker("args");
						dm.add ("id", id);
						dm.push("meta", new String[] {"action", "remove"});
						dm.add(qn);
						dm.pop();
						executor().execute("asset.set",dm.root());					
						w.add("removed", type);
					}
				} 
			}
		}
	}
}
