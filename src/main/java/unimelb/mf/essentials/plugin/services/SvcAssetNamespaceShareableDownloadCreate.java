package unimelb.mf.essentials.plugin.services;


import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.PathUtils;


public class SvcAssetNamespaceShareableDownloadCreate extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.namespace.shareable.download.create";

	private Interface _defn;

	public SvcAssetNamespaceShareableDownloadCreate() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace.", 1, 1));
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Email recipient of the shareable.",
				0, 1));
		_defn.add(new Interface.Element("valid-to", DateType.DEFAULT, "The date and time after which the shareable is no longer valid (defaults to infinity).",
				0, 1));		
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "A name for the shareable.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Create a download shareable for the given namespace.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String namespace  = args.value("namespace");
		String email  = args.value("email");
		Date validTo = args.dateValue("valid-to");
		String name = args.value("name");
		
		// Get the child part of the namespace for naming
		String childPath = PathUtils.getLastComponent(namespace);

		// Make the shareable
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", namespace);
		if (email!=null) {
			dm.push("invitation");
			dm.add("email", email);
			dm.pop();
		}
		dm.add("target-dir-name", childPath);
		if (name!=null) {
			dm.add("name", name);
		}
		if (validTo!=null) {
			dm.add("valid-to", validTo);
		} 
		XmlDoc.Element r = executor().execute("asset.shareable.download.create", dm.root());
		w.add(r.element("shareable"));
}


	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}


}