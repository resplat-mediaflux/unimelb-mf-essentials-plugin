/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetNameSpaceAssetDuplicateFind extends PluginService {

	private Interface _defn;
	private Long countAll_ = 0l;

	public SvcAssetNameSpaceAssetDuplicateFind() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The parent namespace to find in and below.", 1, 1));
		_defn.add(new Interface.Element("nb-threads", StringType.DEFAULT, "The number of threads in the asset query executions, defaults to 1.  This may provide performance gains.", 0, 1));
	}

	@Override
	public String name() {
		return "unimelb.asset.namespace.asset.duplicate.find";
	}

	@Override
	public String description() {
		return "Service to recursively walk down a namespace tree looking for any namespace where assets have the same name (should be impossible, but apparently is not).    Use a literal name mactch so it is NOT fast.";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	@Override
	public void execute(XmlDoc.Element args, Inputs in, Outputs outputs, XmlWriter w) throws Throwable {
		String namespace = args.stringValue("namespace");
	    Integer nb = args.intValue("nb-threads", 1);

		countAll_ = 0l;
		// Check this namespace	
		checkNamespace (executor(), namespace, nb, w);
		
		// Now recursively chekc the rest
		list(executor(), namespace, nb, w);
		w.add("namespace-count", countAll_);

	}



	private void list(ServiceExecutor executor, String namespace,
			Integer nb, XmlWriter w)
			throws Throwable {
		PluginTask.checkIfThreadTaskAborted();
		

		// Iterate over children namespaces
		Vector<String> absPath = new Vector<String>();
		Vector<String> relPath = new Vector<String>();
		Integer s = listNameSpaces(executor, namespace, absPath, relPath);
		if (s==-1) {
			return;
		} else if (s==0) {
			w.add("namespace", new String[] {"failed-to-list-children", "true"}, namespace);
			return;
		}

		for (int i = 0; i < absPath.size(); i++) {
			countAll_++;
			PluginTask.checkIfThreadTaskAborted();
			String tAbs = absPath.elementAt(i);
			
			// CHeck this namespace
			checkNamespace (executor, tAbs, nb, w);

			// Descend into child namespace
			list(executor, tAbs, nb, w);
		}

	}
	
	private void checkNamespace (ServiceExecutor executor,
			String namespace, Integer nb, XmlWriter w) throws Throwable {
		w.mark();
		w.push("namespace", new String[] {"path", namespace});						
		//
		// Iterate through each asset in the namespace
		XmlDocMaker dm = new XmlDocMaker("args");
		String tNamespace = namespace.replace("'", "\\'");
		dm.add("where", "namespace='"+tNamespace+"'");
		dm.add("size", "infinity");
		dm.add("action", "get-name");
		dm.add("pipe-nb-threads", nb);
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		Boolean some = false;
		if (r!=null) {
			Collection<XmlDoc.Element> names =  r.elements("name"); 
			if (names!=null) {
				Vector<String> foundIDs = new Vector<String>();
				for (XmlDoc.Element name : names) {
					String theName = name.value();
					String theID = name.value ("@id");
					
					// If this ID has already been handled, don't look at it again
					if (!foundIDs.contains(theID)) {
						Collection<String> dids = findDuplicates (executor, tNamespace, theID, theName, nb);
						if (dids!=null) {
							Integer size = dids.size();
							w.add("asset", new String[] {"name", theName}, theID);
							w.push("duplicates", new String[] {"size",size.toString()});
							for (String did : dids) {
								w.add("id", did);					
							}
							w.pop();
							some = true;
							// 
							foundIDs.add(theID);
							foundIDs.addAll(dids);
						}
					}
				}
			}
		}
		w.pop();
		if (!some) {
			w.resetToMark();
		}
	}

	private Collection<String> findDuplicates (ServiceExecutor executor, String namespace, 
			String id, String name, Integer nb) throws Throwable {
		String tName = name.replace("'", "\\'");   
		String tNameSpace = namespace.replace("'", "\\\\'"); 		
		XmlDocMaker dm = new XmlDocMaker("args");
		String q = "namespace='"+tNameSpace+"' and id!="+id+
				" and name matches literal('"+tName+"')";
		dm.add("where", q);
		dm.add("size", "infinity");
		dm.add("pipe-nb-threads", nb);
		XmlDoc.Element r = executor.execute("asset.query", dm.root());
		if (r==null) {
			return null;
		}
		Collection<String> ids = r.values("id");
		if (ids!=null) {
			return ids;
		}
		return null;
	}

	static private Integer listNameSpaces(ServiceExecutor executor, String nameSpace, Vector<String> absPath,
			Vector<String> relPath) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		try {
			XmlDoc.Element r = executor.execute("asset.namespace.list", dm.root());
			String path = r.value("namespace/@path");
			//
			Collection<String> nss = r.values("namespace/namespace");
			if (nss == null) {
				return -1;
			}
			// Absolute path
			if (nss != null) {
				for (String ns : nss) {
					ns = path + "/" + ns;
					absPath.add(ns);
				}
			}
			// Relative path
			relPath.addAll(nss);
			return 1;
		} catch (Throwable t) {
			return 0;
		}
	}
}
