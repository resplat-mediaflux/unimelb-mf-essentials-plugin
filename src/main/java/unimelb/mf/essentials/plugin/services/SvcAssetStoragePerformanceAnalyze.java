package unimelb.mf.essentials.plugin.services;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.PluginService.Interface;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetStoragePerformanceAnalyze extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.store.performance.analyze";
	private static Integer N_THREADS = 50;

	private Interface _defn;

	public SvcAssetStoragePerformanceAnalyze() {
		_defn = new Interface();


		Interface.Element name = new Interface.Element("namespace", StringType.DEFAULT, "Existing parent namespace for data creates.", 1, 1);
		_defn.add(name);

		Interface.Element store = new Interface.Element("store", StringType.DEFAULT, "A store to test.", 1, Integer.MAX_VALUE);
		_defn.add(store);

		Interface.Element size = new Interface.Element("size", StringType.DEFAULT, "Asset size in bytes", 1, Integer.MAX_VALUE);
		size.add(new Interface.Attribute("n", IntegerType.DEFAULT, "Number of assets to create.", 0));
		_defn.add(size);

		_defn.add(new Interface.Element("op",
				new EnumType(
						new String[] { "read", "write" }),
				"Read or write (default).", 1, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "For each store, for each size, create n-assets assets and measure either the write or read performance.  For write, we create assets with asset.test.create and measure the time.  For read, we again create assets, but then user asset.content.validate to read them (and we measure the time taken for the read component).  The service will make namespaces under the given parent. These will be named per the store.  After use, the assets and namespaces are destroyed.";
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}
	
	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String namespace = args.value("namespace");
		Collection<String>  stores = args.values("store");
		Collection<XmlDoc.Element>  sizes = args.elements("size");
		String op = args.stringValue("op");

		// Iterate
		for (XmlDoc.Element size : sizes) {
			PluginTask.checkIfThreadTaskAborted();

			String csize = size.value();
			Integer n = size.intValue("@n");
			for (String store : stores) {
				PluginTask.checkIfThreadTaskAborted();

				test (executor(), namespace, store, csize, n, op, w);
			}		
		}


	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	private static void test (ServiceExecutor executor, String namespace,
			String store, String csize, Integer n, String op, XmlWriter w) throws Throwable {

		PluginTask.checkIfThreadTaskAborted();

		// Create child namespace with store
		String ns = namespace + "/" + store;
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", ns);
		dm.add("store", store);
		executor.execute("asset.namespace.create", dm.root());

		// Create assets and time
		PluginTask.checkIfThreadTaskAborted();
		dm = new XmlDocMaker("args");
		dm.add("time", true);
		dm.push("service", new String[] {"name", "asset.test.create"});
		if (op.equals("write")) {
			dm.add("gen-csum", false);
		} else {
			dm.add("gen-csum", true);
		}
		dm.add("namespace", ns);
		dm.add("nb", n);
		dm.add("csize", csize);
		dm.pop();
		XmlDoc.Element r = executor.execute("service.execute", dm.root());

		// If write, we are done. If read, we read them back and measure that
		if (op.equals("write")) {
			formatRate (n, store, csize, r, w);
		} else {
			PluginTask.checkIfThreadTaskAborted();
			// Validate the assets
			dm = new XmlDocMaker("args");
			dm.add("time", true);
			dm.push("service", new String[] {"name", "asset.query"});
			dm.add("where", "namespace='"+ns+"'");
			dm.add("action", "pipe");
			dm.add("service", new String[] {"name", "asset.content.validate"});
			dm.pop();
			r = executor.execute("service.execute", dm.root());
			formatRate (n, store, csize, r, w);
		}

		// Destroy
		PluginTask.checkIfThreadTaskAborted();
		dm = new XmlDocMaker("args");
		dm.add("where", "namespace='"+ns+"'");
		dm.add("action", "pipe");
		dm.add("pipe-nb-threads", N_THREADS);
		dm.add("service", new String[] {"name", "asset.destroy"});
		executor.execute("asset.query", dm.root());
		//
		PluginTask.checkIfThreadTaskAborted();
		dm = new XmlDocMaker("args");
		dm.add("namespace", ns);
		executor.execute("asset.namespace.destroy", dm.root());
	}
	
	
	private static void formatRate (Integer n, String store, String csize, XmlDoc.Element r, XmlWriter w) throws Throwable {
		String time = r.value("time");     // Seconds
		Double ftime = Double.parseDouble(time);
		//
		Double fsize = Double.parseDouble(csize) * n;  // Total bytes written
		Double rate = fsize / ftime;   // B/s
		Double rate2 = rate / 1000000;
		DecimalFormat fmt = new DecimalFormat("#.####");
		//	
		w.add("store", new String[] {"size", csize, "n", ""+n, "rate", fmt.format(rate), 
				"rate_MBs", fmt.format(rate2)}, store);

	}

}
