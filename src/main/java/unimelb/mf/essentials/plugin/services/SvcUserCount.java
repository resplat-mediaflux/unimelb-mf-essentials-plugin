package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcUserCount extends PluginService {

	private static class UserRecord implements Comparable<UserRecord> {
		public final String domain;
		public final String username;
		public final String name;
		public final String email;
		public final boolean enabled;

		UserRecord(XmlDoc.Element ue) throws Throwable {
			this.domain = ue.value("@domain");
			this.username = ue.value("@user");
			this.name = ue.value("name");
			String email = ue.value("e-mail");
			this.email = email == null ? null : email.toLowerCase();
			this.enabled = ue.booleanValue("@enabled", true);
		}

		public String actorName() {
			return String.format("%s:%s", this.domain, this.username);
		}

		public boolean equals(Object o) {
			if (o != null && (o instanceof UserRecord)) {
				UserRecord u = (UserRecord) o;
				return this.domain.equals(u.domain) && this.username.equals(u.username);
			}
			return false;
		}

		public int hashCode() {
			return actorName().hashCode();
		}

		public String toString() {
			return String.format("%s,%s,%s,%s,%s,", this.domain, this.username, this.name == null ? "" : this.name,
					Boolean.toString(this.enabled), this.email == null ? "" : this.email);
		}

		@Override
		public int compareTo(UserRecord u) {
			return actorName().compareTo(u.actorName());
		}
	}

	private Interface _defn;

	public SvcUserCount() {
		_defn = new Interface();
		_defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
				"Include disabled domains and users. Defaults to false", 0, 1));
		_defn.add(new Interface.Element("exclude-domain", StringType.DEFAULT, "Exclude this domain.", 0,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("role", StringType.DEFAULT,
				"The user must hold this role (which must be of type 'role') to be included. If you provide more than one, the user must hold at least one role in the list to be included. For LDAP domains, one role at ,east must be provided.",
				0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("exclude-email-domain", StringType.DEFAULT, "Exclude any accounts when the email comes from this domain (e.g. unimelb.edu.au).", 0,
				Integer.MAX_VALUE));
	}

	public String name() {
		return "unimelb.user.count";
	}

	public String description() {
		return "Counts the number of users in the specified authentication domains.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		boolean includeDisabled = args.booleanValue("include-disabled", false);
		Collection<String> excludeDomains = args.values("exclude-domain");
		Collection<String> roles = args.values("role");
		Collection<String> excludeEmailDomains = args.values("exclude-email-domain");
		countUsers (executor(), excludeDomains, includeDisabled, roles,
				    excludeEmailDomains, w);
	}



	private  void countUsers (ServiceExecutor executor, Collection<String> excludeDomains,
			boolean includeDisabled, Collection<String> roles, 
			Collection<String> excludeEmailDomains, XmlWriter w) throws Throwable {
		Integer nTotal = 0;
		XmlDoc.Element r = executor.execute("authentication.domain.list");
		List<XmlDoc.Element> des = r.elements("domain");
		if (des != null) {

			//Iterate over domains
			for (XmlDoc.Element de : des) {
				String domain = de.value();

				// Filter domains out
				if ((includeDisabled || de.booleanValue("@enabled", true))
						&& (excludeDomains == null || !excludeDomains.contains(domain))) {
					String authority = de.value("@authority");
					String protocol = de.value("@protocol");
					if (protocol!=null && protocol.equalsIgnoreCase("ldap") && roles==null) {
						// Do not allow pulling all users from ldap domain if no
						// role filter is specified.
						throw new Exception("Retrieving all users from ldap domain: " + domain
								+ " without any role filter set is NOT allowed.");
					}

					// Add all users for this domain filtered by roles
					Integer n = countUsers(executor, includeDisabled, authority, protocol, domain, roles,
							excludeEmailDomains);
					w.add("users", new String[] {"domain", domain}, n);;
					nTotal += n;
				}
			}
		}
		w.add("total-users", nTotal);
	}

	private Integer countUsers (ServiceExecutor executor, Boolean includeDisabled, 
			String authority, String protocol, String domain, 
			Collection<String> roles, Collection<String> excludeEmailDomains) throws Throwable {


		if (roles!=null) {
			// Iterate over roles because user.describe ANDs the roles and we want to OR them
			Integer n = 0;
			for (String role : roles) {
				Integer n2 = countUsers (executor, includeDisabled, authority, protocol, 
						domain, role, excludeEmailDomains);
				n+= n2;
			}
			return n;
		} else {
			String role = null;	
			return countUsers (executor, includeDisabled,  authority, protocol, domain, 
					role, excludeEmailDomains);
		}
	}


	private  Integer countUsers (ServiceExecutor executor, Boolean includeDisabled,
			String authority, String protocol, String domain, String role,
			Collection<String> excludeEmailDomains) throws Throwable {


		PluginTask.checkIfThreadTaskAborted();
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("domain", domain);
		if (role!=null) {
			dm.add("role", new String[] {"type", "role"}, role);
		}
		if (authority!=null) {
			if (authority != null) {
				dm.add("authority", new String[] { "protocol", protocol }, authority);
			}
		}
		XmlDoc.Element r = executor.execute("user.describe", dm.root());
		List<XmlDoc.Element> us = r.elements("user");
		if (us != null) {
			int n = 0;
			for (XmlDoc.Element u : us) {
				
				// Filter out as appropriate
				String email = u.value("e-mail");
				Boolean isEnabled = u.booleanValue("@enabled",true);			
				//
				Boolean keepOnEmail = keepOnEmail = keepOnEmail (excludeEmailDomains, email);
				Boolean keepOnDisabled = true;
				if (!includeDisabled && !isEnabled) {
					keepOnDisabled = false;
				}
				// See if we want to keep this one.
				if (keepOnEmail && keepOnDisabled) {
					n++;
				}
			}
			return n;
		} else {
			return 0;
		}
	}

	private Boolean keepOnEmail (Collection<String> excludeDomains, String email) throws Throwable {
		if (excludeDomains==null) {
			return true;
		}
		if (excludeDomains.size()==0) {
			return true;
		}
		if (email==null) {
			return true;
		}
		for (String excludeDomain : excludeDomains) {
			if (email.contains(excludeDomain)) {
				return false;
			}
		}
		return true;
	}


	// TBD: when we upgrade to 4.9.* we can put the cursor into the user.describe
	// as shown below for authenitcation.user.describe instead of setting size to infinity
	private static void addUsersOld(ServiceExecutor executor, Set<UserRecord> results, String authority, String protocol,
			String domain, boolean includeDisabled) throws Throwable {
		boolean complete = false;
		int size = 100;
		int idx = 1;
		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			if (authority != null) {
				dm.add("authority", new String[] { "protocol", protocol }, authority);
			}
			dm.add("idx", idx);
			dm.add("size", size);
			XmlDoc.Element r = executor.execute("authentication.user.describe", dm.root());
			complete = r.booleanValue("complete");
			idx += size;
			List<XmlDoc.Element> ues = r.elements("user");
			if (ues != null) {
				for (XmlDoc.Element ue : ues) {
					if (includeDisabled || ue.booleanValue("@enabled", true)) {
						UserRecord ur = new UserRecord(ue);
						if (ur.email != null) {
							results.add(ur);
						}
					}
				}
			}
		}

	}
}
