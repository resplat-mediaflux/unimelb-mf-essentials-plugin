package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Collections;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentAllHasStore extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.all.has.store";

	private Interface _defn;

	public SvcAssetContentAllHasStore() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"Make sure content is held in this store. If multiple stores are given, they are all checked.  We only require one of the target stores per asset version to be satisifed (an implict OR).", 1, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("include-zero", BooleanType.DEFAULT,
				"Include zero-sized files (default false)", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Iterates through all asset versions and validates that if there is a content (whether singular or in a content copy), that that content is found in one of the listed stores.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String assetID = args.value("id");
		Collection<String> stores = args.values("store");
		checkStores(executor(), stores);
		Boolean includeZero = args.booleanValue("include-zero", false);

		// Get asset versions
		Collection<String> versions = executor()
				.execute("asset.versions", "<args><id>" + assetID + "</id></args>", null, null)
				.values("asset/version/@n");

		// Iterate through all versions 
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("asset", new String[] { "id", assetID });
		Boolean some = false;           // SOmething from at least one version to show
		for (String version : versions) {
			PluginTask.checkIfThreadTaskAborted();

			// Get asset for this version
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", new String[] { "version", version }, assetID);
			XmlDoc.Element r = executor().execute("asset.get", dm.root());
			XmlDoc.Element content = r.element("asset/content");

			// Proceed if we have some content
			if (content!=null) {
				Long csize = content.longValue("size");
				if (csize>0 || includeZero) {

					// See if there are content copies
					XmlDoc.Element contentCopies = content.element("copies"); 
					if (contentCopies==null) {

						// There are no content copies, the content is singular.
						// Make sure it is in one of the desired stores.
						String assetStore = content.value("store");
						if (!stores.contains(assetStore)) {
							some = true;
							dmOut.add("version", new String[] {"store", assetStore, "csize", ""+csize, "copies", "1"}, version);
						}
					} else {
						// Iterate through the content copies and make sure there is a copy in the desired store
						Collection<String> contentCopyStores = contentCopies.values("copy/store");
						Integer nCopies = contentCopyStores.size();
						if (Collections.disjoint(stores, contentCopyStores)) {
							some = true;
							dmOut.add("version", new String[] {"csize", ""+csize, "copies", ""+nCopies}, version);
						}						
					}
				}
			}	
		}


		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}
	}

	private void checkStores (ServiceExecutor executor, Collection<String> stores) throws Throwable {
		for (String store : stores) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", store);
			XmlDoc.Element r = executor.execute("asset.store.exists", dm.root());
			if (!r.booleanValue("exists")) {
				throw new Exception ("Store " + store + " does not exist");
			}
		}
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
