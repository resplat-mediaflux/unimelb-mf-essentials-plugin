package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetNoteGenerate extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.note.generate";

	private Interface _defn;

	public SvcAssetNoteGenerate() {
		_defn = new Interface();

		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "Asset id.", 1, 1));

		_defn.add(new Interface.Element("action", new EnumType(new String[] { "add", "merge", "replace" }),
				"Action to perform when modifying meta information. Defaults to 'merge'. 'add' means append, 'merge' means combine, 'replace' means to replace matching",
				0, 1));
		_defn.add(new Interface.Element("ns", StringType.DEFAULT, "ns attribute for mf-note document.", 0, 1));
		_defn.add(new Interface.Element("tag", StringType.DEFAULT, "tag attribute for mf-note document.", 0, 1));
		_defn.add(new Interface.Element("prefix", StringType.DEFAULT, "prefix to prepend the value.", 0, 1));
		_defn.add(new Interface.Element("suffix", StringType.DEFAULT, "suffix to append to the value.", 0, 1));

		Interface.Element service = new Interface.Element("service", XmlDocType.DEFAULT, "The service to execute.", 1,
				1);
		service.add(new Interface.Attribute("name", StringType.DEFAULT, "Service name", 1));
		service.add(new Interface.Attribute("xpath", StringType.DEFAULT,
				"The xpath to resolve the value from the service result.", 1));
		service.setIgnoreDescendants(true);
		_defn.add(service);

		_defn.add(new Interface.Element("ignore-null-value", BooleanType.DEFAULT,
				"Ignored if the result value is null. Defaults to true.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_MODIFY;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Add the result value of a service to asset meta data: meta/mf-note/note.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String id = args.value("id");
		boolean ignoreNullValue = args.booleanValue("ignore-null-value", true);
		String metaAction = args.stringValue("action", "merge");
		String docNS = args.value("ns");
		String docTag = args.value("tag");

		/*
		 * execute the service
		 */
		XmlDoc.Element service = args.element("service");
		String serviceName = service.value("@name");
		String resultXpath = service.value("@xpath");
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		if (service.hasSubElements()) {
			dm.add(service, false);
		}
		XmlDoc.Element re = executor().execute(serviceName, dm.root());
		String value = re.value(resultXpath);
		if (value == null && ignoreNullValue) {
			w.add("updated", new String[] { "id", id }, false);
			return;
		}

		String prefix = args.value("prefix");
		String suffix = args.value("suffix");
		if (prefix != null) {
			value = prefix + value;
		}
		if (suffix != null) {
			value = "" + value + suffix;
		}

		/*
		 * update asset meta data
		 */
		dm = new XmlDocMaker("args");
		dm.add("id", id);
		dm.push("meta", new String[] { "action", metaAction });
		dm.push("mf-note", new String[] { "ns", docNS, "tag", docTag });
		dm.add("note", value);
		dm.pop();
		dm.pop();
		re = executor().execute("asset.set", dm.root());
		boolean updated = re.booleanValue("version/@changed-or-created", false);
		w.add("updated", new String[] { "id", id }, updated);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
