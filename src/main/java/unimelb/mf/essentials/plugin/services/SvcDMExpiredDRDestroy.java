/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 * 
 * TBD scrap namespace code once we have transitioned to collection assets
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.IDUtils;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.utils.DateUtil;


public class SvcDMExpiredDRDestroy extends PluginService {

	private static String MANIFEST_DOCTYPE = "unimelb:shareable-upload-manifest";

	private Interface _defn;

	public SvcDMExpiredDRDestroy()  throws Throwable {
		_defn = new Interface();
		//
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT,
				"The parent namespace path into which expired DataMover data has been moved, ready for assessment to be destroyed.", 0,1));
		_defn.add(new Interface.Element("collection", StringType.DEFAULT,
				"The parent collection asset path into which expired DataMover data has been moved, ready for assessment to be destroyed.", 0,1));
		_defn.add(new Interface.Element("age", IntegerType.DEFAULT,
				"The number of days (default 30) after the data were moved before considering the data for destruction.", 0,1));
		_defn.add(new Interface.Element("destroy", BooleanType.DEFAULT,
				"If true (default false), instead of just checking, actually destroy the data.", 0,1));
		_defn.add(new Interface.Element("show-all", BooleanType.DEFAULT,
				"If true show all candidate namespaces. If false (default), only show those for which the manifest asset was moved more than 'age' days ago.", 0,1));	
		_defn.add(new Interface.Element("email", StringType.DEFAULT, 
				"Send the output to this email address.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Checks (on the DR server where it must be executed) for DataMover uploaded instrument data (in transactional namespaces) that has expired and been moved to a special namespace and is ready for destruction.  Can check a namespace and/or a collection asset path.";
	}

	public String name() {
		return "unimelb.DM.expired.DR.destroy";
	}

	public boolean canBeAborted() {

		return true;
	}


	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {
		// Inputs
		String namespace = args.stringValue("namespace");
		String collection = args.stringValue("collection");
		if (namespace==null && collection==null) {
			throw new Exception("You must specify a namespace and/or collection");
		}
		if (namespace!=null && collection!=null) {
			throw new Exception("You must specify only one of namespace or collection");
		}
		Boolean destroy = args.booleanValue("destroy", false);
		Boolean showAll = args.booleanValue("show-all", false);
		Integer age = args.intValue("age", 30);
		String email = args.value("email");


		// Find manifest assets under the parent namespace or collection
		Collection<XmlDoc.Element> assets =
				findManifestAssets(executor(), namespace, collection); 
		if (assets.size()==0) {
			return;
		}

		// Iterate through manifest assets
		XmlDocMaker result = new XmlDocMaker("args");
		for (XmlDoc.Element asset : assets) {
			XmlDocMaker res = new XmlDocMaker("args");
			// Find path of asset
			res.push("manifest-asset");
			String path  = asset.value("path");
			String manifestAssetID = asset.value("@id");
			res.add("id", manifestAssetID);
			res.add("path", path);

			// Get the transactional parent path (namespace or collection)
			// into which the data were uploaded and in which the
			// manifest asset currently resides on the DR server
			String nsParentPath = asset.value("namespace");
			String collParentID = asset.value("parent");
			String collParentPath = null;
			if (nsParentPath!=null) {
				res.add("parent-namespace-path", new String[] {"type", "namespace"}, nsParentPath);
			} else if (collParentID!=null) {
				XmlDoc.Element parent = AssetUtils.getAsset(executor(), null,
						null, collParentID);
				collParentPath = parent.value("asset/path");				
				res.add("parent-collection-path", new String[] {"type", "collection"}, collParentPath);
			}

			// Get manifest meta-data
			XmlDoc.Element mfMeta = asset.element("meta/"+MANIFEST_DOCTYPE);
			Date dateMoved = mfMeta.dateValue("date-moved-on-DR");
			if (dateMoved==null) {
				throw new Exception ("No 'date-moved-on-DR' elementy on manifest asset. This is inconsistent state.");
			}
			res.add("date-moved", dateMoved);

			// See if old enough to destroy
			Date today = new Date();
			res.add("today", today);
			long diff = DateUtil.dateDifferenceInDays(today, dateMoved);
			res.add("days-since-moved", diff);
			Date td = unimelb.utils.DateUtil.addDays(dateMoved, age);
			Boolean list = false;
			if (today.after(td)) {
				list = true;

				// Candidate to destroy
				res.add("can-be-destroyed", true);
				if (destroy) {
					// Is this transactional path a namespace or collection asset
					if (nsParentPath!=null) {
						XmlDocMaker dm = new XmlDocMaker("args");
						dm.add("namespace", nsParentPath);
						executor().execute("asset.namespace.destroy", dm.root());
						res.add("destroyed", new String[] {"type", "namespace"}, nsParentPath);
					} else if (collParentID!=null) {
						XmlDocMaker dm = new XmlDocMaker("args");
						dm.add("id", collParentID);
						dm.add("members", true);
						executor().execute("asset.destroy", dm.root());
						res.add("destroyed", new String[] {"id", collParentID, "type", "collection"}, collParentPath);
					}
				} else {
					if (showAll) {
						list = true;
					}
					res.add("destroyed", false);
				}
				res.pop();

				// List findings
				if (list) {
					result.addAll(res.root().elements());
				}

			}

			// Output
			if (result.root().hasSubElements()) {
				w.addAll(result.root().elements());
				if (email!=null) {
					if (result.root().hasSubElements()) {
						result.push("service", new String[] {"name",name()});
						result.add(args);
						result.pop();
						String body = XmlPrintStream.outputToString(result.root());
						long uuid = IDUtils.serverUUID(executor());
						String subject = "["+uuid+"] - DataMover expired (and destroyed on primary) instrument upload destroy on DR (date="+new Date().toString()+")";
						MailUtils.sendEmail(executor(), email, null, null, null, subject, body, false);
					} 
				}
			}
		}
	}

	public static Collection<XmlDoc.Element> findManifestAssets (ServiceExecutor executor, 
			String namespace, String collection) throws Throwable  {
		Vector<XmlDoc.Element> res = new Vector<XmlDoc.Element>();

		if (namespace!=null) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("size", "infinity");
			dm.add("action", "get-meta");
			dm.add("namespace", namespace);
			dm.add("where", "name='__manifest.csv' and " + MANIFEST_DOCTYPE + " has value");			
			XmlDoc.Element r = executor.execute("asset.query", dm.root());
			if (r!=null) {
				Collection<XmlDoc.Element> t = r.elements("asset");
				if (t!=null) {
					res.addAll(t);
				}
			}
		} else if (collection!=null) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("size", "infinity");
			dm.add("action", "get-meta");
			dm.add("collection", "path="+collection);
			dm.add("where", "name='__manifest.csv' and " + MANIFEST_DOCTYPE + " has value");
			XmlDoc.Element r = executor.execute("asset.query", dm.root());
			if (r!=null) {
				Collection<XmlDoc.Element> t = r.elements("asset");
				if (t!=null) {
					res.addAll(t);
				}
			}
		}
		return res;
	}
}
