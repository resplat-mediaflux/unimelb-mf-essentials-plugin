package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.streams.SizedInputStream;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

/**
 * WIP
 * @author Neil Killeen
 *
 */
public class SvcAssetContentTruncate extends PluginService {
	private Interface _defn;

	public SvcAssetContentTruncate() {

		_defn = new Interface();
		Interface.Element me = new Interface.Element("id", AssetType.DEFAULT, "Asset ID to copy.", 0, 1);
		Interface.Attribute ma = new Interface.Attribute("version",  StringType.DEFAULT,  "Asset version to copy (defaults to 0 - latest version)", 0);
		me.add(ma);
		_defn.add(me);
		_defn.add(new Interface.Element("size", LongType.DEFAULT,
				"The size in bytes to truncate at.  If not supplied, use the value in the header.",
				0, 1));
	}

	public String name() {
		return "unimelb.asset.content.truncate";
	}

	public String description() {
		return "Copies the number of bytes from an asset as specified by its content/size to a client side file.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public boolean canBeAborted() {

		return true;
	}

	public int maxNumberOfOutputs() {
		return 1;
	}
	public int minNumberOfOutputs() {
		return 1;
	}


	public void execute(XmlDoc.Element args, Inputs in, Outputs outputs, XmlWriter w) throws Throwable {

		XmlDoc.Element id = args.element("id");
		Long size = args.longValue("size", -1L);

		// Get asset
		PluginService.Outputs o = new PluginService.Outputs(1);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add(id);
		
		// Fetch with no output as with output the size is the actual size, not the size in the DB
		XmlDoc.Element r = executor().execute("asset.get", dm.root());
		if (size.equals(-1L)) {
			size = r.longValue("asset/content/size");	
		}

		// Fetch again
	    r = executor().execute("asset.get", dm.root(), null, o);
		
		// Copy to output
		outputs.output(0).setData(new SizedInputStream(o.output(0).stream(), size), size, null);
	}

	/*
	public static void copy(InputStream in, long length, OutputStream out) throws Throwable {
		byte[] buf = new byte[1024];
		long totalRead = 0L;
		long totalWrite = 0L;
		int nRead;
		while ((nRead = in.read(buf)) >= 0) {
			if (nRead > 0) {
				totalRead += nRead;
				if (totalRead <= length) {
					out.write(buf, 0, nRead);
					totalWrite += nRead;
				} else {
					out.write(buf, 0, (int) (length - totalWrite));
					totalWrite = length;
				}
			}
		}
	}
	*/

}