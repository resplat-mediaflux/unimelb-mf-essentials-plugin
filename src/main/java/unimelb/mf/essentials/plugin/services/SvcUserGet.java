package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.services.SvcUserSearch.UserEntry;

import java.util.ArrayList;
import java.util.List;

public class SvcUserGet extends PluginService {

	public static String SERVICE_NAME = "unimelb.user.get";

	private Interface _defn;

	public SvcUserGet() {
		_defn = new Interface();
		Interface.Element user = new Interface.Element("user", StringType.DEFAULT, "User name.", 1, Integer.MAX_VALUE);
		user.add(new Interface.Attribute("authority", StringType.DEFAULT, "Authority", 0));
		user.add(new Interface.Attribute("domain", StringType.DEFAULT, "User's authentication domain.", 1));
		_defn.add(user);
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Get user details such as name, email and departments";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		List<UserEntry> users = getUsers(executor(), args.elements("user"));
		SvcUserSearch.outputUsers(users, w);
	}

	private static List<UserEntry> getUsers(ServiceExecutor executor, List<XmlDoc.Element> ues) throws Throwable {
		if (ues != null && !ues.isEmpty()) {
			List<UserEntry> users = new ArrayList<UserEntry>(ues.size());
			for (XmlDoc.Element u : ues) {
				String authority = u.value("@authority");
				String domain = u.value("@domain");
				String user = u.value();
				users.add(getUser(executor, authority, domain, user));
			}
			if (users != null) {
				return users;
			}
		}
		return null;
	}

	private static UserEntry getUser(ServiceExecutor executor, String authority, String domain, String user)
			throws Throwable {

		XmlDoc.Element ue = describeUser(executor, authority, domain, user);

		UserEntry.Builder ub = UserEntry.builder().setAuthority(authority).setDomain(domain).setUser(user);
		ub.setEmail(ue.value("e-mail"));
		ub.setName(ue.value("name"));

		XmlDoc.Element ee = null;
		if (SvcUserSearchLdap.isStaffDoaminAlias(executor, domain)) {
			ee = searchLdap(executor, SvcUserSearchLdap.PROVIDER_STAFF, SvcUserSearchLdap.ROOT_PATH_STAFF, user);
		} else if (SvcUserSearchLdap.isStudentDomainAlias(executor, domain)) {
			ee = searchLdap(executor, SvcUserSearchLdap.PROVIDER_STUDENT, SvcUserSearchLdap.ROOT_PATH_STUDENT, user);
		}
		if (ee != null) {
			if (ee.elementExists("department")) {
				ub.addDepartment(ee.value("department"));
			}
			if (ee.elementExists("uOMDepartments")) {
				ub.addDepartments(ee.values("uOMDepartments"));
			}
			if(ee.elementExists("employeeID")) {
				ub.setEmployeeID(ee.value("employeeID"));
			}
		}
		return ub.build();
	}

	private static XmlDoc.Element describeUser(ServiceExecutor executor, String authority, String domain, String user)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (authority != null) {
			dm.add("authority", authority);
		}
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("user.describe", dm.root()).element("user");
	}

	private static XmlDoc.Element searchLdap(ServiceExecutor executor, String provider, String path, String username)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("provider", provider);
		dm.add("path", path);
		dm.add("filter", SvcUserSearchLdap.UID_ATTR_NAME + "=" + username);
		dm.add("attribute", "department");
		dm.add("attribute", "uOMDepartments");
		dm.add("attribute", "employeeID");
		return executor.execute("ldap.search", dm.root()).element("entry");
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
