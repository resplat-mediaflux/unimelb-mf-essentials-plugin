/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.io.PrintWriter;
import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcAssetNameSpaceChildFind extends PluginService {

	private Interface _defn;
	private Long count_ = 0l;
	private Long countAll_ = 0l;

	public SvcAssetNameSpaceChildFind() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The parent namespace to find below.", 1, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A case-sensitive string that the child namespace name (just the relative part) of interest must contain.",
				0, 1));
		_defn.add(new Interface.Element("has-asset", BooleanType.DEFAULT, "Is there an asset with the same name as a namespace? (Default is to not check, false)", 0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, file on the server to save the output as a CSV file. Of the form <namespace path> or, if :has-asset is true, <namespace path>,<asset ID>", 0, 1));
	}

	@Override
	public String name() {
		return "unimelb.asset.namespace.child.find";
	}

	@Override
	public String description() {
		return "Service to recursively list any namespaces that contain the given string in their name.";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}

	@Override
	public void execute(XmlDoc.Element args, Inputs in, Outputs outputs, XmlWriter w) throws Throwable {
		String namespace = args.stringValue("namespace");
		String contains = args.value("contains");
		Boolean hasAsset = args.booleanValue("has-asset", false);
		if (contains==null && !hasAsset) {
			throw new Exception("You must give one of :contains or :has-asset");
		}
		String url = args.value("url");

		PrintWriter pw = null;
		if (url!=null) {
			String urlPath = url.substring(5);   // Get rid of file:
			pw = new PrintWriter (urlPath);
		}

		count_ = 0l;
		countAll_ = 0l;
		list(executor(), namespace, contains, hasAsset, w, pw);
		w.add("count-all", countAll_);
		w.add("count-found", count_);
		if (pw!=null) {
			pw.close();
		}
	}



	private void list(ServiceExecutor executor, String nameSpace, String contains, 
			Boolean hasAsset, XmlWriter w, PrintWriter pw)
					throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Iterate over children namespaces
		Vector<String> absPath = new Vector<String>();
		Vector<String> relPath = new Vector<String>();
		Integer s = listNameSpaces(executor, nameSpace, absPath, relPath);
		if (s==-1) {
			return;
		} else if (s==0) {
			w.add("namespace", new String[] {"failed-to-list-children", "true"}, nameSpace);
			pw.println(nameSpace+" : failed to list children");
			return;
		}
	
		for (int i = 0; i < absPath.size(); i++) {
			countAll_++;
			PluginTask.checkIfThreadTaskAborted();
			String tRel = relPath.elementAt(i);
			String tAbs = absPath.elementAt(i);
			Boolean listIt1 = (contains != null && tRel.contains(contains));
			String assetID = null;
			if (hasAsset) {
				assetID = AssetUtils.exists(executor, tAbs);
			}
			if (listIt1 || assetID!=null) {
				count_++;
				String tc = null;
				if (listIt1) {
					tc = "true";
				}
				String th = null;
				if (assetID!=null) {
					th = "true";
				}
				w.add("namespace", new String[] {"contains", tc, "asset-exists", th, "asset-id", assetID}, tAbs);
				if (pw != null) {
					if (assetID!=null) {
						pw.println(tAbs+","+assetID);
					} else {
						pw.println(tAbs);
					}
				}
			}

			// Descend into child namespace
			list(executor, tAbs, contains, hasAsset, w, pw);

		}
	}

	static public Integer listNameSpaces(ServiceExecutor executor, String nameSpace, Vector<String> absPath,
			Vector<String> relPath) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		try {
			XmlDoc.Element r = executor.execute("asset.namespace.list", dm.root());
			String path = r.value("namespace/@path");
			//
			Collection<String> nss = r.values("namespace/namespace");
			if (nss == null) {
				return -1;
			}
			// Absolute path
			if (nss != null) {
				for (String ns : nss) {
					ns = path + "/" + ns;
					absPath.add(ns);
				}
			}
			// Relative path
			relPath.addAll(nss);
			return 1;
		} catch (Throwable t) {
			return 0;
		}
	}
}
