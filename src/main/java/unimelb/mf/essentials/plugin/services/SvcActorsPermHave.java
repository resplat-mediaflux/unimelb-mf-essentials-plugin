package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.PluginService.Interface.Element;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.ActorUtils;

public class SvcActorsPermHave extends PluginService {

	private Interface _defn;

	public SvcActorsPermHave() {

		_defn = new Interface();
		_defn.add(new Element("type", StringType.DEFAULT, "The type of actor to scrutinize. Defaults to all actor types (actor.type.list).", 0, 1));
		Interface.Element perm = new Interface.Element("perm", XmlDocType.DEFAULT, "Held permission to search for.", 1,1);
		perm.add(new Interface.Element("access", new StringType(64), "Access type.", 1, 1));
		Interface.Element permResource = new Interface.Element("resource", new StringType(255),
				"Pattern for resource. Make sure you include trailing ':' on namespace resources if you set direct=true.", 1, 1);
		permResource.add(new Interface.Attribute("type", new StringType(32), "Resource type.", 1));
		perm.add(permResource);
		_defn.add(perm);
		_defn.add(new Element("direct", BooleanType.DEFAULT, "If true (default), only list actors that hold the permission direcrtly (not through some inherited actor).", 0, 1));

	}

	public String name() {

		return "unimelb.actors.perm.have";

	}

	public String description() {

		return "Finds which actors of the specified type hold the given permission.";

	}

	public Interface definition() {

		return _defn;

	}

	public Access access() {

		return ACCESS_ACCESS;

	}

	public boolean canBeAborted() {

		return true;
	}


	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse
		XmlDoc.Element perm = args.element("perm");
		Boolean direct = args.booleanValue("direct", true);
		String theType = args.value("type");
		//
		Vector<String> types = new Vector<String>();
		if (theType==null) {
			types.addAll(ActorUtils.types(executor()));
		} else {
			types.add(theType);
		}

		//
		// Iterate over actor types
		for (String type : types) {
			PluginTask.checkIfThreadTaskAborted();
			w.push("type", new String[] {"name", type});
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("type", type);
			dm.add(perm);

			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element r = executor().execute("actor.have", dm.root());
			if (r!=null) {
				Collection<XmlDoc.Element> actors = r.elements("actor");
				if (actors!=null) {
					for (XmlDoc.Element actor : actors) {
						PluginTask.checkIfThreadTaskAborted();
						if (actor.booleanValue("perm")) {
							if (direct) {
								if (hasDirectPerm (executor(), perm, actor, w)) {
									w.add(actor);
								}
							} else { 
								w.add(actor);
							}
						}
					}
				}			
			}
			w.pop();
		}
	}


	private Boolean hasDirectPerm (ServiceExecutor executor, XmlDoc.Element thePerm, 
			XmlDoc.Element actor, XmlWriter w) throws Throwable {
		String name = actor.value("@name");
		String type = actor.value("@type");
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("type", type);
		dm.add("name", name);
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor.execute("actor.describe", dm.root());
		if (r!=null) {
			Collection<XmlDoc.Element> perms = r.elements("actor/perm");
			if (perms!=null) {
				for (XmlDoc.Element perm : perms) {
		
					PluginTask.checkIfThreadTaskAborted();
					if (thePerm.equals(perm)) {
						return true;						
					}
				}
			}
		}

		return false;
	}
}