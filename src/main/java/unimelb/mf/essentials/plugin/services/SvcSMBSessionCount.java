package unimelb.mf.essentials.plugin.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcSMBSessionCount extends PluginService {

    public static final String SERVICE_NAME = "unimelb.smb.session.count";

    private final Interface _defn;

    public SvcSMBSessionCount() {
        _defn = new Interface();
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Count smb sessions per user.";
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        long totalNumberOfSessions = 0;
        long totalNumberOfOpenFileHandles = 0;
        long totalNumberOfAsyncCommands = 0;
        long totalNumberOfShareConnections = 0;
        long totalNumberOfChannels = 0;
        Map<String, String> domainNames = new HashMap<>();
        List<XmlDoc.Element> ses = executor().execute("smb.session.describe").elements("session");
        Map<String, UserSessions> userSessions = new HashMap<>();
        if (ses != null) {
            for (XmlDoc.Element se : ses) {
                SessionDetails session = new SessionDetails(se, executor(), domainNames);
                UserSessions us = userSessions.get(session.actorName());
                if (us == null) {
                    us = new UserSessions(session.domain, session.user);
                    userSessions.put(session.actorName(), us);
                }
                us.addSession(session);
                totalNumberOfSessions += 1;
                totalNumberOfOpenFileHandles += session.nbOpenFileHandles;
                totalNumberOfAsyncCommands += session.nbAsyncCommands;
                totalNumberOfShareConnections += session.nbShareConnections;
                totalNumberOfChannels += session.nbChannels;
            }
            List<UserSessions> sorted = new ArrayList<>(userSessions.values());
            Collections.sort(sorted, new Comparator<UserSessions>() {

                @Override
                public int compare(UserSessions a, UserSessions b) {
                    return Long.compare(a.totalNumberOfSessions(), b.totalNumberOfSessions());
                }
            });
            for (UserSessions us : sorted) {
                us.save(w);
            }
        }
        w.push("total");
        w.add("nb-users", userSessions.size());
        w.add("nb-smb-sessions", totalNumberOfSessions);
        w.add("nb-open-file-handles", totalNumberOfOpenFileHandles);
        w.add("nb-async-commands", totalNumberOfAsyncCommands);
        w.add("nb-share-connections", totalNumberOfShareConnections);
        w.add("nb-channels", totalNumberOfChannels);
        w.pop();
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    private static class UserSessions {
        public final String domain;
        public final String user;
        private List<SessionDetails> _sessions;

        UserSessions(String domain, String user) {
            this.domain = domain;
            this.user = user;
            _sessions = new ArrayList<>();
        }

        public void addSession(SessionDetails session) {
            _sessions.add(session);
        }

        public long totalOpenFileHandles() {
            return _sessions.stream().collect(Collectors.summarizingLong(s -> s.nbOpenFileHandles)).getSum();
        }

        public long totalAsyncCommands() {
            return _sessions.stream().collect(Collectors.summarizingLong(s -> s.nbAsyncCommands)).getSum();
        }

        public long totalShareConnections() {
            return _sessions.stream().collect(Collectors.summarizingLong(s -> s.nbShareConnections)).getSum();
        }

        public long totalChannels() {
            return _sessions.stream().collect(Collectors.summarizingLong(s -> s.nbChannels)).getSum();
        }

        public Set<String> ids() {
            Set<String> ids = new TreeSet<>();
            _sessions.forEach(session -> ids.add(session.id));
            return Collections.unmodifiableSet(ids);
        }

        public Set<String> authentications() {
            Set<String> as = new TreeSet<>();
            _sessions.forEach(session -> as.add(session.authentication));
            return Collections.unmodifiableSet(as);
        }

        public Set<String> origins() {
            Set<String> os = new TreeSet<>();
            _sessions.forEach(session -> os.add(session.origin));
            return Collections.unmodifiableSet(os);
        }

        public int totalNumberOfSessions() {
            return _sessions.size();
        }

        public void save(XmlWriter w) throws Throwable {
            w.push("user", new String[] { "domain", this.domain, "user", this.user, "nb-smb-sessions",
                    Integer.toString(totalNumberOfSessions()) });
            w.add("id", String.join(", ", ids()));
            w.add("origin", String.join(", ", origins()));
            w.add("nb-open-file-handles", totalOpenFileHandles());
            w.add("nb-async-commands", totalAsyncCommands());
            w.add("nb-share-connections", totalShareConnections());
            w.add("nb-channels", totalChannels());
            w.add("authentication", String.join(", ", authentications()));
            w.pop();
        }
    }

    private static class SessionDetails {
        public final String id;
        public String domain;
        public final String user;
        public final String origin;
        public final long nbOpenFileHandles;
        public final long nbAsyncCommands;
        public final long nbShareConnections;
        public final long nbChannels;
        public final String authentication;

        SessionDetails(XmlDoc.Element se, ServiceExecutor executor, Map<String, String> domainNames) throws Throwable {
            this.id = se.value("@id");
            this.domain = resolveDomainName(executor, se.value("@domain"), domainNames);
            this.user = se.value("@user").toLowerCase();
            this.origin = se.value("origin");
            this.nbOpenFileHandles = se.longValue("status/open-file-handles/@count", 0);
            this.nbAsyncCommands = se.longValue("status/async-commands/@count", 0);
            this.nbShareConnections = se.longValue("status/share-connections/@count", 0);
            this.nbChannels = se.longValue("channels/@count", 0);
            this.authentication = se.value("channels/@authentication");
        }

        public String actorName() {
            return this.domain + ":" + this.user;
        }
    }

    private static String resolveDomainName(ServiceExecutor executor, String domain, Map<String, String> domainNames)
            throws Throwable {
        String domainName = domainNames.get(domain);
        if (domainName == null) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("domain", domain);
            XmlDoc.Element de = executor.execute("authentication.domain.describe", dm.root()).element("domain");
            domainName = de.value("@name");
            Collection<String> aliases = de.values("aliases/alias");
            if (aliases != null) {
                for (String alias : aliases) {
                    domainNames.put(alias, domainName);
                }
            }
        }
        return domainName;
    }
}
