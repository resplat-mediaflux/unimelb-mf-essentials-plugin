package unimelb.mf.essentials.plugin.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.plugin.util.AssetNamespaceUtils;
import unimelb.mf.plugin.util.AssetUtils;
import unimelb.mf.util.AssetPathUtils;

public class SvcAssetCollectionACLChildList extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.collection.acl.child.list";

	private final Interface _defn;

	public SvcAssetCollectionACLChildList() {
		_defn = new Interface();
		Interface.Element path = new Interface.Element("path", StringType.DEFAULT,
				"The collection/namespace path of interest.", 1, 1);
		path.add(new Interface.Attribute("type", new EnumType(new String[] { "asset", "namespace" }),
				"The collection type. Can be collection 'asset' or asset 'namespace'. If not specified, it will try collection asset first, if collection asset does not exist, try asset namespace.",
				0));
		_defn.add(path);
		_defn.add(new Interface.Element("contains", StringType.DEFAULT,
				"A string that the actor of the ACL must contain (case insensitive) to be shown. Default is all actors.",
				0, 1));
		_defn.add(new Interface.Element("nlevels", IntegerType.DEFAULT,
				"If recurse=true (ignored if false)  only recurse this many levels, default infinity.", 0, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT,
				"Recurse down the whole namespace tree (the listing is simplified in that the specific ACL is not shown, just that it has one). Default is false (just inspect immediate children namespaces).",
				0, 1));
		_defn.add(new Interface.Element("details", BooleanType.DEFAULT,
				"Include full/detailed ACLs. Defaults to false.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "list any collection/namespace ACLs on children of the given collection/namespace. Can recurse down the tree. The returned collection/namespace count is the number of collections/namespaces found with an ACL (any).";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String collectionPath = args.stringValue("path");
		String collectionType = args.value("path/@type");
		boolean isCollectionAsset = false;
		if (collectionType != null) {
			if (collectionType.equals("asset")) {
				if (!AssetUtils.collectionAssetExists(collectionPath, executor())) {
					throw new IllegalArgumentException("Cannot find collection asset: '" + collectionPath + "'.");
				}
				isCollectionAsset = true;
			} else {
				if (!AssetNamespaceUtils.assetNamespaceExists(executor(), collectionPath)) {
					throw new IllegalArgumentException("Cannot find asset namespace: '" + collectionPath + "'.");
				}
				isCollectionAsset = false;
			}
		} else {
			if (AssetUtils.collectionAssetExists(collectionPath, executor())) {
				isCollectionAsset = true;
			} else if (AssetNamespaceUtils.assetNamespaceExists(executor(), collectionPath)) {
				isCollectionAsset = false;
			} else {
				throw new IllegalArgumentException(
						"Cannot find collection asset or asset namespace: '" + collectionPath + "'");
			}
		}
		String contains = args.value("contains");
		int nLevels = args.intValue("nlevels", -1);
		Boolean recurse = args.booleanValue("recurse", false);
		Boolean details = args.booleanValue("details", false);

		AtomicLong collectionCount = new AtomicLong(0);

		int parentDepth = AssetPathUtils.getDepth(collectionPath);
		list(executor(), collectionPath, isCollectionAsset, recurse, contains, details, parentDepth, nLevels,
				collectionCount, w);
		w.add("count", collectionCount.get());
	}

	private void list(ServiceExecutor executor, String collectionPath, boolean isCollectionAsset, boolean recurse,
			String contains, boolean showTree, int parentDepth, int nLevels, AtomicLong collectionCount, XmlWriter w)
			throws Throwable {
		int childDepth = AssetPathUtils.getDepth(collectionPath) + 1;
		if (nLevels > 0 && childDepth - parentDepth > nLevels) {
			return;
		}

		XmlDocMaker dm = new XmlDocMaker("args");
		if (isCollectionAsset) {
			dm.add("id", "path=" + collectionPath);
			dm.add("include", "collections");
		} else {
			dm.add("namespace", collectionPath);
			dm.add("include", "namespaces");
		}
		dm.add("size", "infinity");

		PluginTask.checkIfThreadTaskAborted();
		Collection<String> childrenNames = executor.execute("asset.collection.list", dm.root())
				.values("collection/collection/@name");
		if (childrenNames == null || childrenNames.isEmpty()) {
			return;
		}
		List<String> childrenPaths = new ArrayList<>();
		childrenNames.forEach(name -> childrenPaths.add(collectionPath + "/" + name));

		for (String childPath : childrenPaths) {

			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element ce;
			if (isCollectionAsset) {
				ce = AssetUtils.getAssetByPath(childPath, executor);
			} else {
				ce = AssetNamespaceUtils.describeAssetNamespace(executor, childPath);
			}
			List<XmlDoc.Element> acls = isCollectionAsset ? ce.elements("collection/member-acl") : ce.elements("acl");
			if (acls != null && !acls.isEmpty()) {
				w.push("collection", new String[] { "type", isCollectionAsset ? "asset" : "namespace", "depth",
						"" + childDepth, "path", childPath });
				String inheritACLs = ce.stringValue("inherit-acls", "only-if-no-acls");
				w.add("inherit-acls", inheritACLs);
				collectionCount.getAndIncrement();
				for (XmlDoc.Element acl : acls) {
					PluginTask.checkIfThreadTaskAborted();
					String actor = acl.value("actor");
					String actorType = acl.value("actor/@type");
					String aclType = acl.value("@type");
					if (contains == null || actor.toUpperCase().contains(contains.toUpperCase())) {
						if (showTree) {
							w.add(acl);
						} else {
							w.add("acl", new String[] { "type", aclType, "actor", actor, "actor-type", actorType, });
						}
					}
				}
				w.pop();
			}
			// Descend into child collection
			if (recurse) {
				list(executor, childPath, isCollectionAsset, recurse, contains, showTree, parentDepth, nLevels,
						collectionCount, w);
			}
		}
	}

}
