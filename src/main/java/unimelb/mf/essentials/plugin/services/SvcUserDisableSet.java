package unimelb.mf.essentials.plugin.services;

import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;

public class SvcUserDisableSet extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.disable.set";

	private Interface _defn;

	public SvcUserDisableSet() {
		_defn = new Interface();
		Interface.Element authority  = new Interface.Element("authority", StringType.DEFAULT,
				"The identity of the authority/repository where the user identity originates. If unspecified, then refers to a user in this repository.",
				0, 1);
		authority.add(new Interface.Attribute("protocol", StringType.DEFAULT,
				"The protocol of the identity authority. If unspecified, defaults to federated user within the same type of repository.",
				0));
		_defn.add(authority);
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain the user belongs to", 1,
				1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name.", 1, 1));
		_defn.add(new Interface.Element("date",  DateType.DEFAULT, "The date to disable the account on (will replace any existing)", 0, 1));
		_defn.add(new Interface.Element("remove",  BooleanType.DEFAULT, "Remove the disabling meta-data from the user account (defaults to false). Setting this takes precedence over setting the date.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Sets meta-data specifying the date when the account should be disabled.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		XmlDoc.Element authority = args.element("authority");
		String domain = args.value("domain");
		String user = args.value("user");
		Date date = args.dateValue("date");
		Boolean remove = args.booleanValue("remove", false);
		if (remove && date!=null) {
			throw new Exception ("You must specify either a date or to remove the date");
		}
		if (!remove && date==null) {
			throw new Exception ("You must specify either a date or to remove the date");
		}

		// CHeck is not LDAP
		XmlDocMaker dm = new XmlDocMaker("args");
		if (authority!=null) {
			dm.add(authority);
		}
		dm.add("domain", domain);
		XmlDoc.Element r = executor().execute("authentication.domain.describe", dm.root());
		String protocol = r.value("domain/@protocol");
		if (protocol!=null) {
			w.add("protocol", protocol);
			if (protocol.equals("ldap")) {
				throw new Exception ("You cannot disable LDAP account users.");
			}
		}

		// Proceed
		setDisable (executor(), authority, domain, user, date, remove, w);
	}

	
	public static void setDisable (ServiceExecutor executor, 
			XmlDoc.Element authority, String domain, String user,
			Date date, Boolean remove, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (authority!=null) {
			dm.add(authority);
		}
		dm.add("domain", domain);
		dm.add("user", user);	
		if (remove) {
			dm.push("meta", new String[] {"action", "remove"});
			dm.push("unimelb-admin:user-account");
			dm.push("disable");
			dm.pop();
			dm.pop();
			w.add("user", new String[] {"disable-date", "removed"}, domain+":"+user);
		} else {
			dm.push("meta", new String[] {"action", "replace"});
			dm.push("unimelb-admin:user-account");
			String d = DateUtil.formatDate(date, "dd-MMM-yyyy");
			dm.add("disable", d);
			dm.pop();
			w.add("user", new String[] {"disable-date-set", d}, domain+":"+user);
		}
		dm.pop();

		// Do it
		executor.execute("user.set", dm.root());

	}
	@Override
	public String name() {
		return SERVICE_NAME;
	}

}