package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcUserEmailExport extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.email.export";

	private static class UserRecord implements Comparable<UserRecord> {
		public final String domain;
		public final String username;
		public final String name;
		public final String email;
		public final boolean enabled;

		UserRecord(XmlDoc.Element ue) throws Throwable {
			this.domain = ue.value("@domain");
			this.username = ue.value("@user");
			this.name = ue.value("name");
			String email = ue.value("e-mail");
			this.email = email == null ? null : email.toLowerCase();
			this.enabled = ue.booleanValue("@enabled", true);
		}

		public String actorName() {
			return String.format("%s:%s", this.domain, this.username);
		}

		public boolean equals(Object o) {
			if (o != null && (o instanceof UserRecord)) {
				UserRecord u = (UserRecord) o;
				return this.domain.equals(u.domain) && this.username.equals(u.username);
			}
			return false;
		}

		public int hashCode() {
			return actorName().hashCode();
		}

		public String toString() {
			return String.format("%s,%s,%s,%s,%s,", this.domain, this.username, this.name == null ? "" : this.name,
					Boolean.toString(this.enabled), this.email == null ? "" : this.email);
		}

		@Override
		public int compareTo(UserRecord u) {
			return actorName().compareTo(u.actorName());
		}
	}

	private Interface _defn;

	public SvcUserEmailExport() {
		_defn = new Interface();
		_defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
				"Include disabled domains and users. Defaults to false", 0, 1));
		_defn.add(new Interface.Element("exclude-domain", StringType.DEFAULT, "Exclude this domain.", 0,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("email", StringType.DEFAULT,
				"E-Mail the CSV report to this destination (default none).", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("role", StringType.DEFAULT,
				"The user must hold this role (which must be of type 'role') to be included. If you provide more than one, the user must hold at least one role in the list to be included.",
				0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("distinct", BooleanType.DEFAULT,
				"Only list distinct email addresses (default true).  If set to false, it includes all user accounts with the same email address.",
				0, 1));
		_defn.add(new Interface.Element("list", BooleanType.DEFAULT,
				"List emails in service result. Defaults to false.", 0, 1));

		_defn.add(new Interface.Element("format", new EnumType(new String[] { "csv", "txt" }),
				"File format. Defaults to CSV.", 0, 1));

		Interface.Element id = new Interface.Element("id", AssetType.DEFAULT, "The asset to store the result file.", 0,
				1);
		id.add(new Interface.Attribute("create", BooleanType.DEFAULT,
				"create asset if not exists. Only works if the asset path is specified. Defaults to false.", 0));
		id.add(new Interface.Attribute("retain", IntegerType.POSITIVE,
				"Number of recent versions to retain when pruning the asset. Defaults to 0, which does not prune.", 0));
		_defn.add(id);
	}

	public String name() {
		return SERVICE_NAME;
	}

	public String description() {
		return "Find unique list of email addresses from all user accounts (except given domain exclusions) and optionally export to a CSV or TXT file.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {
		return true;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		boolean includeDisabled = args.booleanValue("include-disabled", false);
		Collection<String> excludeDomains = args.values("exclude-domain");
		String email = args.value("email");
		Collection<String> roles = args.values("role");
		boolean distinct = args.booleanValue("distinct", true);
		boolean list = args.booleanValue("list", false);

		String format = args.stringValue("format", "csv");
		String mimeType = "csv".equalsIgnoreCase(format) ? "text/csv" : "text/plain";

		String dstAssetId = args.value("id");
		boolean dstAssetCreate = dstAssetId == null || !dstAssetId.startsWith("path=") ? false
				: args.booleanValue("id/@create", false);
		int dstAssetRetain = dstAssetId == null ? 0 : args.intValue("id/@retain", 0);

		Set<UserRecord> results = new TreeSet<UserRecord>();
		addUsers(executor(), results, excludeDomains, includeDisabled, roles);

		if (distinct) {
			Set<UserRecord> distinctResults = new TreeSet<UserRecord>();
			Set<String> emails = new HashSet<String>();
			for (UserRecord u : results) {
				if (!emails.contains(u.email)) {
					emails.add(u.email);
					distinctResults.add(u);
				}
			}
			results = distinctResults;
		}

		if (results.isEmpty()) {
			w.add("total", 0);
			return;
		}

		if (list) {
			for (UserRecord u : results) {
				w.add("email", new String[] { "domain", u.domain, "user", u.username, "name", u.name, "enable",
						u.enabled ? null : Boolean.toString(u.enabled) }, u.email);
			}
		}
		w.add("total", results.size());

		File tf = PluginTask.createTemporaryFile("." + format);
		try {
			saveResultsToFile(results, tf, format);

			if (dstAssetId != null) {
				dstAssetId = saveResultsToAsset(executor(), tf, dstAssetId, dstAssetCreate, dstAssetRetain, format,
						mimeType);
				w.add("updated", new String[] { "asset", dstAssetId }, true);
			}

			if (email != null) {
				sendEmail(executor(), email, tf, format, mimeType);
				w.add("sent", new String[] { "to", email }, true);
			}

			if (outputs != null && outputs.output(0) != null) {
				outputs.output(0).setData(PluginTask.deleteOnCloseInputStream(tf), tf.length(), mimeType);
			} else {
				PluginTask.deleteTemporaryFile(tf);
			}
		} catch (Throwable e) {
			PluginTask.deleteTemporaryFile(tf);
			throw e;
		}

	}

	private void saveResultsToFile(Set<UserRecord> results, File tf, String format) throws Throwable {
		try (PrintWriter w = new PrintWriter(new BufferedWriter(new FileWriter(tf)))) {
			if ("csv".equalsIgnoreCase(format)) {
				w.println("DOMAIN, USER, NAME, ENABLED, EMAIL,");
			}
			for (UserRecord u : results) {
				if ("csv".equalsIgnoreCase(format)) {
					w.println(u);
				} else {
					w.println(String.format("%s %s", u.email, u.name));
				}
			}
		}
	}

	private String saveResultsToAsset(ServiceExecutor executor, File file, String assetId, boolean createIfNotExist,
			int retain, String format, String mimeType) throws Throwable {

		Date now = new Date();
		String exportTime = new SimpleDateFormat("yyyyMMddHHmmss").format(now);

		String serverUUID = executor.execute("server.uuid").value("uuid");

		String fileName = String.format("%s Mediaflux User Email List - %s.%s", serverUUID, exportTime, format);

		createIfNotExist = !assetId.startsWith("path=") ? false : createIfNotExist;

		boolean assetExists = AssetUtils.exists(executor, null, assetId, false);
		if (!assetExists && !createIfNotExist) {
			throw new Exception("Asset " + assetId + " does not exist.");
		}

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", assetId);
		dm.add("create", createIfNotExist);
		dm.add("name", fileName);

		try (FileInputStream fis = new FileInputStream(file); BufferedInputStream bis = new BufferedInputStream(fis)) {
			PluginService.Input input = new PluginService.Input(bis, file.length(), mimeType, null);
			try {
				assetId = executor.execute("asset.set", dm.root(), new PluginService.Inputs(input), null)
						.value("version/@id");
			} finally {
				input.close();
			}
		}

		if (retain > 0) {
			dm = new XmlDocMaker("args");
			dm.add("id", assetId);
			dm.add("retain", retain);
			executor.execute("asset.prune", dm.root());
		}

		return assetId;
	}

	private void sendEmail(ServiceExecutor executor, String to, File file, String format, String mimeType)
			throws Throwable {

		Date now = new Date();
		String exportDate = new SimpleDateFormat("yyyy-MM-dd").format(now);
		String exportTime = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(now);

		String serverUUID = executor.execute("server.uuid").value("uuid");

		String fileName = String.format("%s Mediaflux User Email List - %s.%s", serverUUID, exportDate, format);

		String subject = String.format("[%s] %s [%s]", serverUUID, SERVICE_NAME, exportTime);

		try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
			PluginService.Input input = new PluginService.Input(in, file.length(), mimeType, null);
			try {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.push("attachment");
				dm.add("name", fileName);
				dm.add("type", mimeType);
				dm.pop();
				dm.add("to", to);
				dm.add("subject", subject);
				executor.execute("mail.send", dm.root(), new PluginService.Inputs(input), null);
			} finally {
				input.close();
			}
		}
	}

	private void addUsers(ServiceExecutor executor, Set<UserRecord> results, Collection<String> excludeDomains,
			boolean includeDisabled, Collection<String> roles) throws Throwable {
		XmlDoc.Element r = executor.execute("authentication.domain.list");
		List<XmlDoc.Element> des = r.elements("domain");
		if (des != null) {

			// Iterate over domains
			for (XmlDoc.Element de : des) {
				String domain = de.value();

				// Filter domains out
				if ((includeDisabled || de.booleanValue("@enabled", true))
						&& (excludeDomains == null || !excludeDomains.contains(domain))) {
					String authority = de.value("@authority");
					String protocol = de.value("@protocol");
					if (protocol != null && protocol.equalsIgnoreCase("ldap") && roles == null) {
						// Do not allow pulling all users from ldap domain if no
						// role filter is specified.
						throw new Exception("Retrieving all users from ldap domain: " + domain
								+ " without any role filter set is NOT allowed.");
					}

					// Add all users for this domain filtered by roles
					addUsers(executor, includeDisabled, results, authority, protocol, domain, roles);
				}
			}
		}
	}

	private void addUsers(ServiceExecutor executor, Boolean includeDisabled, Set<UserRecord> results, String authority,
			String protocol, String domain, Collection<String> roles) throws Throwable {

		if (roles != null) {
			// Iterate over roles because user.describe ANDs the roles and we want to OR
			// them
			for (String role : roles) {
				addUsers(executor, includeDisabled, results, authority, protocol, domain, role);
			}
		} else {
			String role = null;
			addUsers(executor, includeDisabled, results, authority, protocol, domain, role);
		}
	}

	private void addUsers(ServiceExecutor executor, Boolean includeDisabled, Set<UserRecord> results, String authority,
			String protocol, String domain, String role) throws Throwable {

		PluginTask.checkIfThreadTaskAborted();
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", "infinity");
		dm.add("domain", domain);
		if (role != null) {
			dm.add("role", new String[] { "type", "role" }, role);
		}
		if (authority != null) {
			if (authority != null) {
				dm.add("authority", new String[] { "protocol", protocol }, authority);
			}
		}
		XmlDoc.Element r = executor.execute("user.describe", dm.root());
		List<XmlDoc.Element> us = r.elements("user");
		if (us != null) {
			for (XmlDoc.Element u : us) {
				PluginTask.checkIfThreadTaskAborted();
				Boolean enabled = u.booleanValue("@enabled", true);
				if (includeDisabled || enabled) {
					UserRecord ur = new UserRecord(u);
					if (ur.email != null) {
						results.add(ur);
					}
				}
			}
		}
	}

	// TBD: when we upgrade to 4.9.* we can put the cursor into the user.describe
	// as shown below for authenitcation.user.describe instead of setting size to
	// infinity.
	//
	// Wilson: However, even on 4.9+ user.describe does not respect :idx and :size
	// arguments if :role is specified.
	//
	// @formatter:off
	/*
	private static void addUsersOld(ServiceExecutor executor, Set<UserRecord> results, String authority,
			String protocol, String domain, boolean includeDisabled) throws Throwable {
		boolean complete = false;
		int size = 100;
		int idx = 1;
		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			if (authority != null) {
				dm.add("authority", new String[] { "protocol", protocol }, authority);
			}
			dm.add("idx", idx);
			dm.add("size", size);
			XmlDoc.Element r = executor.execute("authentication.user.describe", dm.root());
			complete = r.booleanValue("complete");
			idx += size;
			List<XmlDoc.Element> ues = r.elements("user");
			if (ues != null) {
				for (XmlDoc.Element ue : ues) {
					if (includeDisabled || ue.booleanValue("@enabled", true)) {
						UserRecord ur = new UserRecord(ue);
						if (ur.email != null) {
							results.add(ur);
						}
					}
				}
			}
		}

	}
	*/
	// @formatter:on
}
