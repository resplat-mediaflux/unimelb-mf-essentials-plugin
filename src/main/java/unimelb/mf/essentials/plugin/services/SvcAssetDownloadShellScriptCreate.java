package unimelb.mf.essentials.plugin.services;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.EnumSet;

import arc.archive.ArchiveOutput;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.xml.XmlDoc.Element;
import unimelb.mf.essentials.plugin.script.TargetOS;
import unimelb.mf.essentials.plugin.script.download.AssetDownloadScriptSettings;
import unimelb.mf.essentials.plugin.script.download.AssetDownloadShellScriptWriter;

public class SvcAssetDownloadShellScriptCreate extends SvcAssetDownloadScriptCreate {

    public static final String SERVICE_NAME = "unimelb.asset.download.shell.script.create";

    public static final String DEFAULT_SCRIPT_FILE_NAME = "unimelb-mf-shell-download";

    public SvcAssetDownloadShellScriptCreate() {
        super();
        addToDefn(this.defn);
    }

    private static void addToDefn(Interface defn) {
        defn.add(new Interface.Element("page-size", IntegerType.POSITIVE_ONE,
                "Query page size. Defaults to " + AssetDownloadShellScriptWriter.DEFAULT_PAGE_SIZE, 0, 1));
        defn.add(new Interface.Element("overwrite", BooleanType.DEFAULT,
                "Whether or not overwrite existing files. Defaults to false", 0, 1));
        defn.add(new Interface.Element("verbose", BooleanType.DEFAULT,
                "Whether or not display the files being downloaded. Defaults to false", 0, 1));
    }

    @Override
    public String description() {
        return "Generates a Unix/Windows shell script to download the selected assets via /mflux/content.mfjp servlet. To execute the script on Unix platform, curl or wget is required.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    protected void addScriptFiles(EnumSet<TargetOS> targets, ArchiveOutput ao, AssetDownloadScriptSettings settings,
            Element args) throws Throwable {

        int pageSize = args.intValue("page-size", AssetDownloadShellScriptWriter.DEFAULT_PAGE_SIZE);
        settings.addArg(AssetDownloadShellScriptWriter.ARG_PAGE_SIZE, pageSize);

        boolean overwrite = args.booleanValue("overwrite", false);
        settings.addArg(AssetDownloadShellScriptWriter.ARG_OVERWRITE, overwrite);

        boolean verbose = args.booleanValue("verbose", false);
        settings.addArg(AssetDownloadShellScriptWriter.ARG_VERBOSE, verbose);

        Date tokenExpiry = args.dateValue("token/to", null);
        settings.setTokenExpiry(tokenExpiry);

        for (TargetOS targetOS : targets) {
            String fileName = settings.fileName();
            String fileExt = targetOS == TargetOS.UNIX ? ".sh" : ".ps1";
            if (!fileName.endsWith(fileExt)) {
                fileName = fileName + fileExt;
            }
            ao.begin("text/plain", fileName, -1);
            try {
                AssetDownloadShellScriptWriter w = AssetDownloadShellScriptWriter.create(ao.stream(), false, targetOS,
                        settings);
                try {
                    w.process(executor());
                } finally {
                    w.close();
                }
            } finally {
                ao.end();
            }

            if (targetOS == TargetOS.WINDOWS) {
                addWindowsBatchWrapper(ao, fileName);
            }
        }

    }

    private void addWindowsBatchWrapper(ArchiveOutput ao, String ps1FileName) throws Throwable {

        assert ps1FileName.endsWith(".ps1");

        String cmdFileName = ps1FileName.substring(0, ps1FileName.length() - 4) + ".cmd";
        ao.begin("text/plain", cmdFileName, -1);
        try {
            PrintWriter w = new PrintWriter(new BufferedWriter(new OutputStreamWriter(ao.stream())));
            w.println("@ECHO OFF\r\n");
            // %% to escape % in String.format
            w.print(String.format("powershell -ExecutionPolicy bypass -File %%~dp0%s %%*\r\n", ps1FileName));
            w.flush();
        } finally {
            ao.end();
        }
    }

    @Override
    protected final String defaultScriptFileName() {
        return DEFAULT_SCRIPT_FILE_NAME;
    }

    @Override
    protected final String tokenApp() {
        // TODO
        // return AssetDownloadShellScriptWriter.TOKEN_APP;
        return null;
    }

    @Override
    protected final String tokenTag() {
        return AssetDownloadShellScriptWriter.TOKEN_TAG;
    }

}
