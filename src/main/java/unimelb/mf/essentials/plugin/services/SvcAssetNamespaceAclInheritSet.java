package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;

public class SvcAssetNamespaceAclInheritSet extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.namespace.acl.inherit.set";

    private final Interface _defn;

    public SvcAssetNamespaceAclInheritSet() {
        _defn = new Interface();
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT,
                "The asset namespace path.",
                1, 1));
        _defn.add(new Interface.Element("inherit-acls",
                new EnumType(new String[]{"only-if-no-acls", "true", "false"}),
                "whether the specified namespace should inherit ACLs from it's parent.",
                1, 1));
        _defn.add(new Interface.Element("recursive", BooleanType.DEFAULT,
                "Recursively set inherit-acls attributes for children namespaces. Defaults to false",
                0, 1));
        _defn.add(new Interface.Element("assets", BooleanType.DEFAULT,
                "Recursively set inherit-acls attributes for assets. Defaults to false",
                0, 1));
    }

    private static void setAssetNamespaceAclInherit(ServiceExecutor executor, String namespacePath,
                                                    String inheritAcls, boolean recursive, XmlWriter w) throws Throwable {
        String currentValue = getAssetNamespaceAclInherit(executor, namespacePath);
        if (!inheritAcls.equals(currentValue)) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("namespace", namespacePath);
            dm.add("inherit", inheritAcls);
            PluginTask.checkIfThreadTaskAborted();
            executor.execute("asset.namespace.acl.inherit.set", dm.root());
            w.push("updated", new String[]{"namespace", namespacePath});
            w.add("inherit-acls", new String[]{"old-value", currentValue}, inheritAcls);
            w.pop();
        }
        if (recursive) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("namespace", namespacePath);
            PluginTask.checkIfThreadTaskAborted();
            Collection<String> subNamespaces = executor.execute("asset.namespace.list", dm.root())
                    .values("namespace/namespace");
            if (subNamespaces != null) {
                for (String subNamespace : subNamespaces) {
                    setAssetNamespaceAclInherit(executor, namespacePath + "/" + subNamespace, inheritAcls,
                            true, w);
                }
            }
        }
    }

    private static String getAssetNamespaceAclInherit(ServiceExecutor executor, String namespacePath) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespacePath);
        PluginTask.checkIfThreadTaskAborted();
        return executor.execute("asset.namespace.describe", dm.root()).value("namespace/inherit-acls");
    }

    private static void setAssetsAclInherit(ServiceExecutor executor, String namespacePath, String inheritAcls) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + namespacePath + "'");
        dm.add("action", "pipe");
        dm.push("service", new String[]{"name", "asset.acl.inherit.set"});
        dm.add("inherit-acls", inheritAcls);
        dm.pop();
        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.query", dm.root());
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Sets inherit-acls attribute of asset namespace.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String namespacePath = args.value("namespace");
        String inheritAcls = args.value("inherit-acls");
        boolean recursive = args.booleanValue("recursive");
        boolean assets = args.booleanValue("assets");
        setAssetNamespaceAclInherit(executor(), namespacePath, inheritAcls, recursive, w);
        if (assets) {
            setAssetsAclInherit(executor(), namespacePath, inheritAcls);
        }
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }
}
