/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetNameSpaceActorExclude extends PluginService {


	private Interface _defn;

	public SvcAssetNameSpaceActorExclude() {
		_defn = new Interface();
		Interface.Element ie = new Interface.Element("actor", StringType.DEFAULT,"The actor of interest", 1, Integer.MAX_VALUE);
		ie.add(new Interface.Attribute("type", StringType.DEFAULT,
				"Actor type.",1));
		_defn.add(ie);
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("access", BooleanType.DEFAULT, 
				"Allow the user to have ACCESS rights (only) to the namespace. By default they have no access at all", 0, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.acl.actor.exclude";
	}

	public String description() {
		return "Grant an exclusion ACL to the namespace to exclude the given actor.  The inheritance on the namespace will be set to true (access determined by parent and this namespace ACLs).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return false;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		Boolean access = args.booleanValue("access", false);
		Collection<XmlDoc.Element> actors = args.elements("actor");
		for (XmlDoc.Element actor : actors) {
			grant (executor(), namespace, actor, access, "true", w);
		}
	}


	private void grant (ServiceExecutor executor, String nameSpace, 
			XmlDoc.Element actor, Boolean access, String inherit, XmlWriter w) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",  nameSpace);
		dm.push("acl", new String[] {"type", "exclusive"});
		dm.add(actor);
		dm.push("access");
		//
		dm.add("asset", "create");
		if (!access) {
			dm.add("asset", "access");
		}
		dm.add("asset", "modify");
		dm.add("asset", "destroy");
		dm.add("asset", "licence");
		dm.add("asset", "publish");
		//
		if (!access) {
			dm.add("asset-content", "access");
		}
		dm.add("asset-content", "modify");
		//
		if (!access) {
			dm.add("namespace", "access");
		}
		dm.add("namespace", "administer");
		dm.add("namespace", "create");
		dm.add("namespace", "modify");
		dm.add("namespace", "destroy");
		dm.add("namespace", "execute");	
		dm.pop();
		dm.pop();
		executor.execute("asset.namespace.acl.grant", dm.root());
		//
		if (inherit!=null) {
			dm = new XmlDocMaker("args");
			dm.add("namespace",  nameSpace);
			dm.add("inherit", inherit);
			executor.execute("asset.namespace.acl.inherit.set", dm.root());
		}
	}
}
