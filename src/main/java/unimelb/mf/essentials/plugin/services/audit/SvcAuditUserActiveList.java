package unimelb.mf.essentials.plugin.services.audit;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.StringType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SvcAuditUserActiveList extends PluginService {

    public static final String SERVICE_NAME = "unimelb.audit.user.active.list";

    public static final String DEFAULT_ACTIVE_FROM = "today-90day";
    private final Interface _defn;

    public SvcAuditUserActiveList() {
        _defn = new Interface();
        addArgs(_defn);

        _defn.add(new Interface.Element("last-logon", BooleanType.DEFAULT,
                "Show the last user.logon time. Defaults to false.", 0, 1));
    }

    static void addArgs(Interface defn) {
        Interface.Element role = new Interface.Element("role", StringType.DEFAULT,
                "Role to select the users.", 1, 1);
        role.add(new Interface.Attribute("type", StringType.DEFAULT, "Role type. Defaults to 'role'", 0));
        defn.add(role);

        defn.add(new Interface.Element("from", DateType.DEFAULT,
                "The date from which the users had user.logon activities. "
                        + "Defaults to '" + DEFAULT_ACTIVE_FROM + "'", 0, 1));

        defn.add(new Interface.Element("to", DateType.DEFAULT,
                "The date to which the users had user.logon activities. "
                        + "Defaults to 'now'", 0, 1));

        defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
                "Include disabled users. Defaults to false.", 0, 1));

        defn.add(new Interface.Element("domain", StringType.DEFAULT,
                "The user's authentication domain. If specified, only the users in the domain will be assessed.",
                0, 255));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "List the user are active (had logon activity) since the specified date.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String activeFrom = args.stringValue("from", DEFAULT_ACTIVE_FROM);
        Date activeFromDate = DateTime.parse(activeFrom);

        String activeTo = args.stringValue("to", "now");
        Date activeToDate = DateTime.parse(activeTo);

        if (activeToDate.getTime() < activeFromDate.getTime()) {
            throw new IllegalArgumentException("Invalid from: " + activeFrom + ". Must be earlier then to: " + DateTime.string(activeToDate));
        }

        long activeDays = TimeUnit.DAYS.convert(Math.abs(activeToDate.getTime() - activeFromDate.getTime()),
                TimeUnit.MILLISECONDS);
        String role = args.value("role");
        String roleType = args.stringValue("role/@type", "role");
        Collection<String> domains = args.values("domain");
        boolean lastLogon = args.booleanValue("last-logon", false);
        boolean includeDisabled = args.booleanValue("include-disabled", false);

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("role", new String[]{"type", roleType}, role);
        dm.add("relationship", "either");

        PluginTask.checkIfThreadTaskAborted();
        Collection<String> actorNames = executor().execute("actors.granted", dm.root()).values("actor/@name");

        long count = 0;
        if (actorNames != null) {
            for (String actorName : actorNames) {
                String[] parts = actorName.split(":");
                String authority, domain, username;
                if (parts.length == 3) {
                    authority = parts[0];
                    domain = parts[1];
                    username = parts[2];
                } else if (parts.length == 2) {
                    authority = null;
                    domain = parts[0];
                    username = parts[1];
                } else {
                    throw new Exception("Failed to parse user name: " + actorName);
                }
                if (authority != null) {
                    // Currently, audit log does not support users from external authorities. Need to revisit.
                    // TODO revisit
                    continue;
                }
                if (domains != null && !domains.contains(domain)) {
                    continue;
                }
                if (!SvcAuditUserInactiveList.userExists(executor(), authority, domain, username)) {
                    continue;
                }
                XmlDoc.Element ue = SvcAuditUserInactiveList.describeUser(executor(), authority, domain, username);
                boolean enabled = ue.booleanValue("@enabled", true);
                if (!includeDisabled && !enabled) {
                    continue;
                }
                if (SvcAuditUserInactiveList.hasLogon(executor(), actorName, activeFromDate, activeToDate)) {
                    w.push("user", new String[]{"authority", authority, "domain", domain, "user", username});
                    String name = ue.value("name");
                    if (name != null) {
                        w.add("name", name);
                    }
                    String email = ue.value("e-mail");
                    if (email != null) {
                        w.add("e-mail", email);
                    }
                    if (lastLogon) {
                        Long lastLogonMillis = SvcAuditUserLogonDescribe.getLastLogonTime(executor(), actorName, activeFromDate, activeToDate);
                        if (lastLogonMillis != null) {
                            w.add("last-logon", DateTime.string(new Date(lastLogonMillis)));
                        }
                    }
                    w.pop();
                    count++;
                }
            }
        }
        w.add("count", count);

        w.push("active");
        w.add("from", activeFromDate);
        w.add("days", activeDays);
        w.pop();

        w.add("role", new String[]{"type", roleType}, role);
    }
}
