package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcUserEmailList extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.email.list";

	private Interface _defn;

	public SvcUserEmailList() {

		_defn = new Interface();

		addToDefn(_defn);

	}

	static void addToDefn(Interface defn) {
		Interface.Element role = new Interface.Element("role", StringType.DEFAULT,
				"If specified, only users holding this role will be described.  Multiple roles are ANDed together.", 1, Integer.MAX_VALUE);
		role.add(new Interface.Attribute("type", StringType.DEFAULT, "Role type.", 1));
		defn.add(role);

		defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
				"Includes disabled user accounts. Defaults to false.", 0, 1));
		defn.add(new Interface.Element("exclude-domain", StringType.DEFAULT, "Exclude this domain.", 0,
				Integer.MAX_VALUE));

		defn.add(new Interface.Element("distinct", BooleanType.DEFAULT,
				"Get distinct email addresses. Defaults to false.", 0, 1));

		defn.add(new Interface.Element("suffix", StringType.DEFAULT, "Suffix filter of the email address.", 0, 1));
		defn.add(new Interface.Element("prefix", StringType.DEFAULT, "Prefix filter of the email address.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "List user emails.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		boolean includeDisabled = args.booleanValue("include-disabled", false);
		boolean distinct = args.booleanValue("distinct", false);
		String prefix = args.value("prefix");
		if (prefix != null) {
			prefix = prefix.toLowerCase();
		}
		String suffix = args.value("suffix");
		if (suffix != null) {
			suffix = suffix.toLowerCase();
		}
		Collection<String> excludeDomains = args.values("exclude-domain");

		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.addAll(args.elements("role"));
		List<XmlDoc.Element> ues = executor().execute("user.describe", dm.root()).elements("user");
		Set<String> emails = distinct ? new TreeSet<String>() : null;
		if (ues != null) {
			for (XmlDoc.Element ue : ues) {
				PluginTask.checkIfThreadTaskAborted();
				String domain = ue.value("@domain");
				if (excludeDomains == null || !excludeDomains.contains(domain))  {
					String authority = ue.value("@authority");
					String protocol = ue.value("@protocol");
					String user = ue.value("@user");
					//
					boolean enabled = isUserEnabled(executor(), authority, protocol, domain, user);
					if (enabled || includeDisabled) {
						String name = ue.value("name");
						if (name == null && ue.elementExists("asset/meta/mf-user/name")) {
							StringBuilder sb = new StringBuilder();
							String fn = ue.value("asset/meta/mf-user/name[@type='first']");
							if (fn != null) {
								sb.append(fn).append(" ");
							}
							String mn = ue.value("asset/meta/mf-user/name[@type='middle']");
							if (mn != null) {
								sb.append(mn).append(" ");
							}
							String ln = ue.value("asset/meta/mf-user/name[@type='last']");
							if (ln != null) {
								sb.append(ln);
							}
							name = sb.toString().trim();
						}
						String email = ue.value("e-mail");
						if (email == null) {
							email = ue.value("asset/meta/mf-user/email");
						}
						if (email != null) {
							email = email.toLowerCase();
							if (prefix != null && !email.startsWith(prefix)) {
								continue;
							}
							if (suffix != null && !email.endsWith(suffix)) {
								continue;
							}
							if (distinct) {
								emails.add(email.toLowerCase());
							} else {
								w.add("email", new String[] { "authority", authority, "protocol", protocol, "domain",
										domain, "user", user, "name", name }, email.toLowerCase());
							}
						}
					}
				}
			}
		}
		if (distinct) {
			for (String email : emails) {
				w.add("email", email);
			}
		}
	}

	private static boolean isUserEnabled(ServiceExecutor executor, String authority, String protocol, String domain,
			String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (authority != null) {
			dm.add("authority", new String[] { "protocol", protocol }, authority);
		}
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("authentication.user.enabled", dm.root()).booleanValue("enabled");
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

}
