package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.AssetType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetVersionsStimeSameCheck extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.versions.stime.same.check";

	private Interface _defn;

	public SvcAssetVersionsStimeSameCheck() {
		_defn = new Interface();

		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The path to the asset.", 1, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Checks asset to see if all versions have the same meta -stime specification (asset merging may do this). Only returns a result if true.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {


		String id = args.value("id");
		Collection<String> versions = executor()
				.execute("asset.versions", "<args><id>" + id + "</id></args>", null, null)
				.values("asset/version/@n");
		String last = null;
		Boolean first = true;
		Boolean same = true;
		for (String version : versions) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", new String[] { "version", version }, id);
			XmlDoc.Element re = executor().execute("asset.get", dm.root());
			String stime = re.value("asset/meta/@stime");
			if (stime!=null) {
				if (first) {
					first = false;
				} else {
					if (!stime.equals(last)) {
						same = false;
						break;
					}
				}
				last = stime;
			}
		}
		if (last==null) {
			w.add("id", new String[] {"same", "failed-to-determine"}, id);
		} else {
			if (same) w.add("id",  new String[] {"same", "true"}, id);
		}
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
