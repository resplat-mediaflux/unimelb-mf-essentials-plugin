package unimelb.mf.essentials.plugin.services;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Collection;
import java.util.EnumSet;

import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginThread;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.script.TargetOS;
import unimelb.mf.essentials.plugin.script.download.AssetDownloadScriptSettings;
import unimelb.mf.essentials.plugin.script.download.AssetDownloadScriptWriter;
import unimelb.mf.essentials.plugin.util.ServerDetails;

public abstract class SvcAssetDownloadScriptCreate extends PluginService {

    protected Interface defn;

    protected SvcAssetDownloadScriptCreate() {
        this.defn = new Interface();
        SvcAssetDownloadScriptCreate.addToDefn(this.defn);
    }

    private static void addToDefn(Interface defn) {

        defn.add(new Interface.Element("name", StringType.DEFAULT, "The output script file name.", 0, 1));

        defn.add(new Interface.Element("where", StringType.DEFAULT, "The query to select the assets to download.", 0,
                1));

        defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The asset namespace to download.", 0,
                Integer.MAX_VALUE));
        defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID to download.", 0, Integer.MAX_VALUE));

        /*
         * target os
         */
        defn.add(new Interface.Element("target", new EnumType(TargetOS.values()),
                "The target operating system. If not specified, the scripts for both windows and unix are generated.",
                0, 1));

        /*
         * server
         */
        Interface.Element server = new Interface.Element("server", XmlDocType.DEFAULT,
                "Server details. If not specified, it will try auto-detecting from the current session.", 0, 1);

        server.add(new Interface.Element("host", StringType.DEFAULT, "server host address", 0, 1));

        server.add(new Interface.Element("port", new IntegerType(0, 65535), "server host address", 0, 1));

        server.add(new Interface.Element("transport", new EnumType(new String[] { "http", "https" }),
                "server transport: http or https?", 0, 1));
        defn.add(server);

        /*
         * token
         */
        Interface.Element token = new Interface.Element("token", XmlDocType.DEFAULT, "Token specification.", 1, 1);
        Interface.Element tokenRole = new Interface.Element("role", new StringType(128),
                "Role (name) to grant. If not specified, defaults to the calling user.", 0, Integer.MAX_VALUE);
        tokenRole.add(new Interface.Attribute("type", new StringType(64), "Role type.", 1));
        token.add(tokenRole);

        Interface.Element tokenPerm = new Interface.Element("perm", XmlDocType.DEFAULT, "Permission to grant.", 0,
                Integer.MAX_VALUE);
        tokenPerm.add(new Interface.Element("access", new StringType(64), "Access type.", 1, 1));
        Interface.Element resource = new Interface.Element("resource", new StringType(255), "Pattern for resource.", 1,
                1);
        resource.add(new Interface.Attribute("type", new StringType(32), "Resource type.", 1));
        tokenPerm.add(resource);
        token.add(tokenPerm);

        token.add(new Interface.Element("from", DateType.DEFAULT,
                "A time, before which the token is not valid. If not supplied token is valid immediately.", 0, 1));
        token.add(new Interface.Element("to", DateType.DEFAULT,
                "A time, after which the token is no longer valid. If not supplied token will not expire.", 1, 1));
        token.add(new Interface.Element("use-count", IntegerType.POSITIVE_ONE,
                "The number of times the token may be used.", 0, 1));
        defn.add(token);
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public Interface definition() {
        return this.defn;
    }

    static void validateArgs(ServiceExecutor executor, XmlDoc.Element args) throws Throwable {
        String where = args.value("where");
        if (where != null) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("where", where);
            executor.execute("asset.query", dm.root());
        }

        Collection<String> namespaces = args.values("namespace");
        if (namespaces != null && !namespaces.isEmpty()) {
            for (String namespace : namespaces) {
                boolean namespaceExists = executor.execute("asset.namespace.exists",
                        "<args><namespace>" + namespace + "</namespace></args>", null, null).booleanValue("exists");
                if (!namespaceExists) {
                    throw new IllegalArgumentException("Asset namespace: " + namespace + " does not exist");
                }
            }
        }
        Collection<String> ids = args.values("id");
        if (ids != null && !ids.isEmpty()) {
            for (String id : ids) {
                boolean assetExists = executor.execute("asset.exists", "<args><id>" + id + "</id></args>", null, null)
                        .booleanValue("exists");
                if (!assetExists) {
                    throw new IllegalArgumentException("Asset ID: " + id + " does not exist");
                }
            }
        }
        Collection<String> roles = args.values("token/role[@type='role']");
        if (roles != null && !roles.isEmpty()) {
            for (String role : roles) {
                boolean roleExists = executor
                        .execute("authorization.role.exists", "<args><role>" + role + "</role></args>", null, null)
                        .booleanValue("exists");
                if (!roleExists) {
                    throw new IllegalArgumentException("Role: " + role + " does not exist");
                }
            }
        }
    }

    @Override
    public void execute(final Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        /*
         * check if specified namespaces and roles exist
         */
        validateArgs(executor(), args);

        final String name = args.stringValue("name", defaultScriptFileName());
        final String where = args.value("where");
        final Collection<String> namespaces = args.values("namespace");
        final Collection<String> ids = args.values("id");

        if (where == null && ((namespaces == null || namespaces.isEmpty()) && (ids == null || ids.isEmpty()))) {
            throw new IllegalArgumentException("where or namespace or id argument must be specified.");
        }

        final ServerDetails serverDetails = resolveServerDetails(executor(), args.element("server"));
        final String token = createToken(executor(), args.element("token"), tokenApp(), tokenTag());
        final TargetOS targetOS = TargetOS.fromString(args.value("target"));

        final AssetDownloadScriptSettings settings = new AssetDownloadScriptSettings();

        // script file name
        settings.setFileName(name);

        // server details
        settings.setServerDetails(serverDetails);

        // token
        settings.setToken(token);
        settings.setTokenApp(tokenApp());

        // where
        settings.setWhere(where);

        // ids
        settings.setIds(ids);

        // namespaces
        settings.setNamespaces(namespaces);

        PipedInputStream pis = new PipedInputStream();
        final PipedOutputStream pos = new PipedOutputStream(pis);
        PluginThread.executeAsync(name(), new Runnable() {

            public void run() {
                try {
                    ArchiveOutput ao = ArchiveRegistry.createOutput(pos, "application/zip", 6, null);
                    try {
                        if (targetOS != null) {
                            addScriptFiles(EnumSet.of(targetOS), ao, settings, args);
                        } else {
                            addScriptFiles(EnumSet.allOf(TargetOS.class), ao, settings, args);
                        }
                    } finally {
                        ao.close();
                        pos.close();
                    }
                } catch (Throwable e) {
                    // destroy token and log
                    // TODO: grant secure.identity.token.destroy perm to the
                    // service.
                    try {
                        destroyToken(executor(), token);
                    } catch (Throwable e1) {
                        PluginLog.log().add(e1);
                    }
                    PluginLog.log().add(e);
                }
            }

        });
        outputs.output(0).setData(pis, -1, "application/zip");
    }

    protected abstract String defaultScriptFileName();

    protected abstract void addScriptFiles(EnumSet<TargetOS> targets, ArchiveOutput ao,
            AssetDownloadScriptSettings settings, XmlDoc.Element args) throws Throwable;

    protected String tokenApp() {
        return null;
    }

    protected String tokenTag() {
        return null;
    }

    private static String createToken(ServiceExecutor executor, XmlDoc.Element te, String tokenApp, String tokenTag)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (tokenApp != null) {
            dm.add("app", tokenApp);
        }
        // grant unimelb:token-downloader role
        dm.add("role", new String[] { "type", "role" }, AssetDownloadScriptWriter.TOKEN_DOWNLOADER_ROLE);

        if (te != null) {
            if (te.elementExists("from")) {
                dm.add(te.element("from"));
            }
            dm.add(te.element("to"));
            if (te.elementExists("use-count")) {
                dm.add(te.element("use-count"));
            }
            /*
             * roles
             */
            if (te.elementExists("role")) {
                dm.addAll(te.elements("role"));
            }
            /*
             * perms
             */
            if (te.elementExists("perm")) {
                dm.addAll(te.elements("perm"));
            }
        }
        if (tokenTag != null) {
            dm.add("tag", tokenTag);
        }

        return executor.execute("secure.identity.token.create", dm.root()).value("token");
    }

    static void destroyToken(ServiceExecutor executor, String token) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("token", token);
        executor.execute("secure.identity.token.destroy", dm.root());
    }

    static ServerDetails resolveServerDetails(ServiceExecutor executor, Element se) throws Throwable {
        ServerDetails sd = ServerDetails.resolve(executor);
        if (se == null) {
            return sd;
        } else {
            String transport = se.value("transport");
            String host = se.value("host");
            int port = se.intValue("port", -1);
            if (transport == null) {
                transport = sd.transport();
            }
            if (host == null) {
                host = sd.host();
            }
            if (port < 0) {
                port = sd.port();
            }
            return new ServerDetails(transport, host, port);
        }
    }

    @Override
    public int minNumberOfOutputs() {
        return 1;
    }

    @Override
    public int maxNumberOfOutputs() {
        return 1;
    }

}
