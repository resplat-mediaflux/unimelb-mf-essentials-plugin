package unimelb.mf.essentials.plugin.services;

import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Arrays;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcSecureIdentityTokenCreate extends PluginService {

    public static final String SERVICE_NAME = "unimelb.secure.identity.token.create";

    public static final String EMAIL_SUBJECT = "Token";

    public static final String EMAIL_RESENT_SUBJECT = "Mediaflux Token recreated, please use new token";

    private static void addCreateArgs(Interface defn) {
        defn.add(new Interface.Element("allow-service-parameter-substitution", BooleanType.DEFAULT,
                "Does this token allow service parameter substitution? Defaults to false.", 0, 1));
        defn.add(new Interface.Element("app", StringType.DEFAULT,
                " If specified, then only this application can use the token.", 0, 1));

        Interface.Element conditionalService = new Interface.Element("conditional-service", XmlDocType.DEFAULT,
                "A specific service to execute, if the required condition is selected. If specified, then the token executes this service only. Add service arguments as sub-elements. If a service is specified, then permission to this service is automatically granted.",
                0, 1);
        Interface.Element conditionalServiceIf = new Interface.Element("if", XmlDocType.DEFAULT, "", 1, 1);
        conditionalServiceIf.add(new Interface.Element("label", StringType.DEFAULT,
                "The label to use when describing the action to a user", 0, 1));
        conditionalServiceIf.add(new Interface.Element("value", StringType.DEFAULT,
                "A string that is used to uniquely identify this condition.", 1, 1));
        conditionalService.add(conditionalServiceIf);

        Interface.Element conditionalServiceThen = new Interface.Element("then", XmlDocType.DEFAULT, "", 1, 1);
        Interface.Element conditionalServiceThenService = new Interface.Element("service", XmlDocType.DEFAULT,
                " A specific service to execute. If specified, then the token executes this service only. Add service arguments as sub-elements. If a service is specified, then permission to this service is automatically granted.",
                1, 1);
        conditionalServiceThenService
                .add(new Interface.Attribute("name", StringType.DEFAULT, "The name of the service.", 1));
        conditionalServiceThenService.setIgnoreDescendants(true);
        conditionalService.add(conditionalServiceThen);
        defn.add(conditionalService);

        Interface.Element createAuthKey = new Interface.Element("create-auth-key", BooleanType.DEFAULT,
                " Create another key that can be used for authentication. The precise method depends on the application. Defaults to false",
                0, 1);
        createAuthKey.add(new Interface.Attribute("min-key-length", new IntegerType(16, 4096),
                "The minimum length of the token. Smaller is less secure. Defaults to 96.", 0));
        createAuthKey.add(new Interface.Attribute("max-key-length", new IntegerType(16, 4096),
                "The maximum length of the token. Defaults to 128.", 0));
        defn.add(createAuthKey);

        defn.add(new Interface.Element("description", StringType.DEFAULT, "An arbitrary description for the token.", 0,
                1));

        defn.add(new Interface.Element("destroy-on-service-call", StringType.DEFAULT,
                "One or more services that, if called, will automatically destroy the token. To destroy after any initial service call supply a wildcard '*' character.",
                0, 255));

        defn.add(new Interface.Element("from", DateType.DEFAULT,
                "A time, before which the token is not valid. If not supplied token is valid immediately.", 0, 1));

        defn.add(new Interface.Element("grant-caller-transient-roles", BooleanType.DEFAULT,
                " Grant the caller's transient roles, such as roles acquired through LDAP and the domain role. Defaults to false.",
                0, 1));

        Interface.Element identity = new Interface.Element("identity", XmlDocType.DEFAULT,
                "Specifies the user that will be recorded in operations executed by this token. Defaults to the caller. Must have system administration rights to create for another user. If created for another user, then the token will not have access to the secure wallet.",
                0, 1);
        Interface.Element identityAuthority = new Interface.Element("authority", StringType.DEFAULT,
                "The identity of the authority/repository where the user identity originates. If unspecified, then refers to a user in this repository.",
                0, 1);
        identityAuthority.add(new Interface.Attribute("protocol", StringType.DEFAULT,
                "The protocol of the identity authority. If unspecified, defaults to federated user within the same type of repository.",
                0));
        identity.add(identityAuthority);
        identity.add(new Interface.Element("domain", StringType.DEFAULT, "The name of the domain.", 1, 1));
        identity.add(new Interface.Element("grant-user-roles", BooleanType.DEFAULT,
                "If true, then lookup and grant roles held by that user. Defaults to false.", 0, 1));
        identity.add(new Interface.Element("user", StringType.DEFAULT, "The name of the user.", 1, 1));
        defn.add(identity);

        defn.add(new Interface.Element("max-token-length", new IntegerType(16, 4096),
                "The maximum length of the token. Defaults to 128.", 0, 1));
        defn.add(new Interface.Element("min-token-length", new IntegerType(16, 4096),
                "The minimum length of the token. Smaller is less secure. Defaults to 96.", 0, 1));

        Interface.Element perm = new Interface.Element("perm", XmlDocType.DEFAULT, "Permission to grant.", 0,
                Integer.MAX_VALUE);
        perm.add(new Interface.Element("access", new StringType(64), "Access type.", 1, 1));
        Interface.Element permResource = new Interface.Element("resource", new StringType(255), "Pattern for resource.",
                1, 1);
        permResource.add(new Interface.Attribute("type", new StringType(32), "Resource type", 1));
        perm.add(permResource);
        defn.add(perm);

        defn.add(new Interface.Element("persistent", BooleanType.DEFAULT,
                "If true (the default), then the identity will remain after server restart. If false, then identity will be gone after a server restart.",
                0, 1));

        Interface.Element restrict = new Interface.Element("restrict", XmlDocType.DEFAULT,
                "Access restrictions, if any. For the details about the restrictions, see the documentation of built-in service: secure.identity.token.create.", 0, 1);
        restrict.setIgnoreDescendants(true);
        defn.add(restrict);

        Interface.Element role = new Interface.Element("role", new StringType(128), "Role (name) to grant.", 0,
                Integer.MAX_VALUE);
        role.add(new Interface.Attribute("type", new StringType(64), "Role type", 1));
        defn.add(role);

        Interface.Element service = new Interface.Element("service", XmlDocType.DEFAULT,
                "A specific service to execute. If specified, then the token executes this service only. Add service arguments as sub-elements. If a service is specified, then permission to this service is automatically granted.",
                0, 1);
        service.add(new Interface.Attribute("destroy", BooleanType.DEFAULT,
                "If true, the token is automatically destroyed on execution. Defaults to false.", 0));
        service.add(new Interface.Attribute("name", StringType.DEFAULT, " The name of the service.", 1));
        service.setIgnoreDescendants(true);
        defn.add(service);

        defn.add(new Interface.Element("tag", StringType.DEFAULT, "An arbitrary tag for later filtering.", 0,
                Integer.MAX_VALUE));

        defn.add(new Interface.Element("to", DateType.DEFAULT,
                "A time, after which the token is no longer valid. If not supplied token will not expire.", 0, 1));

        defn.add(new Interface.Element("use-count", IntegerType.POSITIVE_ONE,
                "The number of times the token may be used.", 0, 1));

        defn.add(new Interface.Element("wallet", BooleanType.DEFAULT,
                "If true, then the identity will have access the the users wallet. Defaults to false.", 0, 1));
    }

    private Interface _defn;

    public SvcSecureIdentityTokenCreate() {
        _defn = new Interface();

        addCreateArgs(_defn);

        _defn.add(new Interface.Element("email-to-identity-user", BooleanType.DEFAULT,
                "Email the token to the identity user. Defaults to true.", 0, 1));
        _defn.add(new Interface.Element("resend",BooleanType.DEFAULT,
                "the token is being recreated due to an error, defaults to false", 0, 1));
        _defn.add(
                new Interface.Element("email-to", EmailAddressType.DEFAULT, "Email the token to recipient(s).", 0, 5));

        _defn.add(new Interface.Element("get-token", BooleanType.DEFAULT,
                "If true the token string will be included the service result. Defaults to false.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Generates a secure identity token and email it to the user.";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        boolean emailToIdentityUser = args.booleanValue("email-to-identity-user", true);
        boolean resending = args.booleanValue("resend", false);
        Set<String> emailTo = new LinkedHashSet<>();
        if (args.elementExists("email-to")) {
            Collection<String> vs = args.values("email-to");
            for (String v : vs) {
                emailTo.add(v.toLowerCase());
            }
        }

        Set<String> projectRoles = new LinkedHashSet<>();
        if (args.elementExists("role")) {
            Collection<String> vs = args.values("role");
            for (String v : vs) {
                projectRoles.add(v.toLowerCase());
            }
        }

        if (emailToIdentityUser) {
            String identityUserEmail = args.elementExists("identity/user")
                    ? getUserEmail(executor(), args.value("identity/authority"),
                            args.value("identity/authority/@protocol"), args.value("identity/domain"),
                            args.value("identity/user"))
                    : getCallerEmail(executor());
            if (identityUserEmail != null) {
                emailTo.add(identityUserEmail.toLowerCase());
            }
        }
        boolean getToken = args.booleanValue("get-token", false);

        if (emailTo.isEmpty() && !getToken) {
            throw new IllegalArgumentException(
                    "No 'email-to' or 'email-to-identity-user' was set, or the email address of the caller (or identity user) could not be identified, furthermore, 'get-token' was set to false. The result token can go nowhere. To resolve this, either set 'get-token' to true and it will return the token via the service result, or specified recipient address via 'email-to' argument so that the token is sent out via email.");
        }

        XmlDocMaker dm = new XmlDocMaker("args");
        List<XmlDoc.Element> es = args.elements();
        List<String> nocopyargs = Arrays.asList("email-to", "email-to-identity-user", "get-token", "resend");
        if (es != null) {
            for (XmlDoc.Element e : es) {
                // add only elements needed for the underlying service
                boolean nocopy = false;
                for (String v : nocopyargs) {
                    if (e.name().equalsIgnoreCase(v)) {
                        nocopy = true;
                        break;
                    }
                }
                if (!nocopy) {
                    dm.add(e);
                }
            }
        }
        XmlDoc.Element te = executor().execute("secure.identity.token.create", dm.root()).element("token");
        if (!emailTo.isEmpty()) {
            String token = te.value();
            emailToken(executor(), token, emailTo, projectRoles,resending);
        }
        if (getToken) {
            w.add(te);
        } else {
            w.add("token", new String[] { "id", te.value("@id"), "actor-type", te.value("@actor-type"), "actor-name",
                    te.value("@actor-name") });
        }
        if (!emailTo.isEmpty()) {
            for (String to : emailTo) {
                w.add("sent-to", to);
            }
        }
    }

    private static String getUserEmail(ServiceExecutor executor, String authority, String protocol, String domain,
            String username) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (authority != null) {
            dm.add("authority", new String[] { "protocol", protocol }, authority);
        }
        dm.add("domain", domain);
        dm.add("user", username);
        return executor.execute("user.describe", dm.root()).value("user/e-mail");
    }

    private static String getCallerEmail(ServiceExecutor executor) throws Throwable {
        XmlDoc.Element ue = executor.execute("user.self.describe").element("user");
        if (ue.elementExists("e-mail")) {
            return ue.value("e-mail");
        } else {
            return getUserEmail(executor, ue.value("@authority"), ue.value("@protocol"), ue.value("@domain"),
                    ue.value("@user"));
        }
    }

    private static void emailToken(ServiceExecutor executor, String token, Collection<String> recipients, Collection<String> roles, Boolean resending)
            throws Throwable {
        for (String to : recipients) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("to", to);

            if (resending) {
                dm.add("subject", EMAIL_RESENT_SUBJECT);
            } else {
                dm.add("subject", EMAIL_SUBJECT);
            }
            dm.add("body-from-input", new String[] { "encoding", "UTF-8", "type", "text/plain" }, true);

            String resendtext = "";
            if (resending){
                resendtext = "The mediaflux token for the following project roles have been recreated due to an error with the previous token. Please use the token below.";
            }

            byte[] bodyBytes = String.format(
                    "Dear colleague%n%n %s%n%n A Mediaflux secure identity token has been generated for you as follows:%n%n%s%n%n with the following project roles:%n%n%s%n%n %n%nRegards%n%nThe Mediaflux Team",
                    resendtext, token,roles.toString()).getBytes("UTF-8");
            executor.execute("mail.send", dm.root(), new PluginService.Inputs(
                    new PluginService.Input(new ByteArrayInputStream(bodyBytes), bodyBytes.length, "text/plain", null)),
                    null);
        }

    }

}
