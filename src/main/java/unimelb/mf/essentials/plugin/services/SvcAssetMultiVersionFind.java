package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServerRoute;import arc.mf.plugin.PluginService.Interface.Element;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.PeerUtil;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.mf.essentials.plugin.util.ServerDetails;


/**
 * 
 * @author Neil Killeen
 *
 */
public class SvcAssetMultiVersionFind extends PluginService {
	private Interface _defn;

	public SvcAssetMultiVersionFind() {

		_defn = new Interface();
		_defn.add(new Element("where", StringType.DEFAULT, "Restrict selection of assets.  A clause selecting only assets with content above the specified size and version above the given count will be automatically added, so don't include those.", 1, 1));
		_defn.add(new Interface.Element("peer", StringType.DEFAULT, "Peer on which to look for replica. If not given, don't look.", 0, 1));
		_defn.add(new Interface.Element("email", StringType.DEFAULT, "Send the report to this email address. If it finds nothing, no email is sent.", 0, 1));
		_defn.add(new Interface.Element("csize", LongType.DEFAULT, "Assets content size (current version) must be larger than this (bytes) to be considered.", 1, 1));
		_defn.add(new Interface.Element("count", IntegerType.DEFAULT, "Assets must have more than this number of versions to be considered.  Defaults to 1", 0, 1));
	}

	public String name() {
		return "unimelb.asset.multi.version.find";
	}

	public String description() {
		return "This service looks for assets that are above a certain content size threshold and have more versions than a certain threshold.  It then optionally looks for the asset on DR as well and reports to the specified email address.  The intent is to find assets used as regular dumps of large structures (e.g. a DB backup) that consume a lot of storage.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Arguments
		Long csize = args.longValue("csize");
		Integer count = args.intValue("count", 1);
		String where = args.value("where");
		String email  = args.value("email");
		String peer  = args.value("peer");

		// Find
		XmlDocMaker dm = new XmlDocMaker("args");
		String where2 = "(asset has content) and (" + where + ")" +
		" and csize>"+csize+" and version count>"+count;
		dm.add("where", where2);
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return;
		}
		Collection<String> ids = r.values("id");
		if (ids==null) {
			return;
		}

		// Find route to peer. Exception if can't reach and build in extra checks to make sure we are 
		// being very safe
		ServerRoute srDR = null;
		if (peer!=null) {
			srDR = PeerUtil.findPeerRoute(executor(), peer);
		}
		String uuid = ServerDetails.serverUUID(executor());

		// Iterate
		String assetStr = "";
		for (String id : ids) {

			// Get Replica
			XmlDoc.Element rAsset = null;
			Long rsum = 0L;
			if (peer!=null) {
				String t = "rid="+uuid+"."+id;
				if (AssetUtils.exists(executor(), srDR, t, false)) {
					rAsset = AssetUtils.getAsset(executor(), 
							srDR, null, t);
					String t2 = "rid='"+uuid+"."+id+"'";
					rsum = sumAllVersions (executor(), srDR, t2);
				}
			}

			// SUm all versions
			long sum = sumAllVersions (executor(), null, "id="+id);
			XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);

			// List
			String path = asset.value("asset/path");
			String cs = asset.value("asset/content/size");
			String vc = asset.value("asset/versioned/@count");
			if (rAsset!=null) {
				String rcs = rAsset.value("asset/content/size");
				String rvc = rAsset.value("asset/versioned/@count");
				w.add("id", new String[] {
						"path", path,
						"csize", cs, 
						"versions", vc,
						"total-csize", ""+sum, 
						"r-csize", rcs,
						"r-versions", rvc,
						"total-rcsize", ""+rsum},
						id);
				assetStr += "id -path " + path + " -csize " + cs + " -versions " + vc + 
						" -total-csize " + sum + 
						" -r-csize " + rcs + " -r-versions " + rvc + 
						" -total-rcsize " + rsum + 
						" " + id + "\n";
			} else {
				w.add("id", new String[] {
						"path", path,
						"csize", cs, 
						"versions", vc,
						"total-csize", ""+sum, },
						id);
				assetStr += "id -path " + path + " -csize " + cs + " -versions " + vc + 
						" -total-csize " + sum + 
						" " + id + "\n";		
			}
		}



		if (email!=null) {
			String from = Properties.getServerProperty(executor(), "mail.from");
			String body = "The following assets were found (on the primary) with " +
					"csize > " + csize + " and " +
					"version count > " + count + "\n\n" +
					assetStr + "\n\n Consider pruning them on primary and DR (if appropriate) \n";

			MailUtils.sendEmail(executor(), email, null, null, from, "["+uuid+"] Mediaflux multi-versioned assets found above size threshold", body, false);
		}
	}

	private long sumAllVersions (ServiceExecutor executor, ServerRoute sr, String where) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where);
		XmlDoc.Element r = executor.execute(sr, "unimelb.asset.content.size.sum.all.versions", dm.root());
		return r.longValue("total");
	}
}