package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcAssetProtocolUserList extends PluginService {


	private Long nHTTP_;
	private Long nSMB_;
	private Long nSSH_;
	private Long nNFS_;
	private Long nDICOM_;
	private Long nUnknown_;
	private Long nTotal_;


	private Interface _defn;

	public SvcAssetProtocolUserList() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Predicate to select assets", 1, 1));
		_defn.add(new Interface.Element("size", IntegerType.DEFAULT, "Size of cursor. Defaults to 10000", 0, 1));
	}

	public String name() {
		return "unimelb.asset.protocol.user.list";
	}

	public String description() {
		return "This service lists the users who have created assets (and how many) with the various available protocols (http/s, ssh, smb, nfs, dicom).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Initialize
		nHTTP_ = 0L;
		nSMB_ = 0L;
		nSSH_ = 0L;
		nNFS_ = 0L;
		nDICOM_ = 0L;
		nUnknown_ = 0L;
		nTotal_ = 0L;
		//
		
		String where = args.value("where");
		Integer size = args.intValue("size", 10000);

		// Accumulate number assets per user per protocol
		// key = user
		// value = number of assets
		HashMap<String,Long> http = new HashMap<String,Long>();
		HashMap<String,Long> smb = new HashMap<String,Long>();
		HashMap<String,Long> ssh = new HashMap<String,Long>();
		HashMap<String,Long> nfs = new HashMap<String,Long>();
		HashMap<String,Long> dicom = new HashMap<String,Long>();
		HashMap<String,Long> unknown = new HashMap<String,Long>();

		boolean complete = false;
		Integer idx = 1;
		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("where", where);
			dm.add("size", size);
			dm.add("idx", idx);
			XmlDoc.Element r = executor().execute("asset.query", dm.root());
			if (r==null) {
				complete = true;
			} else {
				Collection<String> ids = r.values("id");
				if (ids==null) {
					complete = true;
				} else {
					accumulate (ids, unknown, http, ssh, smb, nfs, dicom);
					complete = r.booleanValue("cursor/total/@complete");
				}
			}
			idx += size;
		}

		// List
		w.push("http");
		list(w,http);
		w.add("total", nHTTP_);
		w.pop();
		//
		w.push("smb");
		list(w,smb);
		w.add("total", nSMB_);
		w.pop();
		//
		w.push("ssh");
		list(w,ssh);
		w.add("total", nSSH_);
		w.pop();
		//
		w.push("nfs");
		list(w,nfs);
		w.add("total", nNFS_);
		w.pop();
		//
		w.push("dicom");
		list(w,dicom);
		w.add("total", nDICOM_);
		w.pop();
		//
		w.push("unknown");
		list(w,unknown);
		w.add("total", nUnknown_);
		w.pop();
		w.add("total", nTotal_);
	}


	private void accumulate (Collection<String> ids, HashMap<String,Long> unknown, 
			HashMap<String,Long> http, HashMap<String,Long> ssh, 
			HashMap<String,Long> smb, HashMap<String,Long> nfs, 
			HashMap<String,Long> dicom) throws Throwable {
		for (String id : ids) {
			XmlDoc.Element meta = AssetUtils.getAsset(executor(), null, null, id);
			String protocol = meta.value("asset/created-via");
			XmlDoc.Element creator = meta.element("asset/creator");
			String user = creator.value("domain") + ":" + creator.value("user");
			if (protocol==null) {
				add (unknown, user);
				nUnknown_++;
			} else if (protocol.equals("http")) {
				// Could distinguish between http and https if we want (there is an attribute)
				add (http, user);
				nHTTP_++;
			} else if (protocol.equals("smb")) {
				add (smb, user);
				nSMB_++;
			} else if (protocol.equals("ssh")) {
				add (ssh, user);
				nSSH_++;
			} else if (protocol.equals("nfs")) {
				add (nfs, user);
				nNFS_++;
			} else if (protocol.equals("dicom")) {
				add (dicom, user);
				nDICOM_++;
			}
			nTotal_++;
		}
	}

	private void list (XmlWriter w, HashMap<String,Long> map) throws Throwable {
		Set<String> keys = map.keySet();
		for (String key : keys) {
			w.add("user", new String[] {"total", ""+map.get(key)}, key);
		}
		w.add("total-users", keys.size());
	}


	private void add (HashMap<String,Long> map, String user) throws Throwable {
		if (map.containsKey(user)) {
			Long n = map.get(user);
			map.put(user,++n);
		} else {
			map.put(user, 1L);
		}
	}
}
