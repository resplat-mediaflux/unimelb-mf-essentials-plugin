package unimelb.mf.essentials.plugin.services;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.ObjectUtils;

public class SvcAssetNamespaceStoreOverriddenList extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.store.overridden.list";

    private Interface _defn;

    public SvcAssetNamespaceStoreOverriddenList() {
        _defn = new Interface();
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The root namespace.", 1, 1));
        _defn.add(new Interface.Element("store", StringType.DEFAULT,
                "Include only the namespaces using the specified store.", 0, Integer.MAX_VALUE));
        _defn.add(new Interface.Element("store-policy", StringType.DEFAULT,
                "Include only the namespaces using the specified store-policy.", 0, Integer.MAX_VALUE));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find the asset namespaces with their store/store-policy overridden compared with the specified root namespace.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String namespace = args.value("namespace");
        Set<String> stores = args.elementExists("store") ? new LinkedHashSet<String>(args.values("store")) : null;
        Set<String> storePolicies = args.elementExists("store-policy")
                ? new LinkedHashSet<String>(args.values("store-policy"))
                : null;
        findNamespaces(executor(), namespace, stores, storePolicies, w);
    }

    private static void findNamespaces(ServiceExecutor executor, String parentNS, Set<String> stores,
            Set<String> storePolicies, XmlWriter w) throws Throwable {

        boolean hasStoreFilters = stores != null && !stores.isEmpty();
        boolean hasStorePolicyFilters = storePolicies != null && !storePolicies.isEmpty();
        boolean hasFilters = hasStoreFilters || hasStorePolicyFilters;

        long total = 0;
        Stack<String> namespaces = new Stack<String>();
        namespaces.push(parentNS);
        while (!namespaces.isEmpty()) {

            PluginTask.checkIfThreadTaskAborted();

            String namespace = namespaces.pop();
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.add("namespace", namespace);
            dm.add("levels", 1);
            XmlDoc.Element re = executor.execute("asset.namespace.describe", dm.root());
            String parentStore = re.value("namespace/store");
            String parentStorePolicy = re.value("namespace/store-policy");
            List<XmlDoc.Element> nes = re.elements("namespace/namespace");
            if (nes != null) {
                for (XmlDoc.Element ne : nes) {
                    String path = ne.value("path");
                    String store = ne.value("store");
                    String storePolicy = ne.value("store-policy");
                    if (store != null) {
                        if (!ObjectUtils.equals(store, parentStore)) {
                            if (!hasFilters || (hasStoreFilters && stores.contains(store))) {
                                w.add("namespace", new String[] { "store", store }, path);
                            }
                        }
                    } else {
                        if (!ObjectUtils.equals(storePolicy, parentStorePolicy)) {
                            if (!hasFilters || (hasStorePolicyFilters && storePolicies.contains(storePolicy))) {
                                w.add("namespace", new String[] { "store-policy", storePolicy }, path);
                            }
                        }
                    }
                    if (ne.elementExists("namespace")) {
                        namespaces.push(path);
                    }
                }
                total += nes.size();
                PluginTask.threadTaskCompletedMultipleOf(nes.size(), total);                
            }
        }

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
