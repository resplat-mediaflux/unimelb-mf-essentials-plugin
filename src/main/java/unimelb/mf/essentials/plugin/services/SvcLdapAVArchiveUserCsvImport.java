package unimelb.mf.essentials.plugin.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.Vector;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.EmailUtils;

public class SvcLdapAVArchiveUserCsvImport extends PluginService {

	public static final String SERVICE_NAME = "unimelb.ldap.avarchive.user.csv.import";

	private Interface _defn;

	public SvcLdapAVArchiveUserCsvImport() {
		_defn = new Interface();
		_defn.add(new Interface.Element("archive-name", StringType.DEFAULT, "The archive name.", 1, 1));
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The LDAP authentication domain.  If not supplied, must be supplied as a column specification.", 0, 1));
		_defn.add(new Interface.Element("domain-column", IntegerType.POSITIVE_ONE,
				"Index of the authentication domain column (if not specified by argument 'domain'. Starts from 1.", 0, 1));
		_defn.add(new Interface.Element("employee-id-column", IntegerType.POSITIVE_ONE,
				"Index of the employeed ID  column (if not specified by argument 'domain'. Starts from 1.", 0, 1));
		_defn.add(new Interface.Element("username-column", IntegerType.POSITIVE_ONE,
				"Index of the username column. Starts from 1.", 0, 1));
		_defn.add(new Interface.Element("email-column", IntegerType.POSITIVE_ONE,
				"Index of the email column. Starts from 1.", 0, 1));
		_defn.add(new Interface.Element("role-column", IntegerType.POSITIVE_ONE,
				"Index of the role column. Starts from 1.  The role values should be one of the symbolic (not actual) roles defined for this archive. If they are wrong an exception will arise. For the VCA-FTV archive they are admin,staff,student,guest.", 0, 1));
		_defn.add(new Interface.Element("enrol-column", IntegerType.POSITIVE_ONE,
				"Index of the enrol (value yes/no) column. Starts from 1.  The value should be one of [yes,no]. If the column is not given, all users are enrolled.", 0, 1));
		_defn.add(new Interface.Element("email-suffix", StringType.DEFAULT,
				"Suffix of email address. If specified, only matching email address are included.", 0, 1));
		_defn.add(new Interface.Element("role", StringType.DEFAULT, "Other roles to grant the users.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("skip-rows", IntegerType.DEFAULT, "Skip this many rows before importing users. Default is 0.", 0, 1));
		_defn.add(new Interface.Element("test-only", BooleanType.DEFAULT, 
				"Don't actually enrol or grant, just report what would happen. Default is true.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Find AV Archive users by employee ID, AD user name or email in that order (the email is very unreliable) - parsed from the specified column of the imported CSV file) and then enrols the user and grants them access in the archive according to the specified role.  The  number of users handled (i.e. actually matched in the given domain) is listed at the end.  Of those, it also lists the number enrolled (if already enrolled does not do again) and granted access (if already has the given role does not do again).";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs arg2, XmlWriter w) throws Throwable {

		if (!args.elementExists("username-column") && !args.elementExists("email-column") && !args.elementExists("role-column")) {
			throw new IllegalArgumentException("No username-column, email-column or role-column was specified.");
		}

		String domainCol = args.value("domain-column");
		String domain = args.value("domain");
		if (domainCol!=null && domain!=null) {
			throw new Exception("You must specify only one of 'domain' and 'domain-col'");
		}
		int employeeIDCol = args.intValue("employee-id-column", 0);
		int usernameCol = args.intValue("username-column", 0);
		int emailCol = args.intValue("email-column", 0);
		int roleCol = args.intValue("role-column", 0);
		int enrolCol = args.intValue("enrol-column", 0);
		String emailSuffix = args.value("email-suffix");
		String archiveName = args.value("archive-name");
		Collection<String> roles = args.values("role");
		int skip = args.intValue("skip-rows", 0);
		Boolean testOnly = args.booleanValue("test-only", true);

		// Describe the archive and get roles
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", archiveName);
		XmlDoc.Element r = executor().execute("arc.av.archive.describe", dm.root());
		Collection<XmlDoc.Element> archiveRoles = r.elements("archive/access-control/roles/role");
		if (archiveRoles==null) {
			throw new Exception("No roles were detected for the archive '" + archiveName + "'");
		}

		//
		PluginService.Input input = inputs.input(0);

		int nUsersHandled = 0;
		int nUsersEnrolled = 0;
		int nUsersGranted = 0;
		int nUsersAlreadyWithRoles = 0;
		int nUsersSkipped = 0;
		try (Reader in = new BufferedReader(new InputStreamReader(input.stream()))) {
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withAllowMissingColumnNames().parse(in);
			int numberRead  = 0;
			for (CSVRecord record : records) {
				if (numberRead>=skip) {
					PluginTask.checkIfThreadTaskAborted();
					if (domainCol!=null) {
						Integer idc = Integer.parseInt(domainCol);
						domain = idc > 0 ? record.get(idc - 1) : null;
					}

					// Look for the employeeID first, then username, then email (not reliable)
					String t = (employeeIDCol > 0 ? record.get(employeeIDCol - 1) : null);
					String employeeID = null;
					if (t!=null) {
						employeeID = t.trim().toLowerCase();
					}
					//
					t = (usernameCol > 0 ? record.get(usernameCol - 1) : null);
					String username = null;
					if (t!=null) {
						username = t.trim().toLowerCase();
					}
					//
					t = (emailCol > 0 ? record.get(emailCol - 1) : null);
					String email = null;
					if (t!=null) {
						email = t.trim().toLowerCase();
					}
					//
					t = (roleCol > 0 ? record.get(roleCol - 1) : null);
					String role = null;
					if (t!=null) {
						role = t.trim().toLowerCase();
					}
					//
					String enrol = (enrolCol > 0 ? record.get(enrolCol - 1) : "yes").toLowerCase();
					if (role==null) {
						throw new Exception ("The role is null for 1-rel row " + numberRead+1);
					}
					//
					if (domain==null || (domain!=null && domain.isEmpty())) {
						w.add("skipped-row", new String[] {"reason", "no domain"}, numberRead+1);
						nUsersSkipped++;
					} else {
						if (employeeID!=null && !employeeID.isEmpty()) {
							XmlDoc.Element re = findLdapUserByEmployeeID (executor(), domain, employeeID);
							int nbFound = 0;
							if (re!=null) {
								Collection<XmlDoc.Element> users = re.elements("user");
								if (users!=null) {
									nbFound = users.size();
								}
							}
							if (nbFound == 1) {
								XmlDoc.Element ue = re.element("user");
								String user = ue.value("@user");
								String name = ue.value("@name");

								// Enrol and grant archive role
								nUsersHandled++;
								Boolean ret[] = handleUser (executor(), domain, user, name, email,
										employeeID, archiveName, role, archiveRoles, testOnly, enrol, w);
								if (ret[0]) {
									nUsersEnrolled++;
								}
								if (ret[1]) {
									nUsersGranted++;
								}
								if (ret[2]) {
									nUsersAlreadyWithRoles++;
								}

								// Grant additional non-archive roles if any
								if (!testOnly) {
									grantRolesToUser (executor(),  domain, user, roles);
								}
							} else if (nbFound==0) {
								nUsersSkipped++;
								w.add("skipped-row", new String[] {"domain", domain, "user", username, "email", email, "reason", "employee ID not found"}, numberRead+1);
							} else {
								nUsersSkipped++;
								w.add("skipped-row", new String[] {"domain", domain, "user", username, "email", email, "reason", "multiple users found with employee ID"}, numberRead+1);
							}
						} else if (username != null && !username.isEmpty()) {
							if (userExists(executor(), domain, username)) {

								XmlDoc.Element ue = getUser(executor(), domain, username);

								// Enrol and grant archive role
								nUsersHandled++;
								Boolean ret[] = handleUser (executor(), domain, username, ue.value("name"), 
										ue.value("e-mail"), null, archiveName, role, archiveRoles, 
										testOnly, enrol, w);
								if (ret[0]) {
									nUsersEnrolled++;
								}
								if (ret[1]) {
									nUsersGranted++;
								}
								if (ret[2]) {
									nUsersAlreadyWithRoles++;
								}

								// Grant additional non-archive roles if any
								if (!testOnly) {
									grantRolesToUser (executor(),  domain, username, roles);
								}
							} else {
								nUsersSkipped++;
								w.add("skipped-row", new String[] {"domain", domain, "user", username, "email", email, "reason", "user not found in domain"}, numberRead+1);
							}
						} else if (email != null && !email.isEmpty()) {
							if (EmailUtils.isValidEmailAddress(email)) {
								if (emailSuffix == null || email.toLowerCase().endsWith(emailSuffix.toLowerCase())) {
									XmlDoc.Element re = findLdapUserByEmail(executor(), domain, email);
									int nbFound = 0;
									if (re!=null) {
										Collection<XmlDoc.Element> users = re.elements("user");
										if (users!=null) {
											nbFound = users.size();
										}
									}
									if (nbFound == 1) {
										XmlDoc.Element ue = re.element("user");
										String user = ue.value("@user");
										String name = ue.value("@name");

										// Enrol and grant archive role
										nUsersHandled++;
										Boolean ret[] = handleUser (executor(), domain, user, name, email,
												null, archiveName, role, archiveRoles, testOnly, enrol, w);
										if (ret[0]) {
											nUsersEnrolled++;
										}
										if (ret[1]) {
											nUsersGranted++;
										}
										if (ret[2]) {
											nUsersAlreadyWithRoles++;
										}

										// Grant additional non-archive roles if any
										if (!testOnly) {
											grantRolesToUser (executor(),  domain, user, roles);
										}
									} else if (nbFound==0) {
										nUsersSkipped++;
										w.add("skipped-row", new String[] {"domain", domain, "user", username, "email", email, "reason", "user not in domain"}, numberRead+1);
									} else {
										nUsersSkipped++;
										w.add("skipped-row", new String[] {"domain", domain, "user", username, "email", email, "reason", "multiple users found in domain"}, numberRead+1);
									}
								} else {
									nUsersSkipped++;
									w.add("skipped-row", new String[] {"domain", domain, "user", username, "email", email, "reason", "user filtered by email suffix"}, numberRead+1);
								}
							} else {
								nUsersSkipped++;
								w.add("skipped-row", new String[] {"reason", "no username and invalid email"},numberRead+1);
							}
						} else {
							w.add("skipped-row", new String[] {"reason", "no username or email"},numberRead+1);
						}
					}
				} else {
					w.add("skipped-row", numberRead+1);
				}
				numberRead++;
			}
		}
		w.add("number-users-skipped", nUsersSkipped);;
		w.add("number-users-handled", nUsersHandled);
		w.add("number-users-already-with-archive-roles", nUsersAlreadyWithRoles);
		w.add("number-users-enrolled", nUsersEnrolled);
		w.add("number-users-granted-access", nUsersGranted);
	}

	// returns
	//  ret[0] true if user enrolled
	//  ret[1] true if a role granted 
	//  ret[2] true if user already held an archive role
	private Boolean[] handleUser (ServiceExecutor executor, String domain, String username, String name,
			String email, String employeeID, String archiveName, String archiveRoleToGrant, 
			Collection<XmlDoc.Element> archiveRoles, Boolean testOnly, String enrol, XmlWriter w) throws Throwable {

		Boolean[] ret = new Boolean[3];
		w.push("user");
		w.add("domain", domain);
		w.add("user", username);
		w.add("name", name);
		w.add("email", email);
		if (employeeID!=null) {
			w.add("employee-id", employeeID);
		}
		
		// Create the user as a Person
		// TBD when defect fixed allow this
		// String personID = makePerson (ServiceExecutor executor, archiveName, email, name);

		// Enrol user.
		Boolean isEnrolled = userIsEnrolled (executor, domain, username);
		ret[0] = false;
		if (enrol.equalsIgnoreCase("yes")) {
			if (isEnrolled) {
				w.add("enrolled", new String[]{"reason", "already-enrolled"}, false);
			} else {
				if (!testOnly) {
					enrolUser(executor(), archiveName, domain, username);
					// TBD use when above defect fixed
					// enrolPerson(executor(), archiveName, personID);
				}
				w.add("enrolled", true);
				ret[0] = true;
			}
		} else {
			w.add("enrolled", new String[]{"reason", "not-requested"}, false);
		}

		// Note if user holds any roles already for the archive. These are the actual roles, not the symbolic roles
		Collection<String> activeRoles = activeUserRoles (executor(), domain, username, archiveRoles);
		w.push("archive-role");

		// If the user holds any pre-existing archive roles, revoke all possible roles
		// before we grant the new one
		if (activeRoles.size()>0) {
			if (!testOnly) {
				revokeAllRoles (executor, archiveName, archiveRoles, domain, username);
			}
			for (String activeRole : activeRoles) {
				w.add("existing-archive-role", new String[]{"revoked", "true"}, activeRole);
			}
		}

		if (!testOnly) {
			grantRoleToUser(executor(), domain, username, archiveRoleToGrant, archiveName);
		}
		w.add("granted", new String[] {"role", archiveRoleToGrant}, true);
		ret[1] = true;

		w.pop();
		w.pop();

		// Users that already have a role, any archive role
		if (activeRoles.size()>0) {
			ret[2] = true;
		} else {
			ret[2] = false;
		}
		return ret;
	}


	private void revokeAllRoles (ServiceExecutor executor, String archiveName, Collection<XmlDoc.Element> archiveRoles, String domain, String user) throws Throwable {
		for (XmlDoc.Element archiveRole : archiveRoles) {
			// Revoke symbolic role (NULLOP if not held)
			String symbolicRole = archiveRole.value("@name");
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", archiveName);
			dm.add("domain", domain);
			dm.add("user", user);
			dm.add("role", symbolicRole);
			executor.execute("arc.av.archive.access.revoke", dm.root());
		}

	}
	private Boolean userIsEnrolled (ServiceExecutor executor, String domain, String user) throws Throwable {
		// TBD awaiting  service to implement
		return false;
	}

	private Collection<String> activeUserRoles (ServiceExecutor executor, String domain, String user,
			Collection<XmlDoc.Element> archiveRoles) throws Throwable {

		// Iterate over the actual (not symbolic) roles and see if the user has one
		Vector<String> activeRoles = new Vector<String>();
		for (XmlDoc.Element archiveRole : archiveRoles) {
			String role = archiveRole.value("system-role");
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", domain+":"+user);
			dm.add("role", new String[] {"type", "role"}, role);
			dm.add("type", "user");
			XmlDoc.Element r = executor.execute("actor.have", dm.root());
			Boolean hasRole = r.booleanValue("actor/role", false);
			if (hasRole) {
				activeRoles.add(role);		
			}
		}
		return activeRoles;
	}
	private  boolean userExists(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("authentication.user.exists", dm.root()).booleanValue("exists");
	}

	private  XmlDoc.Element getUser(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("authentication.user.describe", dm.root()).element("user");
	}

	private  void grantRolesToUser(ServiceExecutor executor, String domain, String user,
			Collection<String> roles) throws Throwable {
		if (roles==null) return;
		if (roles.size()==0) return;

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", domain+":"+user);
		for (String role : roles) {
			dm.add("role", new String[] {"type", "role"}, role);
		}
		dm.add("type", "user");
		executor.execute("actor.grant", dm.root());
	}

	private String makePerson (ServiceExecutor executor, String archiveName, String email, 
			String fullName) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("archive-name", archiveName);
		dm.add("email", email);
		dm.push("name");
		dm.add("full", fullName);
		dm.pop();
		XmlDoc.Element r = executor.execute("arc.av.archive.person.create", dm.root());
		return r.value("id");    // TBD validate this is correct when above service fixed

	}
	private  void enrolUser(ServiceExecutor executor, String archiveName, String domain, String user) throws Throwable {
			
		// Enrol the User which  means they become (partly) a Person in this archive and can
		// be associated in meta-data. This adds the facet to the Person
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		dm.add("archive-name", archiveName);
		executor.execute("arc.av.archive.user.enrol", dm.root());
	}
	
	private  void enrolPerson(ServiceExecutor executor, 
			String archiveName, String personID) throws Throwable {
			
		// Enrol the Person which  means they become a Person in this archive and can
		// be associated in meta-data. This adds the facet to the Person
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", personID);
		dm.add("archive-name", archiveName);
		executor.execute("arc.av.archive.person.enrol", dm.root());
	}


	private  void grantRoleToUser(ServiceExecutor executor, String domain, String user,
			String role, String archiveName) throws Throwable {

		// Grant access. Anyone can have access to the archive without
		// being enrolled.
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		dm.add("name", archiveName);
		dm.add("role", role);
		executor.execute("arc.av.archive.access.grant", dm.root());
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public int minNumberOfInputs() {
		return 1;
	}

	public int maxNumberOfInputs() {
		return 1;
	}

	public static XmlDoc.Element findLdapUserByEmail(ServiceExecutor executor, String domain, String email)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("email", email);
		return executor.execute("unimelb.user.search", dm.root());
	}
	
	public static XmlDoc.Element findLdapUserByEmployeeID(ServiceExecutor executor, String domain, String employeeID)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("employee-id", employeeID);
		return executor.execute("unimelb.user.search", dm.root());
	}


}
