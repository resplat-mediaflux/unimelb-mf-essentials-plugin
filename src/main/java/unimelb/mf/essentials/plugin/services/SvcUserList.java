package unimelb.mf.essentials.plugin.services;

//import java.awt.dnd.DnDConstants;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcUserList extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.list";

	private Interface _defn;

	public SvcUserList() {

		_defn = new Interface();

		Interface.Element role = new Interface.Element("role", new StringType(128),
				"Only users holding this role will be included.", 0, Integer.MAX_VALUE);
		role.add(new Interface.Attribute("type", new StringType(64), "Role type. Defaults to role", 0));
		_defn.add(role);

		Interface.Element domain = new Interface.Element("domain", StringType.DEFAULT, "Authentication domain filter.",
				0, Integer.MAX_VALUE);
		domain.add(new Interface.Attribute("action", new EnumType(new String[] { "include", "exclude" }),
				"Include or exclude? Defaults to include.", 0));
		_defn.add(domain);

		_defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
				"Include disabled users. Defaults to false.", 0, 1));

		_defn.add(new Interface.Element("include-non-existent", BooleanType.DEFAULT,
				"Include non-existent users. For instance the users no longer exist in LDAP domain. Defaults to false.",
				0, 1));

		Interface.Element emailSuffix = new Interface.Element("email-suffix", StringType.DEFAULT,
				"Email suffix filter.", 0, Integer.MAX_VALUE);
		emailSuffix.add(new Interface.Attribute("action", new EnumType(new String[] { "include", "exclude" }),
				"Include or exclude? Defaults to include.", 0));
		_defn.add(emailSuffix);

	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public String description() {
		return "List users with the specified roles.";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		Set<String> incDomains = new HashSet<String>();
		Set<String> excDomains = new HashSet<String>();
		List<XmlDoc.Element> des = args.elements("domain");
		if (des != null) {
			for (XmlDoc.Element de : des) {
				String domain = de.value();
				String action = de.stringValue("@action", "include");
				if ("include".equalsIgnoreCase(action)) {
					incDomains.add(domain);
				} else {
					excDomains.add(domain);
				}
			}
		}

		Set<String> incEmailSuffixes = new HashSet<String>();
		Set<String> excEmailSuffixes = new HashSet<String>();
		List<XmlDoc.Element> eses = args.elements("email-suffix");
		if (eses != null) {
			for (XmlDoc.Element ese : eses) {
				String emailSuffix = ese.value();
				String action = ese.stringValue("@action", "include");
				if ("include".equalsIgnoreCase(action)) {
					incEmailSuffixes.add(emailSuffix);
				} else {
					excEmailSuffixes.add(emailSuffix);
				}
			}
		}

		boolean includeDisabled = args.booleanValue("include-disabled", false);
		boolean includeNonExistent = args.booleanValue("include-non-existent", false);

		XmlDocMaker dm = new XmlDocMaker("args");
		List<XmlDoc.Element> res = args.elements("role");
		if (res != null) {
			for (XmlDoc.Element re : res) {
				String role = re.value();
				String type = re.stringValue("@type", "role");
				dm.add("role", new String[] { "type", type }, role);
			}
		}
		dm.add("size", "infinity");
		List<XmlDoc.Element> ues = executor().execute("user.describe", dm.root()).elements("user");

		int count = 0;
		if (ues != null) {
			for (XmlDoc.Element ue : ues) {

				String id = ue.value("@id");
				String domain = ue.value("@domain");
				String authority = ue.value("@authority");
				String protocol = ue.value("@protocol");
				String user = ue.value("@user");
				String name = ue.value("name");
				String email = ue.value("e-mail");

				PluginTask.checkIfThreadTaskAborted();
				boolean exists = userExists(executor(), domain, user);
				if (!exists && !includeNonExistent) {
					// exclude user no longer exists (in LDAP)
					continue;
				}

				PluginTask.checkIfThreadTaskAborted();
				boolean enabled = userEnabled(executor(), domain, user);
				if (!enabled && !includeDisabled) {
					// exclude disabled user
					continue;
				}

				if (!incDomains.isEmpty() && !incDomains.contains(domain)) {
					continue;
				}

				if (!excDomains.isEmpty() && excDomains.contains(domain)) {
					continue;
				}

				if (email != null) {
					if (!incEmailSuffixes.isEmpty() && !matchesEmailSuffixes(email, incEmailSuffixes)) {
						continue;
					}
					if (!excEmailSuffixes.isEmpty() && matchesEmailSuffixes(email, excEmailSuffixes)) {
						continue;
					}
				}

				count++;
				w.add("user", new String[] { "id", id, "name", name, "email", email, "authority", authority, "protocol",
						protocol, "domain", domain }, user);
			}
		}
		w.add("size", count);
	}

	private static boolean userExists(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("authentication.user.exists", dm.root()).booleanValue("exists");
	}

	private static boolean userEnabled(ServiceExecutor executor, String domain, String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		dm.add("user", user);
		return executor.execute("authentication.user.enabled", dm.root()).booleanValue("enabled");
	}

	private static boolean matchesEmailSuffixes(String email, Collection<String> emailSuffixes) {
		for (String emailSuffix : emailSuffixes) {
			if (email.toLowerCase().endsWith(emailSuffix.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

}
