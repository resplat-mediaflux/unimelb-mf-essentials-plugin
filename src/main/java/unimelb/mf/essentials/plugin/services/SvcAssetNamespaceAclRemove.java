package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;
import java.util.List;

public class SvcAssetNamespaceAclRemove extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.namespace.acl.remove";

    private final Interface _defn;

    public SvcAssetNamespaceAclRemove() {
        _defn = new Interface();
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT,
                "The asset namespace path.",
                1, 1));
        _defn.add(new Interface.Element("recursive", BooleanType.DEFAULT,
                "Recursively remove ACLs from children namespaces. Defaults to false",
                0, 1));
        _defn.add(new Interface.Element("assets", BooleanType.DEFAULT,
                "Recursively remove ACLs from children assets. Defaults to false.",
                0, 1));
    }

    private static void revokeAllNamespaceAcls(ServiceExecutor executor, String namespacePath, boolean recursive) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", namespacePath);
        PluginTask.checkIfThreadTaskAborted();
        List<XmlDoc.Element> actorElements = executor.execute("asset.namespace.acl.describe", dm.root())
                .elements("acl/actor");
        if (actorElements != null) {
            for (XmlDoc.Element ae : actorElements) {
                String actorType = ae.value("@type");
                String actorName = ae.value();
                dm = new XmlDocMaker("args");
                dm.add("namespace", namespacePath);
                dm.add("actor", new String[]{"type", actorType}, actorName);
                PluginTask.checkIfThreadTaskAborted();
                executor.execute("asset.namespace.acl.revoke", dm.root());
            }
        }
        if (recursive) {
            dm = new XmlDocMaker("args");
            dm.add("namespace", namespacePath);
            PluginTask.checkIfThreadTaskAborted();
            Collection<String> subNamespaces = executor.execute("asset.namespace.list", dm.root())
                    .values("namespace/namespace");
            if (subNamespaces != null) {
                for (String subNamespace : subNamespaces) {
                    revokeAllNamespaceAcls(executor, namespacePath + "/" + subNamespace, true);
                }
            }
        }
    }

    private static void revokeAllAssetAcls(ServiceExecutor executor, String namespacePath) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", "namespace>='" + namespacePath + "'");
        dm.add("action", "pipe");
        dm.add("service", new String[]{"name", "asset.acl.remove"});
        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.query", dm.root());
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Remove all ACLs from the specified asset namespace.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String namespacePath = args.value("namespace");
        boolean recursive = args.booleanValue("recursive");
        boolean assets = args.booleanValue("assets");
        revokeAllNamespaceAcls(executor(), namespacePath, recursive);
        if(assets) {
            revokeAllAssetAcls(executor(), namespacePath);
        }
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }
}
