package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.utils.XmlUtils;

public class SvcAssetContentChecksumExport extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.checksum.export";

	public static final int DEFAULT_PAGE_SIZE = 100;

	public static final int DEFAULT_NB_THREADS = 1;

	public static final int ASSET_DOWNLOAD_URL_EXPIRE_IN_DAYS = 3;

	private Interface _defn;

	public SvcAssetContentChecksumExport() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Select query.", 1, 1));
		_defn.add(new Interface.Element("algorithm",
				new EnumType(
						new String[] { "crc32", "md5", "sha-1", "sha-256", "sha-384", "sha-512", "aws-tree-sha-256" }),
				"The algorithm to use. Defaults to crc32", 0, 1));
		_defn.add(new Interface.Element("size", IntegerType.POSITIVE_ONE,
				"Query page size. Defaults to " + DEFAULT_PAGE_SIZE, 0, 1));

		_defn.add(new Interface.Element("nb-threads", new IntegerType(1, 8),
				"Number of piped threads. Defaults to " + DEFAULT_NB_THREADS, 0, 1));

		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Email recipients of the exported CSV file.",
				0, 16));

		_defn.add(new Interface.Element("compress", BooleanType.DEFAULT,
				"Compress the output file to .gz format. Defaults to false.", 0, 1));

		_defn.add(new Interface.Element("asset", AssetType.DEFAULT, "The asset to save the output csv.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Generate asset content checksums and export them as CSV file.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs outputs, XmlWriter w) throws Throwable {
		final String where = args.value("where");
		final String algorithm = args.stringValue("algorithm", "crc32");
		final int pageSize = args.intValue("size", DEFAULT_PAGE_SIZE);
		final int nbThreads = args.intValue("nb-threads", DEFAULT_NB_THREADS);
		Collection<String> emails = args.values("email");
		boolean compress = args.booleanValue("compress", false);
		String assetId = args.value("asset");
		if (assetId != null) {
			if (assetId.toLowerCase().endsWith(".csv")) {
				compress = false;
			} else if (assetId.toLowerCase().endsWith(".gz")) {
				compress = true;
			}
		}

		if ((emails == null || emails.isEmpty()) && (outputs == null || outputs.size() == 0) && assetId == null) {
			throw new IllegalArgumentException("Expects one of arguments: asset, email or service output. Found none.");
		}

		final String currentTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		final String outputFileExt = compress ? "csv.gz" : "csv";
		final String outputFileType = compress ? "application/x-gzip" : "text/csv";
		final String outputFileName = String.format("asset-content-checksum_%s_%s.%s", algorithm, currentTime,
				outputFileExt);

		File f = PluginTask.createTemporaryFile(outputFileName);
		try {
			try (FileOutputStream fos = new FileOutputStream(f);
					BufferedOutputStream bos = new BufferedOutputStream(compress ? new GZIPOutputStream(fos) : fos);
					OutputStreamWriter osw = new OutputStreamWriter(bos);
					PrintWriter csv = new PrintWriter(osw)) {
				csv.printf("ID,VERSION,PATH,SIZE,CHECKSUM(%s),\n", algorithm.toUpperCase());
				if ("crc32".equalsIgnoreCase(algorithm)) {
					exportCRC32(executor(), where, pageSize, csv);
				} else {
					exportChecksum(executor(), where, pageSize, nbThreads, algorithm, csv);
				}
			}
		} catch (Throwable e) {
			// delete temp file in case of runtime exception (aborted).
			PluginTask.deleteTemporaryFile(f);
			throw e;
		}

// @formatter:off
//        final PipedInputStream pis = new PipedInputStream();
//        final PipedOutputStream pos = new PipedOutputStream(pis);
//        PluginThread.executeAsync(SERVICE_NAME, new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    try {
//                        try (PrintWriter w = new PrintWriter(
//                                new BufferedWriter(new OutputStreamWriter(pos, "utf-8")))) {
//                            w.printf("ID,VERSION,PATH,SIZE,CHECKSUM(%s),\n", algorithm.toUpperCase());
//                            if ("crc32".equalsIgnoreCase(algorithm)) {
//                                exportCRC32(executor(), where, pageSize, w);
//                            } else {
//                                exportChecksum(executor(), where, pageSize, nbThreads, algorithm, w);
//                            }
//                        }
//                    } finally {
//                        pos.close();
//                        // NOTE: No need to close pis (If do that, it will cause
//                        // problem: corrupt archive).
//                    }
//                } catch (Throwable e) {
//                    e.printStackTrace(System.out);
//                }
//            }
//        });
//        outputs.output(0).setData(pis, -1, "text/csv");
// @formatter:on

		if (assetId != null) {
			saveToAsset(executor(), assetId, outputFileName, f, outputFileType);
			if (emails != null && !emails.isEmpty()) {
				sendNotification(executor(), assetId, outputFileName, emails);
			}
		} else {
			if (emails != null && !emails.isEmpty()) {
				PluginTask.checkIfThreadTaskAborted();
				PluginTask.setCurrentThreadActivity("emailing exported csv file...");
				sendEmail(executor(), emails, args, outputFileName, f, outputFileType);
			}
		}

		if (outputs != null && outputs.size() > 0) {
			outputs.output(0).setData(PluginTask.deleteOnCloseInputStream(f), f.length(), outputFileType);
		} else {
			PluginTask.deleteTemporaryFile(f);
		}
	}

	private static void sendEmail(ServiceExecutor executor, Collection<String> emails, XmlDoc.Element args,
			String outputFileName, File f, String outputFileType) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		for (String email : emails) {
			dm.add("to", email);
		}
		dm.add("subject", emailSubject(executor));
		dm.add("body", emailBody(executor, args));
		dm.push("attachment");
		dm.add("name", outputFileName);
		dm.add("type", outputFileType);
		dm.pop();
		try (InputStream is = new BufferedInputStream(new FileInputStream(f))) {
			PluginService.Input in = new PluginService.Input(is, f.length(), outputFileType, null);
			try {
				executor.execute("mail.send", dm.root(), new PluginService.Inputs(in), null);
			} finally {
				in.close();
			}
		}
	}

	private static String emailSubject(ServiceExecutor executor) throws Throwable {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(executor.execute("server.uuid").value("uuid"));
		sb.append("] ");
		sb.append(SERVICE_NAME);
		return sb.toString();
	}

	private static String emailBody(ServiceExecutor executor, XmlDoc.Element args) throws Throwable {
		StringBuilder sb = new StringBuilder();
		sb.append(SERVICE_NAME).append("\n");
		XmlUtils.saveIndentedText(args, false, sb, 4, 4);
		return sb.toString();
	}

	private static void exportCRC32(ServiceExecutor executor, String where, int pageSize, PrintWriter w)
			throws Throwable {
		long idx = 1;
		long remaining = Long.MAX_VALUE;
		long total = -1;

		while (remaining > 0) {
			PluginTask.checkIfThreadTaskAborted();
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("where", "(" + where + ") and (asset has content)");
			dm.add("action", "get-value");
			dm.add("xpath", new String[] { "ename", "id" }, "id");
			dm.add("xpath", new String[] { "ename", "path" }, "path");
			dm.add("xpath", new String[] { "ename", "version" }, "version");			
			dm.add("xpath", new String[] { "ename", "csize" }, "content/size");
			dm.add("xpath", new String[] { "ename", "csum" }, "content/csum[@base='16']");
			dm.add("count", true);
			dm.add("idx", idx);
			dm.add("size", pageSize);
			// These PluginTask functions are now in aplugin.jar 4.12.019_009
			PluginTask.PluginThreadTask t = PluginTask.suppressCurrentTaskForThread();
			XmlDoc.Element re = executor.execute("asset.query", dm.root());
			PluginTask.restoreCurrentTaskForThread(t);
			remaining = re.longValue("cursor/remaining");
			idx += pageSize;
			if (total < 0) {
				total = re.longValue("cursor/total");
				PluginTask.threadTaskBeginSetOf(total);
			}
			List<XmlDoc.Element> aes = re.elements("asset");
			if (aes != null) {
				for (XmlDoc.Element ae : aes) {
					String id = ae.value("id");
					String version = ae.value("version");
					String path = ae.value("path");
					String size = ae.value("csize");
					String csum = ae.value("csum");
					if (csum == null) {
						csum = calcCRC32(executor, id);
					}
					if (csum != null) {
						csum = csum.toLowerCase();
					}
					w.printf("%s,%s,\"%s\",%s,%s,\n", id, version, path, size, csum);
				}
				PluginTask.threadTaskCompleted(aes.size());
			}
		}
	}

	private static String calcCRC32(ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		return executor.execute("asset.content.checksum.crc.generate", dm.root()).value("checksum").toLowerCase();
	}

	private static void exportChecksum(ServiceExecutor executor, String where, int pageSize, int nbThreads,
			String algorithm, PrintWriter w) throws Throwable {
		long idx = 1;
		long remaining = Long.MAX_VALUE;
		long total = -1;
		// long c = 0;
		while (remaining > 0) {
			PluginTask.checkIfThreadTaskAborted();

			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("where", "(" + where + ") and (asset has content)");
			dm.add("idx", idx);
			dm.add("size", pageSize);
			dm.add("action", "pipe");
			dm.add("count", true);
			dm.add("pipe-nb-threads", nbThreads);
			dm.add("pipe-generate-result-xml", true);
			dm.push("service", new String[] { "name", "asset.content.checksum.digest.generate" });
			dm.add("algorithm", algorithm.toUpperCase());
			dm.pop();
			// TODO: TBD in 4.12.024
			// PluginTask.PluginThreadTask t = PluginTask.suppressCurrentTaskForThread();
			XmlDoc.Element re = executor.execute("asset.query", dm.root());
			// PluginTask.restoreCurrentTaskForThread(t);
			remaining = re.longValue("cursor/remaining");
			idx += pageSize;
			if (total < 0) {
				total = re.longValue("cursor/total");
				// System.out.println("Setting total" + total);
				PluginTask.threadTaskBeginSetOf(total);
			}
			List<XmlDoc.Element> ces = re.elements("checksum");
			if (ces != null) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDocMaker dm2 = new XmlDocMaker("args");
				for (XmlDoc.Element ce : ces) {
					dm2.add("id", new String[] { "version", ce.value("@version") }, ce.value("@id"));
				}
				XmlDoc.Element re2 = executor.execute("asset.get", dm2.root());
				List<XmlDoc.Element> aes = re2.elements("asset");

				assert aes != null && ces.size() == aes.size();
				int n = ces.size();
				for (int i = 0; i < n; i++) {
					XmlDoc.Element ce = ces.get(i);
					XmlDoc.Element ae = aes.get(i);
					String id = ce.value("@id");
					String version = ce.value("@version");
					String csum = ce.value();
					String path = ae.value("path");
					String size = ae.value("content/size");
					w.printf("%s,%s,\"%s\",%s,%s,\n", id, version, path, size, csum);
					// PluginTask.threadTaskCompleted(1);
					// c++;
					// PluginTask.threadTaskSetCompleted(c,total);
					// System.out.println("Updating by 1");

				}
				PluginTask.threadTaskCompleted(n);
			}
		}
	}

	private static void saveToAsset(ServiceExecutor executor, String assetId, String outputFileName, File csvFile,
			String outputFileType) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", assetId);
		if (!assetId.startsWith("path=")) {
			dm.add("name", outputFileName);
		}
		dm.add("create", true);
		dm.add("ctype", outputFileType);
		try (InputStream in = new BufferedInputStream(new FileInputStream(csvFile))) {
			PluginService.Input input = new PluginService.Input(in, csvFile.length(), outputFileType, null);
			executor.execute("asset.set", dm.root(), new PluginService.Inputs(input), null);
		}
	}

	private static String createAssetDownloadUrl(ServiceExecutor executor, String assetId, String outputFileName,
			Date expiry) throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("to", expiry);

		XmlDoc.Element actor = executor.execute("actor.self.describe").element("actor");
		dm.add("role", new String[] { "type", actor.value("@type") }, actor.value("@name"));
		dm.add("role", new String[] { "type", "role" }, "user");
		dm.add("min-token-length", 20);
		dm.add("max-token-length", 20);
		dm.add("tag", SERVICE_NAME);

		dm.push("service", new String[] { "name", "asset.get" });
		dm.add("id", assetId);
		dm.pop();
		String token = executor.execute("secure.identity.token.create", dm.root()).value("token");

		String serverUrl = Properties.getServerProperty(executor, "asset.shareable.address");
		String downloadUrl = String.format("%s/mflux/execute.mfjp?&token=%s&filename=%s", serverUrl, token,
				outputFileName);
		return downloadUrl;
	}

	private static void sendNotification(ServiceExecutor executor, String assetId, String outputFileName,
			Collection<String> emails) throws Throwable {
		XmlDoc.Element ae = AssetUtils.getAsset(executor, null, null, assetId).element("asset");
		String id = ae.value("@id");
		String path = ae.value("path");
		String csize = ae.value("content/size");

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, ASSET_DOWNLOAD_URL_EXPIRE_IN_DAYS);
		Date expiry = c.getTime();
		String downloadUrl = createAssetDownloadUrl(executor, assetId, outputFileName, expiry);

		String subject = String.format("[%s] %s: result is available", executor.execute("server.uuid").value("uuid"),
				SERVICE_NAME);
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		sb.append("<li>").append("<b>Asset ID:</b> ").append(id).append("</li>");
		sb.append("<li>").append("<b>Asset Path:</b> ").append(path).append("</li>");
		sb.append("<li>").append("<b>File Size:</b> ").append(csize).append(" bytes</li>");
		sb.append("<li>").append("<b>Download URL (expire at: ").append(DateTime.string(expiry))
				.append("):</b> <a href=\"").append(downloadUrl).append("\">").append(downloadUrl).append("</a></li>");

		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("subject", subject);
		for (String email : emails) {
			dm.add("to", email);
		}
		dm.add("body", new String[] { "type", "text/html" }, sb.toString());
		executor.execute("mail.send", dm.root());
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public int maxNumberOfOutputs() {
		return 1;
	}

	public int minNumberOfOutputs() {
		return 0;
	}

	public boolean canBeAborted() {
		return true;
	}
}
