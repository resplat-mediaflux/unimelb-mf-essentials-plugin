package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;
import java.util.Date;

public class SvcDataMoverTokenInstallerScriptUrlGet extends PluginService {

	public static final String EXECUTE_SERVLET_PATH = "/mflux/execute.mfjp";

	public static final String SERVICE_NAME = "unimelb.data.mover.token.installer.script.url.get";

	public static final String SECURE_IDENTITY_TOKEN_TAG = "DATA_MOVER_TOKEN_INSTALLER_SCRIPT_GET";

	public static final long MILLISECS_A_WEEK = 604800000L;

	private final Interface _defn;

	public SvcDataMoverTokenInstallerScriptUrlGet() {
		_defn = new Interface();
		_defn.add(new Interface.Element("shareable-token", StringType.DEFAULT, "The shareable token to be installed.",
				0, 1));
		_defn.add(new Interface.Element("shareable-id", StringType.DEFAULT, "The shareable id.", 0, 1));
		_defn.add(new Interface.Element("expiry", DateType.DATE_ONLY, "When the url expires. Defaults to 'now+7day'.",
				0, 1));
		Interface.Element email = new Interface.Element("email", XmlDocType.DEFAULT,
				"Email the url to specified recipients.", 0, 1);
		email.add(new Interface.Element("to", EmailAddressType.DEFAULT, "Recipient email address.", 1, 10));
		email.add(new Interface.Element("subject-suffix", StringType.DEFAULT, "Suffix to the email subject.", 0, 1));
		_defn.add(email);

	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public String description() {
		return "Generate download link to the data mover token installer script.";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		Date expiry = args.dateValue("expiry", new Date(System.currentTimeMillis() + MILLISECS_A_WEEK));
		String shareableToken = args.value("shareable-token");
		String shareableId = args.value("shareable-id");
		if (shareableId == null && shareableToken == null) {
			throw new IllegalArgumentException("Either shareable-id or shareable-token must be specified. Found none.");
		}
		if (shareableId != null && shareableToken != null) {
			throw new IllegalArgumentException("Either shareable-id or shareable-token must be specified. Found both.");
		}
		if (shareableToken == null) {
			shareableToken = executor()
					.execute("asset.shareable.describe",
							"<args><id>" + shareableId + "</id><get-token>true</get-token></args>", null, null)
					.value("shareable/token");
		} else {
			shareableId = executor().execute("asset.shareable.describe",
					"<args><token>" + shareableToken + "</token></args>", null, null).value("shareable/@id");
		}

		String secureIdentityToken = createSecureIdentityToken(executor(), shareableToken, expiry);

		String fileName = fileNameFor(shareableId);

		String url = urlFor(executor(), secureIdentityToken, fileName);

		w.add("url", new String[] { "expiry", DateTime.string(expiry) }, url);

		Collection<String> emailTo = args.values("email/to");
		String subjectSuffix = args.value("email/subject-suffix");
		String subject = subjectFor(shareableId, subjectSuffix);
		if (emailTo != null && !emailTo.isEmpty()) {
			sendEmails(executor(), url, expiry, emailTo, subject);
			for (String email : emailTo) {
				w.add("sent-to", email);
			}
		}

	}

	private static String fileNameFor(String shareableId) {
		return String.format("mediaflux-data-mover-token-%s-installer.zip", shareableId);
	}

	private static String urlFor(ServiceExecutor executor, String secureIdentityToken, String fileName)
			throws Throwable {
		PluginTask.checkIfThreadTaskAborted();
		String serverUrl = executor
				.execute("server.property.get", "<args><name>asset.shareable.address</name></args>", null, null)
				.value("property");
		if (serverUrl == null) {
			throw new Exception("Cannot resolve server property: asset.shareable.address");
		}
		while (serverUrl.endsWith("/")) {
			serverUrl = serverUrl.substring(0, serverUrl.length() - 1);
		}
		return String.format("%s%s?token=%s&filename=%s", serverUrl, EXECUTE_SERVLET_PATH, secureIdentityToken,
				fileName);
	}

	private static String subjectFor(String shareableId, String suffix) {
		StringBuilder sb = new StringBuilder();
		sb.append("Mediaflux Data Mover Token ").append(shareableId).append(" Installer Script");
		if (suffix != null) {
			if (!suffix.startsWith(" ")) {
				sb.append(" ");
			}
			sb.append(suffix);
		}
		return sb.toString();
	}

	private static void sendEmails(ServiceExecutor executor, String url, Date expiry, Collection<String> emailTo,
			String subject) throws Throwable {
		if (emailTo != null && !emailTo.isEmpty()) {
			String msg = new StringBuilder("Hi,\n\n").append(
					"The Mediaflux Data Mover token installer script can be downloaded via the link below: \n\n")
					.append("    ").append(url).append("\n\n").append("The above link will expire at: ")
					.append(DateTime.string(expiry)).append("\n\n").toString();
			for (String email : emailTo) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("to", email);
				dm.add("subject", subject);
				dm.add("body", msg);
				PluginTask.checkIfThreadTaskAborted();
				executor.execute("mail.send", dm.root());
			}
		}
	}

	private static String createSecureIdentityToken(ServiceExecutor executor, String shareableToken, Date expiry)
			throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (expiry != null) {
			dm.add("to", expiry);
		}

		dm.add("tag", SECURE_IDENTITY_TOKEN_TAG);

		dm.add("role", new String[] { "type", "role" }, "user");

		dm.push("perm");
		dm.add("resource", new String[] { "type", "service" }, SvcDataMoverTokenInstallerScriptGet.SERVICE_NAME);
		dm.add("access", "ACCESS");
		dm.pop();

		dm.add("min-token-length", 20);
		dm.add("max-token-length", 20);

		dm.push("service", new String[] { "name", SvcDataMoverTokenInstallerScriptGet.SERVICE_NAME });
		dm.add("shareable-token", shareableToken);
		dm.pop();
		return executor.execute("secure.identity.token.create", dm.root()).value("token");
	}

}
