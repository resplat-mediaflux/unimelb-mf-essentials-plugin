package unimelb.mf.essentials.plugin.services;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcServerCertificateExpiryNotify extends PluginService {

	public static final long MILLISECONDS_PER_DAY = 86400000L;
	public static final long MILLISECONDS_PER_WEEK = MILLISECONDS_PER_DAY * 7L;
	public static final long MILLISECONDS_PER_MONTH = MILLISECONDS_PER_DAY * 30L;

	public static final String SERVICE_NAME = "unimelb.server.certificate.expiry.notify";

	private Interface _defn;

	public SvcServerCertificateExpiryNotify() {
		_defn = new Interface();
		Interface.Element before = new Interface.Element("before", IntegerType.POSITIVE_ONE,
				" The time before server certificate expiry to send notification. Defaults to 30 days.", 0, 1);
		before.add(new Interface.Attribute("unit", new EnumType(new String[] { "day", "week", "month" }),
				"Time unit. Defaults to day.", 0));
		_defn.add(before);

		_defn.add(new Interface.Element("to", EmailAddressType.DEFAULT, "Recipients.", 1, 255));

	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Check the expiry of the default server certificate and send notifications.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		long before = args.longValue("before", 30);
		String unit = args.stringValue("before/@unit", "day");
		Collection<String> to = args.values("to");

		long beforeMillis = 0;
		if ("month".equalsIgnoreCase(unit)) {
			beforeMillis = before * MILLISECONDS_PER_MONTH;
		} else if ("week".equalsIgnoreCase(unit)) {
			beforeMillis = before * MILLISECONDS_PER_WEEK;
		} else {
			beforeMillis = before * MILLISECONDS_PER_DAY;
		}

		Date expiry = getServerCertificateExpiry(executor());
		boolean notified = false;
		if (expiry != null) {
			Date now = executor().execute("server.date").dateValue("date");
			if (expiry.getTime() <= now.getTime() + beforeMillis) {
				notify(executor(), to, expiry, now, beforeMillis);
				notified = true;
			}
		}
		w.add("notified",
				new String[] { "expiry",
						expiry == null ? null : (new SimpleDateFormat(DateTime.DATE_FORMAT).format(expiry)) },
				notified);
	}

	private void notify(ServiceExecutor executor, Collection<String> recipients, Date expiry, Date now,
			long beforeMillis) throws Throwable {
		if (now.getTime() + beforeMillis >= expiry.getTime()) {
			String subject = null;
			String body = null;
			String serverUUID = executor().execute("server.uuid").value("uuid");
			if (now.getTime() >= expiry.getTime()) {
				subject = String.format("[%s] WARNING: Mediaflux server certificate expired [%s]", serverUUID,
						new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(expiry));
				body = String.format("\n[%s] Mediaflux server certificate expired at %s\n", serverUUID,
						new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(expiry));
			} else {
				long diffDays = (expiry.getTime() - now.getTime()) / MILLISECONDS_PER_DAY;
				subject = String.format("[%s] WARNING: Mediaflux server certificate will expire in %d days", serverUUID,
						diffDays);
				body = String.format("\n[%s] Mediaflux server certificate will expire in %d days at %s\n", serverUUID,
						diffDays, new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(expiry));
			}
			XmlDocMaker dm = new XmlDocMaker("args");
			for (String to : recipients) {
				dm.add("to", to);
			}
			dm.add("subject", subject);
			dm.add("body", body);
			executor().execute("mail.send", dm.root());
		}
	}

	static Date getServerCertificateExpiry(ServiceExecutor executor) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", "server.default.certificate.alias");
		long alias = executor.execute("server.property.get", dm.root()).longValue("property", 0);

		XmlDoc.Element re = executor.execute("server.certificate.identity.describe");
		int n = re.count("identity");
		if (n > 0) {
			XmlDoc.Element ie = alias > 0 ? re.element("identity[@id='" + alias + "']") : re.elementAt(n - 1);
			List<Date> toDates = ie.dateValues("certificate/validity/to");
			if (toDates != null) {
				Collections.sort(toDates);
				Date toDate = toDates.get(0);
				return toDate;
			}
		}
		return null;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
