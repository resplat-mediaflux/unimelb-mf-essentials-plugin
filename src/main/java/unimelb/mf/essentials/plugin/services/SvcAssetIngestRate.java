package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetIngestRate extends PluginService {

	public static enum StorageUnit {
		MB, GB, TB, PB;
		// Method to convert an amount of storage in bytes into the relevant unit attached to the object instance
		public XmlDoc.Element convert(long storage, String purpose) throws Throwable{
			double divMB = 1.0e6;
			double divGB = 1.0e9;
			double divTB = 1.0e12;
			double divPB = 1.0e15;
			double quota=0;
			String[] attributes = new String[2];
			switch(this){
			case MB:
				quota = storage / divMB;
				break;
			case GB:
				quota = storage / divGB;
				break;
			case TB:
				quota = storage / divTB;
				break;
			case PB:
				quota = storage / divPB;
				break;
			}
			attributes[0] = "units";
			attributes[1] = this.toString();
			XmlDocMaker dm = new XmlDocMaker("root");
			dm.add(purpose,attributes, quota);
			return dm.root().element(purpose);
		}

		/**
		 * Convert the storage amount to a value with the current units
		 * 
		 * @param storage
		 * @return
		 * @throws Throwable
		 */
		public String convertToString (long storage) throws Throwable {
			XmlDoc.Element r = convert (storage, "value");
			String value = r.value();
			String units = r.value("@units");
			return value + " " + units;
		}

		// Sets the storage units on the object instance
		public static StorageUnit fromString(String s, StorageUnit defaultValue){
			if(s!=null){
				StorageUnit[] vs = values();
				for(int i=0;i<vs.length;i++){
					if(vs[i].name().equalsIgnoreCase(s)){
						return vs[i];
					}
				}
			}
			return defaultValue;
		}
	}

	private Interface _defn;

	public SvcAssetIngestRate() {
		_defn = new Interface();
		Interface.Element me = new Interface.Element("where", StringType.DEFAULT, "Predicate to select assets", 1, 1);
		_defn.add(me);
		me = new Interface.Element("year", StringType.DEFAULT, "The calendar year of interest (no default)", 1, Integer.MAX_VALUE);
		_defn.add(me);
		me = new Interface.Element("month", StringType.DEFAULT, "The calendar month(s) of interest (defaults to all). Use 3 letters: Jan, Feb, Mar etc", 0, 12);
		_defn.add(me);
		me = new Interface.Element("unit", StringType.DEFAULT, "The human friendly units, GB (default) or TB", 0, 1);
		_defn.add(me);
		me = new Interface.Element("show-total", BooleanType.DEFAULT, "Show the total amount of data ingested into the system up to the end of each  given month. The default is false.", 0, 1);
		_defn.add(me);
	}

	public String name() {
		return "unimelb.asset.ingest.rate";
	}

	public String description() {
		return "Specialised service to list storage data ingest rates (i.e. by looking at ctime) per month for the selected assets.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String where = args.value("where");
		Collection<String> years = args.values("year");				
		Collection<String> months = args.values("month");	
		String unit = args.stringValue("unit", "GB");
		if (!unit.equals("GB") && !unit.equals("TB")) {
			throw new Exception ("Unit must be GB or TB");
		}
		Boolean showTotal = args.booleanValue("show-total",  false);

		// Do it
		XmlDoc.Element r = ingestRate (executor(), where, years, 
				months, unit, showTotal);
		w.addAll(r.elements());

	}


	public static XmlDoc.Element  ingestRate (ServiceExecutor executor, String where, Collection<String> years, 
			Collection<String> months, String unit, Boolean showTotal) throws Throwable {
		
		XmlDocMaker dmOut = new XmlDocMaker("args");
		for (String year : years) {
			Long totalSize = 0L;
			for (int i=0;i<12;i++) {
				if (monthIncluded(i, months)) {
					PluginTask.checkIfThreadTaskAborted();
					XmlDocMaker dm = new XmlDocMaker("args");
					String where2 = "(" + where + ") and " + makeWhere(i, year);
					String my = makeMonth (i, year);
					dm.add("where", where2);
					dm.add("action", "sum");
					dm.add("size", "infinity");
					dm.add("xpath", "content/size");
					XmlDoc.Element r = executor.execute("asset.query", dm.root());
					dmOut.push("ingest");
					dmOut.add("month",  my);
					String nAssets = r.value("value/@nbe");
					String bytes = r.value("value");
					//
					Long td = 0L;
					if (bytes!=null) {
						td = Long.parseLong(bytes);
					}
					totalSize += td;
					//
					if (unit.equals("GB")) {
						dmOut.add("size", new String[]{"n", nAssets, "bytes", bytes}, bytesToGBytes (bytes));
					} else {
						dmOut.add("size", new String[]{"n", nAssets, "bytes", bytes}, bytesToTBytes (bytes));
					}

					// Total amount in system at the end of the give month
					// Because years/months may not be consecutive, we have
					// to recompuet this each time.  Very expensive!
					if (showTotal) {
						PluginTask.checkIfThreadTaskAborted();
						dm = new XmlDocMaker("args");
						where2 = "(" + where + ") and " + makeWhere2(i, year);
						dm.add("where", where2);
						dm.add("action", "sum");
						dm.add("size", "infinity");
						dm.add("xpath", "content/size");
						r = executor.execute("asset.query", dm.root());
						bytes = r.value("value");
						dmOut.add("total-size-stored-to-date", new String[] {"units", "bytes"}, bytes);
					}
					dmOut.pop();

				}
			}
			if (unit.equals("GB")) {
				dmOut.add("total-size-ingested",  bytesToGBytes(""+totalSize));
			} else {
				dmOut.add("total-size-ingested",  bytesToTBytes(""+totalSize));
			}

		}
		return dmOut.root();
	}


	private static Boolean monthIncluded (int im, Collection<String> months) {
		if (months==null) return true;
		for (String month : months) {
			if (month.equalsIgnoreCase("Jan")) {
				if (im==0) return true;
			} else if (month.equalsIgnoreCase("Feb")) {
				if (im==1) return true;
			} else if (month.equalsIgnoreCase("Mar")) {
				if (im==2) return true;
			} else if (month.equalsIgnoreCase("Apr")) {
				if (im==3) return true;
			} else if (month.equalsIgnoreCase("May")) {
				if (im==4) return true;
			} else if (month.equalsIgnoreCase("Jun")) {
				if (im==5) return true;
			} else if (month.equalsIgnoreCase("Jul")) {
				if (im==6) return true;
			} else if (month.equalsIgnoreCase("Aug")) {
				if (im==7) return true;
			} else if (month.equalsIgnoreCase("Sep")) {
				if (im==8) return true;
			} else if (month.equalsIgnoreCase("Oct")) {
				if (im==9) return true;
			} else if (month.equalsIgnoreCase("Nov")) {
				if (im==10) return true;
			} else if (month.equalsIgnoreCase("Dec")) {
				if (im==11) return true;
			} 
		}
		return false;
	}
	private static String makeWhere (int i, String year) throws Throwable {
		if (i==0) {
			return "ctime>='01-Jan-" + year + " 00:00:00' and ctime<='31-Jan-" + year + " 24:00:00'";
		} else if (i==1) {
			return "ctime>='01-Feb-" + year + " 00:00:00' and ctime<='29-Feb-" + year + " 24:00:00'";    // Allow for leap year
		} else if (i==2) {
			return "ctime>='01-Mar-" + year + " 00:00:00' and ctime<='31-Mar-" + year + " 24:00:00'";
		} else if (i==3) {
			return "ctime>='01-Apr-" + year + " 00:00:00' and ctime<='30-Apr-" + year + " 24:00:00'";
		} else if (i==4) {
			return "ctime>='01-May-" + year + " 00:00:00' and ctime<='31-May-" + year + " 24:00:00'";
		} else if (i==5) {
			return "ctime>='01-Jun-" + year + " 00:00:00' and ctime<='30-Jun-" + year + " 24:00:00'";
		} else if (i==6) {
			return "ctime>='01-Jul-" + year + " 00:00:00' and ctime<='31-Jul-" + year + " 24:00:00'";
		} else if (i==7) {
			return "ctime>='01-Aug-" + year + " 00:00:00' and ctime<='31-Aug-" + year + " 24:00:00'";
		} else if (i==8) {
			return "ctime>='01-Sep-" + year + " 00:00:00' and ctime<='30-Sep-" + year + " 24:00:00'";
		} else if (i==9) {
			return "ctime>='01-Oct-" + year + " 00:00:00' and ctime<='31-Oct-" + year + " 24:00:00'";
		} else if (i==10) {
			return "ctime>='01-Nov-" + year + " 00:00:00' and ctime<='30-Nov-" + year + " 24:00:00'";
		} else if (i==11) {
			return "ctime>='01-Dec-" + year + " 00:00:00' and ctime<='31-Dec-" + year + " 24:00:00'";
		} else {
			return null;
		}
	}

	private static String makeWhere2 (int i, String year) throws Throwable {
		if (i==0) {
			return "ctime<='31-Jan-" + year + " 24:00:00'";
		} else if (i==1) {
			return "ctime<='29-Feb-" + year + " 24:00:00'";    // Allow for leap year
		} else if (i==2) {
			return "ctime<='31-Mar-" + year + " 24:00:00'";
		} else if (i==3) {
			return "ctime<='30-Apr-" + year + " 24:00:00'";
		} else if (i==4) {
			return "ctime<='31-May-" + year + " 24:00:00'";
		} else if (i==5) {
			return "ctime<='30-Jun-" + year + " 24:00:00'";
		} else if (i==6) {
			return "ctime<='31-Jul-" + year + " 24:00:00'";
		} else if (i==7) {
			return "ctime<='31-Aug-" + year + " 24:00:00'";
		} else if (i==8) {
			return "ctime<='30-Sep-" + year + " 24:00:00'";
		} else if (i==9) {
			return "ctime<='31-Oct-" + year + " 24:00:00'";
		} else if (i==10) {
			return "ctime<='30-Nov-" + year + " 24:00:00'";
		} else if (i==11) {
			return "ctime<='31-Dec-" + year + " 24:00:00'";
		} else {
			return null;
		}
	}
	
	private static String makeWhere3 (int i, String year) throws Throwable {
		if (i==0) {
			return "ctime<'01-Jan-" + year + " 00:00:00'";
		} else if (i==1) {
			return "ctime<'01-Feb-" + year + " 00:00:00'";    // Allow for leap year
		} else if (i==2) {
			return "ctime<'01-Mar-" + year + " 00:00:00'";
		} else if (i==3) {
			return "ctime<'01-Apr-" + year + " 00:00:00'";
		} else if (i==4) {
			return "ctime<'01-May-" + year + " 00:00:00'";
		} else if (i==5) {
			return "ctime<'01-Jun-" + year + " 00:00:00'";
		} else if (i==6) {
			return "ctime<'01-Jul-" + year + " 00:00:00'";
		} else if (i==7) {
			return "ctime<'01-Aug-" + year + " 00:00:00'";
		} else if (i==8) {
			return "ctime<'01-Sep-" + year + " 00:00:00'";
		} else if (i==9) {
			return "ctime<'01-Oct-" + year + " 00:00:00'";
		} else if (i==10) {
			return "ctime<'01-Nov-" + year + " 00:00:00'";
		} else if (i==11) {
			return "ctime<'01-Dec-" + year + " 24:00:00'";
		} else {
			return null;
		}
	}


	private static String makeMonth (int i, String year) throws Throwable {
		if (i==0) {
			return "Jan-" + year;
		} else if (i==1) {
			return "Feb-" + year;
		} else if (i==2) {
			return "Mar-" + year;
		} else if (i==3) {
			return "Apr-" + year;
		} else if (i==4) {
			return "May-" + year;
		} else if (i==5) {
			return "Jun-" + year;
		} else if (i==6) {
			return "Jul-" + year;
		} else if (i==7) {
			return "Aug-" + year;
		} else if (i==8) {
			return "Sep-" + year;
		} else if (i==9) {
			return "Oct-" + year;
		} else if (i==10) {
			return "Nov-" + year;
		} else if (i==11) {
			return "Dec-" + year;
		} else {
			return null;
		}
	}


	private static String bytesToGBytes (String bytes) throws Throwable {
		if (bytes==null) return "0 GB";
		Long s = Long.parseLong(bytes);
		StorageUnit su = StorageUnit.fromString("GB", StorageUnit.GB); 
		return su.convertToString(s);
	}

	private static String bytesToTBytes (String bytes) throws Throwable {
		if (bytes==null) return "0 TB";
		Long s = Long.parseLong(bytes);
		StorageUnit su = StorageUnit.fromString("TB", StorageUnit.TB); 
		return su.convertToString(s);
	}

}
