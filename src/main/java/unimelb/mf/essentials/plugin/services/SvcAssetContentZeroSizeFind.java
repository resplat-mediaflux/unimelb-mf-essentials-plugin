package unimelb.mf.essentials.plugin.services;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.IDUtils;
import unimelb.utils.XmlUtils;
import unimelb.xml.CollectionXmlWriter;
import unimelb.xml.IndentedStringBuilder;

public class SvcAssetContentZeroSizeFind extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.content.zero-size.find";

    public static final int DEFAULT_BATCH_SIZE = 1000;

    private Interface _defn;

    public SvcAssetContentZeroSizeFind() {
        _defn = new Interface();
        _defn.add(new Interface.Element("where", StringType.DEFAULT, "Selection query.", 0, 1));
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace to check.", 0,
                Integer.MAX_VALUE));
        _defn.add(new Interface.Element("action", new EnumType(new String[] { "count", "list" }),
                "Action to apply. Defaults to count.", 0, 1));
        Interface.Element countByExts = new Interface.Element("count-by-extensions", BooleanType.DEFAULT,
                "Count by file extensions. Defaults to false.", 0, 1);
        countByExts
                .add(new Interface.Attribute("ignore-case", BooleanType.DEFAULT, "Ignore case. Defaults to true.", 0));
        _defn.add(countByExts);
        _defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT,
                "Include assets that have been marked as destroyed? Defaults to false.", 0, 1));
        _defn.add(new Interface.Element("batch-size", IntegerType.POSITIVE_ONE,
                "Batch size. Defaults to " + DEFAULT_BATCH_SIZE, 0, 1));

        Interface.Element notify = new Interface.Element("notify", XmlDocType.DEFAULT, "Send email notifications.");
        notify.add(new Interface.Attribute("always", BooleanType.DEFAULT,
                "Always send notifications even if no zero-size file found. "));
        notify.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Recipient emails.", 1, 10));
        _defn.add(notify);
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find assets with zero-size content.";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

        IndentedStringBuilder isb = new IndentedStringBuilder(4, 64000);
        XmlWriter xw = args.elementExists("notify") ? new CollectionXmlWriter(w, isb) : w;

        String where = args.value("where");
        Collection<String> namespaces = args.values("namespace");
        Boolean includeDestroyed = args.booleanValue("include-destroyed", null);
        String action = args.stringValue("action", "count");
        boolean countByExtensions = args.booleanValue("count-by-extensions", false);
        boolean ignoreCase = args.booleanValue("count-by-extensions/@ignore-case", true);
        int batchSize = args.intValue("batch-size", DEFAULT_BATCH_SIZE);

        StringBuilder sb = new StringBuilder("(asset has content and csize=0)");
        if (where != null || (namespaces != null && !namespaces.isEmpty())) {
            sb.append(" and (");
        }

        if (where != null) {
            sb.append("(").append(where).append(")");
        }

        if (namespaces != null && !namespaces.isEmpty()) {
            boolean first = true;
            for (String namespace : namespaces) {
                if (first) {
                    first = false;
                    if (where != null) {
                        sb.append(" or ");
                    }
                } else {
                    sb.append(" or ");
                }
                sb.append("namespace>='").append(namespace).append("'");
            }
        }

        if (where != null || (namespaces != null && !namespaces.isEmpty())) {
            sb.append(")");
        }

        long total = 0L;

        if ("count".equalsIgnoreCase(action) && !countByExtensions) {
            /*
             * count only the total number of zero-sized files.
             */
            XmlDocMaker dm = new XmlDocMaker("args");

            dm.add("where", sb.toString());
            if (includeDestroyed != null) {
                dm.add("include-destroyed", includeDestroyed);
            }
            dm.add("action", "count");
            XmlDoc.Element re = executor().execute("asset.query", dm.root());
            total = re.longValue("value");
            xw.add("total", total);
        } else {
            /*
             * count by extensions
             */
            Map<String, Long> countByExts = countByExtensions ? new TreeMap<String, Long>() : null;
            Long countNoExt = 0L;
            long idx = 1L;
            boolean complete = false;
            while (!complete) {

                PluginTask.checkIfThreadTaskAborted();

                XmlDocMaker dm = new XmlDocMaker("args");
                dm.add("where", sb.toString());
                if (includeDestroyed != null) {
                    dm.add("include-destroyed", includeDestroyed);
                }
                dm.add("action", "get-path");
                dm.add("idx", idx);
                dm.add("size", batchSize);
                XmlDoc.Element re = executor().execute("asset.query", dm.root());
                List<XmlDoc.Element> pes = re.elements("path");
                if (pes != null) {
                    for (XmlDoc.Element pe : pes) {
                        if (countByExtensions) {
                            String path = pe.value();
                            String ext = getFileExtension(path);
                            if (ext != null && !ext.isEmpty()) {
                                if (ignoreCase) {
                                    ext = ext.toLowerCase();
                                }
                                Long c = countByExts.get(ext);
                                if (c == null) {
                                    countByExts.put(ext, 1L);
                                } else {
                                    c++;
                                    countByExts.put(ext, c);
                                }
                            } else {
                                countNoExt++;
                            }
                        }
                        if ("list".equalsIgnoreCase(action)) {
                            xw.add(pe);
                        }
                        total++;
                    }
                }
                idx += batchSize;
                complete = re.booleanValue("cursor/total/@complete");
            }
            if (countByExtensions) {
                if (!countByExts.isEmpty()) {
                    countByExts.entrySet().stream().sorted(new Comparator<Entry<String, Long>>() {

                        @Override
                        public int compare(Entry<String, Long> e1, Entry<String, Long> e2) {
                            return (-1) * (e1.getValue().compareTo(e2.getValue()));
                        }
                    }).forEach((entry) -> {
                        try {
                            xw.add("count", new String[] { "ext", entry.getKey() }, entry.getValue());
                        } catch (Throwable e) {
                            e.printStackTrace();
                            throw new RuntimeException(e);
                        }
                    });
                }
                if (countNoExt > 0) {
                    xw.add("count", new String[] { "no-ext", "true" }, countNoExt);
                }
            }
            xw.add("total", total);
        }
        if (args.elementExists("notify")) {
            boolean always = args.booleanValue("notify/@always", true);
            if (always || total > 0) {
                Collection<String> emails = args.values("notify/email");
                sendEmail(executor(), emails, args, isb.toString());
            }
        }
    }

    private static void sendEmail(ServiceExecutor executor, Collection<String> emails, XmlDoc.Element args,
            String result) throws Throwable {
        String subject = IDUtils.serverUUID(executor) + ": Result of " + SERVICE_NAME;
        StringBuilder sb = new StringBuilder();
        sb.append("Dear Administrator,\n\n\n");
        sb.append(String.format("SERVICE INVOCATION:\n\n%s\n", SERVICE_NAME));
        sb.append(XmlUtils.toIndentedText(args, false, 4, 4));
        sb.append("\n\n");
        sb.append("SERVICE RESULT:\n\n");
        sb.append(result);
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String email : emails) {
            dm.add("to", email);
        }
        dm.add("subject", subject);
        dm.add("body", sb.toString());
        executor.execute("mail.send", dm.root());
    }

    private static String getFileExtension(String path) {
        String name = getFileName(path);
        int i = name.lastIndexOf('.');
        if (i >= 0) {
            return name.substring(i + 1);
        }
        return null;
    }

    private static String getFileName(String path) {
        Path fileName = Paths.get(path).getFileName();
        return fileName == null ? null : fileName.toString();
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

}
