package unimelb.mf.essentials.plugin.services;

import java.io.File;
import java.io.IOException;

import java.util.Collection;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;
import unimelb.utils.XmlUtils;

public class SvcAssetStoreDescribe extends PluginService {

	private static final String SERVICE_NAME = "unimelb.asset.store.describe";
	private Interface _defn;

	public SvcAssetStoreDescribe() {
		_defn = new Interface();
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, directory on the server to save the output as an XML file. The file will be named users-YYYYMMDD-hhmmss.xml.  Of the form file:<path>", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Describe all stores (skipping those that may fail to describe), and optionall write to a server side XML file.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}


	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String url = args.value("url");
		String urlPath = null;
		if (url!=null) {
			if (!url.substring(0,5).equals("file:")) {
				throw new Exception ("url must be of the form 'file:<path>'");
			}
			urlPath = url.substring(5);
			File outDir = new File(urlPath);
			if (!outDir.exists()) {
				throw new IOException ("Path '" + urlPath + "' does not exist");
			}
			if (!outDir.isDirectory()) {
				throw new IOException ("Path '" + urlPath + "' is not a directory");
			}
		}	
		// Find stores
		Collection<String> stores = executor().execute("asset.store.list").values("store/name");

		// Describe
		XmlDocMaker dm = new XmlDocMaker("stores");
		Boolean some = false;
		if (stores != null) {
			for (String store : stores) {
				// Null if exception
				XmlDoc.Element se = describeStore (executor(), store);
				if (se!=null) {
					dm.add(se.element("store"));
					some= true;
				}
			}
			//
			if (url!=null && some) {
				String today = DateUtil.todaysTime(2);
				String fileName = urlPath + "/stores-" + today + ".xml";
				File file = new File(fileName);
				w.add(dm.root());
				XmlUtils.writeServerSideXMLFile(dm.root(), file);
				w.add("file", fileName);
			}
		}
	}

	// If the store is invalid and generates an exception return null
	static private XmlDoc.Element describeStore (ServiceExecutor executor, String name) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", name);
		try {
			XmlDoc.Element r = executor.execute("asset.store.describe", dm.root());	
			return r;
		} catch (Throwable e) {
			return null;
		}
	}


}
