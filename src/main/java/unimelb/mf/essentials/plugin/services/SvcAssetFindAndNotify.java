package unimelb.mf.essentials.plugin.services;


import java.util.Date;

import arc.mf.plugin.PluginService;

import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.PeerUtil;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;

public class SvcAssetFindAndNotify extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.find.and.notify";

	private Interface _defn;

	public SvcAssetFindAndNotify() {
		_defn = new Interface();

		Interface.Element where = new Interface.Element("where", StringType.DEFAULT, "The where clause to select assets. The", 1, 1);
		_defn.add(where);
		Interface.Element email = new Interface.Element("email", EmailAddressType.DEFAULT, "Notification address summarising the result.", 0, 1);
		_defn.add(email);
		Interface.Element suffix = new Interface.Element("suffix", StringType.DEFAULT, "Suffix for email subject.", 0, 1);
		_defn.add(suffix);
	}
	
	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Simple wrapper service such that if the given query finds anything, an email is sent.  You might use it to detect e.g. if any collection assets have been created...";
	}

	@Override
	public boolean canBeAborted() {
		return false;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}
	
	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String where = args.value("where");
		XmlDoc.Element email = args.element("email");
		String suffix = args.value("suffix");
		// 
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where);	
		dm.add("action", "count");
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return;
		}
		Integer n = r.intValue("value");


		// email
		if (n>0) {
			XmlDocMaker res = new XmlDocMaker("args");
			res.add("where", where);
			res.add("n", n);
			String body = XmlPrintStream.outputToString(res.root());
			String uuid = PeerUtil.serverUUIDFromProute(null);
			String subject = "["+uuid+"] - unimelb.asset.find result (date="+new Date().toString()+")";
			if (suffix!=null) {
				subject = subject + suffix;
			}
			MailUtils.sendEmail(executor(), email.value(), null, null, null, subject, body, false);
		}
		//
		w.add("n", n);
	}
}
