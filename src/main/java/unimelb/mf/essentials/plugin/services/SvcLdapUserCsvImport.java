package unimelb.mf.essentials.plugin.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.EmailUtils;

public class SvcLdapUserCsvImport extends PluginService {

    public static final String SERVICE_NAME = "unimelb.ldap.user.csv.import";

    private Interface _defn;

    public SvcLdapUserCsvImport() {
        _defn = new Interface();
        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "The LDAP authentication domain.", 1, 1));
        _defn.add(new Interface.Element("username-column", IntegerType.POSITIVE_ONE,
                "Index of the username column. Starts from 1.", 0, 1));
        _defn.add(new Interface.Element("email-column", IntegerType.POSITIVE_ONE,
                "Index of the email column. Starts from 1.", 0, 1));
        _defn.add(new Interface.Element("email-suffix", StringType.DEFAULT,
                "Suffix of email address. If specified, only matching email address are included.", 0, 1));
        Interface.Element role = new Interface.Element("role", new StringType(128), "Role to grant to the users.", 1,
                Integer.MAX_VALUE);
        role.add(new Interface.Attribute("type", new StringType(64), "Role type.", 1));
        _defn.add(role);
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Find users by emails, which are parsed from the specified column of the imported CSV file, then grant them with the specified roles.";
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs arg2, XmlWriter w) throws Throwable {
        String domain = args.value("domain");

        if (!args.elementExists("username-column") && !args.elementExists("email-column")) {
            throw new IllegalArgumentException("No username-column or email-column is specified.");
        }

        int usernameCol = args.intValue("username-column", 0);

        int emailCol = args.intValue("email-column", 0);

        String emailSuffix = args.value("email-suffix");

        List<XmlDoc.Element> roles = args.elements("role");

        PluginService.Input input = inputs.input(0);

        int nbGranted = 0;
        Map<String, String> errors = new LinkedHashMap<String, String>();
        try (Reader in = new BufferedReader(new InputStreamReader(input.stream()))) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withAllowMissingColumnNames().parse(in);
            for (CSVRecord record : records) {
                PluginTask.checkIfThreadTaskAborted();
                String username = usernameCol > 0 ? record.get(usernameCol - 1) : null;
                String email = emailCol > 0 ? record.get(emailCol - 1) : null;
                if (username != null) {
                    username = username.trim().toLowerCase();
                    if (userExists(executor(), domain, username)) {
                        grantRolesToUser(executor(), domain, username, roles);
                        nbGranted++;
                        XmlDoc.Element ue = getUser(executor(), domain, username);
                        w.add("user", new String[] { "granted", "true", "name", ue.value("name"), "email",
                                ue.value("e-mail"), "domain", domain }, ue.value());
                        continue;
                    }
                }
                if (email != null) {
                    email = email.trim().toLowerCase();
                    if (EmailUtils.isValidEmailAddress(email)) {
                        if (emailSuffix == null || email.toLowerCase().endsWith(emailSuffix.toLowerCase())) {
                            XmlDoc.Element re = findLdapUserByEmail(executor(), domain, email);
                            int nbFound = re.intValue("found");
                            if (nbFound == 1) {
                                XmlDoc.Element ue = re.element("user");
                                String user = ue.value();
                                String name = ue.value("@name");
                                grantRolesToUser(executor(), domain, user, roles);
                                nbGranted++;
                                w.add("user", new String[] { "granted", "true", "name", name, "email", email, "domain",
                                        domain }, user);
                                continue;
                            }
                        }
                    } else {
                        if (username != null) {
                            continue;
                        }
                    }
                }
                errors.put(username, email);
            }
        }
        w.add("granted", nbGranted);
        if (!errors.isEmpty()) {
            w.push("errors", new String[] { "count", Integer.toString(errors.size()), "message",
                    "user not found or multiple users found" });
            errors.forEach((u, e) -> {
                try {
                    w.add("error", new String[] { "username", u, "email", e });
                } catch (Throwable ex) {
                    throw new RuntimeException(ex);
                }
            });
            w.pop();
        }

    }

    private static boolean userExists(ServiceExecutor executor, String domain, String user) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("domain", domain);
        dm.add("user", user);
        return executor.execute("authentication.user.exists", dm.root()).booleanValue("exists");
    }

    private static XmlDoc.Element getUser(ServiceExecutor executor, String domain, String user) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("domain", domain);
        dm.add("user", user);
        return executor.execute("authentication.user.describe", dm.root()).element("user");
    }

    private static void grantRolesToUser(ServiceExecutor executor, String domain, String user,
            List<XmlDoc.Element> roleElements) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("domain", domain);
        dm.add("user", user);
        dm.addAll(roleElements);
        executor.execute("user.grant", dm.root());
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    public int minNumberOfInputs() {
        return 1;
    }

    public int maxNumberOfInputs() {
        return 1;
    }

    public static XmlDoc.Element findLdapUserByEmail(ServiceExecutor executor, String domain, String email)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("domain", domain);
        dm.add("email", email);
        return executor.execute(SvcLdapUserFind.SERVICE_NAME, dm.root());
    }

}
