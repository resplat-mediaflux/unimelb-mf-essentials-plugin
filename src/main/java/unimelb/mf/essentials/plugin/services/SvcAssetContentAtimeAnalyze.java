package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.FileSizeUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class SvcAssetContentAtimeAnalyze extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.content.atime.analyze";

    public static final int NUM_LATEST_ACCESSED = 0;
    public static final int NUM_EARLIEST_ACCESSED = 0;
    public static final int NUM_NEVER_ACCESSED = 0;
    public static final int DEFAULT_STEP = 1000;

    public static class Asset implements Comparable<Asset> {
        public final String path;
        public final String id;
        public final int version;
        public final long csize;
        public final long ctime;
        public final long atime;

        public Asset(String id, int version, String path, long csize, long ctime, long atime) {
            this.id = id;
            this.version = version;
            this.path = path;
            this.csize = csize;
            this.ctime = ctime;
            this.atime = atime;
        }

        public Asset(XmlDoc.Element ae) throws Throwable {
            this(ae.value("@id"), ae.intValue("@version"), ae.value("path"), ae.longValue("csize", -1),
                    ae.longValue("ctime", -1), ae.longValue("atime", -1));
        }

        private static String asDateString(long time) {
            if (time >= 0) {
                return DateTime.string(new Date(time));
            } else {
                return null;
            }
        }

        public String ctimeAsDateString() {
            return asDateString(this.ctime);
        }

        public String atimeAsDateString() {
            return asDateString(this.atime);
        }

        public String csizeAsString() {
            return this.csize >= 0 ? Long.toString(this.csize) : null;
        }

        @Override
        public int compareTo(Asset o) {
            if (o == null) {
                return 1;
            }
            return Long.compare(this.atime, o.atime);
        }

        public void save(XmlWriter w) throws Throwable {
            w.add("asset",
                    new String[]{"id", this.id, "atime", this.atimeAsDateString(), "ctime", this.ctimeAsDateString(),
                            "csize", this.csizeAsString()}, path);
        }

    }

    public static class Range {
        public final Date from;
        public final Date to;
        private final AtomicLong nbAssets;
        private final AtomicLong contentSize;

        Range(Date from, Date to) {
            this.from = from;
            this.to = to;
            this.nbAssets = new AtomicLong(0);
            this.contentSize = new AtomicLong(0);
        }

        void incNumOfAssets() {
            this.nbAssets.getAndIncrement();
        }

        void addContentSize(long delta) {
            this.contentSize.getAndAdd(delta);
        }

        public long numOfAssets() {
            return this.nbAssets.get();
        }

        public long contentSize() {
            return this.contentSize.get();
        }

        public void save(XmlWriter w) throws Throwable {
            w.push("range", new String[]{"from", DateTime.string(this.from), "to", DateTime.string(this.to)});
            w.add("number-of-assets", this.numOfAssets());
            long csize = this.contentSize();
            w.add("content-size", new String[]{"hr", csize > 0 ? FileSizeUtils.toHumanReadable(csize) : null},
                    csize);
            w.pop();
        }

        public boolean match(long atime) {
            long fromTime = this.from.getTime();
            long toTime = this.to.getTime();
            return atime >= fromTime && atime < toTime;
        }

        public boolean match(Asset asset) {
            if (asset != null) {
                return match(asset.atime);
            }
            return false;
        }

        public void countIfMatch(Asset asset) {
            if (match(asset)) {
                this.incNumOfAssets();
                this.addContentSize(asset.csize);
            }
        }
    }

    public static class Summary {
        private final AtomicLong totalNumOfAssets;
        private final AtomicLong totalContentSize;
        private final AtomicLong numOfNeverAccessed;
        public final int maxNumberOfEarliestAccessed;
        private List<Asset> earliestAccessed;
        public final int maxNumberOfLatestAccessed;
        private List<Asset> latestAccessed;
        public final int maxNumberOfNeverAccessed;
        private List<Asset> neverAccessed;
        private long minAtime = -1;
        private long maxAtime = -1;
        private final List<Range> ranges;

        public Summary(int maxNumberOfEarliestAccessed, int maxNumberOfLatestAccessed, int maxNumberOfNeverAccessed,
                       Collection<Range> ranges) {
            this.maxNumberOfEarliestAccessed = maxNumberOfEarliestAccessed;
            if (maxNumberOfEarliestAccessed > 0) {
                this.earliestAccessed = new ArrayList<>();
            }
            this.maxNumberOfLatestAccessed = maxNumberOfLatestAccessed;
            if (maxNumberOfLatestAccessed > 0) {
                this.latestAccessed = new ArrayList<>();
            }
            this.maxNumberOfNeverAccessed = maxNumberOfNeverAccessed;
            if (maxNumberOfNeverAccessed > 0) {
                this.neverAccessed = new ArrayList<>();
            }
            this.ranges = ranges == null ? null : new ArrayList<>(ranges);
            this.totalNumOfAssets = new AtomicLong(0);
            this.totalContentSize = new AtomicLong(0);
            this.numOfNeverAccessed = new AtomicLong(0);
        }

        public long numberOfNeverAccessed() {
            return this.numOfNeverAccessed.get();
        }

        public long totalNumberOfAssets() {
            return this.totalNumOfAssets.get();
        }

        public long totalContentSize() {
            return this.totalContentSize.get();
        }

        public long minAtime() {
            return this.minAtime;
        }

        public long maxAtime() {
            return this.maxAtime;
        }


        public List<Range> ranges() {
            if (this.ranges != null) {
                return Collections.unmodifiableList(this.ranges);
            }
            return null;
        }

        public List<Asset> earliestAccessed() {
            return (this.earliestAccessed == null || this.earliestAccessed.isEmpty()) ? null : Collections.unmodifiableList(
                    this.earliestAccessed);
        }

        public List<Asset> latestAccessed() {
            return (this.latestAccessed == null || this.latestAccessed.isEmpty()) ? null : Collections.unmodifiableList(
                    this.latestAccessed);
        }

        public List<Asset> neverAccessed() {
            return (this.neverAccessed == null || this.neverAccessed.isEmpty()) ? null : Collections.unmodifiableList(
                    this.neverAccessed);
        }


        public void save(XmlWriter w) throws Throwable {
            w.add("total-number-of-assets", this.totalNumberOfAssets());
            long totalCSize = this.totalContentSize();
            w.add("total-content-size",
                    new String[]{"hr", totalCSize > 0 ? FileSizeUtils.toHumanReadable(totalCSize) : null}, totalCSize);
            w.add("number-of-never-accessed", this.numberOfNeverAccessed());
            if (this.minAtime >= 0) {
                w.add("min-atime", new String[]{"millisecs", Long.toString(this.minAtime)},
                        DateTime.string(new Date(this.minAtime), true));
            }
            if (this.maxAtime >= 0) {
                w.add("max-atime", new String[]{"millisecs", Long.toString(this.maxAtime)},
                        DateTime.string(new Date(this.maxAtime), true));
            }
            if (this.ranges != null) {
                for (Range range : ranges) {
                    range.save(w);
                }
            }
            if (this.earliestAccessed() != null) {
                w.push("earliest-accessed");
                for (Asset asset : earliestAccessed) {
                    asset.save(w);
                }
                w.pop();
            }
            if (this.latestAccessed() != null) {
                w.push("latest-accessed");
                for (Asset asset : latestAccessed) {
                    asset.save(w);
                }
                w.pop();
            }
            if (this.neverAccessed() != null) {
                w.push("never-accessed", new String[]{"total", Long.toString(this.numberOfNeverAccessed())});
                for (Asset asset : this.neverAccessed) {
                    asset.save(w);
                }
                w.pop();
            }
        }


        public void analyze(Asset asset) {
            this.totalNumOfAssets.getAndIncrement();
            if (asset.csize > 0) {
                this.totalContentSize.getAndAdd(asset.csize);
            }
            if (asset.atime >= 0) {
                if (this.minAtime < 0) {
                    this.minAtime = asset.atime;
                }
                if (this.maxAtime < 0) {
                    this.maxAtime = asset.atime;
                }
                if (asset.atime < this.minAtime) {
                    this.minAtime = asset.atime;
                }
                if (asset.atime > this.maxAtime) {
                    this.maxAtime = asset.atime;
                }
                if (this.maxNumberOfEarliestAccessed > 0) {
                    if (this.earliestAccessed.size() < this.maxNumberOfEarliestAccessed) {
                        this.earliestAccessed.add(asset);
                        Collections.sort(this.earliestAccessed);
                    } else {
                        if (asset.atime < this.earliestAccessed.get(this.earliestAccessed.size() - 1).atime) {
                            // remove the last element
                            this.earliestAccessed.remove(this.earliestAccessed.size() - 1);
                            this.earliestAccessed.add(asset);
                            Collections.sort(this.earliestAccessed);
                        }
                    }
                }
                if (this.maxNumberOfLatestAccessed > 0) {
                    if (this.latestAccessed.size() < this.maxNumberOfLatestAccessed) {
                        this.latestAccessed.add(asset);
                        Collections.sort(this.latestAccessed);
                    } else {
                        if (asset.atime > this.latestAccessed.get(0).atime) {
                            // remove the first element
                            this.latestAccessed.remove(0);
                            this.latestAccessed.add(asset);
                            Collections.sort(this.latestAccessed);
                        }
                    }
                }
            } else {
                if (this.maxNumberOfNeverAccessed > 0 && this.neverAccessed.size() < this.maxNumberOfNeverAccessed) {
                    this.neverAccessed.add(asset);
                }
                this.numOfNeverAccessed.getAndIncrement();
            }
            if (ranges != null) {
                for (Range range : ranges) {
                    range.countIfMatch(asset);
                }
            }
        }
    }

    private final Interface _defn;

    public SvcAssetContentAtimeAnalyze() {
        _defn = new Interface();
        _defn.add(new Interface.Element("namespace", StringType.DEFAULT, "Asset namespace path.", 0, 1));
        _defn.add(new Interface.Element("where", StringType.DEFAULT, "Query to select the assets.", 0, 1));
        _defn.add(new Interface.Element("latest-accessed", new IntegerType(0, 10000),
                "The number of latest accessed assets (max content/atime) to list. Defaults to " + NUM_LATEST_ACCESSED + ". Set to 0 to disable.",
                0, 1));
        _defn.add(new Interface.Element("earliest-accessed", new IntegerType(0, 10000),
                "The number of earliest accessed assets (min content/atime) to list. Defaults to " + NUM_EARLIEST_ACCESSED + ". Set to 0 to disable.",
                0, 1));
        _defn.add(new Interface.Element("never-accessed", new IntegerType(0, 10000),
                "The number of never accessed assets (no content/atime) to list. Defaults to " + NUM_NEVER_ACCESSED + ". Set to 0 to disable.",
                0, 1));
        Interface.Element range = new Interface.Element("range", XmlDocType.DEFAULT, "Time range for asset atime.", 0,
                16);
        range.add(new Interface.Element("from", DateType.DEFAULT,
                "Start of atime. If not specified, defaults to '" + DateTime.string(new Date(0),
                        false) + "'. Either this argument or the 'to' argument must be specified for the range.",
                0, 1));
        range.add(
                new Interface.Element("to", DateType.DEFAULT,
                        "End of atime. Defaults to current server time. Either 'from' or this argument must be specified for the range.",
                        0, 1));
        _defn.add(range);
        _defn.add(new Interface.Element("step", new IntegerType(1, 100000),
                "Step size for queries. Defaults to " + DEFAULT_STEP, 0, 1));

    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Analyze the content atime of the selected assets.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }


    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        String namespace = args.value("namespace");
        String where = args.value("where");
        if (namespace == null && where == null) {
            throw new IllegalArgumentException("Expects either 'namespace' or 'where' argument. Found none.");
        }
        if (namespace != null && where != null) {
            throw new IllegalArgumentException("Expects either 'namespace' or 'where' argument. Found both.");
        }
        if (where == null) {
            where = "namespace>=" + namespace;
        }
        where = "(" + where + ") and asset has content";
        int step = args.intValue("step", DEFAULT_STEP);

        int maxNumOfEarliest = args.intValue("earliest-accessed", NUM_EARLIEST_ACCESSED);
        int maxNumOfLatest = args.intValue("latest-accessed", NUM_LATEST_ACCESSED);
        int maxNumOfNever = args.intValue("never-accessed", NUM_NEVER_ACCESSED);

        Date now = new Date();
        List<Range> ranges = null;
        List<XmlDoc.Element> res = args.elements("range");
        if (res != null && !res.isEmpty()) {
            ranges = new ArrayList<>(res.size());
            for (XmlDoc.Element re : res) {
                if (!re.elementExists("from") && !re.elementExists("to")) {
                    throw new IllegalArgumentException("Invalid range. No range/from or range/to is specified.");
                }
                Date from = re.dateValue("from", new Date(0));
                Date to = re.dateValue("to", now);
                if (from.after(to)) {
                    throw new IllegalArgumentException(
                            "Invalid range: " + re.value("from") + " ~ " + re.stringValue("to", ""));
                }
                ranges.add(new Range(from, to));
            }
        }
        Summary summary = analyze(executor(), where, maxNumOfEarliest, maxNumOfLatest,
                maxNumOfNever, ranges, step);
        summary.save(w);

    }


    private static Summary analyze(ServiceExecutor executor, String where,
                                   int maxNumOfEarliest,
                                   int maxNumOfLatest,
                                   int maxNumOfNever,
                                   List<Range> ranges, int step) throws Throwable {
        Summary summary = new Summary(maxNumOfEarliest, maxNumOfLatest, maxNumOfNever, ranges);

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", where);
        dm.add("as", "iterator");
        dm.add("action", "get-value");
        dm.add("xpath", new String[]{"ename", "path"}, "path");
        dm.add("xpath", new String[]{"ename", "ctime"}, "content/ctime/@millisec");
        dm.add("xpath", new String[]{"ename", "atime"}, "content/atime/@millisec");
        dm.add("xpath", new String[]{"ename", "csize"}, "content/size");
        PluginTask.checkIfThreadTaskAborted();
        long iteratorId = executor.execute("asset.query", dm.root()).longValue("iterator");

        boolean complete = false;
        dm = new XmlDocMaker("args");
        dm.add("id", iteratorId);
        dm.add("size", step);
        try {
            while (!complete) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element re = executor.execute("asset.query.iterate", dm.root());
                complete = re.booleanValue("iterated/@complete");
                List<XmlDoc.Element> aes = re.elements("asset");
                if (aes != null) {
                    for (XmlDoc.Element ae : aes) {
                        Asset asset = new Asset(ae);
                        summary.analyze(asset);
                    }
                }
            }
        } catch (Throwable e) {
            // make sure the iterator is destoryed.
            executor.execute("asset.query.iterator.destroy",
                    "<args><ignore-missing>true</ignore-missing><id>" + iteratorId + "</id></args>", null, null);
            throw e;
        }
        return summary;
    }


    @Override
    public boolean canBeAborted() {
        return true;
    }
}
