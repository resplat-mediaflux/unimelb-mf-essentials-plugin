package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetShareableAllDescribe extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.shareable.all.describe";

	private Interface _defn;

	public SvcAssetShareableAllDescribe() {
		_defn = new Interface();
		_defn.add(new Interface.Element("type", new EnumType(new String[] { "upload", "download"}),
				"The type of shareable, upload or download or both (default). ", 0, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Describe all shareables regardless of whi created them.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		String type = args.stringValue("type", "both");
		if (type!=null && type.equals("both")) {
			type = null;
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		if (type!=null) {
			dm.add("type", type);
		}
		dm.add("size", "infinity");
		XmlDoc.Element r = executor().execute("asset.shareable.all.list", dm.root());
		if (r!=null) {
			Collection<String> ids = r.values("shareable");
			if (ids!=null) {
				for (String id : ids) {
					dm = new XmlDocMaker("args");
					dm.add("id", id);
					XmlDoc.Element r2 = executor().execute("asset.shareable.describe", dm.root());
					w.add(r2.element("shareable"));;
				}
			}
		}		
	}
}
