package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
//import java.util.Collections;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentAllIsSingular extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.all.is.singular";

	private Interface _defn;

	public SvcAssetContentAllIsSingular() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"The name of a store to check. If multiples given, all stores are checked to see if there is singular content fro.", 1, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("include-zero", BooleanType.DEFAULT,
				"If true, include zero-sized files (default false).", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Iterates through all asset versions and finds assets that have content that is singular in the given store.  That is, that there are no other content copies in other stores. If there are no copies for the given store(s), this service does not report them. The intent of this service is for finding assets for which a content copy should have been made in  aidfferent store but hasn't - so we are looking for precisely one content copy. ";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String assetID = args.value("id");
		Collection<String> stores = args.values("store");
		Boolean includeZero = args.booleanValue("include-zero", false);
		checkStores(executor(), stores);

		// Get asset versions
		Collection<String> versions = executor()
				.execute("asset.versions", "<args><id>" + assetID + "</id></args>", null, null)
				.values("asset/version/@n");

		// Iterate through all versions 
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("asset", new String[] { "id", assetID });
		Boolean some = false;           // SOmething from at least one version to show
		for (String version : versions) {
			PluginTask.checkIfThreadTaskAborted();

			// Get asset for this version
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", new String[] { "version", version }, assetID);
			XmlDoc.Element r = executor().execute("asset.get", dm.root());
			XmlDoc.Element content = r.element("asset/content");

			// Proceed if we have some content
			if (content!=null) {
				Long csize = content.longValue("size");


				// See if there are content copies
				XmlDoc.Element contentCopies = content.element("copies"); 
				if (contentCopies==null) {

					// There are no content copies, the content is singular by definition
					// so list it if found in the desired store.
					String assetStore = content.value("store");
					for (String store : stores) {
						if (store.equals(assetStore)) {
							if (!includeZero) {
								if (csize>0) {
									some = true;
									dmOut.add("version", new String[] {"store", store, "csize", ""+csize, "copies", "1"}, version);
								}
							} else {
								some = true;
								dmOut.add("version", new String[] {"store", store, "csize", ""+csize, "copies", "1"}, version);
							}
						}
					}
				} else {
					// If there were content copies, there must always be 2 or more.
					// If we find our store for one of the copies it's not the only one
					// If we don't find it we don't care - this service only cares if there is precisely one copy
				}
			}	
		}


		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}
	}

	private void checkStores (ServiceExecutor executor, Collection<String> stores) throws Throwable {
		for (String store : stores) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("name", store);
			XmlDoc.Element r = executor.execute("asset.store.exists", dm.root());
			if (!r.booleanValue("exists")) {
				throw new Exception ("Store " + store + " does not exist");
			}
		}
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
