package unimelb.mf.essentials.plugin.services.audit;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.DateType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAuditUserLogonStat extends PluginService {

	public static final String SERVICE_NAME = "unimelb.audit.user.logon.stat";

	private Interface _defn;

	public SvcAuditUserLogonStat() {
		_defn = new Interface();
		_defn.add(new Interface.Element("from", DateType.DEFAULT, "Start date/time inclusive.", 0, 1));
		_defn.add(new Interface.Element("to", DateType.DEFAULT, "End date/time inclusive.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Summarise user.login events from audit records.";
	}

	@Override
	public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		Date from = args.dateValue("from", null);
		Date to = args.dateValue("to", new Date());

		long countTotal = 0L;
		long countInternal = 0L;
		long countExternal = 0L;
		Map<String, Long> vectorCounts = new HashMap<>();
		Map<String, Long> vectorInternalCounts = new HashMap<>();
		Map<String, Long> vectorExternalCounts = new HashMap<>();

		boolean complete = false;
		long idx = 1;
		int size = 10000;
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("type", "user.logon");
		if (from != null) {
			dm.add("from", from);
		}
		if (to != null) {
			dm.add("to", to);
		}
		dm.add("idx", idx);
		dm.add("size", size);
		XmlDoc.Element queryArgs = dm.root();

		Set<String> knownInternalIPs = new HashSet<>();
		Set<String> knownExternalIPs = new HashSet<>();

		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			queryArgs.element("idx").setValue(idx);
			XmlDoc.Element re = executor().execute("audit.query", queryArgs);
			List<XmlDoc.Element> ees = re.elements("event");
			if (ees != null && !ees.isEmpty()) {
				for (XmlDoc.Element ee : ees) {
					countTotal++;
					String ipAddr = ee.value("ip-address");
					String vector = ee.value("vector");
					boolean internalIP = ipAddr == null ? true
							: SvcAuditAssetAccessStat.isUnimelbIPAddr(ipAddr, knownInternalIPs, knownExternalIPs);
					long vectorCount = vectorCounts.containsKey(vector) ? vectorCounts.get(vector) + 1 : 1;
					vectorCounts.put(vector, vectorCount);
					if (internalIP) {
						countInternal++;
						long vectorInternalCount = vectorInternalCounts.containsKey(vector)
								? vectorInternalCounts.get(vector) + 1
								: 1;
						vectorInternalCounts.put(vector, vectorInternalCount);
					} else {
						countExternal++;
						long vectorExternalCount = vectorExternalCounts.containsKey(vector)
								? vectorExternalCounts.get(vector) + 1
								: 1;
						vectorExternalCounts.put(vector, vectorExternalCount);
					}
				}
				PluginTask.threadTaskProcessed(ees.size());
				idx += size;
			}
			complete = ees == null || ees.size() < size;
		}
		if (to != null || from != null) {
			w.push("time");
			if (from != null) {
				w.add("from", from);
			}
			if (to != null) {
				w.add("to", to);
			}
			w.pop();
		}

		Set<String> vectors = vectorCounts.keySet();
		for (String vector : vectors) {
			long vectorTotal = vectorCounts.get(vector);
			long vectorInternal = vectorInternalCounts.containsKey(vector) ? vectorInternalCounts.get(vector) : 0;
			long vectorExternal = vectorExternalCounts.containsKey(vector) ? vectorExternalCounts.get(vector) : 0;
			double vectorPercent = countTotal == 0 ? 0.0 : (double) vectorTotal / countTotal * 100.0;
			double vectorInternalPercent = vectorTotal == 0 ? 0.0 : (double) vectorInternal / vectorTotal * 100.0;
			double vectorExternalPercent = vectorTotal == 0 ? 0.0 : (double) vectorExternal / vectorTotal * 100.0;
			w.push("vector", new String[] { "type", vector, "percent", String.format("%.2f", vectorPercent) });
			w.add("internal", new String[] { "percent", String.format("%.2f", vectorInternalPercent) }, vectorInternal);
			w.add("external", new String[] { "percent", String.format("%.2f", vectorExternalPercent) }, vectorExternal);
			w.add("total", vectorTotal);
			w.pop();
		}

		double countInternalPercent = countTotal == 0 ? 0.0 : (double) countInternal / countTotal * 100.0;
		double countExternalPercent = countTotal == 0 ? 0.0 : (double) countExternal / countTotal * 100.0;
		w.push("count");
		w.add("internal", new String[] { "percent", String.format("%.2f", countInternalPercent) }, countInternal);
		w.add("external", new String[] { "percent", String.format("%.2f", countExternalPercent) }, countExternal);
		w.add("total", countTotal);
		w.pop();
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

}
