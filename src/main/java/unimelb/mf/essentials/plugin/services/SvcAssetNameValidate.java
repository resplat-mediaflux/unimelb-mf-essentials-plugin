package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.*;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.FileNameUtils;
import unimelb.utils.XmlUtils;

import java.io.*;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPOutputStream;

public class SvcAssetNameValidate extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.name.validate";

    public static final String QUERY_FOR_INVALID_NAMES = String.format(
            "name like '%s' or name like '%s' or name like '%s'", FileNameUtils.REGEX_ILLEGAL_CHARS,
            FileNameUtils.REGEX_LEADING_SPACES, FileNameUtils.REGEX_TRAILING_SPACES);


    public static final String QUERY_FOR_INVALID_PATHS = String.format("function(matches(xvalue('path'),'%s'))",
            FileNameUtils.REGEX_ILLEGAL_PATHS);

    public static final int QUERY_RESULT_SIZE = 100;

    public static final int MAX_MESSAGE_LENGTH = 60000;

    public static class Asset {
        public final String id;
        public final int version;
        public final String path;
        public final String name;

        public Asset(String id, int version, String path, String name) {
            this.id = id;
            this.version = version;
            this.path = path;
            this.name = name;
        }
    }

    public interface AssetHandler {
        void handle(Asset asset) throws Throwable;
    }

    private final Interface _defn;

    public SvcAssetNameValidate() {
        _defn = new Interface();
        _defn.add(new Interface.Element("where", StringType.DEFAULT, "Selection query.", 0, 1));

        Interface.Element namespace = new Interface.Element("namespace", StringType.DEFAULT, "Namespace path.", 0, 1);
        namespace.add(new Interface.Attribute("recursive", BooleanType.DEFAULT,
                "include descendant namespaces. Defaults to true.", 0));
        _defn.add(namespace);

        _defn.add(new Interface.Element("id", LongType.POSITIVE_ONE, "Asset identifier.", 0, Integer.MAX_VALUE));

        _defn.add(new Interface.Element("compress", BooleanType.DEFAULT,
                "Compress the output CSV file to GZIP format. Defaults to false.", 0, 1));

        _defn.add(new Interface.Element("parents", BooleanType.DEFAULT,
                "Validate the parent namespace names. Defaults to true.", 0, 1));

        Interface.Element notify = new Interface.Element("notify", XmlDocType.DEFAULT,
                "Notify of the result via emails.", 0, 1);
        notify.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Recipient to receive the result.", 1, 10));
        _defn.add(notify);
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Scan for assets with invalid file names. It can optionally export the results to a CSV file if service output is specified. The REGEX to filter illegal characters is "
                + FileNameUtils.REGEX_ILLEGAL_CHARS;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        // construct selection query...
        String query = constructSelectionQuery(args);

        Collection<String> notifyEmails = args.values("notify/email");
        boolean notify = notifyEmails != null && !notifyEmails.isEmpty();
        StringBuilder msg = notify ? new StringBuilder() : null;
        if (msg != null) {
            msg.append("---\n");
            msg.append(SERVICE_NAME).append("\n");
            msg.append(XmlUtils.toIndentedText(args, false, 4, 4));
            msg.append("---\n\n\n");
        }

        PluginService.Output output = outputs == null ? null : outputs.output(0);
        boolean compress = args.booleanValue("compress", false);
        boolean parents = args.booleanValue("parents", true);
        String outputMimeType = compress ? "application/gzip" : "text/csv";
        String outputFileExt = compress ? "csv.gz" : "csv";
        File outputFile = (output == null && !notify) ? null : PluginTask.createTemporaryFile("." + outputFileExt);

        AtomicLong nbInvalid = new AtomicLong(0);

        try {
            validateAsset(executor(), query, parents, w, output, outputFile, compress, notify, msg, nbInvalid);
            if (notify) {
                String subject = String.format("%s: %s", executor().execute("server.uuid").value("uuid"), SERVICE_NAME);
                String fileName = compress ? "invalid-asset-names.csv.gz" : "invalid-asset-names.csv";
                notify(executor(), subject, msg.toString(), outputFile, fileName, outputMimeType, notifyEmails);
            }

            if (output != null) {
                output.setData(PluginTask.deleteOnCloseInputStream(outputFile), outputFile.length(), outputMimeType);
            } else {
                if (outputFile != null && outputFile.exists()) {
                    PluginTask.deleteTemporaryFile(outputFile);
                }
            }
        } catch (Throwable e) {
            if (outputFile != null && outputFile.exists()) {
                PluginTask.deleteTemporaryFile(outputFile);
            }
            throw e;
        }
    }

    private static void validateAsset(ServiceExecutor executor, String query, boolean parents, XmlWriter w,
                                      PluginService.Output output,
                                      File outputFile, boolean compress, boolean notify, StringBuilder msg,
                                      AtomicLong nbInvalid) throws Throwable {
        try (PrintStream outputPS = (output == null && !notify) ? null : new PrintStream(compress ?
                new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile))) :
                new BufferedOutputStream(new FileOutputStream(outputFile)))) {
            if (outputPS != null) {
                outputPS.println("ID,Version,Path,Name");
            }
            validateAsset(executor, query, parents, asset -> {
                w.add("invalid", new String[]{"id", asset.id, "version", Integer.toString(asset.version), "path",
                        asset.path}, asset.name);
                if (outputPS != null) {
                    outputPS.printf("%s,%d,\"%s\",\"%s\"%n", asset.id, asset.version, asset.path,
                            asset.name);
                }
                if (msg != null) {
                    if (msg.length() < MAX_MESSAGE_LENGTH) {
                        msg.append(String.format("    :invalid -id %s -version %d -path \"%s\" \"%s\"\n",
                                asset.id, asset.version, asset.path, asset.name));
                    }
                }
                nbInvalid.getAndIncrement();
            });
            w.add("count", nbInvalid.get());
            if (msg != null) {
                if (msg.length() > +MAX_MESSAGE_LENGTH) {
                    msg.append("    ... ... ...\n");
                }
                msg.append(String.format("    :count %d", nbInvalid.get())).append("\n");
            }
        }
    }

    private static String constructSelectionQuery(XmlDoc.Element args) throws Throwable {
        StringBuilder sb = new StringBuilder();
        String where = args.value("where");
        if (where != null) {
            sb.append("(").append(where).append(")");
        }
        String namespace = args.value("namespace");
        if (namespace != null) {
            boolean recursive = args.booleanValue("namespace/@recursive", true);
            if (sb.length() > 0) {
                sb.append(" or ");
            }
            sb.append("(namespace").append(recursive ? ">=" : "=").append("'").append(namespace).append("')");
        }
        Collection<String> ids = args.values("id");
        if (ids != null && !ids.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(" or ");
            }
            sb.append("(");
            boolean first = true;
            for (String id : ids) {
                if (first) {
                    first = false;
                } else {
                    sb.append(" or ");
                }
                sb.append("id=").append(id);
            }
            sb.append(")");
        }
        return sb.length() > 0 ? sb.toString() : null;
    }

    static void notify(ServiceExecutor executor, String subject, String msg, File outputFile, String fileName,
                       String mimeType, Collection<String> notifyEmails) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String notifyEmail : notifyEmails) {
            dm.add("to", notifyEmail);
        }
        dm.add("subject", subject);
        dm.add("body", msg);
        dm.add("async", false);
        dm.push("attachment");
        dm.add("name", fileName);
        dm.add("type", mimeType);
        dm.pop();
        try (InputStream in = new BufferedInputStream(new FileInputStream(outputFile))) {
            PluginService.Input input = new PluginService.Input(in, outputFile.length(), mimeType, null);
            try {
                executor.execute("mail.send", dm.root(), new PluginService.Inputs(input), null);
            } finally {
                input.close();
            }
        }
    }

    private static void validateAsset(ServiceExecutor executor, String where, boolean parents,
                                      AssetHandler ah) throws Throwable {
        validateAsset(executor, where, null, parents, ah);
    }

    static void validateAsset(ServiceExecutor executor, String where, String filter, boolean parents,
                              AssetHandler ah) throws Throwable {
        long idx = 1;
        boolean complete = false;
        while (!complete) {
            XmlDocMaker dm = new XmlDocMaker("args");
            if (where != null && !where.isEmpty()) {
                dm.add("where", where);
            }
            if (filter != null) {
                dm.add("where", filter);
            }
            if (parents) {
                dm.add("where", QUERY_FOR_INVALID_PATHS);
            } else {
                dm.add("where", QUERY_FOR_INVALID_NAMES);
            }
            dm.add("action", "get-value");
            dm.add("xpath", new String[]{"ename", "path"}, "path");
            dm.add("xpath", new String[]{"ename", "name"}, "name");
            dm.add("size", QUERY_RESULT_SIZE);
            dm.add("idx", idx);
            PluginTask.checkIfThreadTaskAborted();
            XmlDoc.Element re = executor.execute("asset.query", dm.root());
            complete = re.booleanValue("cursor/total/@complete");
            List<XmlDoc.Element> aes = re.elements("asset");
            if (aes != null && !aes.isEmpty()) {
                for (XmlDoc.Element ae : aes) {
                    String id = ae.value("@id");
                    int version = ae.intValue("@version");
                    String path = ae.value("path");
                    String name = ae.value("name");
                    ah.handle(new Asset(id, version, path, name));
                }
            }
            idx += QUERY_RESULT_SIZE;
        }
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public int maxNumberOfOutputs() {
        return 1;
    }

    @Override
    public int minNumberOfOutputs() {
        return 0;
    }


}
