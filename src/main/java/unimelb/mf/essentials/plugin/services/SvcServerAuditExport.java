/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;

import java.io.File;

// TBD : read user resourcing asset as input instead of command line args,


import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.DateType;
import arc.mf.plugin.dtype.IntegerType;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.ServerDetails;
import unimelb.utils.DateUtil;

public class SvcServerAuditExport extends PluginService {
	private Interface _defn;		
	static private String url = "/opt/mediaflux/volatile/audit-export/";
	public SvcServerAuditExport()  throws Throwable {
		_defn = new Interface();
	//	_defn.add(new Interface.Element("url", StringType.DEFAULT, "The url (just specify the directory path, don't prefix with file:) for the output - must be accessible to the server.", 1, 1));
		_defn.add(new Interface.Element("retain", IntegerType.DEFAULT, "How many export files do we maintain in the export folder. If specified then that number of recent files will be maintained. If 0 is specified only the most recent backup is retained. Defaults to infinite.", 0, 1));
		_defn.add(new Interface.Element("from", DateType.DEFAULT, "Export records from this date. Defaults to first.", 0, 1));
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public Interface definition() {
		return _defn;
	}

	public String description() {
		return "Service exports the audit log to the directory /opt/mediaflux/volatile/audit-export with a file name according to the date and time at execution.";
	}

	public String name() {
		return "unimelb.server.audit.export";
	}
	
	public boolean canBeAborted() {
		return true;
	}

	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

		// Destination must exist
		File d = new File(url);
		if (d.exists()) {
			if (!d.isDirectory()) {
				throw new Exception ("The output path +'" + url + "' exists but is not a directory");
			}
		} else {
			throw new Exception ("The output path +'" + url + "' does not exist");
		}
		
		String retain  = args.value("retain");
		Integer nBackups = 0;
		if (retain!=null) {
			nBackups = Integer.parseInt(retain);
			if (nBackups<=0) {
				throw new Exception ("You must retain at least one backup");
			}
		}

		Date from = args.dateValue("from");

		// Get the date and time
		//  YYYYMMDDhhmmss.SSS
		String dateTime = DateUtil.todaysTime(2);

		String uuid = ServerDetails.serverUUID(executor());
		String hostName = InetAddress.getLocalHost().getHostName();
		String prefixIn = "audit-log-export";
		String fileName = prefixIn + "_"+uuid+"_"+hostName+"_"+dateTime + ".csv.zip";
		//
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("url", url + fileName);
		dm.add("compress", true);
		if (from!=null) {
			dm.add("from", from);
		}
		executor().execute("audit.export.csv", dm.root());

		// Remove old exports
		if (retain!=null) {
			PluginTask.checkIfThreadTaskAborted();

			// Get list of files in the final location of the exports so we can remove old ones
			File folder = new File(url);
			File[] files = folder.listFiles();

			if (files!=null) {
				List<File> listOfFiles = Arrays.asList(files);


				// Sanity checks since we are going to delete files. Skip the ones
				// that aren't compressed audit export files
				Vector<File> filteredList = new Vector<File>();
				for (File file : listOfFiles) {
					PluginTask.checkIfThreadTaskAborted();

					// Currently throws exception if encounters anything unexpected
					if (checkFileName (file, prefixIn, ".zip")) {
						filteredList.add(file);
					} else {
						w.add("file", new String[] {"skipped", "true"}, file.getName());
					}
				}

				//
				if (filteredList.size()>0) {

					// Sort in reverse time order so oldest files at the end of the list
					Collections.sort(filteredList, new Comparator<File>() {
						@Override
						public int compare(File o1, File o2) {
							// Names of the form   audit-log-export_1128_n1-mf1-qh2.storage.unimelb.edu.au_20170813-133202.dbb
							String n1 = o1.getName();
							String[]  parts1 = n1.split("_");
							String n2 = o2.getName();
							String[] parts2 = n2.split("_");

							String date1s = parts1[3].substring(0, 8);
							String date2s = parts2[3].substring(0, 8);
							Long date1 = Long.parseLong(date1s);
							Long date2 = Long.parseLong(date2s);
							if (date1<date2) {
								return 1;
							} else if (date1.equals(date2)) {
								Long time1 = Long.parseLong(parts1[3].substring(9, 15));
								Long time2 = Long.parseLong(parts2[3].substring(9, 15));

								if (time1<time2){
									return 1;
								} else if (time1.equals(time2)) {
									return 0;
								} else {
									return -1;
								}
							} else {
								return -1;
							}
						}
					});

					// Discard older files
					int cnt = 1;
					for (File file : filteredList) {
						PluginTask.checkIfThreadTaskAborted();

						if (cnt>nBackups) {
							w.add("file", new String[]{"destroyed", "true"}, file.getName());
							file.delete();
						} else {
							w.add("file", new String[]{"destroyed", "false"}, file.getName());
						}
						cnt++;
					} 
				}
			}
		} 
	}


	private Boolean checkFileName (File file, String prefixIn, String ext) throws Throwable {
		// Names of the form  audit-log-export-1128_n1-mf1-qh2.storage.unimelb.edu.au_20170813-133202.csv.zip
		String name = file.getName();
		int n2 = name.length();
		String suffix = name.substring(n2-4,n2);
		if (!suffix.equals(ext)) {
			//return false;
			throw new Exception ("The file '" + name + "' does not appear to be an expected audit log  export file (suffix should be .zip");
		}
		String[] parts = name.split("_");
		if (parts.length!=4) {
			//return false;
			throw new Exception ("The file '" + name + "' does not appear to be an expected audit log  export file. Name should split into three parts on '_'");
		}
		String prefix = name.substring(0, 25);
		if (!prefix.equals(prefix)) {
			//return false;
			throw new Exception ("The file '" + name + "' does not appear to be an expected audit log export file. Root name is not '" + prefixIn + "'");
		}
		return true;
	}
}
