package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetShareableAllInvalidDestroy extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.shareable.all.invalid.destroy";

	private Interface _defn;

	public SvcAssetShareableAllInvalidDestroy() {
		_defn = new Interface();
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT,
				"Instead of destroy, list the expired and owner disabled/removed shareables in the service result. Defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "If any shareables are found, email the result to the recipient(s).", 0, 1));
		_defn.add(new Interface.Element("type", new EnumType(new String[] { "upload", "download", "both" }),
				"The type of shareable, upload or download or both (default). ", 0, 1));
		_defn.add(new Interface.Element("reason", new EnumType(new String[] { "expired", "owner-removed", "owner-disabled", "missing", "all"}),
				"Only find invalid shareables for the given reason. Defaults to expired,owner-removed and owner-disabled. The reasons are ORed, so any one of them will cause the shareable to be listed. ", 0, Integer.MAX_VALUE));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Find and optionally destroy invalid shareables. They may be invalid because they have 1) expired,2) the  owner user is disabled, 3) the owner user is removed and 4) the path (namespace or collection) is missing. This service is intended for administrators to find and clean up shareables owned by all users.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		boolean listOnly = args.booleanValue("list-only", false);
		String email = args.value("email");
		String type = args.stringValue("type", "both");
		//
		Collection<String> reasons = args.values("reason");
		Boolean showMissing = false;
		Boolean showRemoved = false;
		Boolean showDisabled = false;
		Boolean showExpired = false;
		if (reasons!=null) {
			for (String reason : reasons) {
				if (reason.equals ("missing")) {
					showMissing = true;
				} else if (reason.equals("expired")) {
					showExpired = true;
				} else if (reason.equals("owner-removed")) {
					showRemoved = true;
				} else if (reason.equals("owner-disabled")) {
					showDisabled = true;
				} else if (reason.equals("all")) {
					showExpired = true;
					showRemoved = true;
					showDisabled = true;
					showMissing = true;
				}
			}
		} else {
			showExpired = true;
			showRemoved = true;
			showDisabled = true;
		}
		if (!showMissing && !showExpired && !showRemoved && !showDisabled) {
			throw new Exception("You must specify at least one reason");
		}

		long idx = 1;
		long size = 100;
		boolean complete = false;
		int count = 0;
		int nbDownload = 0;
		int nbUpload = 0;
		StringBuilder sb = new StringBuilder();
		if (email != null) {
			sb.append("<html>");
			sb.append("<head>");
			sb.append("<style>");
			sb.append("table, th, td { border: 1px solid black; }");
			sb.append("</style>");
			sb.append("</head>");
			sb.append("<body>");
			sb.append(
					"<table><thead><tr><th>ID</th><th>Type</th><th>Name</th><th>Owner</th><th>Facility?</th><th>Expiry</th><th>Issue</th><th>Destroyed</th></tr></thead><tbody>");
		}
		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			Collection<String> sids = executor().execute("asset.shareable.all.list",
					"<args><idx>" + idx + "</idx><size>" + size + "</size></args>", null, null).values("shareable");
			int nbDestroyed = 0;
			if (sids != null && !sids.isEmpty()) {
				for (String sid : sids) {
					PluginTask.checkIfThreadTaskAborted();
					XmlDoc.Element se = executor()
							.execute("asset.shareable.describe", "<args><id>" + sid + "</id></args>", null, null)
							.element("shareable");
					Date validTo = se.dateValue("valid-to", null);
					boolean expired = false;
					if (showExpired) {
						expired = (validTo != null) && (validTo.before(new Date()));
					}
					boolean ownerRemoved = false;
					if (showRemoved) {
						ownerRemoved = !shareableOwnerExists(executor(), se);
					}
					boolean ownerDisabled = false;
					if (showDisabled) {
						ownerDisabled = ownerRemoved ? false : !shareableOwnerEnabled(executor(), se);
					}
					String shareableType = se.value("@type");
					//
					String collection  = se.value("collection");
					String namespace = se.value("namespace");
					Boolean pathMissing = pathIsMissing (executor(), namespace, collection);
					boolean typeMatches = type.equals("both") || type.equals(shareableType);
					if (typeMatches && ( (showExpired&&expired) || (showRemoved&&ownerRemoved) || 
							(showDisabled&&ownerDisabled) || (showMissing&&pathMissing))) {
						count += 1;
						if ("download".equals(shareableType)) {
							nbDownload += 1;
						}
						if ("upload".equals(shareableType)) {
							nbUpload += 1;
						}
						boolean destroyed = false;
						if (!listOnly) {
							PluginTask.checkIfThreadTaskAborted();
							SvcAssetShareableExpiredDestroy.destroyShareable(executor(), se);
							destroyed = true;
							nbDestroyed += 1;
						}
						String issue = "";
						if (showExpired&&expired) {
							issue = "expired";
						}
						if (showRemoved&&ownerRemoved) {
							if (issue.isEmpty()) {
								issue = "owner-removed";
							} else {
								issue += ",owner-removed";
							}	
						} 
						if (showDisabled&&ownerDisabled) {
							if (issue.isEmpty()) {
								issue = "owner-disabled";
							} else {
								issue += ",owner-disabled";
							}	
						} 
						if (pathMissing) {
							if (issue.isEmpty()) {
								issue = "missing";
							} else {
								issue += ",missing";
							}	
						}
						saveXml(executor(), se, issue, destroyed, namespace, collection, pathMissing, w);
						if (email != null) {
							saveHtml(se, issue, destroyed, sb);
						}
					}
				}
				idx += (size - nbDestroyed);
			} else {
				complete = true;
			}
		}
		w.add("count", new String[] { "upload", "" + nbUpload, "download", "" + nbDownload }, count);
		if (count > 0 && email != null) {
			sb.append("</tbody></table><br/>");
			sb.append("Invalid Upload Shareables: <b>").append(nbUpload).append("</b><br/>");
			sb.append("Invalid Download Shareables: <b>").append(nbDownload).append("</b><br/>");
			sb.append("Invalid Total: <b>").append(count).append("</b><br/>");
			sb.append("</body></html>");
			sendEmail(executor(), email, sb.toString(), count, !listOnly);
			w.add("sent-to", email);
		}
	}

	private static boolean shareableOwnerExists(ServiceExecutor executor, XmlDoc.Element se) throws Throwable {
		String domain = se.value("owner/domain");
		String user = se.value("owner/user");
		return executor.execute("user.exists",
				String.format("<args><domain>%s</domain><user>%s</user></args>", domain, user), null, null)
				.booleanValue("exists");
	}

	private static boolean shareableOwnerEnabled(ServiceExecutor executor, XmlDoc.Element se) throws Throwable {
		String domain = se.value("owner/domain");
		String user = se.value("owner/user");
		return executor
				.execute("authentication.user.enabled",
						String.format("<args><domain>%s</domain><user>%s</user></args>", domain, user), null, null)
				.booleanValue("enabled");
	}

	private static void saveXml(ServiceExecutor executor, XmlDoc.Element se, 
			String issue, boolean destroyed, String namespace, 
			String collection, Boolean pathMissing, XmlWriter w) throws Throwable {
		String id = se.value("@id");
		String type = se.value("@type");
		String name = se.value("name");
		String expiry = se.value("valid-to");
		String domain = se.value("owner/domain");
		String user = se.value("owner/user");
		String completionServiceName = se.value("completion-service/@name");
		boolean isFacilityShareable = completionServiceName != null
				&& (completionServiceName.startsWith("ds.facility.shareable.") ||
						completionServiceName.startsWith("vicnode.facility.shareable."));
		w.push("shareable", new String[] { "id", id, "type", type, "facility", Boolean.toString(isFacilityShareable),
				"destroyed", Boolean.toString(destroyed) });
		if (name != null) {
			w.add("name", name);
		} else {
			w.add("name", "missing");
		}
		if (namespace!=null) {
			w.add("namespace", namespace);
		} 
		if (collection!=null) {
			String path = null;
			if (!pathMissing) {
				path = unimelb.mf.plugin.util.AssetUtils.assetPathFromId(executor, collection);
				w.add("collection", new String[] {"path", path}, collection);
			} else {
				w.add("collection", new String[] {"path", "missing"}, collection);
			}
		} 
		
		// It's cactus, no namespace or collection
		if (namespace==null&&collection==null) {
			w.add("path", "missing");
		}
		w.add("owner", domain + ":" + user);
		if (expiry != null) {
			w.add("expiry", expiry);
		}
		w.add("issue", issue);
		w.pop();
	}

	private static void saveHtml(XmlDoc.Element se, String issue, boolean destroyed, StringBuilder sb)
			throws Throwable {
		String id = se.value("@id");
		String type = se.value("@type");
		String name = se.value("name");
		String expiry = se.value("valid-to");
		String domain = se.value("owner/domain");
		String user = se.value("owner/user");
		String owner = domain + ":" + user;
		String completionServiceName = se.value("completion-service/@name");
		boolean isFacilityShareable = completionServiceName != null
				&& (completionServiceName.startsWith("ds.facility.shareable.") ||
						completionServiceName.startsWith("vicnode.facility.shareable."));
		sb.append("<tr>");
		sb.append("<td>").append(id).append("</td>");
		sb.append("<td>").append(type).append("</td>");
		sb.append("<td>").append(name == null ? "&nbsp;" : name).append("</td>");
		sb.append("<td>").append(owner).append("</td>");
		sb.append("<td>").append(isFacilityShareable ? "Yes" : "No").append("</td>");
		sb.append("<td>").append(expiry == null ? "&nbsp;" : expiry).append("</td>");
		sb.append("<td>").append(issue).append("</td>");
		sb.append("<td>").append(destroyed).append("</td>");
		sb.append("</tr>");
	}

	private static void sendEmail(ServiceExecutor executor, String email, String htmlMessage, int nbInvalid,
			boolean destroyed) throws Throwable {
		String serverUUID = executor.execute("server.uuid").value("uuid");
		String subject = String.format("[%s] invalid asset shareable%s %s (%d)", serverUUID, nbInvalid > 1 ? "s" : "",
				destroyed ? "destroyed" : "found", nbInvalid);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("to", email);
		dm.add("subject", subject);
		dm.add("body", new String[] { "type", "text/html" }, htmlMessage);
		executor.execute("mail.send", dm.root());
	}

	private static Boolean  pathIsMissing (ServiceExecutor executor,
			String namespace, String collection) throws Throwable {
		if (collection!=null) {
			return !(AssetUtils.exists(executor, null, collection, false));
		} else if (namespace!=null) {
			return !(NameSpaceUtil.assetNameSpaceExists (executor, namespace));
		}
		return true;	// Have seen some shareables with neither namespace nor collection	
	}

}
