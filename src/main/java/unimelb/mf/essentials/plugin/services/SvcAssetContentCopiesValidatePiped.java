package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.PeerUtil;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;

public class SvcAssetContentCopiesValidatePiped extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.copies.validate.piped";

	private Interface _defn;
	String DEFAULT_NB_THREADS = "50";
	String SIZE = "1000";

	public SvcAssetContentCopiesValidatePiped() {
		_defn = new Interface();

		_defn.add(new Interface.Element("all-versions", BooleanType.DEFAULT,
				"If true, then validate the content for all versions (over-riding the -version attribute of :id). If false, then only validate the selected version. Defaults to false.",
				0,1));
		_defn.add(new Interface.Element("copy", IntegerType.DEFAULT,
				"The copy to check. If unspecified, then all copies are checked.", 0, Integer.MAX_VALUE));
		//
		Interface.Element where = new Interface.Element("where", StringType.DEFAULT, "The where clause to select assets.", 1, Integer.MAX_VALUE);
		_defn.add(where);
		//
		_defn.add(new Interface.Element("nb-threads", new IntegerType(1, 50),
				"Number of piped threads. Defaults to " + DEFAULT_NB_THREADS, 0, 1));

		// store
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"If specified, validate only content in this specified store(s).", 0, Integer.MAX_VALUE));	
		_defn.add(new Interface.Element("store-tag", StringType.DEFAULT,
				"If specified, validate only content in this specified store(s).", 0, Integer.MAX_VALUE));	
		//
		Interface.Element email = new Interface.Element("email", EmailAddressType.DEFAULT, "Notification address summarising the result.", 0, 1);
		Interface.Attribute a = new Interface.Attribute("only-if-issues", BooleanType.DEFAULT, "If true (default), only send the email if issues are found.", 0);
		email.add(a);
		_defn.add(email);

		_defn.add(new Interface.Element("must-match", BooleanType.DEFAULT,
				"If true, all of 'copy', 'store', 'store-tag' must match to select assets.  If false (default), assets will be selected if any of them match.",
				0, 1));	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Wrapper for unimelb.asset.content.copies.validate in that 1) it uses a piped query and 2) it only returns a result if there is an issue with an asset.  The result will show an :asset element for each version of an asset. So one asset with two versions means two :asset elements, one for each asset version.  The counts at the end refer then to asset*version counts. n-total is the total number of assets x versions. n-failed is the  number of assets x versions that failed.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		Boolean allVersions = args.booleanValue("all-versions", false);
		Collection<XmlDoc.Element> copy = args.elements("copy");
		Collection<String> wheres = args.values("where");
		String nbThreads = args.stringValue("nb-threads", DEFAULT_NB_THREADS);
		Collection<XmlDoc.Element> store = args.elements("store");
		Collection<XmlDoc.Element> storeTag = args.elements("store-tag");
		XmlDoc.Element email = args.element("email");
		Boolean matchAll = args.booleanValue("must-match", false);
		// 
		XmlDocMaker dm = new XmlDocMaker("args");
		for (String where : wheres) {
			dm.add("where", where);		
		}
		dm.add("pipe-nb-threads", nbThreads);
		dm.add("action", "pipe");
		dm.add("pipe-generate-result-xml", true);
		dm.add("as", "iterator");
		dm.push("service", new String[] {"name", "unimelb.asset.content.copies.validate"});
		if (copy!=null) {
			dm.addAll(copy);
		}
		dm.add("all-versions", allVersions);
		if (store!=null) {
			dm.addAll(store);
		}
		if (storeTag!=null) {
			dm.addAll(storeTag);
		}
		dm.add("must-match", matchAll);
		dm.pop();

		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return;
		}

		// Fetch the iterator
		long iteratorID = r.longValue("iterator");
		Boolean complete =false;
		dm = new XmlDocMaker("args");
		dm.add("id", iteratorID);
		dm.add("size", SIZE);

		// Iterate
		XmlDocMaker res = new XmlDocMaker("args");
		long nFailed = 0l;
		long nTotal = 0l;
		try {
			while (!complete) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element re = executor().execute("asset.query.iterate", dm.root());
				complete = re.booleanValue("iterated/@complete");
				Collection<XmlDoc.Element> assets = re.elements("asset");			

				// Parse results. 
				if (assets!=null) {
					nTotal += assets.size();
					for (XmlDoc.Element asset : assets) {
						PluginTask.checkIfThreadTaskAborted();
						String status = asset.value("status");
						if (status!=null && !status.equals("OK")) {
							res.add(asset);
							nFailed ++;
						}
					}
				}			
			}
		} catch (Throwable e) {
			// make sure the iterator is destroyed.
			executor().execute("asset.query.iterator.destroy",
					"<args><ignore-missing>true</ignore-missing><id>" + iteratorID + "</id></args>", null, null);
			throw e;
		}

		// Populate result to terminal
		if (res.root().hasSubElements()) {
			w.addAll(res.root().elements());
		}

		// email
		if (email!=null) {
			Boolean onlyIfIssues = email.booleanValue("@only-if-issues", true);
			if ( (onlyIfIssues&&nFailed>0) || !onlyIfIssues) {
				res.add("n-total", new String[] {"n-failed", ""+nFailed}, nTotal);

				res.push("service", new String[] {"name", name()});
				res.add(args);
				res.pop();
				String body = XmlPrintStream.outputToString(res.root());
				String uuid = PeerUtil.serverUUIDFromProute(null);
				String subject = "["+uuid+"] - unimelb.asset.content.copies.validate.piped content validation result (date="+new Date().toString()+")";
				MailUtils.sendEmail(executor(), email.value(), null, null, null, subject, body, false);
			}
		}
		//
		w.add("n-total", new String[] {"n-failed", ""+nFailed}, nTotal);
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
