package unimelb.mf.essentials.plugin.services;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetProcessingQueueEntryReexecute extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.processing.queue.entry.reexecute";

	public static final long DEFAULT_MAX_ENTRIES = 10000;
	public static final int DEFAULT_NB_THREADS = 1;
	public static final int DEFAULT_STEP_SIZE = 100;
	public static final int BLOCKING_QUEUE_CAPACITY = 100;

	private final Interface _defn;

	public SvcAssetProcessingQueueEntryReexecute() {
		_defn = new Interface();
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "The name of the asset processing queue.", 1, 1));
		_defn.add(new Interface.Element("max-entries", LongType.POSITIVE_ONE,
				"The maximum number of entries to reexecute. Defaults to " + DEFAULT_MAX_ENTRIES, 0, 1));
		_defn.add(new Interface.Element("nb-threads", new IntegerType(1, 255),
				"Number of threads to use. Defaults to " + DEFAULT_NB_THREADS, 0, 1));
		_defn.add(new Interface.Element("step-size", new IntegerType(1, 10000),
				"The size for asset.processing.queue.entry.describe service. Defaults to " + DEFAULT_STEP_SIZE, 0, 1));
		_defn.add(new Interface.Element("debug", BooleanType.DEFAULT, "Extra logging for debugging. Defaults to false",
				0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Re-execute remaining entries in the asset processing queue.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs arg2, XmlWriter w) throws Throwable {
		final String queueName = args.value("name");
		final long maxEntries = args.longValue("max-entries", DEFAULT_MAX_ENTRIES);
		final int nbThreads = args.intValue("nb-threads", DEFAULT_NB_THREADS);
		int stepSize = args.intValue("step-size", DEFAULT_STEP_SIZE);
		if (maxEntries < stepSize) {
			stepSize = (int) maxEntries;
		}
		final boolean debug = args.booleanValue("debug", false);
		final PluginLog log = PluginLog.log("unimelb-asset-processing-queue-entry-reexecute");

		final AtomicLong nbProcessed = new AtomicLong(0L);
		long nbSubmitted = 0L;

		// create blocking queue
		LinkedBlockingQueue<Entry> entries = new LinkedBlockingQueue<>(BLOCKING_QUEUE_CAPACITY);

		// initialize worker threads
		ExecutorService threadPool = Executors.newFixedThreadPool(nbThreads);

		boolean submittedAllEntries = false;

		try {
			for (int i = 0; i < nbThreads; i++) {
				threadPool.submit(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						try {
							Entry e;
							while ((e = entries.take()).isPoison == false) {
								if (debug) {
									log.add(PluginLog.WARNING, "Processing entry " + e.id);
								}
								reexecute(executor(), queueName, e.id);
								nbProcessed.getAndIncrement();
							}
							return null;
						} catch (Throwable e) {
							log.add(PluginLog.ERROR, "Worker thread error: " + e.getMessage(), e);
							if (e instanceof Exception) {
								throw (Exception) e;
							} else {
								throw new RuntimeException(e);
							}
						}
					}
				});
			}

			// add entries to blocking queue
			long after = 0;
			List<XmlDoc.Element> ees = describeQueueEntries(executor(), queueName, stepSize, after);
			while (ees != null && !ees.isEmpty() && nbSubmitted < maxEntries) {
				if (debug) {
					log.add(PluginLog.WARNING, "Adding " + ees.size() + " entries to the queue");
				}
				for (XmlDoc.Element ee : ees) {
					long entryId = ee.longValue("@id");
					entries.put(new Entry(entryId));
					after = entryId;
				}
				nbSubmitted += ees.size();
				
				PluginTask.checkIfThreadTaskAborted();
				if (debug) {
					log.add(PluginLog.WARNING, "Last added entry id: " + after);
					log.add(PluginLog.WARNING, "Processed " + nbProcessed.get() + " entries");
				}
				ees = describeQueueEntries(executor(), queueName, stepSize, after);
			}

			// send poisons to worker threads
			if (debug) {
				log.add(PluginLog.WARNING, "Adding poison entries...");
			}
			for (int i = 0; i < maxEntries; i++) {
				entries.put(Entry.newPoisonEntry());
			}
			submittedAllEntries = true;

			// wait for all workers to finish
			if (debug) {
				log.add(PluginLog.WARNING, "Shutting down the worker thread pool...");
			}
			threadPool.shutdown();
			if (debug) {
				log.add(PluginLog.WARNING, "Waiting for the worker threads to finish...");
			}
			threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

		} finally {
			if (!submittedAllEntries) {
				if (debug) {
					log.add(PluginLog.WARNING, "Abnormal termination. Adding poison entries...");
				}
				for (int i = 0; i < maxEntries; i++) {
					entries.put(Entry.newPoisonEntry());
				}
			}
			if (!threadPool.isShutdown()) {
				if (debug) {
					log.add(PluginLog.WARNING, "Abnormal termination. Shutting down the worker thread pool...");
				}
				threadPool.shutdown();
			}
			if (!threadPool.isTerminated()) {
				if (debug) {
					log.add(PluginLog.WARNING, "Abnormal termination. Awaiting thread pool termination...");
				}
				if (!threadPool.awaitTermination(1, TimeUnit.MINUTES)) {
					throw new Exception(SERVICE_NAME + ": failed to shutdown the worker thread pool in 1 minute.");
				}
			}
		}

		w.add("processed", nbProcessed.get());

	}

	private static List<XmlDoc.Element> describeQueueEntries(ServiceExecutor executor, String queueName, int stepSize,
			long after) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("size", stepSize);
		dm.add("name", queueName);
		if (after > 0) {
			dm.add("after", after);
		}
		return executor.execute("asset.processing.queue.entry.describe", dm.root()).elements("entry");
	}

	private static void reexecute(ServiceExecutor executor, String queueName, long id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("name", queueName);
		dm.add("id", id);
		dm.add("fail-on-error", true);
		executor.execute("asset.processing.queue.entry.reexecute", dm.root());
	}

	private static class Entry {
		public final long id;
		public final boolean isPoison;

		private Entry(long id, boolean isPoison) {
			this.id = id;
			this.isPoison = isPoison;
		}

		public Entry(long id) {
			this(id, false);
		}

		public static Entry newPoisonEntry() {
			return new Entry(-1, true);
		}
	}

}
