package unimelb.mf.essentials.plugin.services;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.model.Actor;

public class SvcAssetMetadataCSVDump extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.metadata.csv.dump";

	private Interface _defn;

	public SvcAssetMetadataCSVDump() {
		_defn = new Interface();
		SvcAssetMetadataCSVExport.addArgs(_defn);
		_defn.add(new Interface.Element("url", UrlType.DEFAULT,
				"Server accessible location for the output CSV file. The only supported output URLS are file paths.", 1,
				1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Exports metadata values to a CSV file on the server. NOTE: for security reasons, this service supposes to be executed by system-administrator. To export metadata for unprivileged users, use service: "
				+ SvcAssetMetadataCSVExport.SERVICE_NAME;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		/*
		 * parse arguments
		 */
		String argWhere = args.value("where");
		String argNamespace = args.value("namespace");
		// construct query
		StringBuilder sb = new StringBuilder();
		if (argWhere != null) {
			sb.append("(").append(argWhere).append(")");
		}
		if (argNamespace != null) {
			if (sb.length() > 0) {
				sb.append(" and ");
			}
			sb.append("(namespace>='").append(argNamespace).append("')");
		}

		final String where = sb.length() > 0 ? sb.toString() : null;
		final List<XmlDoc.Element> xpaths = args.elements("xpath");
		final int step = args.intValue("step", SvcAssetMetadataCSVExport.DEFAULT_PAGE_SIZE);
		final boolean compress = args.booleanValue("compress", false);

		// url
		URL url = new URL(args.value("url"));
		if (!"file".equalsIgnoreCase(url.getProtocol())) {
			throw new IllegalArgumentException("Unsupported URL protocol: " + url.getProtocol()
					+ ". Only server accessible file path is accepted.");
		}
		if (!Actor.Self.isSystemAdministrator(executor())) {
			throw new UnsupportedOperationException(
					"Only system-administrator is allowed to write to server file: " + url.getPath());
		}
		final Path outputPath = Paths.get(url.toURI());
		if (Files.exists(outputPath)) {
			// For security reason, do not allow overwriting existing files on the server.
			throw new IllegalArgumentException(url.toString() + " already exists.");
		}

		try (OutputStream os = Files.newOutputStream(outputPath);
				BufferedOutputStream bos = new BufferedOutputStream(os)) {
			if (compress) {
				try (GZIPOutputStream gos = new GZIPOutputStream(bos)) {
					SvcAssetMetadataCSVExport.exportCSV(executor(), where, xpaths, step, gos);
				}
			} else {
				SvcAssetMetadataCSVExport.exportCSV(executor(), where, xpaths, step, bos);
			}
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

}
