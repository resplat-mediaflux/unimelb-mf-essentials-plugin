package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentCopyAllCreate extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.copy.all.create";

	private Interface _defn;

	public SvcAssetContentCopyAllCreate() {
		_defn = new Interface();

		Interface.Element checkOnly = new Interface.Element("check-only", BooleanType.DEFAULT,
				"Checks and reports only, does not make any copies.  Reports asset versions that do not have content in the given store (arg 'store'). Args 'from-store' and 'copy' are irrelevant.. Defaults to true.", 0, 1);
		checkOnly.add(new Interface.Attribute("all",
				BooleanType.DEFAULT, 
				"By default, only reports versions missing the desired content copy.  If all is true, all versions are reported on regardless.'",
				0));
		_defn.add(checkOnly);
		_defn.add(new Interface.Element("count", BooleanType.DEFAULT,
				"Count the number of content copies per version (takes more time).  Default false.", 0,	1));
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("copy", StringType.DEFAULT,
				"The copy to duplicate. If unspecified, then system will choose an existing copy.", 0, 1));
		_defn.add(new Interface.Element("from-store", StringType.DEFAULT,
				"The name of a store to copy from. If specified, then the 'copy' argument is ignored.", 0, 1));
		_defn.add(
				new Interface.Element("store", StringType.DEFAULT, "The store to create the content copies in.", 1, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Iterates through all asset versions and makes a content copy for each version as required (handles the situaiton where there are few content versions than asset versions). Does not create copies for zero-sized content.  If the output include a -cid attribute it means it actually created the copy and that is the copy id. If the contgent copies already exist they are skipped.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String assetID = args.value("id");
		String copy = args.value("copy");
		String fromStore = args.value("from-store");
		String toStore = args.value("store");
		Boolean count = args.booleanValue("count", false);
		Boolean checkOnly = args.booleanValue("check-only", true);
		Boolean checkAll = args.booleanValue("check-only/@all", false);

		//
		Collection<String> versions = executor()
				.execute("asset.versions", "<args><id>" + assetID + "</id></args>", null, null)
				.values("asset/version/@n");

		// Iterate through versions and copy
		XmlDocMaker dmOut = new XmlDocMaker("args");
		dmOut.push("asset", new String[] { "id", assetID });
		Boolean some = false;           // SOmething from at least one version to show
		for (String version : versions) {
			PluginTask.checkIfThreadTaskAborted();
			//
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", new String[] { "version", version }, assetID);
			//
			XmlDoc.Element r = executor().execute("asset.get", dm.root());
			XmlDoc.Element content = r.element("asset/content");


			if (checkOnly) {
				if (content!=null) {
					// Not all versions will have content
					Long csize = content.longValue("size");

					Collection<XmlDoc.Element> contentCopies = content.elements("copies/copy"); 
					if (contentCopies!=null) {

						// Iterate through content copies for this asset version
						Boolean show = false;
						Boolean handled = false;
						String cid = null;
						for (XmlDoc.Element contentCopy : contentCopies) {
							String assetStore = contentCopy.value("store");
							if (assetStore==null) {
								// This means that we have a copy but no store. Can happen when  DR processes copies
								// a zero-sized asset so check that is the case and proceed as this is ok.
								if (csize==0) {
									handled = true;
								} else {
									throw new Exception("Asset version has content and non-zero size but no store");
								}
							} else {
								if (assetStore.equals(toStore)) {
									cid = contentCopy.value("@id");
									if (checkAll) {
										dmOut.add("version", new String[] {"store", toStore, "cid", cid }, version);
										show = true;
									}
									handled = true;
								}
							}
						}

						// THe target store was not found in any content copies
						if (!handled) {
							dmOut.add("version", new String[] {"store", toStore, "cid", "none"}, version);
							show = true;
						} 
						if (show) some = true;
					} else {
						// Only one version of the content (so no 'copy' element)
						// Sometimes when content is removed, the user can choose to leave 
						// evidence behind that the content was once there but has been removed
						XmlDoc.Element removed = content.element("removed");
						if (removed==null) {
							String assetStore = content.value("store");
							String cid = content.value("@id");
							if (assetStore==null) {
								// This means that we have content but no store. Can happen when  DR processes copies
								// a zero-sized asset so check that
								if (csize==0) {
									if (checkAll) {
										dmOut.add("version", new String[]{"store", "none", "zero-sized", "true"}, version);
										some = true;
									}
								} else {
									throw new Exception("Asset version has content and non-zero size but no store");
								}
							} else {
								if (assetStore.equals(toStore)) {
									if (checkAll) {
										dmOut.add("version", new String[] {"store", toStore, "cid", cid }, version);
										some = true;
									} 
								} else {
									some = true;
									dmOut.add("version", new String[] {"store", toStore, "cid", "none"}, version);
								}
							}
						}
					}	
				}
			} else {
				// Sometimes when content is removed, the user can choose to leave 
				// evidence behind that the content was once there but has been removed
				XmlDoc.Element removed = content.element("removed");
				if (content!=null && removed==null) {
					Long csize = content.longValue("size");
					if (csize>0) {
						// Although a content copy when there is no content is a NULLOP (so ok)
						// we don't want to make content copies of content which is already of
						// zero size (it's legitimate but makes a mess)
						dm = new XmlDocMaker("args");
						dm.add("id", new String[] { "version", version }, assetID);

						dm.add("store", toStore);
						if (fromStore != null) {
							dm.add("from-store", fromStore);
						}
						if (copy != null) {
							dm.add("copy", copy);
						}

						// Make the copy
						r = executor().execute("asset.content.copy.create", dm.root());
						String cid = r.value("cid");

						// Count number of content copies as a validation
						if (cid != null) {
							some = true;
							if (count) {
								dm = new XmlDocMaker("args");
								dm.add("id", new String[] { "version", version + "" }, assetID);
								r = executor().execute("asset.get", dm.root());
								Collection<XmlDoc.Element> t = r.elements("asset/content/copies/copy");

								if (t != null) {
									int nCopies = t.size();
									dmOut.add("version", new String[] { "cid", cid, "copies", nCopies + "" }, version);
								} else {
									dmOut.add("version", new String[] {"cid", cid }, version);
								}
							} else {
								dmOut.add("version", new String[] {"store", toStore, "cid", cid }, version);
							}
						}
					}
				}
			}
		}
		dmOut.pop();
		if (some) {
			w.addAll(dmOut.root().elements());
		}
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
