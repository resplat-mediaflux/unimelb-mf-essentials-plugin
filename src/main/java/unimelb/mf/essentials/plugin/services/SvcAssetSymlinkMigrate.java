package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class SvcAssetSymlinkMigrate extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.symlink.migrate";

    public static final String DEFAULT_TYPE = "posix/symlink";

    public static final int DEFAULT_STEP = 100;

    private final Interface _defn;

    public SvcAssetSymlinkMigrate() {
        _defn = new Interface();
        _defn.add(new Interface.Element("id", AssetType.DEFAULT,
                "Asset id.", 0, Integer.MAX_VALUE));
        _defn.add(new Interface.Element("where", StringType.DEFAULT,
                "Query to select the assets.", 0, 1));
        _defn.add(new Interface.Element("prune", BooleanType.DEFAULT,
                "Prune the old versions. Defaults to false.", 0, 1));
        _defn.add(new Interface.Element("type", StringType.DEFAULT,
                "Set asset type. ", 0, 1));
        _defn.add(new Interface.Element("show-errors", BooleanType.DEFAULT,
                "Show errors in service result. Defaults to false.", 0, 1));
        _defn.add(new Interface.Element("csize-check", BooleanType.DEFAULT,
                "Check asset content size. Skip the asset with error if its content size is greater than zero. Defaults to true.",
                0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Migrate symbolic link assets to the new style introduced in Mediaflux server 4.10.16.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        Collection<String> ids = args.values("id");
        String where = args.value("where");
        boolean prune = args.booleanValue("prune", false);
        String type = args.value("type");
        boolean showErrors = args.booleanValue("show-errors", false);
        boolean csizeCheck = args.booleanValue("csize-check", true);

        AtomicLong nbMigrated = new AtomicLong(0);
        AtomicLong nbErrors = new AtomicLong(0);

        if (ids != null && !ids.isEmpty()) {
            migrate(executor(), ids, type, prune, nbMigrated, nbErrors, showErrors, csizeCheck, w);
        }

        if (where != null) {
            migrate(executor(), where, type, prune, nbMigrated, nbErrors, showErrors, csizeCheck, w);
        }
        if (nbErrors.get() > 0) {
            w.add("failed", nbErrors.get());
        }
        w.add("migrated", nbMigrated.get());
    }

    /**
     * migrate list of assets.
     *
     * @param executor   service executor
     * @param assetIds   asset ids
     * @param type       asset mime type
     * @param prune      whether or not prune old verisons
     * @param nbMigrated counter to count number of migrated assets
     * @param nbErrors   counter to count number of assets failed with errors
     * @param showErrors display errors in service result
     * @param csizeCheck check the csize of the asset and skip the asset with error if its content size is greater than zero.
     * @param w          service result xml writer
     * @throws Throwable throwable
     */
    private static void migrate(ServiceExecutor executor, Collection<String> assetIds, String type, boolean prune,
                                AtomicLong nbMigrated, AtomicLong nbErrors, boolean showErrors, boolean csizeCheck,
                                XmlWriter w) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        for (String assetId : assetIds) {
            dm.add("id", assetId);
        }
        PluginTask.checkIfThreadTaskAborted();
        List<XmlDoc.Element> aes = executor.execute("asset.get", dm.root()).elements("asset");
        for (XmlDoc.Element ae : aes) {
            migrate(executor, ae, type, prune, nbMigrated, nbErrors, showErrors, csizeCheck, w);
        }
    }

    /**
     * migrate assets selected by a query.
     *
     * @param executor   service executor
     * @param where      query to select the assets.
     * @param type       asset mime type
     * @param prune      whether or not prune the old versions
     * @param nbMigrated counter to count number of migrated assets
     * @param nbErrors   counter to count number of assets failed with errors
     * @param showErrors display errors in service result
     * @param csizeCheck check the csize of the asset and skip the asset with error if its content size is greater than zero.
     * @param w          service result xml writer
     * @throws Throwable throwable
     */
    private static void migrate(ServiceExecutor executor, String where, String type, boolean prune,
                                AtomicLong nbMigrated, AtomicLong nbErrors, boolean showErrors, boolean csizeCheck,
                                XmlWriter w) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", String.format("(%s) and asset has content and type='%s'", where, DEFAULT_TYPE));
        dm.add("as", "iterator");
        dm.add("action", "get-meta");
        PluginTask.checkIfThreadTaskAborted();
        long iteratorId = executor.execute("asset.query", dm.root()).longValue("iterator");

        boolean complete = false;
        dm = new XmlDocMaker("args");
        dm.add("id", iteratorId);
        dm.add("size", DEFAULT_STEP);
        try {
            while (!complete) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element re = executor.execute("asset.query.iterate", dm.root());
                complete = re.booleanValue("iterated/@complete");
                List<XmlDoc.Element> aes = re.elements("asset");
                if (aes != null) {
                    for (XmlDoc.Element ae : aes) {
                        migrate(executor, ae, type, prune, nbMigrated, nbErrors, showErrors, csizeCheck, w);
                    }
                }
            }
        } catch (Throwable e) {
            // make sure the iterator is destroyed.
            executor.execute("asset.query.iterator.destroy",
                    "<args><ignore-missing>true</ignore-missing><id>" + iteratorId + "</id></args>", null, null);
            throw e;
        }
    }

    /**
     * migrate the asset.
     *
     * @param executor   service executor
     * @param ae         asset meta data
     * @param type       asset mime type
     * @param prune      prune old versions?
     * @param nbMigrated counter to count number of migrated assets.
     * @param nbErrors   counter to count number of assets failed with errors
     * @param showErrors display errors in service result
     * @param csizeCheck check the csize of the asset and skip the asset with error if its content size is greater than zero.
     * @param w          service result xml writer
     * @throws Throwable throwable
     */
    private static void migrate(ServiceExecutor executor, XmlDoc.Element ae, String type, boolean prune,
                                AtomicLong nbMigrated, AtomicLong nbErrors, boolean showErrors, boolean csizeCheck,
                                XmlWriter w) throws Throwable {
        String assetId = ae.value("@id");
        String assetPath = ae.value("path");
        String assetType = ae.value("type");
        if (!DEFAULT_TYPE.equals(assetType)) {
            String error = String.format("unexpected asset/type: %s (expects %s)", assetType, DEFAULT_TYPE);
            // log the error
            PluginLog.log().add(PluginLog.WARNING,
                    String.format("[%s: id=%s, path=%s] %s", SERVICE_NAME, assetId, assetPath, error));
            if (showErrors) {
                // add error to the result
                w.add("error", new String[]{"id", assetId, "path", assetPath}, error);
            }
            nbErrors.getAndIncrement();
            return;
        }
        String url = ae.value("content/url");
        if (url == null || !url.startsWith("file:") || url.length() <= 5) {
            String error = "unexpected value. asset/content/url: " + url;
            // log the error
            PluginLog.log().add(PluginLog.WARNING,
                    String.format("[%s: id=%s, path=%s] %s", SERVICE_NAME, assetId, assetPath, error));
            if (showErrors) {
                // add error to the result
                w.add("error", new String[]{"id", assetId, "path", assetPath}, error);
            }
            nbErrors.getAndIncrement();
            return;
        }
        String referent = url.substring(5);
        boolean managed = ae.booleanValue("content/url/@managed");
        if (managed) {
            String error = "unexpected value. asset/content/url/@managed: true.";
            // log the error
            PluginLog.log().add(PluginLog.WARNING,
                    String.format("[%s: id=%s, path=%s] %s", SERVICE_NAME, assetId, assetPath, error));
            if (showErrors) {
                w.add("error", new String[]{"id", assetId, "path", assetPath}, error);
            }
            nbErrors.getAndIncrement();
            return;
        }
        long csize = ae.longValue("content/size", 0);
        if (csize > 0 && csizeCheck) {
            String error = "unexpected value. asset/content/size: " + csize + ". (expecting 0)";
            // log the error
            PluginLog.log().add(PluginLog.WARNING,
                    String.format("[%s: id=%s, path=%s] %s", SERVICE_NAME, assetId, assetPath, error));
            if (showErrors) {
                w.add("error", new String[]{"id", assetId, "path", assetPath}, error);
            }
            nbErrors.getAndIncrement();
            return;
        }

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        if (type != null) {
            dm.add("type", type);
        }

        dm.push("link");
        dm.add("posix-symlink", referent);
        dm.pop();

        PluginTask.checkIfThreadTaskAborted();
        executor.execute("asset.set", dm.root());

        dm = new XmlDocMaker("args");
        dm.add("retain", 1);
        dm.add("id", assetId);

        if (prune) {
            PluginTask.checkIfThreadTaskAborted();
            executor.execute("asset.prune", dm.root());
        }
        nbMigrated.getAndIncrement();
    }
}
