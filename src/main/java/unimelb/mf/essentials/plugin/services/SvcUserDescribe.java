package unimelb.mf.essentials.plugin.services;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.UrlType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;
import unimelb.utils.XmlUtils;

public class SvcUserDescribe extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.describe";

	private Interface _defn;

	public SvcUserDescribe() {

		_defn = new Interface();

		Interface.Element role = new Interface.Element("role", new StringType(128),
				"Only users holding these roles (AND not OR)  will be included.", 0, Integer.MAX_VALUE);
		role.add(new Interface.Attribute("type", new StringType(64), "Role type. Defaults to role", 0));
		_defn.add(role);
		Interface.Element domain = new Interface.Element("domain", StringType.DEFAULT, "Authentication domain of interest.  If not specified, all ENABLED domains are considered.  Domains are also described in the service output.",
				0, Integer.MAX_VALUE);
		_defn.add(domain);

		_defn.add(new Interface.Element("include-disabled", BooleanType.DEFAULT,
				"Include disabled users. Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("url", UrlType.DEFAULT, "If specified, directory on the server to save the output as an XML file. The file will be named users-YYYYMMDD-hhmmss.xml.  Of the form file:<path>", 0, 1));
		_defn.add(new Interface.Element("list", BooleanType.DEFAULT,
				"List mode (default false) just lists the users.", 0, 1));

	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public String description() {
		return "Describes users with permissions and save to server side XML file.  Two files are saved, one describing the users and one describing the domain itself.  This service also describes the domains that are requested, so there are two primary output element types, 'domain' and 'user'";
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Parse
		Collection<String> domains = args.values("domain");
		if (domains==null) {
			domains = listDomains(executor(), w);
			if (domains==null) {
				throw new Exception ("No ENABLED authebntication domains found");
			}
		}
		Boolean includeDisabled = args.booleanValue("include-disabled", false);

		// Complements what we get with unimelb.user.list which requires a role to be given
		Boolean list = args.booleanValue("list", false);
		Collection<XmlDoc.Element> roles = args.elements("role");
		// Server-side URL
		String url = args.value("url");
		String urlPath = null;
		if (url!=null) {
			if (!url.substring(0,5).equals("file:")) {
				throw new Exception ("url must be of the form 'file:<path>'");
			}
			urlPath = url.substring(5);
			File outDir = new File(urlPath);
			if (!outDir.exists()) {
				throw new IOException ("Path '" + urlPath + "' does not exist");
			}
			if (!outDir.isDirectory()) {
				throw new IOException ("Path '" + urlPath + "' is not a directory");
			}
		}

		// Iterate through domains
		XmlDocMaker dmOutUsers = new XmlDocMaker("users");
		XmlDocMaker dmOutDomains = new XmlDocMaker("domain");
		for (String domain : domains) {
			XmlDocMaker dm = new XmlDocMaker("args");
			XmlDoc.Element r = null;
			if (list) {
				dm.add("domain", domain);
				dm.add("size", "infinity");
				r = executor().execute("authentication.user.list", dm.root());
			} else {
				dm.add("domain", domain);
				dm.add("permissions", true);
				if (roles!=null) {
					for (XmlDoc.Element role : roles) {
						dm.add(role);
					}
				}
				dm.add("size", "infinity");
				r = executor().execute("user.describe", dm.root());
			}
			// Parse users
			Collection<XmlDoc.Element>users = r.elements("user"); 
			if (users!=null) {
				for (XmlDoc.Element user : users) {
					Boolean enabled = user.booleanValue("@enabled",  true);

					// Filter
					Boolean keep = false;
					if (includeDisabled) {
						keep = true;
					} else {
						if (enabled) {
							keep = true;
						}
					}
					//
					if (keep) {
						dmOutUsers.add(user);
					}
				}
			}

			// Describe domain and permissions. We need to combine
			// them both into one 'domain' parent.

			XmlDocMaker dmOut = new XmlDocMaker("args");
			dmOut.push("domain", new String[] {"name", domain});
			dm = new XmlDocMaker("args");
			dm.add("domain", domain);
			r = executor().execute("authentication.domain.describe", dm.root());
			dmOut.addAll(r.element("domain").elements());

			// Add permissions
			dm = new XmlDocMaker("args");
			dm.add("name", domain);
			dm.add("type", "domain");
			r = executor().execute("actor.describe", dm.root());
			dmOut.push("permissions");
			dmOut.addAll(r.elements("actor"));
			dmOut.pop();
			dmOut.pop();

			// Combine
			dmOutDomains.add(dmOut.root().element("domain"));
		}

		// Outputs
		w.addAll(dmOutDomains.root().elements());
		w.addAll(dmOutUsers.root().elements());

		// Write server side XML file
		if (url != null) {

			// Users
			String today = DateUtil.todaysTime(2);
			{
				String fileName = urlPath + "/users-" + today + ".xml";
				File file = new File(fileName);
				XmlUtils.writeServerSideXMLFile(dmOutUsers.root(), file);
				w.add("file", fileName);
			}

			// Domains
			{
				String fileName = urlPath + "/domains-" + today + ".xml";
				File file = new File(fileName);
				XmlUtils.writeServerSideXMLFile(dmOutDomains.root(), file);
				w.add("file", fileName);
			}

		}	
	}


	private static Collection<String> listDomains (ServiceExecutor executor, XmlWriter w) throws Throwable {
		XmlDoc.Element r = executor.execute("authentication.domain.list");
		if (r==null) {
			return null;
		}
		Collection<XmlDoc.Element> ts = r.elements();
		Vector<String> domains = new Vector<String>();
		if (ts!=null) {
			for (XmlDoc.Element t : ts) { 
				Boolean enabled = t.booleanValue("@enabled", true);
				if (enabled) {
					domains.add(t.value());
				}
			}
		}
		return domains;
	}

}
