package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetReplicaNeedToReplicateAllVersions extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.replica.need.to.replicate.all-versions";

    private Interface _defn;

    public SvcAssetReplicaNeedToReplicateAllVersions() {
        _defn = new Interface();

        _defn.add(new Interface.Element("id", AssetType.DEFAULT, "The path to the asset.", 1, Integer.MAX_VALUE));
        _defn.add(new Interface.Element("peer", StringType.DEFAULT, "The name of the peer. If not specified, all discoverable peers will be used.", 0, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Checks all versions of an asset for the need to replicate (with asset.replica.need.to.replicate).";
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

  
        Collection<String> ids = args.values("id");
        String peer = args.value("peer");
        for (String id : ids) {
            PluginTask.checkIfThreadTaskAborted();
            Collection<String> versions = executor()
                    .execute("asset.versions", "<args><id>" + id + "</id></args>", null, null)
                    .values("asset/version/@n");
            for (String version : versions) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDocMaker dm = new XmlDocMaker("args");
                dm.add("id", new String[] { "version", version }, id);
                if (peer!=null) {
                	dm.push("peer");
                	dm.add("name", peer);
                	dm.pop();
                }
                XmlDoc.Element re = executor().execute("asset.replica.need.to.replicate", dm.root());
                w.add(re.element("asset"));
            }
        }
    }

	
	
	
    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
