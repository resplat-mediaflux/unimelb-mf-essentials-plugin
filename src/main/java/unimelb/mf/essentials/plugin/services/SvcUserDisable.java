package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlPrintStream;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;
import unimelb.mf.essentials.plugin.util.ServerDetails;
import unimelb.utils.DateUtil;

public class SvcUserDisable extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.disable";
	private static final long WARNING_PERIOD = 30l;     // days

	private Interface _defn;

	public SvcUserDisable() {
		_defn = new Interface();
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The authentication domain the user belongs to", 1,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("check-only",  BooleanType.DEFAULT, "Don't actually disable accounts, check only. Defaults to true.", 0, 1));
		_defn.add(new Interface.Element("email",  StringType.DEFAULT, "Notify this email (e.g. ops email) of any disabled accounts (default false).", 0, Integer.MAX_VALUE));
		Interface.Element el = new Interface.Element("notify-user",  BooleanType.DEFAULT, "Notify user their account has been disabled (default false).", 0, 1);
		el.add(new Interface.Attribute("server-name", StringType.DEFAULT, "Mediaflux server name for email message to user.", 0));
		el.add(new Interface.Attribute("tcs-url", StringType.DEFAULT, "Advise user to read our terms and conditions at this URL when they request account be enabled.", 0));
		_defn.add(el);
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Examines meta-data (unimelb-admin:user-account) set on local user accounts in the given domains, and warns the user if the account is going to be disabled in 30 days or disables the account if the current date is after  the expiry date.  The operations team can be notified by specifying the email argument. The disabled accounts users can be optionally notified.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		Collection<String> domains = args.values("domain");
		XmlDoc.Element notifyUser = args.element("notify-user");
		Boolean checkOnly = args.booleanValue("check-only", true);
		Collection<String> emails  = args.values("email");
		//

		// Iterate through domains
		String from = Properties.getServerProperty(executor(), "mail.from");
		XmlDocMaker dmw2 = new XmlDocMaker("args");
		for (String domain : domains) {
			XmlDocMaker dm = new XmlDocMaker("args");		
			dm.add("domain", domain);
			dm.add("size", "infinity");
			XmlDoc.Element r = executor().execute("user.describe", dm.root());
			if (r!=null) {
				Collection<XmlDoc.Element> users = r.elements("user");
				if (users!=null) {
					handleUsers (executor(), from, users, notifyUser, checkOnly,  dmw2, w);
				}
			}
		}

		// Send notifications to ops
		if (emails!=null && dmw2.root().hasSubElements()) {
			String uuid = ServerDetails.serverUUID(executor());
			String msg = XmlPrintStream.outputToString(dmw2.root());
			String subject = "[" + uuid + "] Mediaflux Accounts Disabled Summary";
			MailUtils.sendEmail(executor(), emails, null, null, from, subject, msg, 
					null, null, 0, null, true);
		}		
	}

	private void handleUsers (ServiceExecutor executor, String from, Collection<XmlDoc.Element> users, XmlDoc.Element notifyUser, Boolean checkOnly,
			XmlDocMaker dmw2, XmlWriter w) throws Throwable {

		Boolean notifyUserB = notifyUser.booleanValue(false);
		String serverName = Properties.getServerProperty(executor(), "asset.shareable.address");
		String tcsUrl = Properties.getApplicationProperty(executor(), "documentation.user.rcs-tos", "project-provisioning");
		if (notifyUserB) {
			notifyUser.value("@server-name");
			notifyUser.value("@tcs-url");
		}

		// Iterate
		Date now = new Date();
		for (XmlDoc.Element user : users) {
			XmlDocMaker dmw = new XmlDocMaker("args");
			String protocol = user.value("@protocol");
			if (protocol!=null && protocol.equals("ldap")) {
				throw new Exception ("You cannot disable LDAP account users.");
			}
			Boolean isEnabled = user.booleanValue("@enabled",  true);
			dmw.push("user");
			dmw.add("domain", user.value("@domain"));
			dmw.add("user", user.value("@user"));
			dmw.add("is-enabled", isEnabled);
			dmw.add("todays-date", DateUtil.formatDate(now,  "dd-MMM-yyyy"));

			Boolean wasDisabled = false;
			Boolean wasWarned = false;
			if (isEnabled) {
				Date disableDate = user.dateValue("asset/meta/unimelb-admin:user-account/disable");		
				if (disableDate!=null) {
					long dateDiff = DateUtil.dateDifferenceInDays(disableDate, now);
					dmw.add("disable-date", DateUtil.formatDate(disableDate, "dd-MMM-yyyy"));
					if (now.after(disableDate)) {
						wasDisabled = true;
						if (checkOnly) {
							dmw.add("disabled-status", "would be disabled");					
						} else {
							disableUser (executor(), user);
							dmw.add("disabled-status", "disabled");	
							if (notifyUserB) {
								notifyUser (executor(), false, disableDate, serverName, tcsUrl, user, from);
								dmw.add("user-notified", new String[]{"email", user.value("e-mail")}, true);
							}
						}
					} else if (dateDiff<WARNING_PERIOD) {
						wasWarned = true;
						// They will get as many notifications as times the
						// service is run (typically weekly)
						if (checkOnly) {
							dmw.add("warning-status", "would-be-warned");
						} else {
							dmw.add("warning-status", "warned");
							if (notifyUserB) {
								notifyUser (executor(), true, disableDate, serverName, tcsUrl, user, from);
								dmw.add("user-notified", new String[]{"email", user.value("e-mail")}, true);
							}
						}
					} else {
						dmw.add("disabled-status", "not scheduled yet");
					}
				} else {
					dmw.add("disable-date", "none");
				}
			} 
			dmw.pop();
			// Service output
			if (wasWarned || wasDisabled) {
				w.addAll(dmw.root().elements());
			}
			// email List for the ops team. They don't
			// need to see the warnings
			if (!checkOnly && wasDisabled) {
				dmw2.addAll(dmw.root().elements());
			}
		}

	}

	private void notifyUser (ServiceExecutor executor, Boolean warningOnly, Date disableDate,
			String serverName, String tcsURL, XmlDoc.Element user, String from) throws Throwable {
		String email = user.value("e-mail");
		String userName =  user.value("@domain") + ":" + user.value("@user");
		Vector<String> l = new Vector<String>();
		l.add(email);
		//
		if (warningOnly) {
			String dateS = DateUtil.formatDate(disableDate, false, false);
			String subject = "Mediaflux account " + userName + " will be disabled on " + dateS;
			String msg = "Dear " + user.value("name") + 
					"\n\n Your Mediaflux account " + userName + " on the server " + serverName + 
					"\n will be disabled on " + dateS + " when its expiry date is reached."+
					"\n\n To apply for the account to be re-enabled, please send an email to " +
					"\n your University of Melbourne research contact (the person that initially requested" +
					"\n this account be created) and ask them to submit a ServiceNow ticket requesting " +
					"\n that your account be re-enabled for another year." +
					"\n\n You must also advise your contact that you accept our Terms and Conditions " +
					"\n which you can read on the web page : " + tcsURL +		
					"\n\n regards \n Mediaflux Operations Team \n Research Computing Services \n The University of Melbourne";	
			MailUtils.sendEmail (executor, l, null, null, from, subject, msg, null, null, 0, null, false);						
		} else {
			String subject = "Mediaflux account " + userName + " disabled";
			String msg = "Dear " + user.value("name") + 
					"\n\n Your Mediaflux account " + userName + " on the server " + serverName + 
					"\n has been disabled as it's expiry date has been reached." +
					"\n\n To apply for the account to be re-enabled, please send an email to " +
					"\n your University of Melbourne research contact (the person that initially requested" +
					"\n this account be created) and ask them to submit a ServiceNow ticket requesting " +
					"\n that your account be re-enabled for another year." +
					"\n\n You must also advise your contact that you accept our Terms and Conditions " +
					"\n which you can read on the web page : " + tcsURL +		
					"\n\n regards \n Mediaflux Operations Team \n Research Computing Services \n The University of Melbourne";
			MailUtils.sendEmail (executor, l, null, null, from, subject, msg, null, null, 0, null, false);						
		}
	}

	private void disableUser (ServiceExecutor executor, XmlDoc.Element user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		String authority = user.value("@authority");
		String protocol = user.value("@protocol");
		if (authority!=null) {
			// Will cope if protocol null
			dm.add("authority", new String[] {"protocol", protocol}, authority);
		} 
		dm.add("domain", user.value("@domain"));
		dm.add("user", user.value("@user"));
		executor.execute("authentication.user.disable", dm.root());
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}