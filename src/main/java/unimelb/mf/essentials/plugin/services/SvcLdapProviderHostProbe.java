package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.DateUtil;

public class SvcLdapProviderHostProbe extends PluginService {

	public static final String SERVICE_NAME = "unimelb.ldap.provider.host.probe";

	private Interface _defn;

	public SvcLdapProviderHostProbe() {
		_defn = new Interface();
		_defn.add(new Interface.Element("provider", StringType.DEFAULT, "The name of the LDAP provider.", 1,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("domain", StringType.DEFAULT, "The user domain.", 1, 1));
		_defn.add(new Interface.Element("user", StringType.DEFAULT, "The user name.", 1, 1));
		_defn.add(new Interface.Element("role", StringType.DEFAULT, "Role to test for.", 1, 1));
		_defn.add(new Interface.Element("n-inner", IntegerType.DEFAULT, "The number of inner loop (actor.describe) iterations per provider host.", 1,
				1));
		_defn.add(new Interface.Element("n-outer", IntegerType.DEFAULT, "The number of outer loop iterations. For every outer loop iteration, we iterate over all hosts, and for each host  we iterate n-inner times calling actor.have.", 1,
				1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Test LDAP provider hosts by iterating over provider hosts and running actor.have on the given LDAP user with the provided role. Builds and displays a failure profile per host.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		String provider  = args.value("provider");
		String domain = args.value("domain");
		String user = args.value("user");
		String role = args.value("role");
		String name = domain+":"+user;
		//
		Integer nInner = args.intValue("n-inner");
		Integer nOuter = args.intValue("n-outer");

		// Get provider 
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("provider", provider);
		XmlDoc.Element p = executor().execute("ldap.provider.describe", dm.root());

		// Get all current hosts
		Collection<String> hosts = p.values("provider/host");
		if (hosts==null) {
			throw new Exception ("No provider hosts found");
		}

		// Map of failures.  Key is host
		HashMap<String,Integer> mapErrors = new HashMap<String,Integer>();
		long nErrors = 0L;
		long nSuccesses = 0L;
		long maxErrors = 1000L;
		long maxSuccesses = 10000L;
		long nExecuted = 0;

		for (int j=0;j<nOuter;j++) {
			PluginService.checkIfThreadTaskAborted();

//			w.push("outer");
//			w.add("n", j);
			for (String host : hosts) {
				PluginService.checkIfThreadTaskAborted();

//				w.add("host", host);

				// Modify provider to be just this host
				dm = new XmlDocMaker("args");
				dm.add("provider", provider);
				dm.add("host", host);
				executor().execute("ldap.provider.modify", dm.root());

				// Execute inner loop
				for (int i=0;i<nInner;i++) {
					PluginService.checkIfThreadTaskAborted();
					try {
						dm = new XmlDocMaker("args");
						dm.add("name", name);
						dm.add("type", "user");
						Date d1 = new Date();
						dm.add("role", new String[] {"type", "role"}, role);
						executor().execute("actor.have", dm.root());
						Date d2 = new Date();
						if (nSuccesses<maxSuccesses) {
							String t1 = DateUtil.formatDate(d1, "dd-MMM-yyyy HH:mm:ss.ssss");
							String t2 = DateUtil.formatDate(d2, "dd-MMM-yyyy HH:mm:ss.ssss");
							w.add("success", new String[] {"before", t1, "after", t2}, nExecuted);
						}
						nSuccesses++;

					} catch (Throwable e) {
						nErrors++;
						// There was a failure for this host. Add to the count
						Boolean hasKey = mapErrors.containsKey(host);
						if (hasKey) {
							Integer n = mapErrors.get(host);
							mapErrors.put(host,++n);
						} else {
							mapErrors.put(host,1);
						}
						if (nErrors<maxErrors) {
							w.add("error", e.getMessage());
						}
					}
					nExecuted++;
				}
			}
//			w.pop();
		}


		// Update hosts back to current set
		dm = new XmlDocMaker("args");
		dm.add("provider", provider);
		executor().execute("unimelb.ldap.provider.host.update", dm.root());

		w.add("n-outer", nOuter);
		w.add("n-inner", nInner);;

		// List results
		Set<String> keys = mapErrors.keySet();
		if (keys.size()==0) {
			w.add("failures", "none");
		} else {
			w.push("failures");
			for (String key : keys) {
				w.add("host", new String[]{"nfail", ""+mapErrors.get(key)}, key);
			}
			w.pop();
		}
	}


	@Override
	public String name() {
		return SERVICE_NAME;
	}

	
	@Override
	public boolean canBeAborted() {
		return true;
	}

}
