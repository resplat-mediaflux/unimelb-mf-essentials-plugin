package unimelb.mf.essentials.plugin.services;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.streams.StreamCopy;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

// TODO test and enable
public class SvcNotificationSend extends PluginService {

    public static final String SERVICE_NAME = "unimelb.notification.send";

    private Interface _defn;

    public SvcNotificationSend() {
        _defn = new Interface();

        _defn.add(new Interface.Element("from", EmailAddressType.DEFAULT,
                "The reply address. If not set, uses default for the domain.", 0, 1));

        _defn.add(new Interface.Element("reply-to", EmailAddressType.DEFAULT, "he reply addresses, if any.", 0,
                Integer.MAX_VALUE));

        _defn.add(new Interface.Element("subject", StringType.DEFAULT, "Message subject.", 0, 1));

        Interface.Element role = new Interface.Element("role", StringType.DEFAULT,
                "Recipient role. If specified, users holding this role will be recipients.", 0, Integer.MAX_VALUE);
        role.add(new Interface.Attribute("type", StringType.DEFAULT, "Role type.", 1));
        _defn.add(role);

        Interface.Element user = new Interface.Element("user", StringType.DEFAULT, "Recipient user.", 0,
                Integer.MAX_VALUE);
        user.add(new Interface.Element("domain", StringType.DEFAULT, "Authentication domain.", 1, 1));
        Interface.Element authority = new Interface.Element("authority", StringType.DEFAULT, "Authority.", 0, 1);
        authority.add(new Interface.Attribute("protocol", StringType.DEFAULT, "Protocol", 0));
        user.add(authority);
        user.add(new Interface.Element("user", StringType.DEFAULT, "User name.", 0, 1));
        _defn.add(user);

        _defn.add(new Interface.Element("email", EmailAddressType.DEFAULT, "Recipient email address.", 0,
                Integer.MAX_VALUE));

        _defn.add(new Interface.Element("body", StringType.DEFAULT,
                "Message body. If not specified, the message body will read from service input. Therefore, either this argument or the service input must be supplied.",
                0, 1));

    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Send notification emails to the users.";
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        String body = args.value("body");
        if (body == null && (inputs == null || inputs.size() <= 0)) {
            throw new IllegalArgumentException("Missing message body. Expects either body argument or service input.");
        }
        if (body != null && inputs != null && inputs.size() > 0) {
            throw new IllegalArgumentException("Both body argument and service input are supplied. Expects only one.");
        }
        Set<String> emails = new LinkedHashSet<String>();
        if (args.elementExists("role")) {
            XmlDocMaker dm = new XmlDocMaker("args");
            dm.addAll(args.elements("role"));
            dm.add("include-disabled", false);
            dm.add("distinct", true);
            Collection<String> es = executor().execute(SvcUserEmailList.SERVICE_NAME, dm.root()).values("email");
            if (es != null && !es.isEmpty()) {
                emails.addAll(es);
            }
            PluginTask.checkIfThreadTaskAborted();
        }
        if (args.elementExists("user")) {
            List<XmlDoc.Element> ues = args.elements("user");
            for (XmlDoc.Element ue : ues) {
                XmlDocMaker dm = new XmlDocMaker("args");
                dm.add(ue, false);
                XmlDoc.Element re = executor().execute("user.describe", dm.root());
                String email = re.value("user/e-mail");
                if (email == null) {
                    email = re.value("user/meta/mf-user/email");
                }
                if (email != null) {
                    emails.add(email.toLowerCase().trim());
                }
            }
            PluginTask.checkIfThreadTaskAborted();
        }
        if (args.elementExists("email")) {
            Collection<String> es = args.values("email");
            for (String e : es) {
                if (e != null) {
                    emails.add(e.trim().toLowerCase());
                }
            }
        }

        String mimeType = "text/plain";
        if (body == null) {
            PluginService.Input in = inputs.input(0);
            if (in.mimeType() != null) {
                mimeType = in.mimeType();
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                StreamCopy.copy(in.stream(), baos);
                body = baos.toString();
            } finally {
                in.stream().close();
                in.close();
                baos.close();
            }
        }

        if (emails.isEmpty()) {
            return;
        }

        for (String email : emails) {
            XmlDocMaker dm = new XmlDocMaker("args");
            if (args.elementExists("from")) {
                dm.add("from", args.value("from"));
            }
            if (args.elementExists("reply-to")) {
                dm.addAll(args.elements("reply-to"));
            }
            if (args.elementExists("subject")) {
                dm.add(args.element("subject"));
            }
            dm.add("to", email);
            dm.add("body", new String[] { "type", mimeType }, body);
            executor().execute("mail.send", dm.root());
            w.add("sent", new String[] { "to", email });
            PluginTask.checkIfThreadTaskAborted();
        }
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public int minNumberOfInputs() {
        return 0;
    }

    @Override
    public int maxNumberOfInputs() {
        return 1;
    }
}