package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcAssetContentZeroSizeVersionOnePrune extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.zerosize.v1.prune";

	private Interface _defn;

	public SvcAssetContentZeroSizeVersionOnePrune() {
		_defn = new Interface();

		_defn.add(new Interface.Element("prune", BooleanType.DEFAULT, "If true prune version 1.  The default is false.", 0, 1));
		_defn.add(new Interface.Element("verbose", BooleanType.DEFAULT, "By default, only return a result if the asset is suitable for pruning.", 0, 1));
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The asset ID", 1, 1));


	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Very specialised service to assess if asset has 1) multiple versions and 2) version 1 has content but zero size and a zero checksum (causes a check-sum mismatch) and optionally prune version 1.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		Boolean prune = args.booleanValue("prune", false);
		Boolean verbose = args.booleanValue("verbose", false);
		String id = args.value("id");

		// Get version info
		Collection<XmlDoc.Element> versions = AssetUtils.versions(executor(), id);

		// Must have at least 2 versions to be considered
		Integer nVersions = versions.size();
		if (nVersions<=1) {
			if (verbose) {
				w.add("not-considered", "only-one-version");
			}
			return;
		}

		// Must have a specific version 1 to be considered
		Boolean hasOne = false;
		for (XmlDoc.Element version : versions) {
			String v  = version.value("@n");
			if (v.equals("1")) {
				hasOne = true;
			}
		}
		if (!hasOne) {
			if (verbose) {
				w.add("not-considered", "no-version-one");
			}
			return;
		}

		// Get version 1
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", new String[] {"version", "1"}, id);
		XmlDoc.Element asset = executor().execute("asset.get", dm.root());
		if (asset==null) {
			if (verbose) {
				w.add("not-considered", "version-one-not-found");
			}
			return;
		}

		// Has content ?
		XmlDoc.Element c = asset.element("asset/content");
		if(c==null) {
			if (verbose) {
				w.add("not-considered", "version-one-has-no-content");
			}
			return;
		}

		// Is zero-sized and checksum zero
		String csize = c.value("size");
		String csum = c.value("csum");
		if (csize.equals("0") && csum.equals("0")) {
			String path = asset.value("asset/path");
			String rid = asset.value("asset/rid");
			String ctime = asset.value("asset/ctime");

			// Prune version 1
			if (prune) {
				dm = new XmlDocMaker("args");
				dm.add("id", id);
				dm.add("version", "1");
				dm.add("keep-content", true);    // hard to work out what this really does
				executor().execute("asset.prune", dm.root());
				//
				w.add("id", new String[] {"rid", rid, "path", path, "ctime", ctime, "n-versions", ""+nVersions, "pruned", "true"}, id);
			} else {
				w.add("id", new String[] {"rid", rid, "path", path, "ctime", ctime, "n-versions", ""+nVersions, "pruned", "false"}, id);
			}	
		} else {
			if (verbose) {
				w.add("not-considered", "version-one-not-zero-sized-with-zero-csum");
			}
			return;
		}

	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
