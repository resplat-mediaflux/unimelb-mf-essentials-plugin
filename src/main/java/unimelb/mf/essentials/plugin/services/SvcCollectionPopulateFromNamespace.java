/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;
import unimelb.mf.essentials.plugin.util.CollectionAssetUtil;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;
import unimelb.mf.plugin.util.UserUtils;

public class SvcCollectionPopulateFromNamespace extends PluginService {


	private static String SERVICE_NAME = "unimelb.collection.populate.from.namespace";

	private Interface _defn;
	private Long COUNT_ = 0l;
	private Long COUNT_RENAME_ = 0l;
	private Long COUNT_ACLS_ = 0l;
	private Long COUNT_META_ = 0l;
	private Long INCR_ = 1000l;

	public SvcCollectionPopulateFromNamespace() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The path of the namespace of interest.  E.g. /projects/proj-test-1128.3.43", 1, 1));
		_defn.add(new Interface.Element("collection", StringType.DEFAULT, "The path of the destination parent collection to populate.  E.g. /projects/proj-test-1128.3.43", 1, 1));
		_defn.add(new Interface.Element("list", BooleanType.DEFAULT, "List everything that gets created.  Not scaleable for large structures.  Default is false.", 0, 1));
		_defn.add(new Interface.Element("list-padded", BooleanType.DEFAULT, "List any namespace where the child part has leading or trailing whitespace. Default is false.", 0, 1));
		_defn.add(new Interface.Element("rename-padded", BooleanType.DEFAULT, "If true (default false), rename any  namespace found with leading or trailig whitespace and trim off the white space.  This is done before the collection assets are populated of course.", 0, 1));
		_defn.add(new Interface.Element("log", BooleanType.DEFAULT, "Write a log file with total number of namespaces and then count (every 1000) as the collection assets are created (default false). File in standard mediaflux log folder named by this service.", 0, 1));
		_defn.add(new Interface.Element("allow-exclusive", BooleanType.DEFAULT, "Older versions of the server don't handle exclusive ACLs on collection assets.  Set this to true if the server does handle them (default true). If false, only inclusive ACLs will be transferred.", 0, 1));
		//		_defn.add(new Interface.Element("move", BooleanType.DEFAULT, "Actually move the assets (default false) in the namespace tree over to the collection asset tree", 0, 1));
	}

	public String name() {
		return SERVICE_NAME;
	}

	public String description() {
		return "Administration service to traverse a namespace tree and create equivalent collection assets in the corresponding (i.e. the collection asset with the identical path) collection asset project (which must pre-exist). "+
				"It transfers any meta-data, ACLs and visibility actors across from any namespace to the corresponding collection asset. " +
				"The service can be run multiple times - pre-existing destination collection assets will be handled.  " + 
				"The service will always attempt to transfer visibility actors, meta-data and ACLs (since it does not hurt). ";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {
		String ns  = args.stringValue("namespace");
		String coll  = args.stringValue("collection");
		Boolean list = args.booleanValue("list", false);
		Boolean log = args.booleanValue("log",  false);
		Boolean listPadded = args.booleanValue("list-padded", false);
		Boolean renamePadded = args.booleanValue ("rename-padded", false);
		Boolean allowExclusive = args.booleanValue ("allow-exclusive", true);
		//
		validatePath (executor(), ns, coll);
		if (list) {
			w.add("namespace", ns);
			w.add("collection",coll);
		}
		//
		COUNT_ = 0l;
		COUNT_RENAME_ = 0l;
		COUNT_ACLS_ = 0l;
		COUNT_META_ = 0l;
		INCR_ = 1000l;
		PluginLog logger = null;
		if (log) {
			logger = PluginLog.log(SERVICE_NAME);
			Long n = NameSpaceUtil.count(executor(), ns) - 1;
			w.add("total-child-namespaces", n);
			log(logger,"total number of child namespaces = " + n);
		}

		populate (executor(), logger, ns, coll, list, 
				listPadded, renamePadded, allowExclusive, w);
		if (COUNT_RENAME_>0l) {
			w.add("renamed-namespaces", COUNT_RENAME_);
		}
		if (COUNT_ACLS_>0) {
			w.add("ACLs-set-on-assets", COUNT_ACLS_);
		}
		if (COUNT_META_>0) {
			w.add("meta-data-set-on-assets", COUNT_META_);
		}
		w.add("assets-created", COUNT_);

	}


	private void populate (ServiceExecutor executor, PluginLog logger, String nsParent, 
			String collParent, Boolean list, Boolean listPadded,
			Boolean renamePadded, Boolean allowExclusive,
			XmlWriter w) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Iterate over children namespaces
		Collection<String> relNSs = listRelativeNameSpaces(executor, nsParent);
		if (relNSs==null) {
			return;
		}
		if (relNSs.size()==0) {
			return;
		}
		String assetID = null;
		for (String relNS : relNSs) {
			String absNS = nsParent + "/" + relNS;
			String absColl = collParent + "/"+ relNS;

			// List/rename the namespace if name is padded with white space
			// Handle if there already exists a namespace of the same
			// name as the trimmed one.
			if (listPadded||renamePadded) {
				String relNSTrim = relNS.trim();
				String absNSTrim = nsParent + "/" + relNSTrim;

				if (relNSTrim.length()<relNS.length()) {
					if (renamePadded) {
						// Return just the child part of the path
						// This function will disambiguate if the
						// namespace name we want is taken
						relNSTrim = renameAssetNamespace(executor, absNS, 
								nsParent, relNSTrim);
						absNSTrim = nsParent + "/" + relNSTrim;
						absColl = collParent + "/" + relNSTrim;  // Used below
						COUNT_RENAME_++;
					} 
					w.add("namespace", 
							new String[] {"trimmed-name", absNSTrim, "renamed", renamePadded.toString()}, absNS);
					absNS = absNSTrim;    // Used below
				}
			}
			// 
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element exists = assetExists (executor, "path="+absColl);

			// Create collection if does not exist
			Boolean created = false;
			if (!exists.booleanValue("exists")) {
				assetID = CollectionAssetUtil.create(null, executor, absColl, null, null);
				COUNT_++;
				if (COUNT_%INCR_==0l) {
					log(logger, "   created " + COUNT_ + " collection assets");				
				}
				created = true;
			} else {
				assetID = exists.value("exists/@id");
			}

			// Are there ACLs on the namespace ?
			XmlDoc.Element nsMeta = NameSpaceUtil.describe(null, executor, absNS);
			Collection<XmlDoc.Element> acls = nsMeta.elements ("namespace/acl"); 
			String inherit = nsMeta.value("namespace/inherit-acls");
			Boolean copiedACLs = false;
			if (acls!=null) {
				copyACLs (executor, allowExclusive, inherit, acls, assetID);
				copiedACLs = true;
				COUNT_ACLS_++;
			}

			// Are there meta-data to copy to the collection asset ?
			XmlDoc.Element assetMeta = nsMeta.element("namespace/asset-meta");
			Boolean copiedMeta = false;
			if (assetMeta!=null) {
				copyMeta (executor, assetMeta, assetID);
				copiedMeta = true;
				COUNT_META_++;
			}

			// Transfer visibility actor
			XmlDoc.Element visMeta = nsMeta.element("namespace/visible-to");
			Boolean copiedVis = false;
			if (visMeta!=null) {
				copyVisibility (executor, visMeta, assetID);
				copiedVis = true;
			}

			if (list) {
				w.add("path", new String[] {"created", created.toString(), 
						"id", assetID, "copied-ACLs", copiedACLs.toString(),
						"copied-meta", copiedMeta.toString(), "copied-visibility", copiedVis.toString()}, absColl);
			}


			// Descend into child namespace
			populate (executor, logger, absNS, absColl, list, 
					listPadded, renamePadded, 
					allowExclusive, w);
		}
	}



	static String renameAssetNamespace (ServiceExecutor executor, 
			String oldNamespace, String parentPart,
			String newChildPart) throws Throwable {
		String newNamespace = parentPart + "/" + newChildPart;
		//
		if (NameSpaceUtil.assetNameSpaceExists(executor, newNamespace)) {
			// The name we want is already taken, so add some digits
			Boolean done = false;
			Integer i = 1;
			while (!done) {
				newChildPart += "_" +i;
				String t = parentPart + "/" + newChildPart;
				if (!NameSpaceUtil.assetNameSpaceExists(executor, t)) {
					NameSpaceUtil.renameAssetNameSpace(executor, null, oldNamespace, newChildPart);
					return newChildPart;
				}
				i++;
				if (i>10000) {
					throw new Exception("Seem to be stuck in rename loop");
				}
			}

		} else {
			NameSpaceUtil.renameAssetNameSpace(executor, null, oldNamespace, newChildPart);
			return newChildPart;
		}
		return null;
	}



	static private void copyVisibility (ServiceExecutor executor, 
			XmlDoc.Element visMeta, String assetID) throws Throwable {
		Collection<XmlDoc.Element> actors = visMeta.elements();
		XmlDocMaker dm = new XmlDocMaker ("args");
		dm.add("id", assetID);
		for (XmlDoc.Element actor : actors) {
			actor.removeAttribute("id");
			dm.add(actor);
		}
		executor.execute("asset.collection.visibility.set", dm.root());
	}



	static private XmlDoc.Element assetExists (ServiceExecutor executor, String id) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", id);
		return executor.execute("asset.exists", dm.root());
	}

	static private void copyMeta (ServiceExecutor executor, XmlDoc.Element assetMeta, 
			String assetID) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("id", assetID);
		dm.push("meta", new String[] {"action", "merge"});
		//
		Collection<XmlDoc.Element> docs = assetMeta.elements();
		for (XmlDoc.Element doc : docs) {
			doc.removeAttribute("id");
			dm.add(doc);
		}
		dm.pop();
		executor.execute("asset.set", dm.root());
	}


	/**
	 * Copy ACLs from a namespace to a collection asset, managing the separation
	 * into :acl and :member-acl
	 * 
	 * If the ACLs already exist on the destination, these grant 
	 * service executions are Null Ops
	 * 
	 * @param executor
	 * @param allowExclusive
	 * @param inherit
	 * @param nsACLs
	 * @param assetID
	 * @throws Throwable
	 */
	static private void copyACLs (ServiceExecutor executor, 
			Boolean allowExclusive, String inherit, 
			Collection<XmlDoc.Element> nsACLs, String assetID) throws Throwable {

		Boolean some = false;
		for (XmlDoc.Element nsACL : nsACLs) {
			String ieType = nsACL.stringValue("@type", "inclusive");
			XmlDoc.Element actor = nsACL.element("actor");
			String actorType = actor.value("@type");
			String actorName = actor.value();

			actor.removeAttribute("id");

			// Check ACL type and optionally bug out
			if (!allowExclusive && ieType.equals("exclusive")) {
				break;
			}

			// Handle departed actors. They may be labelled as 'missing'
			// or if they are users, they may have been destroyed 
			// in local domains
			Boolean proceed = true;
			if (actorName.equals("missing") || actorType.equals("unknown")) {
				proceed = false;
			} else {
				if (actorType.equals("user")) {
					String[] t = actorName.split(":");
					String authority = null;
					String domain = null;
					String user = null;
					if (t.length==2) {
						domain = t[0];
						user = t[1];
					} else {
						authority = t[0];
						domain = t[1];
						user = t[2];
					}
					if (UserUtils.isAuthDomainEnabled(executor, domain)) {
						proceed = UserUtils.exists(executor, authority, domain, user);
					} else {
						proceed = false;
					}
				}
			}

			// Bug out if appropriate for this ACL
			if (!proceed) {	
				break;
			}

			// Older servers have no type for the ACLs.  So if we
			// are not allowing exclusive ACLs, we don't set 
			// the inheritance type
			XmlDocMaker collACL = null;
			if (allowExclusive) {
				collACL = new XmlDocMaker("acl", new String[] {"type", ieType});
			} else {
				collACL = new XmlDocMaker("acl");
			}
			//
			XmlDocMaker collMemberACL = null;
			if (allowExclusive) {
				collMemberACL = new XmlDocMaker("member-acl", 
						new String[] {"type", ieType});	
			} else {
				collMemberACL = new XmlDocMaker("member-acl");
			}
			//
			collACL.add(actor);
			collMemberACL.add(actor);
			collMemberACL.push("access");	
			//
			Boolean someACL = false;
			Boolean someMemberACL = false;

			Collection<String> namespaceACLs = nsACL.values("access/namespace");
			if (namespaceACLs!=null) {
				for (String namespaceACL : namespaceACLs) {
					if (namespaceACL.equals("access")) {
						collACL.add("access", "read");
						collMemberACL.add("collection", "access");
						someACL = true;
						someMemberACL = true;
					} else if (namespaceACL.equals("administer")) {
						collACL.add("asset", "administer");
						collMemberACL.add("asset", "administer");
						someACL = true;
						someMemberACL = true;
					} else if (namespaceACL.equals("administer-children")) {
						collMemberACL.add("collection", "administer");
						someMemberACL = true;
					} else if (namespaceACL.equals("create")) {
						collMemberACL.add("collection", "create");
						someMemberACL = true;
					} else if (namespaceACL.equals("modify")) {
						collMemberACL.add("collection", "modify");
						someMemberACL = true;
					} else if (namespaceACL.equals("destroy")) {
						collMemberACL.add("collection", "destroy");
						someMemberACL = true;
					} else if (namespaceACL.equals("execute") ) {
						collMemberACL.add("collection", "execute");
						someMemberACL = true;
					}
				}
			}

			Collection<String> assetACLs = nsACL.values("access/asset");
			if (assetACLs!=null) {
				for (String assetACL : assetACLs) {
					if (assetACL.equals("create")) {
						collMemberACL.add("asset", "create");
						someMemberACL = true;
					} else if (assetACL.equals("access")) {
						collMemberACL.add("asset", "access");
						someMemberACL = true;
					} else if (assetACL.equals("modify")) {
						collMemberACL.add("asset", "modify");
						someMemberACL = true;
					} else if (assetACL.equals("destroy")) {
						collMemberACL.add("asset", "destroy");
						someMemberACL = true;
					} else if (assetACL.equals("licence")) {
						collMemberACL.add("asset", "licence");
						someMemberACL = true;
					} else if (assetACL.equals("publish")) {
						// TBD No equivalent currently
					}
				}
			}
			Collection<String> assetContentACLs = nsACL.values ("access/asset-content");
			if (assetContentACLs!=null) {
				for (String assetContentACL : assetContentACLs) {
					if (assetContentACL.equals("access")) {
						collMemberACL.add("asset-content", "access");
						someMemberACL = true;
					} else if (assetContentACL.equals("modify")) {
						collMemberACL.add("asset-content", "modify");
						someMemberACL = true;
					}
				}
			}
			collMemberACL.pop();

			// Set the ACLs on the collection asset
			if (someACL) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("id", assetID);
				dm.add(collACL.root());
				executor.execute("asset.acl.grant", dm.root());
				//
			}
			//
			if (someMemberACL) {
				XmlDocMaker dm = new XmlDocMaker("args");
				dm.add("id", assetID);
				dm.add(collMemberACL.root());
				executor.execute("asset.collection.member.acl.grant", dm.root());
			}

			if (someACL || someMemberACL) {
				some = true;
			}
		}
		
		// Asset ACL inheritance applies to the whole asset
		//' and applies to ACLs and member ACLs
		if (some && inherit!=null) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", assetID);
			dm.add("inherit-acls", inherit);
			executor.execute("asset.acl.inherit.set", dm.root());
		}
	}



	static Collection<String> listRelativeNameSpaces (ServiceExecutor executor, String nameSpace) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace", nameSpace);
		dm.add("size", "infinity");
		XmlDoc.Element r = executor.execute("asset.namespace.list", dm.root());
		return r.values("namespace/namespace");
	}


	private static void validatePath (ServiceExecutor executor, 
			String ns, String coll) throws Throwable {


		if(!NameSpaceUtil.assetNameSpaceExists(executor, ns)) {
			throw new Exception("The namespace path'" + ns + "' does not exist");
		}

		if (!AssetUtils.exists(executor, null, "path="+coll, false)) {
			throw new Exception("The collection path'" + coll + "' does not exist");
		}
	}

	private void log (PluginLog logger, String message) throws Throwable {
		if (logger==null) {
			return;
		}
		logger.add(PluginLog.INFORMATION, message);
	}

}
