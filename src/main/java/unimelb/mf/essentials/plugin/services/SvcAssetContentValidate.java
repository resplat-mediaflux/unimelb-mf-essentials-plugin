package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.IDUtils;
import unimelb.mf.essentials.plugin.util.MailUtils;
import unimelb.mf.essentials.plugin.util.Properties;

public class SvcAssetContentValidate extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.content.validate";
	public static final int DEFAULT_NB_THREADS = 1;
	public static final int MAX_EMAIL_BODY_SIZE = 64000;

	private Interface _defn;

	public SvcAssetContentValidate() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where", StringType.DEFAULT, "Query to select the assets.", 0,
				Integer.MAX_VALUE));
		_defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT,
				"Include assets that have been marked as destroyed? Defaults to false.", 0, 1));

		Interface.Element recover = new Interface.Element("recover", BooleanType.DEFAULT,
				"If invalid data is found, should it be recovered from valid copies. Defaults to false.", 0, 1);
		recover.add(new Interface.Attribute("if-no-copy",
				new EnumType(new String[] { "exception", "status", "notify" }),
				"The behaviour if there is no recoverable copy available. Defaults to 'exception' - throw an exception. 'status' will return the recovery status, and 'notify' will generate an error event on the notification object type 'asset' with name 'validate'",
				0));
		recover.add(new Interface.Attribute("notify-on-recover", BooleanType.DEFAULT,
				"If true and recovery occurred, generate warning notification object type 'asset' with name 'validate'. Defaults to true.",
				0));
		recover.add(new Interface.Attribute("pdist", IntegerType.POSITIVE,
				"If specified, and recovery cannot be performed from a local copy, then attempt to find a replica in any reachable federation (up to the specified peer distance 'pdist') and recover from there.",
				0));
		_defn.add(recover);
		_defn.add(new Interface.Element("nb-threads", IntegerType.POSITIVE_ONE,
				"Number of worker threads. Defaults to " + DEFAULT_NB_THREADS + ".", 0, 1));

		_defn.add(new Interface.Element("copy", IntegerType.POSITIVE_ONE,
				"The copy to check. If unspecified, then all copies are checked.", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("store", StringType.DEFAULT,
				"If specified, validate only content in this specified store(s).", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT, "Use indexes. Defaults to true.", 0, 1));

		_defn.add(new Interface.Element("email", EmailAddressType.DEFAULT,
				"Send result (in CSV format) to mail recipients", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("subject", StringType.DEFAULT, "SUbject for email", 0, 1));
		_defn.add(new Interface.Element("all-versions", BooleanType.DEFAULT,
				" If true (default), then validate the content for all versions. If false, then only validate the most recent version.",
				0, Integer.MAX_VALUE));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Checks that the content for the asset (if the asset has content) is accessible and that value is consistent with the recorded check sum. The output counts are for asset versions only (not assets). We do not count the number of assets processed, or the number of assets that are ok.  This wraps unimelb.asset.content.validate.basic service and sets the argument show-error-only true (so we only see assets with an error).";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		// Parse inputs
		List<XmlDoc.Element> wheres = args.elements("where"); // preserve order
		boolean includeDestroyed = args.booleanValue("include-destroyed", false);
		boolean useIndexes = args.booleanValue("use-indexes", true);

		XmlDoc.Element recover = args.element("recover");
		int nbThreads = args.intValue("nb-threads", DEFAULT_NB_THREADS);

		Collection<String> emails = args.values("email");
		String subject = args.value("subject");
		boolean sendEmail = emails != null && !emails.isEmpty();
		//

		File csvFile = sendEmail ? PluginTask.createTemporaryFile("_" + SERVICE_NAME + "_result.csv") : null;
		PrintWriter pw = sendEmail ? new PrintWriter(new BufferedWriter(new FileWriter(csvFile))) : null;
		StringBuilder emailBody = sendEmail ? new StringBuilder() : null;
		Boolean emailBodySizeExceeded = sendEmail ? false : null;
		boolean allVersions = args.booleanValue("all-versions", true);

		try {
			if (sendEmail) {
				emailBody.append("Dear Administrator,\n\n");
				emailBody.append("Below is the result summary of service:\n");
				emailBody.append("    " + SERVICE_NAME + "\n");
				if (wheres != null) {
					for (XmlDoc.Element where : wheres) {
						emailBody.append(String.format("        :where %s\n", where.value()));
					}
				}
				emailBody.append("        :nb-threads " + nbThreads+"\n");
				emailBody.append("        :all-versions " + allVersions + "\n");
				emailBody.append("\n\n\n");
				pw.println("ID,VERSION,COPIES,STATUS,");
			}

			Integer nbErrors = 0;
			AtomicLong nbMismatch = new AtomicLong(0L);
			AtomicLong nbMissing = new AtomicLong(0L);

			PluginTask.checkIfThreadTaskAborted();

			//Prepare piped query
			XmlDocMaker dm = new XmlDocMaker("args");
			if (wheres != null) {
				boolean first = true;
				for (XmlDoc.Element where : wheres) {
					if (first) {
						dm.add("where", "(" + where.value() + ") and asset has content");
						first = false;
					} else {
						dm.add("where", where.value());
					}
				}
			} else {
				dm.add("where", "asset has content");
			}
			// dm.add("where", sb.toString());
			dm.add("include-destroyed", includeDestroyed);
			dm.add("size", "infinity");
			dm.add("action", "pipe");
			dm.add("pipe-generate-result-xml", "true");
			dm.add("pipe-nb-threads", nbThreads);
			dm.add("use-indexes", useIndexes);
			dm.push("service", new String[] { "name", "unimelb.asset.content.validate.basic" });
			dm.add("all-versions", allVersions);	
			dm.add("show-error-only", true);

			if (recover != null) {
				dm.add(recover, true);
			}
			if (args.elementExists("copy")) {
				dm.addAll(args.elements("copy"));
			}
			if (args.elementExists("store")) {
				dm.addAll(args.elements("store"));
			}
			dm.pop();

			// Do piped query. Assets that don't exist in the call
			// are just dropped from the pipe. They don't generate 
			// an error.
			PluginTask.checkIfThreadTaskAborted();
			XmlDoc.Element re = executor().execute("asset.query", dm.root());
			nbErrors = re.intValue("errors/fail", 0);	

			// Iterate through the results - only assets that have an issue
			List<XmlDoc.Element> aes = re.elements("asset");
			if (aes != null) {
				for (XmlDoc.Element ae : aes) {
					PluginTask.checkIfThreadTaskAborted();
					outputResult(ae, nbMissing, nbMismatch, sendEmail, 
							emailBody, w, pw);
				}
			}

			// These are asset version counts only, not asset counts.
			// E.g. an asset with 2 versions, version 1 mismatch and
			// version 2 ok, would result in mismatch=1.  
			w.add("missing", nbMissing.get());
			w.add("mismatch", nbMismatch.get());
			w.add("errors", nbErrors);
			//
			if (sendEmail) {
				pw.println(",,");
				pw.println(String.format("\"missing:\",%d", nbMissing.get()));
				pw.println(String.format("\"mismatch:\",%d", nbMismatch.get()));
				if (nbErrors > 0) {
					pw.println(String.format("\"errors:\",%d", nbErrors));
				}
				pw.close();
				emailBody.append(String.format("    missing: %d\n", nbMissing.get()));
				emailBody.append(String.format("    mismatch: %d\n", nbMismatch.get()));
				if (nbErrors > 0) {
					emailBody.append(String.format("    errors: %d\n", nbErrors));
				}
				if (emailBodySizeExceeded) {
					emailBody.append(
							"\n\nNote: the above result is incomplete. See the attached CSV spreadsheet for more details.\n");
				}
				sendEmail(executor(), subject, emails, emailBody.toString(), csvFile);
			}

		} finally {
			if (pw != null) {
				pw.close();
			}
			if (csvFile != null) {
				PluginTask.deleteTemporaryFile(csvFile);
			}
		}

	}

	private static void outputResult(XmlDoc.Element ae, AtomicLong nbMissing, AtomicLong nbMismatch, 
			boolean sendEmail, StringBuilder emailBody, XmlWriter w, PrintWriter csvWriter) throws Throwable {

		String id = ae.value("@id");
		String version = ae.value("@version");
		String status = ae.value("status");
		String copies = ae.value("copies");
		if ("missing".equalsIgnoreCase(status)) {
			w.add("id", new String[] { "version", version, "status", status, "copies", copies }, id);
			if (sendEmail) {
				csvWriter.println(String.format("%s,%s,%s,%s,", id, version, copies, status));
				if (emailBody.length() <= MAX_EMAIL_BODY_SIZE) {
					emailBody.append(String.format("    :id -version %s -copies %s -status %s %s\n", version, copies,
							status, id));
				}
			}
			nbMissing.getAndIncrement();
		} else if ("mismatch".equalsIgnoreCase(status)) {
			w.add("id", new String[] { "version", version, "status", status, "copies", copies }, id);
			if (sendEmail) {
				csvWriter.println(String.format("%s,%s,%s,%s,", id, version, copies, status));
				if (emailBody.length() <= MAX_EMAIL_BODY_SIZE) {
					emailBody.append(String.format("    :id -version %s -copies %s -status %s %s\n", version, copies,
							status, id));
				}
			}
			nbMismatch.getAndIncrement();
		} else if ("OK".equalsIgnoreCase(status)) {
			// we won't see this status
		} else {
			throw new UnsupportedOperationException("Unknown status: " + status + " for asset " + id
					+ ". This service may need to be reviewed and updated. Possibly because the underlying asset.content.validate service has changed its status values.");
		}
	}

	private static void sendEmail(ServiceExecutor executor, String subject, Collection<String> recipients, String body,
			File resultCSVFile) throws Throwable {
		String from = Properties.getServerProperty(executor, "mail.from");
		Date time = new Date();
		if (subject == null) {
			long uuid = IDUtils.serverUUID(executor);
			subject = String.format("%d: Result of %s[%s]", uuid, SERVICE_NAME,
					new SimpleDateFormat("HH:mm:ss dd-MMM-yyyy").format(time));
		}
		String attachmentFileName = String.format("%s_result_%s.csv", SERVICE_NAME,
				new SimpleDateFormat("yyyyMMddHHmmss").format(time));
		InputStream attachmentInputStream = new BufferedInputStream(new FileInputStream(resultCSVFile));
		try {
			MailUtils.sendEmail(executor, recipients, null, null, from, subject, body, attachmentFileName,
					attachmentInputStream, resultCSVFile.length(), "text/csv", false);
		} finally {
			attachmentInputStream.close();
		}
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}
}