package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcAssetWormStateSet extends PluginService {


	private Interface _defn;


	public SvcAssetWormStateSet() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The path to the asset to be replicated.", 0,
				1));
		_defn.add(new Interface.Element("can-add-versions", BooleanType.DEFAULT,
				"Indicates whether new versions of metadata and content are allowed when in the WORM state. Defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("can-move", BooleanType.DEFAULT,
				" Indicates whether the collection, or asset(s) can be moved. Defaults to 'true'",
				0, 1));
		_defn.add(new Interface.Element("show-new-only", BooleanType.DEFAULT,
				"If true, only assets that are newly set into WORM state are reported.  The default (false) is that all assets are rpeorted on, describing what the asset state is after execution (WORM state set, WORM state already set and active, WORM state already set but not active).",
				0, 1));
	}
	public String name() {
		return "unimelb.asset.worm.state.set";
	}

	public String description() {
		return "Set an asset into WORM state (no expiry, applies to all asset components). If it is already in WORM (any) state, no action is taken (this includes a WORM state that is no longer active).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Get inputs
		String id = args.value("id");
		Boolean canAddVersions = args.booleanValue("can-add-versions", false);
		Boolean canMove = args.booleanValue("can-move", true);
		Boolean showNewOnly = args.booleanValue("show-new-only", false);

		// Get asset
		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);
		XmlDoc.Element worm = asset.element("asset/worm-status");
		if (worm==null) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", id);
			dm.add("applies-to", "all");
			dm.add("can-add-versions", canAddVersions);
			dm.add("can-alter-if", "system-manager");
			dm.add("can-move", canMove);
			dm.add("destroy-on-expiry", false);  // The expiry is unset which means no expiry
			PluginTask.checkIfThreadTaskAborted();
			executor().execute("asset.worm.set", dm.root());
			w.add("id", new String[] {"worm-state", "set"}, id);
		} else {
			Boolean active = worm.booleanValue("expiry/@active");
			if (!showNewOnly) {
				if (active) {
					w.add("id", new String[] {"worm-state", "active"}, id);
				} else {
					w.add("id", new String[] {"worm-state", "expired"}, id);
				}
			}
		}
	}


}
