package unimelb.mf.essentials.plugin.services.user;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.*;

public class SvcUserRevokeOnlyRole extends PluginService {

    public static final String SERVICE_NAME = "unimelb.user.revoke.only.role";

    public static final String DEFAULT_ROLE_TYPE = "role";
    public static final String SYSTEM_ADMIN_ROLE = "system-administrator";
    public static final String SYSTEM_DOMAIN_NAME = "system";
    public static final String ARCITECTA_DOMAIN_NAME = "arcitecta";
    private final Interface _defn;

    public SvcUserRevokeOnlyRole() {
        _defn = new Interface();
        _defn.add(new Interface.Element("exclude-self", BooleanType.DEFAULT, "Exclude the current user who executes this service. Defaults to true.", 0, 1));
        _defn.add(new Interface.Element("exclude-system-domain", BooleanType.DEFAULT, "Exclude the users from system domain. Defaults to true.", 0, 1));
        _defn.add(new Interface.Element("exclude-arcitecta-domain", BooleanType.DEFAULT, "Exclude the users from arcitecta domain. Defaults to true.", 0, 1));
        _defn.add(new Interface.Element("exclude-domain", StringType.DEFAULT, "Exclude the users from the specified domain(s).", 0, 255));
        Interface.Element role = new Interface.Element("role", StringType.DEFAULT, "The role to revoke.", 1, 1);
        role.add(new Interface.Attribute("type", StringType.DEFAULT, "The role type. Defaults to 'role'.", 0));
        _defn.add(role);
        _defn.add(new Interface.Element("list-only", BooleanType.DEFAULT, "Only list the users, instead of revoke the role from the users. Defaults to true.", 0, 1));
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public String description() {
        return "Revoke the only role left from the users.";
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(XmlDoc.Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        boolean excludeSelf = args.booleanValue("exclude-self", true);
        boolean excludeSystemDomain = args.booleanValue("exclude-system-domain", true);
        boolean excludeArcitectaDomain = args.booleanValue("exclude-arcitecta-domain", true);
        Collection<String> excludeDomains = args.values("exclude-domain");
        boolean listOnly = args.booleanValue("list-only", true);
        String roleName = args.value("role");
        String roleType = args.stringValue("role/@type", DEFAULT_ROLE_TYPE);

        if (SYSTEM_ADMIN_ROLE.equalsIgnoreCase(roleName)) {
            throw new IllegalArgumentException("This service cannot be used to revoke " + SYSTEM_ADMIN_ROLE + " role");
        }

        String selfActorName = getUserSelfActorName(executor());

        Set<String> actorNames = getUserActorNames(executor(), roleName, roleType);

        Map<String, Boolean> domainHasRoleOrPerm = new HashMap<>();

        long count = 0;

        for (String actorName : actorNames) {
            String[] parts = actorName.split(":");
            String domain;
            String domainActorName;
            if (parts.length == 2) {
                domain = parts[0];
                domainActorName = parts[0];
            } else if (parts.length == 3) {
                String authority = parts[0];
                domain = parts[1];
                domainActorName = authority + ":" + domain;
            } else {
                throw new Exception("Failed to parse actor name: " + actorName);
            }
            if (excludeSystemDomain && SYSTEM_DOMAIN_NAME.equalsIgnoreCase(domain)) {
                continue;
            }
            if (excludeArcitectaDomain && ARCITECTA_DOMAIN_NAME.equalsIgnoreCase(domain)) {
                continue;
            }
            if (excludeDomains != null && excludeDomains.contains(domainActorName)) {
                continue;
            }
            if (excludeSelf && actorName.equals(selfActorName)) {
                continue;
            }
            if (isDomainWithRoleOrPerm(executor(), domainActorName, domainHasRoleOrPerm)) {
                continue;
            }
            if (hasOnlyRole(executor(), actorName, roleName, roleType)) {
                boolean revoked = false;
                if (!listOnly) {
                    revokeUserRole(executor(), actorName, roleName, roleType);
                    revoked = true;
                }
                w.add("actor", new String[]{"name", actorName, "revoked", Boolean.toString(revoked)});
                count++;
            }
        }
        w.add("count", count);
    }

    private static Set<String> getUserActorNames(ServiceExecutor executor, String roleName, String roleType) throws Throwable {
        Set<String> actorNames = new LinkedHashSet<>();
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("role", new String[]{"type", roleType}, roleName);
        dm.add("relationship", "direct");

        PluginTask.checkIfThreadTaskAborted();
        List<XmlDoc.Element> aes = executor.execute("actors.granted", dm.root()).elements("actor");
        if (aes != null) {
            for (XmlDoc.Element ae : aes) {
                boolean destroyed = ae.booleanValue("@destroyed", false);
                if (!destroyed) {
                    String actorName = ae.value("@name");
                    if (actorName != null) {
                        actorNames.add(actorName);
                    }
                }
            }
        }
        return actorNames;
    }

    private static String getUserSelfActorName(ServiceExecutor executor) throws Throwable {
        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element ue = executor.execute("user.self.describe").element("user");
        String authority = ue.value("@authority");
        String domain = ue.value("@domain");
        String username = ue.value("@user");

        if (authority == null) {
            return domain + ":" + username;
        } else {
            return authority + ":" + domain + ":" + username;
        }
    }

    private static boolean hasOnlyRole(ServiceExecutor executor, String userActorName, String roleName, String roleType) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("name", userActorName);
        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element ae = executor.execute("actor.describe", dm.root()).element("actor");
        List<XmlDoc.Element> es = ae.elements();
        if (es != null && es.size() == 1) {
            XmlDoc.Element e = es.get(0);
            if ("role".equals(e.name())) {
                return roleName.equals(e.value()) && roleType.equals(e.value("@type"));
            }
        }
        return false;
    }

    private static boolean isDomainWithRoleOrPerm(ServiceExecutor executor, String domain, Map<String, Boolean> domainHasRoleOrPerm) throws Throwable {
        if (domainHasRoleOrPerm.containsKey(domain)) {
            Boolean hasRole = domainHasRoleOrPerm.get(domain);
            if (hasRole != null) {
                return hasRole;
            }
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "domain");
        dm.add("name", domain);
        PluginTask.checkIfThreadTaskAborted();
        XmlDoc.Element ae = executor.execute("actor.describe", dm.root()).element("actor");
        boolean hasSomeRoleOrPerm = ae.hasSubElements();
        domainHasRoleOrPerm.put(domain, hasSomeRoleOrPerm);
        return hasSomeRoleOrPerm;
    }

    private static void revokeUserRole(ServiceExecutor executor, String userActorName, String roleName, String roleType) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("type", "user");
        dm.add("name", userActorName);
        dm.add("role", new String[]{"type", roleType}, roleName);
        PluginTask.checkIfThreadTaskAborted();
        executor.execute("actor.revoke", dm.root());
    }


}
