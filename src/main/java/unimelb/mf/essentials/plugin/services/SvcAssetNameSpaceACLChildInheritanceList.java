/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.PluginService.Interface;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceACLChildInheritanceList extends PluginService {


	private Interface _defn;
	private long _n = 0l;

	public SvcAssetNameSpaceACLChildInheritanceList() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("nlevels", IntegerType.DEFAULT, "If recursing, only recurse this many levels, default infinity.", 0, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "Recurse down the whole namespace tree (the listing is simplified in that the specific ACL is not shown, just that it has one). Default is false (just inspect immediate children namespaces).", 0, 1));
		_defn.add(new Interface.Element("type",  new EnumType(
				new String[] { "only-if-no-acls", "true", "false", "all"}),"List if this ACL inheritance type is set. Defaults to 'all' (show all types)", 0, 1));						
		_defn.add(new Interface.Element("reset", BooleanType.DEFAULT, "If true (default false), reset the found namespaces to have inheritance 'only-if-no-acls'.", 0, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.acl.child.inheritance.list";
	}

	public String description() {
		return "Service to list the ACL inheritance on children of the given namespace. Can recurse down the tree.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		if (namespace.equals("/")) {
			throw new Exception("You cannot give the root namespace");
		}
		String type = args.stringValue("type", "all");
		int nLevels = args.intValue("nlevels",-1);
		Boolean recurse = args.booleanValue("recurse", false);
		Boolean reset = args.booleanValue("reset", false);
		//
		_n = 0;
		Integer parentDepth = NameSpaceUtil.assetNameSpaceDepth(executor(), namespace);
		list (executor(), namespace, type, recurse, 
				reset, parentDepth, nLevels, w);	
		w.add("namespace-count", _n);
	}



	private void list (ServiceExecutor executor, String nameSpace, 
			String inheritType, Boolean recurse, Boolean reset,
			int parentDepth, int nLevels, 
			XmlWriter w) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Iterate over children namespaces
		Collection<String> nss = NameSpaceUtil.listNameSpaces(executor, nameSpace, true);
		if (nss==null) {
			return;
		}
		for (String ns : nss) {
			int depth = NameSpaceUtil.assetNameSpaceDepth(executor, ns);
			int diff = depth - parentDepth; 

			// If we have reached our maximum depth, bug out
			if (nLevels>0 && diff>nLevels) {
				return;
			} else {
				XmlDoc.Element meta = NameSpaceUtil.describe(null, executor(), ns);
				String inheritACLs = meta.value("namespace/inherit-acls");
				if (inheritType.equals("all") || (!inheritType.equals("all") && inheritACLs.equals(inheritType))) {
					_n++;
					w.add("namespace", new String[] {"inherit-acls", inheritACLs, "depth", ""+depth}, ns);					  
					
					// Reset ACL inheritance to only-if-no-acls
					if (reset && !inheritACLs.equals("only-if-no-acls")) {
						NameSpaceUtil.resetACLInheritance(executor, ns);
					}
				}
			} 

			// Descend into child namespace
			if (recurse) {
				list (executor, ns, inheritType, true, reset, parentDepth, nLevels, w);
			}
		}
	}
}
