package unimelb.mf.essentials.plugin.services;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.Map.Entry;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcLdapDomainSidGet extends PluginService {

	public static final String SERVICE_NAME = "unimelb.ldap.domain.sid.get";

	private Interface _defn;

	public SvcLdapDomainSidGet() {
		_defn = new Interface();
		_defn.add(new Interface.Element("domain", StringType.DEFAULT,
				"LDAP authentication domain name. If not specified, get SID for all LDAP domains.", 0,
				Integer.MAX_VALUE));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Retrieve ObjectSID (security identifier) value of the Microsoft Active Directory domain.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		Collection<String> domains = args.values("domain");
		if (domains == null) {
			domains = executor().execute("authentication.domain.list").values("domain[@protocol='ldap']");
		}
		if (domains != null) {
			for (String domain : domains) {
				Entry<String, String> entry = getDomainObjectSID(executor(), domain);
				if (entry != null) {
					String sid = entry.getValue();
					String path = entry.getKey();
					if (sid != null) {
						w.add("sid", new String[] { "domain", domain, "path", path }, sid);
					}
				}
			}
		}
	}

	public static Entry<String, String> getDomainObjectSID(ServiceExecutor executor, String domain) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		XmlDoc.Element de = executor.execute("authentication.domain.describe", dm.root()).element("domain");
		if (!"ldap".equalsIgnoreCase(de.value("@protocol"))) {
			throw new IllegalArgumentException("Authentication domain: " + domain + " is not a LDAP domain.");
		}

		String provider = de.value("provider");
		String path = de.stringValue("user/path", de.value("userp"));

		dm = new XmlDocMaker("args");
		dm.add("provider", provider);
		dm.add("path", path);
		dm.add("attribute", "ObjectSid");
		dm.add("scope", "object");

		XmlDoc.Element arguments = dm.root();
		String sid = null;
		while (path != null && !path.isEmpty() && sid == null) {
			arguments.element("path").setValue(path);
			sid = executor.execute("ldap.search", arguments).value("entry/objectSid");
			if (sid == null) {
				int idx = path.indexOf(',');
				if (idx >= 0) {
					path = path.substring(idx + 1);
				} else {
					path = null;
					break;
				}
			}
		}
		if (sid != null && path != null) {
			return new SimpleEntry<String, String>(path, sid);
		}
		return null;
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

}
