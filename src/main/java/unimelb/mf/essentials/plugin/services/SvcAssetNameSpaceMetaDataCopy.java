package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import arc.mf.plugin.*;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetNameSpaceMetaDataCopy extends PluginService {


	private Interface _defn;

	public SvcAssetNameSpaceMetaDataCopy() {
		_defn = new Interface();
		Interface.Element me = new Interface.Element("from", StringType.DEFAULT, "The source namespace to copy from.", 1, 1);
		_defn.add(me);
		//
		me = new Interface.Element("to", StringType.DEFAULT, "The destination namespace to copy to.", 1, 1);
		_defn.add(me);
		//
	}

	public String name() {
		return "unimelb.asset.namespace.metadata.copy";
	}

	public String description() {
		return "Service to copy (set) namespace meta-data from one namespace to another.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String from = args.value("from");
		String to = args.value("to");


		// Copy over
		copy (executor(), from, to, w);
	}


	private void copy (ServiceExecutor executor, String from, String to, XmlWriter w) throws Throwable {


		// Check abort
		PluginTask.checkIfThreadTaskAborted();


		// Get namespace
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("namespace",from);
		XmlDoc.Element nsMeta = executor.execute(null, "asset.namespace.describe", dm.root());

		// Set namespace meta-data 
		XmlDoc.Element meta = nsMeta.element("namespace/asset-meta");
		if (meta==null) return;
		//
		dm = new XmlDocMaker("args");
		dm.add("namespace", to);
		dm.push("asset-meta");
		Collection<XmlDoc.Element> els = meta.elements();
		if (els!=null) {
			for (XmlDoc.Element el : els) {
				dm.add(el);
				w.add("name", el.qname());
			}
			dm.pop();
			executor.execute(null, "asset.namespace.asset.meta.set", dm.root());
		}
	}
}
