/** 
 * @author Neil Killeen
 *
 * Copyright (c) 2016, The University of Melbourne, Australia
 *
 * All rights reserved.
 */
package unimelb.mf.essentials.plugin.services;


import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.NameSpaceUtil;

public class SvcAssetNameSpaceACLChildList extends PluginService {


	private Interface _defn;
	private long _n = 0l;

	public SvcAssetNameSpaceACLChildList() {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The namespace of interest.", 1, 1));
		_defn.add(new Interface.Element("contains", StringType.DEFAULT, "A string that the actor of the ACL must contain (case insensitive) to be shown. Default is all actors.", 0, 1));
		_defn.add(new Interface.Element("nlevels", IntegerType.DEFAULT, "If recurse=true (ignored if false)  only recurse this many levels, default infinity.", 0, 1));
		_defn.add(new Interface.Element("recurse", BooleanType.DEFAULT, "Recurse down the whole namespace tree (the listing is simplified in that the specific ACL is not shown, just that it has one). Default is false (just inspect immediate children namespaces).", 0, 1));
		_defn.add(new Interface.Element("tree", BooleanType.DEFAULT, "Present as a tree with the full ACLs displayed - in this approach, all nodes are presented, whether they have an ACL or not.  By default, the results are presented as a flat list - in this approach, only nodes with ACLs are presented.", 0, 1));
		_defn.add(new Interface.Element("revoke", BooleanType.DEFAULT, "Revoke the ACLs (default false) and reset the inheritance to 'only-if-no-acls'. Use carefully.", 0, 1));
	}

	public String name() {
		return "unimelb.asset.namespace.acl.child.list";
	}

	public String description() {
		return "Service to list any namespace ACLs on children of the given namespace. Can recurse down the tree. The returned namespace count is the number of namespaces found with an ACL (any).";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		String namespace  = args.stringValue("namespace");
		String contains = args.value("contains");
		int nLevels = args.intValue("nlevels",-1);
		Boolean recurse = args.booleanValue("recurse", false);
		Boolean showTree = args.booleanValue("tree", false);
		Boolean revoke = args.booleanValue("revoke", false);
		//
		_n = 0;
		Integer parentDepth = NameSpaceUtil.assetNameSpaceDepth(executor(), namespace);
		list (executor(), showTree, namespace, contains, recurse, 
				revoke, parentDepth, nLevels, w);	
		w.add("namespace-count", _n);
	}



	private void list (ServiceExecutor executor, Boolean showTree, String nameSpace, 
			String contains, Boolean recurse, Boolean revoke,
			int parentDepth, int nLevels, 
			XmlWriter w) throws Throwable {
		PluginTask.checkIfThreadTaskAborted();

		// Iterate over children namespaces
		Collection<String> nss = NameSpaceUtil.listNameSpaces(executor, nameSpace, true);
		if (nss==null) {
			return;
		}
		for (String ns : nss) {
			int depth = NameSpaceUtil.assetNameSpaceDepth(executor, ns);
			int diff = depth - parentDepth; 

			// If we have reached our maximum depth, bug out
			if (nLevels>0 && diff>nLevels) {
				return;
			} else {
				if (showTree) {
					w.push("namespace");
					w.add("path", ns);
				}

				XmlDoc.Element meta = NameSpaceUtil.describe(null, executor(), ns);
				Collection<XmlDoc.Element> acls = meta.elements("namespace/acl");
				String inheritACLs = meta.value("namespace/inherit-acls");
				if (acls!=null) {
					_n++;
					for (XmlDoc.Element acl : acls) {
						PluginTask.checkIfThreadTaskAborted();
						String actor = acl.value("actor");
						String actorType = acl.value("actor/@type");
						String type = acl.value("@type");
						if (contains!=null) {
							if (actor.toUpperCase().contains(contains.toUpperCase())) {
								if (revoke) {
									NameSpaceUtil.revokeACL(executor, actor, actorType, ns);
								}
								if (showTree) {
									w.add(acl);
									w.add("revoked", revoke.toString());								
								} else {
									w.add("namespace", new String[] {"inherit-acls", inheritACLs, "depth", ""+depth, "actor", actor, "actor-type", actorType, "type", type, "revoked", revoke.toString()}, ns);					  
								}
							}
						} else {
							if (revoke) {
								NameSpaceUtil.revokeACL(executor, actor, actorType, ns);
							}
							if (showTree) {
								w.add(acl);
								w.add("revoked", revoke.toString());
							} else {
								w.add("namespace", new String[] {"inherit-acls", inheritACLs,"depth", ""+depth, "actor", actor, "actor-type", actorType, "type", type, "revoked", revoke.toString()}, ns);					  
							}
						}
					}
					
					// Reset ACL inheritance as we have revoked 
					// all ACLs
					if (revoke) {
						NameSpaceUtil.resetACLInheritance(executor, ns);
					}
				}
			} 

			// Descend into child namespace
			if (recurse) {
				list (executor, showTree, ns, contains, true, revoke, parentDepth, nLevels, w);
			}
			if (showTree) {
				w.pop();
			}
		}
	}
}
