package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServerRoute;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlDocWriter;
import arc.xml.XmlWriter;
import unimelb.utils.ObjectUtils;
import unimelb.utils.PathUtils;

public class SvcAssetReplicateTo extends PluginService {

	public static final PluginLog log = PluginLog.log();

	public static final String SERVICE_NAME = "unimelb.asset.replicate.to";

	public static final int PAGE_SIZE = 100;

	public static final int DEFAULT_RETRY_TIMES = 120;

	public static final int DEFAULT_RETRY_INTERVAL = 1000; // 1 second

	private Interface _defn;

	public SvcAssetReplicateTo() {

		_defn = new Interface();

		_defn.add(new Interface.Element("allow-incomplete-metadata", BooleanType.DEFAULT,
				"Should incomplete metadata be allowed. Defaults to true.", 0, 1));
		_defn.add(new Interface.Element("allow-move", BooleanType.DEFAULT,
				"If set to true, allows an replicated asset to be moved from its current namespace if the source namespace is different. Defaults to true. NOTE: asset.replicate.to service defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("cmode", new EnumType(new String[] { "push", "pull", "ref" }),
				"Defines the mode of content replication. If 'pull', then the receiver will request the content when processing the replication request - the receiver requires access to this replicating server. If set to 'push' the content is included in the same message, which has greater preparation cost at the sender. If set to 'ref' the existence of the content is recorded against the asset, with the content data being retrieved by a user on demand. Defaults to 'push'. Note: asset.replicate.to service defaults to 'pull'.",
				0, 1));
		_defn.add(new Interface.Element("clevel", IntegerType.DEFAULT,
				"Level of compression. 0 is uncompressed, 9 is the highest level. Defaults to 6.",
				0, 1));				
		_defn.add(new Interface.Element("compatible-with", IntegerType.POSITIVE_ONE,
				"Determines the format version of the archive structure. Allows archives to be created that can be restored on older servers, by setting the compatibility to version '1'. Note that when set to a lower version some limitations are incurred on the data transfered. Defaults to '2', being the most recent version.",
				0, 1));
		_defn.add(new Interface.Element("create-roles", BooleanType.DEFAULT,
				"If set to true, will automatically create authorization roles that do not exist, but have been placed on an asset ACL. Defaults to true. If false and roles do not exist, then the restoration will fail.",
				0, 1));
		_defn.add(new Interface.Element("dst", StringType.DEFAULT,
				"The destination namespace to restore into. If not specified, restores into the root namespace.",
				0, 1));

		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The path to the asset to be replicated.", 0,
				Integer.MAX_VALUE));

		_defn.add(new Interface.Element("include-components", BooleanType.DEFAULT,
				"Should we archive the definitions of an assets components (Document Types, Models, Lables, etc). Defaults to true.",
				0, 1));
		_defn.add(new Interface.Element("include-destroyed", BooleanType.DEFAULT,
				"Should assets which have been soft deleted be archived? Defaults to 'true'.", 0, 1));
		_defn.add(new Interface.Element("include-external-content", BooleanType.DEFAULT,
				"Should external (URL) content be included. Defaults to 'true'. Should be set to 'false' if there are relative URLs.",
				0, 1));
		_defn.add(new Interface.Element("include-licences", BooleanType.DEFAULT,
				"If set to true, visible licences covering this asset will be included in the archive. Defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("include-remote-content", BooleanType.DEFAULT,
				"Should remotely referenced content be included. Defaults to 'true'.", 0, 1));
		_defn.add(new Interface.Element("keepacls", BooleanType.DEFAULT,
				"If set to true, will maintain the original ACL entries on the assets. If false, ACLs will be removed. Defaults to true. Requires ADMINISTER access to this service if set to false.",
				0, 1));
		_defn.add(new Interface.Element("localize-content", BooleanType.DEFAULT,
				"If set to true any local asset with remotely referenced content will localize the content data found in the archive. If set to false the restore will not localize the content or add it to the content cache. This allows for an asset archive containing content to be restored in scenarios were the local server only wishes to participate in federated content retrieval. Defaults to true.",
				0, 1));
		_defn.add(new Interface.Element("locally-modified-only", BooleanType.DEFAULT,
				"Set to true to include only locally modified assets. Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("overwrite", BooleanType.DEFAULT,
				"If set to true, will overwrite an existing asset with the same namespace and name. When false and an asset with a different identifier, but with the same path exists, will generate an error. Defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("parts", new EnumType(new String[] { "meta", "content", "all" }),
				"Specifies which parts of the assets to replicate. Defaults to 'all'.", 0, 1));
		_defn.add(new Interface.Element("peer", StringType.DEFAULT, "A local name for the remote server/peer.", 1, 1));
		_defn.add(new Interface.Element("preserve-rid", BooleanType.DEFAULT,
				"Indicates whether to set the replica ID (rid), which indicates the object is a replica. If the version already has a replica ID (from another system) then this is retained, otherwise it is set to the asset id of the source system. Defaults to true. Setting to false will exclude the replica id. Use false if restoring deleted assets from a replica system, that should become the primary asset.",
				0, 1));
		_defn.add(new Interface.Element("related", IntegerType.POSITIVE,
				"Specifies the number of levels of related assets (primary relationship) to be replicated. Defaults to 1. Specify infinity to traverse all relationships.",
				0, 1));
		_defn.add(new Interface.Element("restore-missing-content", BooleanType.DEFAULT,
				"If set to true then further comparison is made to determine if existing content is physically missing an needs to be recovered. Defaults to false.",
				0, 1));
		_defn.add(new Interface.Element("update-doc-types", BooleanType.DEFAULT,
				"If set to true any document types in the archive will update existing document types in the destination server, keeping them synchronized. Defaults to true.",
				0, 1));
		_defn.add(new Interface.Element("update-models", BooleanType.DEFAULT,
				"If set to true any models in the archive will update existing models in the destination server, keeping them synchronized. Defaults to true.",
				0, 1));
		_defn.add(new Interface.Element("validate-meta", BooleanType.DEFAULT,
				"Should metadata be validated against current document definitions. Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("validate-model", BooleanType.DEFAULT,
				"Should assets be validated against current model definitions. Defaults to false.", 0, 1));
		_defn.add(new Interface.Element("versions", new EnumType(new String[] { "match", "all" }),
				"Replicate all versions, or just the version matched by the query? Defaults to 'all'. Note: asset.replicate.to service defaults to 'match'",
				0, 1));
		_defn.add(new Interface.Element("where", StringType.DEFAULT,
				"The selection query - all assets matching this query will be replicated. If not specified, then must specify an asset (id).",
				0, 1));

		_defn.add(new Interface.Element("check", BooleanType.DEFAULT,
				"Check existence before replication. Skip if exists. Defaults to true.", 0, 1));
// @formatter:off
//        defn.add(new Interface.Element("destroy", BooleanType.DEFAULT,
//                "Destroy replicated assets with cid conflicts. Defaults to false.", 0, 1));
// @formatter:on

		Interface.Element retry = new Interface.Element("retry", IntegerType.POSITIVE,
				"Number of retries if the remote peer returns empty result. Defaults to " + DEFAULT_RETRY_TIMES + ".",
				0, 1);
		retry.add(new Interface.Attribute("interval", new IntegerType(100, 3600000),
				"Time interval in millicsecs between retries. Defaults to " + DEFAULT_RETRY_INTERVAL + ".", 0));
		_defn.add(retry);

	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Service wraps asset.replicate.to serice. It replicate assets one by one. and it changes default values of some args of asset.replicate.to.";
	}

	@Override
	public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
		long stime = executor().execute("server.clock.time").longValue("stime");
		String uuid = executor().execute("server.uuid").value("uuid");
		boolean check = args.booleanValue("check", true);

// @formatter:off
//        boolean destroy = args.booleanValue("destroy", false);
// @formatter:on

		final Options options = new Options(args, executor());

		long total = 0L;
		AtomicLong replicated = new AtomicLong(0L);
		AtomicLong moved = new AtomicLong(0L);
		AtomicLong skipped = new AtomicLong(0L);
		AtomicLong failed = new AtomicLong(0L);

		Collection<String> ids = args.values("id");
		if (ids != null) {
			total += ids.size();
		}

		String where = args.value("where");
		if (where != null) {
			total += count(executor(), where, options.includeDestroyed);
		}

		PluginTask.checkIfThreadTaskAborted();
		if (total > 0) {
			PluginTask.threadTaskBeginSetOf(total);
			if (ids != null && !ids.isEmpty()) {
				if (check) {
					XmlDocMaker dm = new XmlDocMaker("args");
					for (String id : ids) {
						dm.add("rid", String.format("%s.%s", uuid, id));
					}
					final int count1 = ids.size();
					XmlDoc.Element re = SvcAssetReplicateCheck.peerExecuteInsistently(executor(), options.peerRoute,
							"asset.exists", dm.root(), result -> {
								if (result == null || !result.elementExists("exists")) {
									log.add(PluginLog.WARNING,
											"[peer " + options.peerRoute.target() + " asset.exists] returned nothing");
								}
								return result != null && result.count("exists") == count1;
							}, options.retryTimes, options.retryInterval);
					PluginTask.checkIfThreadTaskAborted();

					List<XmlDoc.Element> es = re.elements("exists");

					XmlDocMaker dm1 = new XmlDocMaker("args");
					XmlDocMaker dm2 = new XmlDocMaker("args");

					for (XmlDoc.Element e : es) {
						boolean exists = e.booleanValue();
						String rid = e.value("@rid");
						String id = rid.split("\\.")[1];
						if (exists) {
							dm1.add("id", id);
							dm2.add("id", "rid=" + rid);
						} else {
							String cid = getCid(executor(), id);
							if (cid != null && replicatedAssetExists(executor(), options.peerRoute, cid,
									options.retryTimes, options.retryInterval)) {
// @formatter:off
//                                if (destroy) {
//                                    // destroy replicated asset with cid conflict. (could be the asset deleted locally).
//                                    PluginTask.checkIfThreadTaskAborted();
//                                    destroyReplicatedAsset(executor(), options.peerRoute, cid, uuid);
//                                } else {
								log("Asset " + cid + " exists on peer " + options.peerUUID
										+ " conflicts with local asset " + id + ".");
								failed.getAndIncrement();
								w.add("cid", new String[] { "failed", "true" }, cid);
								continue;
//                                }
// @formatter:on                                   
							}
							String msg = String.format("replicating asset %s", id);
							PluginTask.setCurrentThreadActivity(msg);
							replicateAsset(executor(), id, options);
							PluginTask.threadTaskCompleted(1);
							PluginTask.checkIfThreadTaskAborted();
							replicated.getAndIncrement();

						}
					}

					if (dm1.root().hasSubElements()) {
						XmlDoc.Element re1 = executor().execute("asset.get", dm1.root());
						PluginTask.checkIfThreadTaskAborted();
						List<XmlDoc.Element> aes1 = re1.elements("asset");

						final int count2 = dm2.root().count("id");
						XmlDoc.Element re2 = SvcAssetReplicateCheck.peerExecuteInsistently(executor(),
								options.peerRoute, "asset.get", dm2.root(), result -> {
									if (result == null || !result.elementExists("asset")) {
										log.add(PluginLog.WARNING,
												"[peer " + options.peerRoute.target() + " asset.get] returned nothing");
									}
									return result != null && result.count("asset") == count2;
								}, options.retryTimes, options.retryInterval);
						PluginTask.checkIfThreadTaskAborted();
						List<XmlDoc.Element> aes2 = re2.elements("asset");

						for (int i = 0; i < aes1.size(); i++) {
							XmlDoc.Element ae1 = aes1.get(i);
							XmlDoc.Element ae2 = aes2.get(i);
							boolean nsUpdated = false;
							if (options.allowMove) {
								nsUpdated = move(executor(), ae1, ae2, options);
							}
							if (differ(ae1, ae2)) {
								String id = ae1.value("@id");
								String cid = ae1.value("cid");
								String msg = String.format("re-replicating asset %s (cid=%s)", id, cid);
								PluginTask.setCurrentThreadActivity(msg);
								replicateAsset(executor(), id, options);
								PluginTask.threadTaskCompleted(1);
								PluginTask.checkIfThreadTaskAborted();
								replicated.getAndIncrement();
								w.add("id", new String[] { "cid", cid, "re-replicated", "true" }, id);
							} else {
								if (nsUpdated) {
									moved.getAndIncrement();
								} else {
									skipped.getAndIncrement();
								}
							}
						}
					}

				} else {
					for (String id : ids) {
						PluginTask.setCurrentThreadActivity("replicating asset " + id);
						replicateAsset(executor(), id, options);
						PluginTask.threadTaskCompleted(1);
						PluginTask.checkIfThreadTaskAborted();
						replicated.getAndIncrement();
					}
				}
			}
			if (where != null) {
				long idx = 1;
				int size = PAGE_SIZE;
				long remaining = Long.MAX_VALUE;
				while (remaining > 0) {
					XmlDocMaker dm = new XmlDocMaker("args");
					dm.add("where", where);
					dm.add("action", check ? "get-meta" : "get-id");
					dm.add("count", true);
					dm.add("idx", idx);
					dm.add("size", size);
					dm.add("include-destroyed", options.includeDestroyed);
					XmlDoc.Element re = executor().execute("asset.query", dm.root());
					PluginTask.checkIfThreadTaskAborted();
					idx += size;
					remaining = re.longValue("cursor/remaining");
					if (check) {
						List<XmlDoc.Element> aes = re.elements("asset");
						if (aes != null) {
							XmlDocMaker dm1 = new XmlDocMaker("args");
							for (XmlDoc.Element ae : aes) {
								String id = ae.value("@id");
								String rid = String.format("%s.%s", uuid, id);
								dm1.add("rid", rid);
							}
							final int count3 = aes.size();
							XmlDoc.Element re1 = SvcAssetReplicateCheck.peerExecuteInsistently(executor(),
									options.peerRoute, "asset.exists", dm1.root(), result -> {
										if (result == null || !result.elementExists("exists")) {
											log.add(PluginLog.WARNING, "[peer " + options.peerRoute.target()
													+ " asset.exists] returned nothing");
										}
										return result != null && result.count("exists") == count3;
									}, options.retryTimes, options.retryInterval);
							PluginTask.checkIfThreadTaskAborted();

							XmlDocMaker dm2 = new XmlDocMaker("args");
							for (XmlDoc.Element ae : aes) {
								String assetId = ae.value("@id");
								String rid = String.format("%s.%s", uuid, assetId);
								XmlDoc.Element ee = re1.element("exists[@rid='" + rid + "']");
								boolean exists = ee.booleanValue();
								if (exists) {
									dm2.add("id", ee.value("@id"));
								} else {
									String cid = ae.value("cid");
									if (cid != null && replicatedAssetExists(executor(), options.peerRoute, cid,
											options.retryTimes, options.retryInterval)) {
// @formatter:off
//                                        if (destroy) {
//                                            destroyReplicatedAsset(executor(), options.peerRoute, cid, uuid);
//                                        } else {
										log("Asset " + cid + " exists on peer " + options.peerUUID
												+ " conflicts with local asset " + assetId + ".");
										failed.getAndIncrement();
										w.add("cid", new String[] { "failed", "true" }, cid);
										continue;
//                                        }
// @formatter:on
									}
									String msg = String.format("replicating asset %s (cid=%s)", assetId, cid);
									PluginTask.setCurrentThreadActivity(msg);
									replicateAsset(executor(), assetId, options);
									PluginTask.threadTaskCompleted(1);
									PluginTask.checkIfThreadTaskAborted();
									replicated.getAndIncrement();
								}
							}
							if (dm2.root().hasSubElements()) {
								final int count4 = dm2.root().count("id");
//								System.out.println("DEBUG: route==null? " + options.peerRoute == null);
//								System.out.println("DEBUG: route: " + options.peerRoute.target());
								XmlDoc.Element re2 = SvcAssetReplicateCheck.peerExecuteInsistently(executor(),
										options.peerRoute, "asset.get", dm2.root(), result -> {
											if (result == null || !result.elementExists("asset")) {
												log.add(PluginLog.WARNING, "[peer " + options.peerRoute.target()
														+ " asset.get] returned nothing");
											}
											return result != null && result.count("asset") == count4;
										}, options.retryTimes, options.retryInterval);
								PluginTask.checkIfThreadTaskAborted();

								List<XmlDoc.Element> aes2 = re2.elements("asset");
								if (aes2 != null) {
									for (XmlDoc.Element ae2 : aes2) {
										String rid = ae2.value("rid");
										if (rid == null) {
											throw new Exception("No rid found on remote asset " + ae2.value("@id"));
										}
										String assetId = rid.split("\\.")[1];
										XmlDoc.Element ae1 = re.element("asset[@id='" + assetId + "']");
										boolean nsUpdated = false;
										if (options.allowMove) {
											nsUpdated = move(executor(), ae1, ae2, options);
											PluginTask.checkIfThreadTaskAborted();
										}
										if (differ(ae1, ae2)) {
											String cid = ae1.value("cid");
											String msg = String.format("re-replicating asset %s (cid=%s)", assetId,
													cid);
											PluginTask.setCurrentThreadActivity(msg);
											replicateAsset(executor(), assetId, options);
											PluginTask.threadTaskCompleted(1);
											PluginTask.checkIfThreadTaskAborted();
											replicated.getAndIncrement();
											w.add("id", new String[] { "cid", cid, "re-replicated", "true" }, assetId);
										} else {
											if (nsUpdated) {
												moved.getAndIncrement();
											} else {
												skipped.getAndIncrement();
											}
										}
									}
								}
							}
						}
					} else {
						Collection<String> assetIds = re.values("id");
						if (assetIds != null) {
							for (String assetId : assetIds) {
								PluginTask.setCurrentThreadActivity("replicating asset " + assetId);
								replicateAsset(executor(), assetId, options);
								PluginTask.threadTaskCompleted(1);
								PluginTask.checkIfThreadTaskAborted();
								replicated.getAndIncrement();
							}
						}
					}
				}
			}
			PluginTask.threadTaskEndSet();
		}

		w.add("stime", stime);
		w.add("replicated", replicated.get());
		if (check) {
			w.add("failed", failed.get());
			w.add("moved", moved.get());
			w.add("skipped", skipped.get());
		}
		w.add("total", total);
	}

	static boolean differ(XmlDoc.Element ae, XmlDoc.Element rae) throws Throwable {
		if (ae.longValue("mtime/@millisec") != rae.longValue("mtime/@millisec")) {
			return true;
		}
		if (!ObjectUtils.equals(ae.value("cid"), rae.value("cid"))) {
			return true;
		}
		if (!ObjectUtils.equals(ae.value("content/size"), rae.value("content/size"))) {
			return true;
		}
		if (!ObjectUtils.equals(ae.value("content/csum[@base='10']"), rae.value("content/csum[@base='10']"))) {
			return true;
		}
		return false;
	}

	static String getCid(ServiceExecutor executor, String id) throws Throwable {
		return executor.execute("asset.identifier.get", "<args><id>" + id + "</id></args>", null, null)
				.value("id/@cid");
	}

	static boolean replicatedAssetExists(ServiceExecutor executor, ServerRoute peerRoute, String cid, int retryTimes,
			int retryInterval) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("cid", cid);
		return SvcAssetReplicateCheck.peerExecuteInsistently(executor, peerRoute, "asset.exists", dm.root(), result -> {
			if (result == null || !result.elementExists("exists")) {
				log.add(PluginLog.WARNING, "[peer " + peerRoute.target() + " asset.exists] returned nothing");
			}
			return result != null && result.elementExists("exists");
		}, retryTimes, retryInterval).booleanValue("exists");
	}

// @formatter:off
//       
// 
//    /**
//     * Destroy replicated assets with cid conflicts (AKA assets destroyed locally)
//     *  
//     * @param executor
//     * @param route
//     * @param cid
//     * @param localUUID
//     * @throws Throwable
//     */
//    private static void destroyReplicatedAsset(ServiceExecutor executor, ServerRoute route, String cid, String localUUID)
//            throws Throwable {
//        XmlDoc.Element ae = executor.execute(route, "asset.get", "<args><cid>" + cid + "</cid></args>", null, null)
//                .element("asset");
//        String rid = ae.value("rid");
//        if (rid != null) {
//            String[] parts = rid.split("\\.");
//            String uuid = parts[0];
//            String id = parts[parts.length - 1];
//            if (uuid.equals(localUUID) && !executor
//                    .execute("asset.exists", "<args><id>" + id + "</id></args>", null, null).booleanValue("exists")) {
//                System.out.println(SERVICE_NAME + ": DEBUG: need to destroy replicated asset cid");
//                executor.execute(route, "asset.destroy", "<args><cid>" + cid + "</cid><members>false</members></args>",
//                        null, null);
//            }
//        }
//    }
// @formatter:on

	static boolean move(ServiceExecutor executor, XmlDoc.Element ae, XmlDoc.Element rae, Options opts)
			throws Throwable {
		String ns = ae.value("namespace");
		String rns = rae.value("namespace");
		String id = rae.value("@id");
		String ens = PathUtils.joinSystemIndependent("/", opts.dst, ns);
		if (!rns.equals(ens)) {
			XmlDocMaker dm = new XmlDocMaker("args");
			dm.add("id", id);
			dm.add("namespace", new String[] { "create", "true" }, ens);
			SvcAssetReplicateCheck.peerExecuteInsistently(executor, opts.peerRoute, "asset.move", dm.root(), result -> {
				return result != null;
			}, opts.retryTimes, opts.retryInterval);
			log(String.format("moved %s to %s", rae.value("path"), ens));
			return true;
		}
		return false;
	}

	static long count(ServiceExecutor executor, String where, boolean includeDestroyed) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where);
		dm.add("include-destroyed", includeDestroyed);
		dm.add("action", "count");
		return executor.execute("asset.query", dm.root()).longValue("value");
	}

	static void replicateAsset(ServiceExecutor executor, String assetId, Options options) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		XmlDocWriter xw = new XmlDocWriter(dm);
		xw.add("where", "id=" + assetId);
		options.save(xw);
		executor.execute("asset.replicate.to", dm.root());
		log("replicated asset " + assetId);
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}

	private static class Options {

		final Boolean allowIncompleteMetadata;
		final Boolean allowMove;
		final String cmode;
		final Integer compatibleWith;
		final Boolean createRoles;
		final String dst;
		final Boolean includeComponents;
		final boolean includeDestroyed;
		final Boolean includeRemoteContent;
		final Boolean keepacls;
		final Boolean localizeContent;
		final Boolean locallyModifiedOnly;
		final Boolean overwrite;
		final String parts;
		final String peerName;
		final String peerUUID;
		final ServerRoute peerRoute;
		final Boolean preserveRid;
		final Integer related;
		final Boolean restoreMissingContent;
		final Boolean updateDocTypes;
		final Boolean updateModels;
		final Boolean validateMeta;
		final Boolean validateModel;
		final String versions;
		final int retryTimes;
		final int retryInterval;
		final Integer clevel;

		Options(XmlDoc.Element args, ServiceExecutor executor) throws Throwable {
			this.allowIncompleteMetadata = args.booleanValue("allow-incomplete-metadata", null);
			this.allowMove = args.booleanValue("allow-move", true);
			this.cmode = args.stringValue("cmode", "push");
			this.compatibleWith = args.intOrNullValue("compatible-with");
			this.createRoles = args.booleanValue("create-roles", null);
			this.dst = args.stringValue("dst", executor.execute("server.uuid").value("uuid"));
			this.includeComponents = args.booleanValue("include-components", null);
			this.includeDestroyed = args.booleanValue("include-destroyed", true);
			this.includeRemoteContent = args.booleanValue("include-remote-content", null);
			this.keepacls = args.booleanValue("keepacls", null);
			this.localizeContent = args.booleanValue("localize-content", null);
			this.locallyModifiedOnly = args.booleanValue("locally-modified-only ", false);
			this.overwrite = args.booleanValue("overwrite", null);
			this.parts = args.stringValue("parts", "all");
			this.peerName = args.stringValue("peer");

			if (!executor.execute("server.peer.exists", "<args><name>" + peerName + "</name></args>", null, null)
					.booleanValue("exists")) {
				throw new IllegalArgumentException("Peer: " + peerName + " does not exist.");
			}
			this.peerUUID = executor
					.execute("server.peer.describe", "<args><name>" + peerName + "</name></args>", null, null)
					.value("peer/@uuid");
			this.peerRoute = new ServerRoute(this.peerUUID);
			this.preserveRid = args.booleanValue("preserve-rid", null);
			this.related = args.intOrNullValue("related");
			this.restoreMissingContent = args.booleanValue("restore-missing-content", null);
			this.updateDocTypes = args.booleanValue("update-doc-types", null);
			this.updateModels = args.booleanValue("update-models", null);
			this.validateMeta = args.booleanValue("validate-meta", null);
			this.validateModel = args.booleanValue("validate-model", null);
			this.versions = args.stringValue("versions", "all");
			this.retryTimes = args.intValue("retry", DEFAULT_RETRY_TIMES);
			this.retryInterval = args.intValue("retry/@interval", DEFAULT_RETRY_INTERVAL);
			this.clevel = args.intValue("clevel", 6);
		}

		void save(XmlWriter w) throws Throwable {
			if (this.allowIncompleteMetadata != null) {
				w.add("allow-incomplete-metadata", this.allowIncompleteMetadata);
			}
			if (this.allowMove != null) {
				w.add("allow-move", this.allowMove);
			}
			w.add("cmode", this.cmode);
			if (this.compatibleWith != null) {
				w.add("compatible-with", this.compatibleWith);
			}
			if (this.createRoles != null) {
				w.add("create-roles", this.createRoles);
			}
			w.add("dst", new String[] { "create", "false" }, this.dst);
			if (this.includeComponents != null) {
				w.add("include-components", this.includeComponents);
			}
			w.add("include-destroyed", this.includeDestroyed);
			if (this.includeRemoteContent != null) {
				w.add("include-remote-content", this.includeRemoteContent);
			}
			if (this.keepacls != null) {
				w.add("keepacls", this.keepacls);
			}
			if (this.localizeContent != null) {
				w.add("localize-content", this.localizeContent);
			}
			if (this.locallyModifiedOnly != null) {
				w.add("locally-modified-only", this.locallyModifiedOnly);
			}
			if (this.overwrite != null) {
				w.add("overwrite", this.overwrite);
			}
			w.add("parts", this.parts);
			w.push("peer");
			w.add("name", this.peerName);
			w.pop();
			if (this.preserveRid != null) {
				w.add("preserve-rid", this.preserveRid);
			}
			if (this.related != null) {
				w.add("related", this.related);
			}
			if (this.restoreMissingContent != null) {
				w.add("restore-missing-content", this.restoreMissingContent);
			}
			if (this.updateDocTypes != null) {
				w.add("update-doc-types", this.updateDocTypes);
			}
			if (this.updateModels != null) {
				w.add("update-models", this.updateModels);
			}
			if (this.validateMeta != null) {
				w.add("validate-meta", this.validateMeta);
			}
			if (this.validateModel != null) {
				w.add("validate-model", this.validateModel);
			}
			if (this.versions != null) {
				w.add("versions", this.versions);
			}
			if (this.clevel!=null) {
				w.add("clevel", this.clevel);
			}
		}
	}

	private static void log(String message) throws Throwable {
		PluginLog.log().add(PluginLog.WARNING, message);
	}

}
