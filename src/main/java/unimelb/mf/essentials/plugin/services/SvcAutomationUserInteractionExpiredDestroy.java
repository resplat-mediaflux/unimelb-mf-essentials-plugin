package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.Date;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAutomationUserInteractionExpiredDestroy extends PluginService {

	public static final String SERVICE_NAME = "unimelb.automation.user.interaction.expired.destroy";

	private Interface _defn;

	public SvcAutomationUserInteractionExpiredDestroy() {
		_defn = new Interface();
		_defn.add(new Interface.Element("list-only", BooleanType.DEFAULT,
				"Instead of destroy, list the expired user interactions in the service result. Defaults to false.", 0,
				1));
		_defn.add(
				new Interface.Element("email", EmailAddressType.DEFAULT, "The recipient to recieve the result.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ADMINISTER;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Find and destory expired user interactions.";
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		boolean listOnly = args.booleanValue("list-only", false);
		String email = args.value("email");

		long idx = 0;
		long size = 100;
		boolean complete = false;
		int nbExpired = 0;
		StringBuilder sb = new StringBuilder();
		if (email != null) {
			sb.append("<html>");
			sb.append("<head>");
			sb.append("<style>");
			sb.append("table, th, td { border: 1px solid black; }");
			sb.append("</style>");
			sb.append("</head>");
			sb.append("<body>");
			sb.append(
					"<table><thead><tr><th>ID</th><th>Name</th><th>Expiry</th><th>Destroyed</th></tr></thead><tbody>");
		}
		while (!complete) {
			PluginTask.checkIfThreadTaskAborted();
			Collection<String> ids = executor()
					.execute("automation.user.interaction.list",
							"<args><idx>" + idx + "</idx><size>" + size + "</size></args>", null, null)
					.values("user-interaction/@id");
			int nbDestroyed = 0;
			if (ids != null && !ids.isEmpty()) {
				for (String id : ids) {
					PluginTask.checkIfThreadTaskAborted();
					XmlDoc.Element uie = executor().execute("automation.user.interaction.describe",
							"<args><id>" + id + "</id></args>", null, null).element("user-interaction");

					Date expiresAt = uie.dateValue("expires-at", null);
					boolean expired = expiresAt != null && expiresAt.before(new Date());
					if (expired) {
						nbExpired += 1;
						boolean destroyed = false;
						if (!listOnly) {
							executor().execute("automation.user.interaction.destroy",
									"<args><id>" + id + "</id></args>", null, null);
							destroyed = true;
							nbDestroyed += 1;
						}
						saveXml(uie, destroyed, w);
						if (email != null) {
							saveHtml(uie, destroyed, sb);
						}
					}
				}
				idx += (size - nbDestroyed);
			} else {
				complete = true;
			}
		}
		w.add("count", nbExpired);
		if (nbExpired > 0 && email != null) {
			sb.append("</tbody></table>");
			sb.append("<br/>").append("Count: <b>").append(nbExpired).append("</b><br/>");
			sb.append("</body></html>");
			sendEmail(executor(), email, sb.toString(), nbExpired, !listOnly);
			w.add("sent-to", email);
		}

	}

	private static void saveXml(XmlDoc.Element uie, boolean destroyed, XmlWriter w) throws Throwable {
		String id = uie.value("@id");
		String name = uie.value("name");
		String expiry = uie.value("expires-at");
		w.add("user-interaction",
				new String[] { "id", id, "name", name, "expiry", expiry, "destroyed", Boolean.toString(destroyed) });
	}

	private static void saveHtml(XmlDoc.Element uie, boolean destroyed, StringBuilder sb) throws Throwable {
		String id = uie.value("@id");
		String name = uie.value("name");
		String expiry = uie.value("expires-at");
		sb.append("<tr>");
		sb.append("<td>").append(id).append("</td>");
		sb.append("<td>").append(name).append("</td>");
		sb.append("<td>").append(expiry).append("</td>");
		sb.append("<td>").append(destroyed).append("</td>");
		sb.append("</tr>");
	}

	private static void sendEmail(ServiceExecutor executor, String email, String htmlMessage, int nbExpired,
			boolean destroyed) throws Throwable {
		String serverUUID = executor.execute("server.uuid").value("uuid");
		String subject = String.format("[%s] expired user interaction%s %s (%d)", serverUUID, nbExpired > 1 ? "s" : "",
				destroyed ? "destroyed" : "found", nbExpired);
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("to", email);
		dm.add("subject", subject);
		dm.add("body", new String[] { "type", "text/html" }, htmlMessage);
		executor.execute("mail.send", dm.root());
	}

}
