package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EnumType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetWormCheck extends PluginService {


	private Interface _defn;


	public SvcAssetWormCheck() {
		_defn = new Interface();
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to restrict the selected assets on the local host. If unset, all assets are considered. If there is more than one where clause, then the second and subsequent clauses are evaluated (linearly) against the result set of the first where clause (a post filter).", 0, Integer.MAX_VALUE));
		_defn.add(new Interface.Element("size",IntegerType.DEFAULT, "Limit the accumulation loop to this number of assets per iteration (if too large, the host may run out of virtual memory).  Defaults to 5000.", 0, 1));
		_defn.add(new Interface.Element("nb-threads", IntegerType.DEFAULT,
				"Number of threads to execute (via piped query). Defaults to 1.",
				0, 1));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT, "Turn on or off the use of indexes in the query. Defaults to true.", 0, 1));
		_defn.add(new Interface.Element("list", BooleanType.DEFAULT, "List the state of each asset as well as totals (default false).", 0, 1));
		_defn.add(new Interface.Element("list-type",new EnumType(new String[] { "all", "set", "unset"}), "If list true, which WORM state to list (default all).", 0, 1));
	}
	public String name() {
		return "unimelb.asset.worm.check";
	}

	public String description() {
		return "Lists totals of the WORM state of assets.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Get inputs
		Collection<String> wheres = args.values("where");
		String size = args.stringValue("size", "5000");
		Boolean useIndexes = args.booleanValue("use-indexes", true);
		Boolean list = args.booleanValue("list", false);
		String listType = args.stringValue("list-type", "all");
		String nbThreads = args.stringValue("nb-threads","1");

		int[] count = new int[]{0,0,0,0};  // No. assets, not in worm at all, active worm state, expired worm
		find (executor(),  wheres, nbThreads, size, useIndexes, count, list, listType, w);

		w.push("totals");
		w.add("checked", count[0]);
		w.add("not-in-worm-state", count[1]);
		w.add("active-worm-state", count[2]);
		w.add("expired-worm-state", count[3]);
		w.pop();
	}


	private void find (ServiceExecutor executor, Collection<String> wheres, 
			String nbThreads, String size, Boolean useIndexes, int[] count, 
			Boolean list, String listType, XmlWriter w)	throws Throwable {

		XmlDocMaker dm = new XmlDocMaker("args");
		if (wheres!=null) {
			for (String where : wheres) {
				dm.add("where", where);
			}
		}
		dm.add("pdist", 0);
		dm.add("pipe-nb-threads", nbThreads);
		dm.add("action", "pipe");
		dm.add("use-indexes", useIndexes);
		dm.add("as", "iterator");
		dm.add("pipe-generate-result-xml", true);
		dm.push("service", new String[] {"name", "unimelb.asset.worm.state.check"});
		dm.add("unset-only", false);
		dm.pop();
		PluginTask.checkIfThreadTaskAborted();
		XmlDoc.Element r = executor().execute("asset.query", dm.root());
		if (r==null) {
			return;  
		}

		// Get the iterator and iterate
		long iteratorID = r.longValue("iterator");
		boolean complete = false;
		dm = new XmlDocMaker("args");
		dm.add("id", iteratorID);
		dm.add("size", size);
		try {
			while (!complete) {
				PluginTask.checkIfThreadTaskAborted();
				XmlDoc.Element re = executor.execute("asset.query.iterate", dm.root());
				complete = re.booleanValue("iterated/@complete");
				Collection<XmlDoc.Element> ids = re.elements("id");
				if (ids!=null) {
					for (XmlDoc.Element id : ids) {
						PluginTask.checkIfThreadTaskAborted();
						String wormState = id.value("@worm-state");
						if (wormState.equals("unset")) {
							count[1] += 1;
						} else if (wormState.equals("active")) {
							count[2]++;
						} else if (wormState.equals("expired")) {
							count[3]++;
						}
						if (list) {
							if (listType.equals("all")) {
								w.add(id);
							} else if (listType.equals("set")) {
								if (!wormState.equals("unset")) {
									w.add(id);
								}
							} else if (listType.equals("unset")) {
								if (wormState.equals("unset")) {
									w.add(id);
								}							
							}
						}
						count[0] += 1;
					}
				}
			}
		} catch (Throwable e) {
			// make sure the iterator is destroyed.
			executor.execute("asset.query.iterator.destroy",
					"<args><ignore-missing>true</ignore-missing><id>" + iteratorID + "</id></args>", null, null);
			throw e;
		}
	}
}
