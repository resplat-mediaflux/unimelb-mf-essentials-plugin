package unimelb.mf.essentials.plugin.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.utils.XmlUtils;

public class SvcAssetMetadataCSVSend extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.metadata.csv.send";

	private Interface _defn;

	public SvcAssetMetadataCSVSend() {
		_defn = new Interface();
		SvcAssetMetadataCSVExport.addArgs(_defn);
		_defn.add(new Interface.Element("from", EmailAddressType.DEFAULT, "The sender's email address.", 0, 1));
		_defn.add(new Interface.Element("to", EmailAddressType.DEFAULT, "The email recipients.", 1, 100));
		_defn.add(new Interface.Element("subject", StringType.DEFAULT, "The email subject.", 0, 1));

	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "exports metadata to csv file and send it to email recipients specified.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
		/*
		 * parse arguments
		 */
		String argWhere = args.value("where");
		String argNamespace = args.value("namespace");
		// construct query
		StringBuilder sb = new StringBuilder();
		if (argWhere != null) {
			sb.append("(").append(argWhere).append(")");
		}
		if (argNamespace != null) {
			if (sb.length() > 0) {
				sb.append(" and ");
			}
			sb.append("(namespace>='").append(argNamespace).append("')");
		}

		final Date time = new Date();
		final String where = sb.length() > 0 ? sb.toString() : null;
		final List<XmlDoc.Element> xpaths = args.elements("xpath");
		final int step = args.intValue("step", SvcAssetMetadataCSVExport.DEFAULT_PAGE_SIZE);
		final boolean compress = args.booleanValue("compress", false);
		final Collection<String> recipients = args.values("to");
		final String from = args.value("from");
		final String subject = args.stringValue("subject", emailSubject(executor(), time));
		final String body = emailBody(executor(), args);
		final String fileName = fileName(executor(), compress, time);
		final String mimeType = compress ? "application/gzip" : "text/csv";

		File tmpFile = PluginTask.createTemporaryFile(SERVICE_NAME + (compress ? ".csv.gz" : ".csv"));
		try {
			SvcAssetMetadataCSVExport.exportCSV(executor(), where, xpaths, step, tmpFile.toPath());

			PluginTask.checkIfThreadTaskAborted();
			sendEmail(executor(), from, recipients, subject, body, tmpFile, fileName, mimeType);
		} catch (Throwable e) {
			PluginTask.deleteTemporaryFile(tmpFile);
			throw e;
		}
	}

	private static String fileName(ServiceExecutor executor, boolean compress, Date time) {
		StringBuilder sb = new StringBuilder();
		sb.append(SvcAssetMetadataCSVExport.FILE_NAME_PREFIX);
		sb.append(new SimpleDateFormat("yyyyMMddHHmmss").format(time));
		sb.append(compress ? ".csv.gz" : ".csv");
		return sb.toString();
	}

	private static String emailSubject(ServiceExecutor executor, Date time) throws Throwable {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(executor.execute("server.uuid").value("uuid")).append("] ");
		sb.append(SERVICE_NAME);
		sb.append(" [").append(new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(time)).append("]");
		return sb.toString();
	}

	private static String emailBody(ServiceExecutor executor, Element args) throws Throwable {
		return XmlUtils.serviceCallToIndentedText(SERVICE_NAME, args);
	}

	private static void sendEmail(ServiceExecutor executor, String from, Collection<String> recipients, String subject,
			String body, File tmpFile, String fileName, String mimeType) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (from != null) {
			dm.add("from", from);
		}
		for (String recipient : recipients) {
			dm.add("to", recipient);
		}
		dm.add("subject", subject);
		dm.add("body", body);
		dm.push("attachment");
		dm.add("name", fileName);
		dm.add("type", mimeType);
		dm.pop();
		try (FileInputStream fis = new FileInputStream(tmpFile);
				BufferedInputStream bis = new BufferedInputStream(fis)) {
			PluginService.Input input = new PluginService.Input(bis, tmpFile.length(), mimeType, null);
			executor.execute("mail.send", dm.root(), new PluginService.Inputs(input), null);
		}
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}
}
