package unimelb.mf.essentials.plugin.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcUserSearch extends PluginService {

	public static final String SERVICE_NAME = "unimelb.user.search";

	public static final int DEFAULT_RESULT_SIZE = 10;

	static class UserEntry {

		public final String authority;
		public final String domain;
		public final String user;
		public final String employeeID;
		public final String name;
		public final String email;
		private final List<String> departments;
		private final Map<String, String> attributes;
		public final boolean enabled;
		private final List<XmlDoc.Element> permissions;

		private UserEntry(Builder builder) {
			this(builder.authority, builder.domain, builder.user, builder.employeeID, builder.name, builder.email,
					builder.departments, builder.attributes, builder.enabled, builder.permissions);
		}

		public UserEntry(String authority, String domain, String user, String employeeID, String name, String email,
				Collection<String> departments, Map<String, String> attributes, boolean enabled,
				List<XmlDoc.Element> permissions) {
			this.authority = authority;
			this.domain = domain;
			this.user = user;
			this.employeeID = employeeID;
			this.name = name;
			this.email = email;
			this.departments = new ArrayList<>();
			if (departments != null) {
				this.departments.addAll(departments);
			}
			this.attributes = attributes == null ? new LinkedHashMap<>() : new LinkedHashMap<>(attributes);
			this.enabled = enabled;
			this.permissions = permissions;
		}

		public UserEntry(XmlDoc.Element ue, Map<String, String> attributes, List<XmlDoc.Element> permissions)
				throws Throwable {
			this.authority = ue.value("@authority");
			this.domain = ue.value("@domain");
			this.user = ue.value("@user");
			this.name = ue.value("name");
			this.email = ue.value("e-mail");
			this.enabled = ue.booleanValue("@enabled", true);
			this.departments = new ArrayList<>();
			if (ue.elementExists("department")) {
				this.departments.addAll(ue.values("department"));
			}
			this.employeeID = ue.value("employeeID");
			this.attributes = attributes == null ? Collections.emptyMap() : new LinkedHashMap<>(attributes);
			this.permissions = permissions;
		}

		public Map<String, String> attributes() {
			return Collections.unmodifiableMap(this.attributes);
		}

		public List<String> departments() {
			return Collections.unmodifiableList(this.departments);
		}

		public String attribute(String name) {
			return this.attributes.get(name);
		}

		public List<XmlDoc.Element> permissions() {
			if (this.permissions == null) {
				return null;
			} else {
				return Collections.unmodifiableList(this.permissions);
			}
		}

		/**
		 * Creates builder to build {@link UserEntry}.
		 *
		 * @return created builder
		 */
		public static Builder builder() {
			return new Builder();
		}

		public static final class Builder {

			private String authority;
			private String domain;
			private String user;
			private String employeeID;
			private String name;
			private String email;
			private final Set<String> departments = new LinkedHashSet<>();
			private Map<String, String> attributes = new LinkedHashMap<>();
			private boolean enabled = true;
			private List<XmlDoc.Element> permissions;

			private Builder() {

			}

			public Builder setAuthority(String authority) {
				this.authority = authority;
				return this;
			}

			public Builder setDomain(String domain) {
				this.domain = domain;
				return this;
			}

			public Builder setUser(String user) {
				this.user = user;
				return this;
			}

			public Builder setEmployeeID(String employeeID) {
				this.employeeID = employeeID;
				return this;
			}

			public Builder setName(String name) {
				this.name = name;
				return this;
			}

			public Builder setEmail(String email) {
				this.email = email;
				return this;
			}

			public Builder setEnabled(boolean enabled) {
				this.enabled = enabled;
				return this;
			}

			public Builder setAttribute(String name, String value) {
				if (this.attributes == null) {
					this.attributes = new LinkedHashMap<>();
				}
				this.attributes.put(name, value);
				return this;
			}

			public Builder setAttributes(Map<String, String> attributes) {
				this.attributes = attributes;
				return this;
			}

			public Builder setDepartments(Collection<String> departments) {
				this.departments.clear();
				this.departments.addAll(departments);
				return this;
			}

			public Builder addDepartment(String department) {
				this.departments.add(department);
				return this;
			}

			public Builder addDepartments(Collection<String> uomDepartments) {
				this.departments.addAll(uomDepartments);
				return this;
			}

			public Builder setPermissions(List<XmlDoc.Element> permissions) {
				this.permissions = permissions;
				return this;
			}

			public UserEntry build() {
				return new UserEntry(this);
			}

		}
	}

	private final Interface _defn;

	public SvcUserSearch() {
		_defn = new Interface();
		addArgs(_defn);
		addEmployeeIdArg(_defn);
	}

	static void addArgs(Interface defn) {
		Interface.Element name = new Interface.Element("name", StringType.DEFAULT, "User's name", 0, 1);
		defn.add(name);

		Interface.Element email = new Interface.Element("email", EmailAddressType.DEFAULT, "User's email address.", 0,
				1);
		defn.add(email);

		Interface.Element domain = new Interface.Element("domain", StringType.DEFAULT, "The authentication domain.", 0,
				1);
		defn.add(domain);

		Interface.Element excludeDomain = new Interface.Element("exclude-domain", StringType.DEFAULT,
				"The domains to exclude.", 0, 255);
		defn.add(excludeDomain);

		Interface.Element excludeDisabled = new Interface.Element("exclude-disabled", BooleanType.DEFAULT,
				"Exclude disabled user accounts. Defaults to true.", 0, 1);
		defn.add(excludeDisabled);

		Interface.Element permissions = new Interface.Element("permissions", BooleanType.DEFAULT,
				"Include permissions. Defaults to true.", 0, 1);
		defn.add(permissions);

		defn.add(new Interface.Element("size", IntegerType.POSITIVE_ONE,
				"The number of entries to retrieve. If not specified, defaults to " + DEFAULT_RESULT_SIZE, 0, 1));

	}

	static void addEmployeeIdArg(Interface defn) {
		Interface.Element employeeId = new Interface.Element("employee-id", StringType.DEFAULT,
				"employeeID attribute of the LDAP entry.", 0, 1);
		defn.add(employeeId);
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Search for users in local, saml and ldap domains.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {

		String name = args.value("name");
		String email = args.value("email");
		String domain = resolveDomainName(executor(), args.value("domain"));
		String employeeId = args.value("employee-id");
		Collection<String> excludeDomains = args.values("exclude-domain");
		boolean excludeDisabled = args.booleanValue("exclude-disabled", true);
		boolean permissions = args.booleanValue("permissions", true);
		int size = args.intValue("size", SvcUserSearch.DEFAULT_RESULT_SIZE);

		List<SvcUserSearch.UserEntry> users = new ArrayList<>();
		if (domain != null) {
			if (SvcUserSearchLdap.isLdapDomain(executor(), domain)) {
				SvcUserSearchLdap.searchLdapUsers(executor(), domain, excludeDomains, name, email, employeeId,
						excludeDisabled, permissions, users, size);
			} else {
				if (name != null || email != null) {
					SvcUserSearchLocal.searchLocalUsers(executor(), domain, excludeDomains, name, email,
							excludeDisabled, permissions, users, size);
				}
			}
		} else {
			if (name != null || email != null) {
				SvcUserSearchLocal.searchLocalUsers(executor(), null, excludeDomains, name, email, excludeDisabled,
						permissions, users, size);
			}
			PluginTask.checkIfThreadTaskAborted();
			if (users.size() < size) {
				SvcUserSearchLdap.searchLdapUsers(executor(), null, excludeDomains, name, email, employeeId,
						excludeDisabled, permissions, users, size);
			}
		}

		SvcUserSearch.outputUsers(users, w);

	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	public boolean canBeAborted() {
		return true;
	}

	static String resolveDomainName(ServiceExecutor executor, String domainNameOrAlias) throws Throwable {
		if (domainNameOrAlias == null) {
			return null;
		}
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domainNameOrAlias);
		return executor.execute("authentication.domain.describe", dm.root()).value("domain/@name");
	}

	static void outputUsers(List<UserEntry> users, XmlWriter w) throws Throwable {
		for (SvcUserSearch.UserEntry u : users) {
			w.push("user", new String[] { "domain", u.domain, "user", u.user, "email", u.email, "name", u.name });
			if (u.employeeID != null) {
				w.add("employeeID", u.employeeID);
			}
			List<String> departments = u.departments();
			if (departments != null) {
				for (String department : departments) {
					w.add("department", department);
				}
			}
			Map<String, String> attrs = u.attributes();
			if (attrs != null) {
				Set<String> attrNames = attrs.keySet();
				for (String attrName : attrNames) {
					String attrValue = attrs.get(attrName);
					if (attrValue != null) {
						w.add("attribute", new String[] { "name", attrName }, attrValue);
					}
				}
			}
			List<XmlDoc.Element> permissions = u.permissions();
			if (permissions != null) {
				w.addAll(permissions);
			}
			w.pop();
		}
	}
}
