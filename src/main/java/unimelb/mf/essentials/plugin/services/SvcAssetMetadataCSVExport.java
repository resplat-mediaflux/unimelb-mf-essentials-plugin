package unimelb.mf.essentials.plugin.services;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.PluginThread;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.IntegerType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetMetadataCSVExport extends PluginService {

	public static final String SERVICE_NAME = "unimelb.asset.metadata.csv.export";

	public static final int DEFAULT_PAGE_SIZE = 100;

	public static final String FILE_NAME_PREFIX = "unimelb-asset-metadata-export-";

	private Interface _defn;

	public SvcAssetMetadataCSVExport() {
		_defn = new Interface();
		addArgs(_defn);
	}

	static void addArgs(Interface defn) {
		defn.add(new Interface.Element("where", StringType.DEFAULT, "Select query.", 0, 1));
		defn.add(new Interface.Element("namespace", StringType.DEFAULT, "The asset namespace.", 0, 1));
		Interface.Element xpath = new Interface.Element("xpath", StringType.DEFAULT,
				"The xpath to select the metadata.", 1, Integer.MAX_VALUE);
		xpath.add(new Interface.Attribute("ename", StringType.DEFAULT, "The element/column name.", 0));
		defn.add(xpath);
		defn.add(new Interface.Element("step", IntegerType.POSITIVE_ONE,
				"Query page size. Defaults to " + DEFAULT_PAGE_SIZE, 0, 1));
		defn.add(new Interface.Element("compress", BooleanType.DEFAULT,
				"Compress the output CSV file to GZIP format. Defaults to false.", 0, 1));
	}

	@Override
	public Access access() {
		return ACCESS_ACCESS;
	}

	@Override
	public Interface definition() {
		return _defn;
	}

	@Override
	public String description() {
		return "Exports the specified metadata (by xpath) to CSV file.";
	}

	@Override
	public void execute(Element args, Inputs arg1, Outputs outputs, XmlWriter w) throws Throwable {

		/*
		 * parse arguments
		 */
		String argWhere = args.value("where");
		String argNamespace = args.value("namespace");
		// construct query
		StringBuilder sb = new StringBuilder();
		if (argWhere != null) {
			sb.append("(").append(argWhere).append(")");
		}
		if (argNamespace != null) {
			if (sb.length() > 0) {
				sb.append(" and ");
			}
			sb.append("(namespace>='").append(argNamespace).append("')");
		}

		final String where = sb.length() > 0 ? sb.toString() : null;
		final List<XmlDoc.Element> xpaths = args.elements("xpath");
		final int step = args.intValue("step", DEFAULT_PAGE_SIZE);
		final boolean compress = args.booleanValue("compress", false);

		final PluginService.Output output = outputs.output(0);

		final PipedInputStream pis = new PipedInputStream();
		final PipedOutputStream pos = new PipedOutputStream(pis);
		PluginThread.executeAsync(SERVICE_NAME, () -> {
			try {
				try {
					if (compress) {
						try (GZIPOutputStream gos = new GZIPOutputStream(pos)) {
							exportCSV(executor(), where, xpaths, step, gos);
						}
					} else {
						exportCSV(executor(), where, xpaths, step, pos);
					}
				} finally {
					pos.close();
				}
			} catch (Throwable e) {
				e.printStackTrace(System.out);
			}
		});
		output.setData(pis, -1, compress ? "application/gzip" : "text/csv");
	}

	static void exportCSV(ServiceExecutor executor, String query, List<XmlDoc.Element> xpaths, int step, Path f)
			throws Throwable {
		try (OutputStream os = Files.newOutputStream(f)) {
			exportCSV(executor, query, xpaths, step, os);
		}
	}

	static void exportCSV(ServiceExecutor executor, String query, List<XmlDoc.Element> xpaths, int step,
			OutputStream os) throws Throwable {
		try (OutputStreamWriter osw = new OutputStreamWriter(os, "utf-8");
				BufferedWriter bw = new BufferedWriter(osw);
				PrintWriter pw = new PrintWriter(bw)) {
			exportCSV(executor, query, xpaths, step, pw);
		}
	}

	static void exportCSV(ServiceExecutor executor, String query, List<XmlDoc.Element> xpaths, int step, PrintWriter pw)
			throws Throwable {
		// header line
		writeCSVHeader(xpaths, pw);

		XmlDocMaker dm = new XmlDocMaker("args");
		if (query != null) {
			dm.add("where", query);
		}
		dm.add("idx", 1);
		dm.add("action", "get-value");
		dm.add("size", step);
		for (XmlDoc.Element xpath : xpaths) {
			dm.add(xpath);
		}
		XmlDoc.Element args = dm.root();

		long idx = 1;
		long remaining = Long.MAX_VALUE;
		boolean complete = false;
		while (!complete && remaining > 0) {
			PluginTask.checkIfThreadTaskAborted();
			args.element("idx").setValue(idx);
			XmlDoc.Element re = executor.execute("asset.query", args);
			complete = re.booleanValue("cursor/total/@complete");
			if (re.elementExists("cursor/remaining")) {
				remaining = re.longValue("cursor/remaining");
			}
			idx += step;

			List<XmlDoc.Element> aes = re.elements("asset");
			if (aes != null) {
				for (XmlDoc.Element ae : aes) {
					writeCSVRecord(ae, pw);
				}
				PluginTask.threadTaskCompleted(aes.size());
				PluginTask.threadTaskProcessed(aes.size());
			}
		}
	}

	private static void writeCSVRecord(Element ae, PrintWriter pw) {
		List<XmlDoc.Element> es = ae.elements();
		if (es != null && !es.isEmpty()) {
			for (XmlDoc.Element e : es) {
				String value = e.value();
				pw.print(safeCSVValue(value) + ",");
			}
			pw.println();
		}
	}

	private static String safeCSVValue(String value) {
		if (value == null) {
			return "";
		}
		if (value.contains("\"")) {
			value = value.replace('"', '\'');
		}
		if (value.contains(" ") || value.contains(",")) {
			return "\"" + value + "\"";
		}
		return value;
	}

	private static void writeCSVHeader(List<XmlDoc.Element> xpaths, PrintWriter w) throws Throwable {
		for (XmlDoc.Element xpath : xpaths) {
			String header = xpath.value("@ename");
			if (header == null) {
				header = xpath.value();
				if (!header.contains("(") && !header.contains(")")) {
					int idx = header.lastIndexOf("/");
					if (idx >= 0) {
						header = header.substring(idx + 1);
					}
					idx = header.lastIndexOf("@");
					if (idx >= 0) {
						header = header.substring(idx + 1);
					}
				}
			}
			w.print(header + ",");
		}
		w.println();
	}

	@Override
	public String name() {
		return SERVICE_NAME;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 1;
	}

	@Override
	public int minNumberOfOutputs() {
		return 1;
	}

	@Override
	public boolean canBeAborted() {
		return true;
	}

}
