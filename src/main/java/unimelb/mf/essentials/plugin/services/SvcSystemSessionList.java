package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.ServiceExecutor;
import arc.mf.plugin.dtype.*;
import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

import java.util.*;

public class SvcSystemSessionList extends PluginService {

    public static final String SERVICE_NAME = "unimelb.system.session.list";

    static class FilterOptions {
        public final String scope;
        public final String creatingThreadName;
        public final Long creatingThreadID;
        public final String originIP;
        public final String originTransport;
        public final String host;
        public final String domain;
        public final String user;
        public final String actorName;
        public final String actorType;
        public final String app;
        public final String hid;
        public final String licence;
        public final String active;
        public final Boolean pinned;
        public final String service;
        public final Date ctimeMin;
        public final Date ctimeMax;
        public final Date atimeMin;
        public final Date atimeMax;
        public final Date timeoutMin;
        public final Date timeoutMax;
        public final Long activityMin;
        public final Long activityMax;
        public final Integer priorityMin;
        public final Integer priorityMax;
        public final Integer nbTasksMin;
        public final Integer nbTasksMax;

        FilterOptions(XmlDoc.Element args) throws Throwable {
            this.scope = args.value("scope");
            this.creatingThreadName = args.value("create-thread-name");
            this.creatingThreadID = args.longValue("creating-thread-id", null);
            this.originIP = args.value("origin-ip");
            this.originTransport = args.value("origin-transport");
            this.host = args.value("host");
            this.domain = args.value("domain");
            this.user = args.value("user");
            this.actorName = args.value("actor-name");
            this.actorType = args.value("actor-type");
            this.app = args.value("app");
            this.hid = args.value("hid");
            this.licence = args.value("licence");
            this.active = args.value("active");
            this.pinned = args.booleanValue("pinned", null);
            this.service = args.value("service");

            this.ctimeMin = args.dateValue("ctime-min", null);
            this.ctimeMax = args.dateValue("ctime-max", null);
            if (this.ctimeMin != null && this.ctimeMax != null && this.ctimeMin.getTime() > this.ctimeMax.getTime()) {
                throw new IllegalArgumentException("Invalid arguments: ctime-min and ctime-max conflict");
            }

            this.atimeMin = args.dateValue("atime-min", null);
            this.atimeMax = args.dateValue("atime-max", null);
            if (this.atimeMin != null && this.atimeMax != null && this.atimeMin.getTime() > this.atimeMax.getTime()) {
                throw new IllegalArgumentException("Invalid arguments: atime-min and atime-max conflict");
            }

            this.timeoutMin = args.dateValue("timeout-min", null);
            this.timeoutMax = args.dateValue("timeout-max", null);
            if (this.timeoutMin != null && this.timeoutMax != null
                    && this.timeoutMin.getTime() > this.timeoutMax.getTime()) {
                throw new IllegalArgumentException("Invalid arguments: timeout-min and timeout-max conflict");
            }

            this.activityMin = args.longValue("activity-min", null);
            this.activityMax = args.longValue("activity-max", null);
            if (this.activityMin != null && this.activityMax != null && this.activityMin > this.activityMax) {
                throw new IllegalArgumentException("Invalid arguments: activity-min and activity-max conflict");
            }

            this.priorityMin = args.intValue("priority-min", null);
            this.priorityMax = args.intValue("priority-max", null);
            if (this.priorityMin != null && this.priorityMax != null && this.priorityMin > this.priorityMax) {
                throw new IllegalArgumentException("Invalid arguments: priority-min and priority-max conflict");
            }

            this.nbTasksMin = args.intValue("nb-tasks-min", null);
            this.nbTasksMax = args.intValue("nb-tasks-max", null);
            if (this.nbTasksMin != null && this.nbTasksMax != null && this.nbTasksMin > this.nbTasksMax) {
                throw new IllegalArgumentException("Invalid arguments: nb-tasks-min and nb-tasks-max conflict");
            }

        }

        public void save(XmlWriter w) throws Throwable {
            w.push("filters");
            if (this.scope != null) {
                w.add("scope", this.scope);
            }
            if (this.creatingThreadID != null) {
                w.add("creating-thread-id", this.creatingThreadID);
            }
            if (this.creatingThreadName != null) {
                w.add("creating-thread-name", this.creatingThreadName);
            }
            if (this.originIP != null) {
                w.add("origin-ip", this.originIP);
            }
            if (this.originTransport != null) {
                w.add("origin-transport", this.originTransport);
            }
            if (this.host != null) {
                w.add("host", this.host);
            }
            if (this.domain != null) {
                w.add("domain", this.domain);
            }
            if (this.user != null) {
                w.add("user", this.user);
            }
            if (this.actorName != null) {
                w.add("actor-name", this.actorName);
            }
            if (this.actorType != null) {
                w.add("actor-type", this.actorType);
            }
            if (this.app != null) {
                w.add("app", this.app);
            }
            if (this.hid != null) {
                w.add("hid", this.hid);
            }
            if (this.licence != null) {
                w.add("licence", this.licence);
            }
            if (this.active != null) {
                w.add("active", this.active);
            }
            if (this.pinned != null) {
                w.add("pinned", this.pinned);
            }
            if (this.service != null) {
                w.add("service", this.service);
            }
            if (this.ctimeMin != null || this.ctimeMax != null) {
                w.add("ctime", new String[]{"min", this.ctimeMin == null ? null : DateTime.string(this.ctimeMin),
                        "max", this.ctimeMax == null ? null : DateTime.string(this.ctimeMax)});
            }
            if (this.atimeMin != null || this.atimeMax != null) {
                w.add("atime", new String[]{"min", this.atimeMin == null ? null : DateTime.string(this.atimeMin),
                        "max", this.atimeMax == null ? null : DateTime.string(this.atimeMax)});
            }
            if (this.timeoutMin != null || this.timeoutMax != null) {
                w.add("timeout",
                        new String[]{"min", this.timeoutMin == null ? null : DateTime.string(this.timeoutMin), "max",
                                this.timeoutMax == null ? null : DateTime.string(this.timeoutMax)});
            }
            if (this.activityMin != null || this.activityMax != null) {
                w.add("activity",
                        new String[]{"min", this.activityMin == null ? null : Long.toString(this.activityMin),
                                "max", this.activityMax == null ? null : Long.toString(this.activityMax)});
            }
            if (this.priorityMin != null || this.priorityMax != null) {
                w.add("priority",
                        new String[]{"min", this.priorityMin == null ? null : Integer.toString(this.priorityMin),
                                "max", this.priorityMax == null ? null : Integer.toString(this.priorityMax)});
            }
            if (this.nbTasksMin != null || this.nbTasksMax != null) {
                w.add("nb-tasks",
                        new String[]{"min", this.nbTasksMin == null ? null : Integer.toString(this.nbTasksMin), "max",
                                this.nbTasksMax == null ? null : Integer.toString(this.nbTasksMax)});

            }
            w.pop();
        }
    }

    static class Session implements Comparable<Session> {
        public final long id;
        public final boolean external;
        public final Long creatingThreadID;
        public final String creatingThreadName;
        public final String originIP;
        public final String originTransport;
        public final String host;
        public final String domain;
        public final String user;
        public final String app;
        public final String hid;
        public final String licence;
        public final String actorType;
        public final String actorName;
        public final boolean pinned;
        public final int active;
        public final long activity;
        public final long maxblocktime;
        public final int priority;
        public final boolean transactionsSync;
        public final Collection<String> tasks;
        public final String service;
        public final Date ctime;
        public final Date atime;
        public final Date timeout;
        public final double timeoutSecs;
        public final String timezone;

        public Session(XmlDoc.Element se) throws Throwable {
            this.id = se.longValue("@id");
            this.external = se.booleanValue("@external");
            this.creatingThreadID = se.longValue("creating-thread/@id", null);
            this.creatingThreadName = se.value("creating-thread");
            this.originIP = se.value("origin");
            this.originTransport = se.value("origin/@transport");
            this.host = se.value("host");
            this.domain = se.value("domain");
            this.user = se.value("user");
            this.app = se.value("app");
            this.hid = se.value("hid");
            this.licence = se.value("licence");
            this.actorType = se.value("actor/@type");
            this.actorName = se.value("actor");
            this.pinned = se.booleanValue("pinned");
            this.active = se.intValue("active");
            this.activity = se.longValue("activity");
            this.maxblocktime = se.longValue("maxblocktime");
            this.priority = se.intValue("priority");
            this.transactionsSync = se.booleanValue("transactions/sync");
            this.tasks = se.values("task");
            this.ctime = se.dateValue("ctime");
            this.atime = se.dateValue("atime");
            this.timeout = se.dateValue("timeout");
            this.timeoutSecs = se.doubleValue("timeout/@secs");
            this.timezone = se.value("timezone");
            this.service = se.value("service");
        }

        public int nbTasks() {
            return this.tasks == null ? 0 : this.tasks.size();
        }

        @Override
        public int compareTo(Session o) {
            if (!this.actorName.equals(o.actorName)) {
                return this.actorName.compareTo(o.actorName);
            }
            if (this.app != null && o.app != null) {
                return this.app.compareTo(o.app);
            }
            if (this.service != null && o.service != null) {
                return this.service.compareTo(o.service);
            }
            if (!this.originIP.equals(o.originIP)) {
                return this.originIP.compareTo(o.originIP);
            }
            if (this.hid != null && o.hid != null) {
                return this.hid.compareTo(o.hid);
            }
            if (this.nbTasks() >= 0 && o.nbTasks() >= 0) {
                return Integer.compare(this.nbTasks(), o.nbTasks());
            }
            if (this.activity >= 0 && o.activity >= 0) {
                return Long.compare(o.activity, this.activity);
            }
            if (this.licence != null && o.licence != null) {
                return this.licence.compareTo(o.licence);
            }
            if (this.active != o.active) {
                return Integer.compare(o.active, this.active);
            }
            if (this.priority != o.priority) {
                return Integer.compare(o.priority, this.priority);
            }
            if (this.pinned != o.pinned) {
                return Boolean.compare(this.pinned, o.pinned);
            }
            if (this.timeout != null && o.timeout != null) {
                return this.timeout.compareTo(o.timeout);
            }
            return Long.compare(o.id, this.id);
        }

        public boolean matches(FilterOptions fops) {
            if (fops.active != null) {
                if ("active".equalsIgnoreCase(fops.active)) {
                    if (this.active == 0) {
                        return false;
                    }
                } else if ("inactive".equalsIgnoreCase(fops.active)) {
                    if (this.active == 1) {
                        return false;
                    }
                }
            }
            if (fops.activityMin != null) {
                if (this.activity < fops.activityMin) {
                    return false;
                }
            }
            if (fops.activityMax != null) {
                if (this.activity > fops.activityMax) {
                    return false;
                }
            }
            if (fops.actorName != null) {
                if (this.actorName == null || !this.actorName.equals(fops.actorName)) {
                    return false;
                }
            }
            if (fops.actorType != null) {
                if (this.actorType != null && !fops.actorType.equals(this.actorType)) {
                    return false;
                }
            }
            if (fops.app != null) {
                if (this.app == null || !this.app.equals(fops.app)) {
                    return false;
                }
            }
            if (fops.atimeMin != null) {
                if (this.atime.compareTo(fops.atimeMin) < 0) {
                    return false;
                }
            }
            if (fops.atimeMax != null) {
                if (this.atime.compareTo(fops.atimeMax) > 0) {
                    return false;
                }
            }
            if (fops.creatingThreadID != null) {
                if (!fops.creatingThreadID.equals(this.creatingThreadID)) {
                    return false;
                }
            }
            if (fops.creatingThreadName != null) {
                if (!fops.creatingThreadName.equals(this.creatingThreadName)) {
                    return false;
                }
            }
            if (fops.ctimeMin != null) {
                if (this.ctime.compareTo(fops.ctimeMin) < 0) {
                    return false;
                }
            }
            if (fops.ctimeMax != null) {
                if (this.ctime.compareTo(fops.ctimeMax) > 0) {
                    return false;
                }
            }
            if (fops.domain != null) {
                if (!fops.domain.equals(this.domain)) {
                    return false;
                }
            }
            if (fops.hid != null) {
                if (!fops.hid.equalsIgnoreCase(this.hid)) {
                    return false;
                }
            }
            if (fops.host != null) {
                if (!fops.host.equalsIgnoreCase(this.host)) {
                    return false;
                }
            }
            if (fops.licence != null) {
                if (!fops.licence.equalsIgnoreCase(this.licence)) {
                    return false;
                }
            }
            if (fops.originIP != null) {
                if (!fops.originIP.equalsIgnoreCase(this.originIP)) {
                    return false;
                }
            }
            if (fops.originTransport != null) {
                if (!fops.originTransport.equalsIgnoreCase(this.originTransport)) {
                    return false;
                }
            }
            if (fops.pinned != null) {
                if (fops.pinned != this.pinned) {
                    return false;
                }
            }
            if (fops.priorityMin != null) {
                if (this.priority < fops.priorityMin) {
                    return false;
                }
            }
            if (fops.priorityMax != null) {
                if (this.priority > fops.priorityMax) {
                    return false;
                }
            }
            if (fops.scope != null) {
                if (fops.scope.equalsIgnoreCase("internal")) {
                    if (this.external) {
                        return false;
                    }
                } else if (fops.scope.equalsIgnoreCase("external")) {
                    if (!this.external) {
                        return false;
                    }
                }
            }
            if (fops.service != null) {
                if (!fops.service.equals(this.service)) {
                    return false;
                }
            }
            if (fops.nbTasksMin != null) {
                if (this.nbTasks() < fops.nbTasksMin) {
                    return false;
                }
            }
            if (fops.nbTasksMax != null) {
                if (this.nbTasks() > fops.nbTasksMax) {
                    return false;
                }
            }
            if (fops.timeoutMin != null) {
                if (this.timeout.compareTo(fops.timeoutMin) < 0) {
                    return false;
                }
            }
            if (fops.timeoutMax != null) {
                if (this.timeout.compareTo(fops.timeoutMax) > 0) {
                    return false;
                }
            }
            return true;
        }

        public void save(XmlWriter w) throws Throwable {
            w.add("session",
                    new String[]{"id", Long.toString(this.id), "domain", this.domain, "user", this.user, "actor",
                            this.actorName, "origin", this.originIP, "app", this.app, "service", this.service,
                            "nb-tasks", Integer.toString(this.nbTasks())});
        }
    }

    private final Interface _defn;

    public SvcSystemSessionList() {

        _defn = new Interface();

        _defn.add(new Interface.Element("scope", new EnumType(new String[]{"external", "internal", "both"}),
                "Filter by scope: external, interal, or both. The default is 'both'", 0, 1));

        _defn.add(new Interface.Element("creating-thread-name", StringType.DEFAULT, "Filter by creating-thread name", 0,
                1));

        _defn.add(new Interface.Element("creating-thread-id", LongType.POSITIVE_ONE, "Filter by creating-thread/@id", 0,
                1));

        _defn.add(new Interface.Element("origin-ip", StringType.DEFAULT,
                "Filter by origin of connection that created the session. This will be the IP address of the host where the client is running.",
                0, 1));

        _defn.add(new Interface.Element("origin-transport", StringType.DEFAULT, "Filter by origin/@transport", 0, 1));

        _defn.add(new Interface.Element("host", StringType.DEFAULT, "Filter by host name.", 0, 1));

        _defn.add(new Interface.Element("domain", StringType.DEFAULT, "Filter by domain name.", 0, 1));

        _defn.add(new Interface.Element("user", StringType.DEFAULT, "Filter by user name.", 0, 1));

        _defn.add(new Interface.Element("hid", StringType.DEFAULT, "Filter by hid.", 0, 1));

        _defn.add(new Interface.Element("licence", StringType.DEFAULT, "Filter by licence type.", 0, 1));

        _defn.add(new Interface.Element("actor-name", StringType.DEFAULT, "Filter by actor", 0, 1));

        _defn.add(new Interface.Element("actor-type", StringType.DEFAULT, "Filter by actor type", 0, 1));

        _defn.add(new Interface.Element("pinned", BooleanType.DEFAULT, "Filter by pinned.", 0, 1));

        _defn.add(new Interface.Element("active", new EnumType(new String[]{"active", "inactive", "both"}),
                "Filter by sessions that are active, inactive, or both. The default is 'both'", 0, 1));

        _defn.add(new Interface.Element("app", StringType.DEFAULT,
                "If specified limits to sessions for this application.", 0, 1));

        _defn.add(new Interface.Element("service", StringType.DEFAULT, "Filter by service name.", 0, 1));

        _defn.add(new Interface.Element("ctime-min", DateType.DEFAULT, "Filter by minimum ctime (inclusive).", 0, 1));

        _defn.add(new Interface.Element("ctime-max", DateType.DEFAULT, "Filter by maximum ctime (inclusive).", 0, 1));

        _defn.add(new Interface.Element("atime-min", DateType.DEFAULT, "Filter by minimum atime (inclusive).", 0, 1));

        _defn.add(new Interface.Element("atime-max", DateType.DEFAULT, "Filter by maximum atime (inclusive).", 0, 1));

        _defn.add(
                new Interface.Element("timeout-min", DateType.DEFAULT, "Filter by minimum timeout (inclusive).", 0, 1));

        _defn.add(
                new Interface.Element("timeout-max", DateType.DEFAULT, "Filter by maximum timeout (inclusive).", 0, 1));

        _defn.add(new Interface.Element("priority-min", IntegerType.POSITIVE, "Filter by minimum priority (inclusive).",
                0, 1));

        _defn.add(new Interface.Element("priority-max", IntegerType.POSITIVE, "Filter by maximum priority (inclusive).",
                0, 1));

        _defn.add(new Interface.Element("activity-min", LongType.POSITIVE, "Filter by minimum activity (inclusive).",
                0, 1));

        _defn.add(new Interface.Element("activity-max", LongType.POSITIVE, "Filter by maximum activity (inclusive).",
                0, 1));

        _defn.add(new Interface.Element("nb-tasks-min", IntegerType.POSITIVE,
                "Filter by minimum number of tasks (inclusive).", 0, 1));

        _defn.add(new Interface.Element("nb-tasks-max", IntegerType.POSITIVE,
                "Filter by maximum number of tasks (inclusive).", 0, 1));

        _defn.add(new Interface.Element("show-filters", BooleanType.DEFAULT, "Show filters in the service result.", 0,
                1));
    }

    @Override
    public Access access() {
        return ACCESS_ADMINISTER;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "list the system sessions.";
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {
        boolean showFilters = args.booleanValue("show-filters", false);
        FilterOptions fops = new FilterOptions(args);
        List<Session> sessions = getSessions(executor(), fops);
        if (showFilters) {
            fops.save(w);
        }
        int count = 0;
        if (sessions != null) {
            count = sessions.size();
            for (Session session : sessions) {
                session.save(w);
            }
        }
        w.add("count", count);
    }

    static List<Session> getSessions(ServiceExecutor executor, FilterOptions fops) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("scope", "both");
        dm.add("permissions", false);
        dm.add("size", "infinity");

        if (fops.active != null) {
            dm.add("active", fops.active);
        }
        if (fops.app != null) {
            dm.add("app", fops.app);
        }
        if (fops.domain != null) {
            dm.add("domain", fops.domain);
        }
        if (fops.user != null) {
            dm.add("user", fops.user);
        }
        if (fops.licence != null) {
            dm.add("licence", fops.licence);
        }
        if (fops.originIP != null) {
            dm.add("origin", fops.originIP);
        }

        XmlDoc.Element re = executor.execute("system.session.all.describe", dm.root());
        List<XmlDoc.Element> ses = re.elements("session");
        if (ses != null && !ses.isEmpty()) {
            List<Session> sessions = new ArrayList<>();
            for (XmlDoc.Element se : ses) {
                Session session = new Session(se);
                if (session.matches(fops)) {
                    sessions.add(session);
                }
            }
            Collections.sort(sessions);
            return sessions;
        }
        return null;
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

}
