package unimelb.mf.essentials.plugin.services;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.EmailAddressType;
import arc.mf.plugin.dtype.StringType;
import arc.mf.plugin.dtype.XmlDocType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.plugin.util.AssetNamespaceUtils;
import unimelb.mf.plugin.util.AssetUtils;
import unimelb.mf.plugin.util.collection.CollectionDetails;
import unimelb.mf.plugin.util.collection.CollectionType;
import unimelb.utils.FileSizeUtils;

public class SvcAssetCollectionDestroy extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.collection.destroy";

    public static final String NOTIFY_SUBJECT = "Completed Deletion of Mediaflux Asset Collections";

    private final Interface _defn;

    public SvcAssetCollectionDestroy() {
        _defn = new Interface();
        _defn.add(new Interface.Element("path", StringType.DEFAULT,
                "The path of the asset namespace or collection asset to be deleted.", 1, Integer.MAX_VALUE));
        Interface.Element notify = new Interface.Element("notify", XmlDocType.DEFAULT,
                "Send email notification after all the collections are destroyed.", 0, 1);
        notify.add(new Interface.Element("to", EmailAddressType.DEFAULT, "Recipient email address.", 0, 10));
        notify.add(new Interface.Element("self", BooleanType.DEFAULT,
                "Send to the current user if email address is available. Defaults to false.", 0, 1));
        notify.add(new Interface.Element("subject", StringType.DEFAULT, "Email subject.", 0, 1));
        _defn.add(notify);
    }

    @Override
    public Access access() {
        return ACCESS_MODIFY;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Destorys one or more asset namespaces or collection assets (recursively).";
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public void execute(Element args, Inputs inputs, Outputs outputs, XmlWriter w) throws Throwable {

        final String notifySubject = args.stringValue("notify/subject", NOTIFY_SUBJECT);
        final Set<String> notifyRecipients = new LinkedHashSet<String>();
        final Collection<String> notifyTo = args.values("notify/to");
        if (notifyTo != null) {
            notifyRecipients.addAll(notifyTo);
        }
        final boolean notifySelf = args.booleanValue("notify/self", false);
        if (notifySelf) {
            String selfEmail = executor().execute("user.self.describe").value("user/e-mail");
            if (selfEmail != null) {
                notifyRecipients.add(selfEmail);
            }
        }
        final boolean notify = !notifyRecipients.isEmpty();

        Map<String, long[]> collectionSizes = notify ? new LinkedHashMap<String, long[]>() : null;

        Collection<String> collectionPaths = args.values("path");
        for (String collectionPath : collectionPaths) {
            PluginTask.checkIfThreadTaskAborted();
            CollectionDetails collection;
            if (AssetUtils.collectionAssetExists(collectionPath, executor())) {
                collection = new CollectionDetails(collectionPath, CollectionType.COLLECTION_ASSET, true);
            } else if (AssetNamespaceUtils.assetNamespaceExists(executor(), collectionPath)) {
                collection = new CollectionDetails(collectionPath, CollectionType.ASSET_NAMESPACE, true);
            } else {
                throw new IllegalArgumentException("Collection '" + collectionPath + "' does not exist");
            }
            if (notify) {
                PluginTask.checkIfThreadTaskAborted();
                long[] nbFilesAndCSize = sumContentSize(collection);
                collectionSizes.put(collectionPath, nbFilesAndCSize);
            }
            PluginTask.checkIfThreadTaskAborted();
            destoryAssetCollection(collection);
        }

        if (notify) {
            PluginTask.checkIfThreadTaskAborted();
            sendNotification(notifyRecipients, notifySubject, collectionSizes);
        }
    }

    private void sendNotification(Set<String> recipients, String notifySubject, Map<String, long[]> nbFilesAndCSize)
            throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("subject", notifySubject);
        dm.add("body", new String[] { "type", "text/html" }, constructEmailBody(nbFilesAndCSize));
        for (String email : recipients) {
            dm.add("to", email);
        }
        executor().execute("mail.send", dm.root());
    }

    private String constructEmailBody(Map<String, long[]> namespaceSizes) throws Throwable {

        String serverAddr = getServerAddress();

        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<style>\n");
        sb.append(
                "table, th, td { border: 1px solid black; font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace; }\n");
        sb.append(".tal { text-align: left; }\n");
        sb.append(".tar { text-align: right; }\n");
        sb.append("tr:nth-child(even) { background: #cccccc }\n");
        sb.append("tr:nth-child(odd) { background: #ffffff }\n");
        sb.append("</style>\n");
        sb.append("<p>The following asset collections (folders) have been deleted from Mediaflux server")
                .append(serverAddr == null ? "" : "(" + serverAddr + ")").append(":</p>");
        sb.append("<table style=\"width: 80%;\">");
        sb.append(
                "<thead><tr><th>Collection Path</th><th>Number of Files</th><th colspan=\"2\">Content Size</th></tr></thead>");

        sb.append("<tbody>");

        AtomicLong totalNbFiles = new AtomicLong(0);
        AtomicLong totalCSize = new AtomicLong(0);
        namespaceSizes.forEach((ns, nbFilesAndCSize) -> {
            totalNbFiles.getAndAdd(nbFilesAndCSize[0]);
            totalCSize.getAndAdd(nbFilesAndCSize[1]);
            sb.append("<tr>");
            sb.append("<td class=\"tal\">").append(ns).append("</td>");
            sb.append("<td class=\"tar\">").append(nbFilesAndCSize[0]).append("</td>");
            sb.append("<td class=\"tar\">").append(String.format("%d bytes", nbFilesAndCSize[1])).append("</td>");
            sb.append("<td class=\"tar\">").append(FileSizeUtils.toHumanReadable(nbFilesAndCSize[1])).append("</td>");
            sb.append("</tr>");
        });

        sb.append("<tr>");
        sb.append("<th class=\"tar\">Total</th>");
        sb.append("<th class=\"tar\">").append(totalNbFiles.get()).append("</th>");
        sb.append("<th class=\"tar\">").append(String.format("%d bytes", totalCSize.get())).append("</th>");
        sb.append("<th class=\"tar\">").append(FileSizeUtils.toHumanReadable(totalCSize.get())).append("</th>");
        sb.append("</tr>");

        sb.append("</tbody>");
        sb.append("</table>");
        sb.append("</html>");
        return sb.toString();
    }

    private long[] sumContentSize(CollectionDetails collection) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        if (collection.isAssetNamespace()) {
            dm.add("namespace", collection.path());
        } else {
            dm.add("collection", "path=" + collection.path());
        }
        dm.add("action", "sum");
        dm.add("xpath", "content/size");
        XmlDoc.Element re = executor().execute("asset.query", dm.root());
        long nbFiles = re.longValue("value/@nbe", 0);
        long csize = re.longValue("value", 0);
        return new long[] { nbFiles, csize };
    }

    private void destoryAssetCollection(CollectionDetails collection) throws Throwable {
        if (collection.isAssetNamespace()) {
            AssetNamespaceUtils.destroyAssetNamespace(executor(), collection.path());
        } else {
            AssetUtils.destroyAsset(executor(), "path=" + collection.path(), true);
        }
    }

    private String getServerAddress() throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("name", "asset.shareable.address");
        String url = executor().execute("server.property.get", dm.root()).value("property");
        if (url != null) {
            if (url.startsWith("https://")) {
                return url.substring(8);
            } else if (url.startsWith("http://")) {
                return url.substring(7);
            }
        }
        return url;
    }

}
