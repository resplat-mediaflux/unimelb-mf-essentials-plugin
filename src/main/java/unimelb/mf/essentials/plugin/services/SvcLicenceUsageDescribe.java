package unimelb.mf.essentials.plugin.services;

import unimelb.mf.essentials.plugin.util.AssetUtils;

import java.util.Collection;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;


public class SvcLicenceUsageDescribe extends PluginService {

	private Interface _defn;

	public SvcLicenceUsageDescribe()  throws Throwable {
		_defn = new Interface();
		_defn.add(new Interface.Element("namespace", StringType.DEFAULT, "Namespace where the licence tracking asset is located.", 1, 1));
		_defn.add(new Interface.Element("name", StringType.DEFAULT, "Name of licence usage asset. If not supplied, defaults to'"+SvcLicenceUsage.LICENCE_USAGE_ASSET_NAME+"'.", 0, 1));
		_defn.add(new Interface.Element("last-only", BooleanType.DEFAULT, "List only the last entry (default true).", 0, 1));
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public Interface definition() {
		return _defn;

	}

	public String description() {
		return "Describe tracked licence usage asset (created by service unimelb.licence.usage).";
	}

	public String name() {
		return "unimelb.licence.usage.describe";
	}

	public boolean canBeAborted() {
		return false;
	}

	@Override
	public int minNumberOfOutputs() {
		return 0;
	}

	@Override
	public int maxNumberOfOutputs() {
		return 0;
	}



	public void execute(Element args, Inputs inputs, Outputs outputs, final XmlWriter w) throws Throwable {

		String nameSpace = args.value("namespace");
		String name = args.value("name");
		if (name==null) {
			name = SvcLicenceUsage.LICENCE_USAGE_ASSET_NAME;
		}
		Boolean lastOnly = args.booleanValue("last-only", true);


		// FInd the licence usage asset
		String id = SvcLicenceUsage.findLicenceUsageAsset(executor(), nameSpace, name);
		if (id==null) {
			throw new Exception ("Asset of name '" + name + 
					"' could not be found in specified namespace '" + nameSpace + "'");
		}

		// Get the meta-data
		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);
		if (asset==null) return;
		XmlDoc.Element meta = asset.element("asset/meta/" + SvcLicenceUsage.LICENCE_USAGE_DOCTYPE);
		if (meta==null) return;
		if (lastOnly) {
			Collection<XmlDoc.Element> values = meta.elements();
			int n= values.size();
			XmlDoc.Element[] valArray = values.toArray(new XmlDoc.Element[n]);
			w.add(valArray[n-1]);
		} else {
			w.addAll(meta.elements());
		}
	}

}
