package unimelb.mf.essentials.plugin.services;

import arc.mf.plugin.PluginService;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;

public class SvcAssetContentSizeSumAllVersions extends PluginService {

    public static final String SERVICE_NAME = "unimelb.asset.content.size.sum.all.versions";
    private final Interface _defn;

    public SvcAssetContentSizeSumAllVersions() {
        _defn = new Interface();
        _defn.add(new Interface.Element("where", StringType.DEFAULT, "Query to select the assets.", 1, 1));
    }

    @Override
    public Access access() {
        return ACCESS_ACCESS;
    }

    @Override
    public Interface definition() {
        return _defn;
    }

    @Override
    public String description() {
        return "Sum the content size of all asset versions.";
    }

    @Override
    public boolean canBeAborted() {
        return true;
    }

    @Override
    public String name() {
        return SERVICE_NAME;
    }

    @Override
    public void execute(Element args, Inputs arg1, Outputs arg2, XmlWriter w) throws Throwable {
        String where = args.value("where");

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("where", where);
        dm.add("action", "sum");
        dm.add("xpath", new String[]{"result", "data.size.format(result)"}, "content/@total-size");
        XmlDoc.Element re = executor().execute("asset.query", dm.root());

        long nbe = re.longValue("value/@nbe", 0);
        long totalSize = re.longValue("value", 0);
        String totalSizeHR = re.value("value/@result");

        w.add("total", new String[]{"nbe", Long.toString(nbe), "h", totalSizeHR}, totalSize);
    }

}
