package unimelb.mf.essentials.plugin.services;

//import java.util.Collection;

import arc.mf.plugin.*;
//import arc.mf.plugin.PluginService.Interface;
import arc.mf.plugin.dtype.AssetType;
import arc.mf.plugin.dtype.BooleanType;
//import arc.mf.plugin.dtype.IntegerType;
//import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
//import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.AssetUtils;

public class SvcAssetWormStateCheck extends PluginService {


	private Interface _defn;


	public SvcAssetWormStateCheck() {
		_defn = new Interface();
		_defn.add(new Interface.Element("id", AssetType.DEFAULT, "The path to the asset to be replicated.", 0,
				1));
		_defn.add(new Interface.Element("unset-only", BooleanType.DEFAULT, "Only report if the asset is not in WORM state (default false).", 0, 1));
	}
	public String name() {
		return "unimelb.asset.worm.state.check";
	}

	public String description() {
		return "Checks the WORM state of an asset.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ADMINISTER;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		// Get inputs
		String id = args.value("id");
		Boolean unsetOnly = args.booleanValue("unset-only", false);

		// Get the asset
		XmlDoc.Element asset = AssetUtils.getAsset(executor(), null, null, id);
		XmlDoc.Element worm = asset.element("asset/worm-status");
		if (worm==null) {
			w.add("id", new String[] {"worm-state", "unset"}, id);
		} else {
			XmlDoc.Element expiry = worm.element("expiry");
			Boolean active = expiry.booleanValue("@active");
			if (!unsetOnly) {
				if (active) {
					w.add("id", new String[] {"worm-state", "active"}, id);
				} else {
					w.add("id", new String[] {"worm-state", "expired"}, id);
				}
			}
		}
	}
}
