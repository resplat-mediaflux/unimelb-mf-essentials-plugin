package unimelb.mf.essentials.plugin.services;

import java.util.Collection;

import arc.mf.plugin.*;
import arc.mf.plugin.dtype.BooleanType;
import arc.mf.plugin.dtype.LongType;
import arc.mf.plugin.dtype.StringType;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.essentials.plugin.util.PeerUtil;

public class SvcReplicatePrimaryFindFromReplica extends PluginService {


	private Interface _defn;

	private Long _count = 0L;

	public SvcReplicatePrimaryFindFromReplica() {
		_defn = new Interface();
		_defn.add(new Interface.Element("peer",StringType.DEFAULT, "Name of peer that objects have been replicated to.", 1, 1));
		_defn.add(new Interface.Element("where",StringType.DEFAULT, "Query predicate to restrict the selected assets on the remote peer server.", 1, 1));
		_defn.add(new Interface.Element("size", LongType.DEFAULT, "Iterator size for the query cursor (default 10000).", 0, 1));
		_defn.add(new Interface.Element("use-indexes", BooleanType.DEFAULT, "Turn on or off the use of indexes in the query. Defaults to true.", 0, 1));
	}
	public String name() {
		return "unimelb.replicate.primary.find.from.replica";
	}

	public String description() {
		return "This (very specialised) service iterates through all the assets on the replica peer specified in the 'where' element.  It then attempts to find the primary asset by primary ID (on the local host). If it finds it, it compares the paths (including asset name) and reports any where the primary path is not contained in replica path (so it's only useful if the assets are all named). This is a means of finding assets that may have been replciated twice (from the same primary) with different names (or paths). If a replica asset is not found on the primary by ID that is reported also.";
	}

	public Interface definition() {
		return _defn;
	}

	public Access access() {
		return ACCESS_ACCESS;
	}

	public int executeMode() {
		return EXECUTE_LOCAL;
	}

	public boolean canBeAborted() {

		return true;
	}

	public void execute(XmlDoc.Element args, Inputs in, Outputs out, XmlWriter w) throws Throwable {

		
		int[] idx = new int[]{1};

		// Get inputs
		String where = args.value("where");
		String size = args.stringValue("size", "10000");
		Boolean useIndexes = args.booleanValue("use-indexes", true);
		String peer = args.value("peer");


		// Iterate through cursor and build list of assets 
		boolean more = true;
		ServerRoute srDR = PeerUtil.findPeerRoute(executor(), peer);

		_count = 0L;
		while (more) {
			more = find (executor(), where, srDR,  size, useIndexes, idx, w);
			PluginTask.checkIfThreadTaskAborted();
		}
		w.add("total-checked", _count);
	}




	private boolean find (ServiceExecutor executor, String where, ServerRoute srDR, 
			String size,  Boolean useIndexes,
			 int[] idx, XmlWriter w)	throws Throwable {

		
		// FInd assets on DR server
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("where", where);
		dm.add("idx", idx[0]);
		dm.add("size", size);
		dm.add("pdist", 0);
		dm.add("action", "get-meta");
		dm.add("use-indexes", useIndexes);
		XmlDoc.Element r = executor().execute(srDR, "asset.query", dm.root());
		if (r==null) return false;  
		//
		
		Collection<XmlDoc.Element> rAssets = r.elements("asset");
		if (rAssets==null) return false;

		// Get the cursor and increment for next time
		XmlDoc.Element cursor = r.element("cursor");
		boolean more = !(cursor.booleanValue("total/@complete"));
		if (more) {
			Integer next = cursor.intValue("next");
			idx[0] = next;
		}

		// Iterate through replica assets
		_count += rAssets.size();
		for (XmlDoc.Element rAsset : rAssets) {
			PluginTask.checkIfThreadTaskAborted();

			String rid = rAsset.value("rid"); 
			String rpath = rAsset.value("path");

			
			// FInd asset locally using the rid which contains the primary ID
			String parts[] = rid.split("\\.");
			String id = parts[1];
			dm = new XmlDocMaker("args");
			dm.add("id", id);
			r = executor.execute ("asset.exists", dm.root());		
			if (r.booleanValue("exists")) {
				dm = new XmlDocMaker("args");
				dm.add("id", id);
				r = executor.execute("asset.get", dm.root());
				String path = r.value("asset/path");
				if (!rpath.contains(path)) {
					w.push("asset");
					w.add("rid", rid);
					w.add("rpath", rpath);
					w.add("path", path);
					w.pop();
				}
			} else {
				w.push("asset");
				w.push("asset");
				w.add("rid", rid);
				w.add("rpath", rpath);
				w.add("path", "does-not-exist");
				w.pop();
			}
		}

		//
		return more;
	}


}
