package unimelb.mf.essentials.plugin.script;

public enum TargetOS {
    WINDOWS("\r\n"), UNIX("\n");
    private String _lineSeparator;

    TargetOS(String lineSeparator) {
        _lineSeparator = lineSeparator;
    }

    public String lineSeparator() {
        return _lineSeparator;
    }

    public static TargetOS fromString(String s) {
        if (s != null) {
            TargetOS[] vs = values();
            for (TargetOS v : vs) {
                if (v.name().equalsIgnoreCase(s)) {
                    return v;
                }
            }
        }
        return null;
    }
}
