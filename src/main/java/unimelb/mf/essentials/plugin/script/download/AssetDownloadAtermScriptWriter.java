package unimelb.mf.essentials.plugin.script.download;

import java.io.OutputStream;
import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import unimelb.mf.essentials.plugin.script.TargetOS;
import unimelb.utils.URIBuilder;

public abstract class AssetDownloadAtermScriptWriter extends AssetDownloadScriptWriter {

    public static final String TOKEN_APP = "aterm";

    public static final String TOKEN_TAG = "UNIMELB_DOWNLOAD_ATERM_SCRIPT";

    public static final String ATERM_PATH = "/mflux/aterm.jar";

    public static final int DEFAULT_NUMBER_OF_CONCURRENT_SERVER_REQUESTS = 1;

    public static final String ARG_NCSR = "ncsr"; // int
    public static final String ARG_OVERWRITE = "overwrite"; // boolean
    public static final String ARG_VERBOSE = "verbose"; // boolean

    protected AssetDownloadAtermScriptWriter(OutputStream out, boolean closeOut, AssetDownloadScriptSettings settings)
            throws Throwable {
        super(out, closeOut, settings);
    }

    public String atermUrl() throws Throwable {
        return new URIBuilder().setScheme(server().transport()).setHost(server().host()).setPort(server().port())
                .setPath(ATERM_PATH).build().toString();
    }

    public int numberOfConcurrentServerRequests() {
        Integer ncsr = (Integer) argValue(ARG_NCSR);
        if (ncsr == null) {
            return DEFAULT_NUMBER_OF_CONCURRENT_SERVER_REQUESTS;
        }
        return ncsr;
    }

    public boolean verbose() {
        Boolean verbose = (Boolean) argValue(ARG_VERBOSE);
        if (verbose == null) {
            return true;
        } else {
            return verbose;
        }
    }

    public boolean overwrite() {
        Boolean overwrite = (Boolean) argValue(ARG_OVERWRITE);
        if (overwrite == null) {
            return true;
        } else {
            return overwrite;
        }
    }

    public abstract void addNamespace(String namespace);

    public void addNamespaces(Collection<String> namespaces) {
        if (namespaces != null) {
            for (String namespace : namespaces) {
                addNamespace(namespace);
            }
            flush();
        }
    }

    public void addAssets(Collection<String> ids) {
        if (ids != null) {
            for (String id : ids) {
                addQuery("id=" + id);
            }
            flush();
        }
    }

    public abstract void addQuery(String where);

    public void process(ServiceExecutor executor) {
        if (this.ids != null) {
            addAssets(this.ids);
        }
        if (this.namespaces != null) {
            addNamespaces(this.namespaces);
        }
        if (this.where != null) {
            addQuery(where);
        }
    }

    public static AssetDownloadAtermScriptWriter create(OutputStream out, boolean closeOut, TargetOS targetOS,
            AssetDownloadScriptSettings settings) throws Throwable {
        return targetOS == TargetOS.UNIX ? new AssetDownloadAtermUnixScriptWriter(out, closeOut, settings)
                : new AssetDownloadAtermWindowsScriptWriter(out, closeOut, settings);
    }

}
