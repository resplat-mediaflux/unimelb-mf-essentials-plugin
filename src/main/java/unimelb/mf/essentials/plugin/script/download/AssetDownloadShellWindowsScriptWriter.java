package unimelb.mf.essentials.plugin.script.download;

import java.io.OutputStream;

import arc.utils.DateTime;
import unimelb.mf.essentials.plugin.script.TargetOS;

public class AssetDownloadShellWindowsScriptWriter extends AssetDownloadShellScriptWriter {

    public AssetDownloadShellWindowsScriptWriter(OutputStream out, boolean closeOut,
            AssetDownloadScriptSettings settings) throws Throwable {
        super(out, closeOut, settings);
    }

    @Override
    protected void writeHead() throws Throwable {
        // -LiteralPath is required to fix the issue that it failed to find the file
        // when its name contains square bracket.
        //
        // https://superuser.com/questions/212808/powershell-bug-in-copy-move-rename-when-filename-contains-square-bracket-charac
        // https://www.reddit.com/r/PowerShell/comments/2wmzv8/why_you_should_consider_always_using_literalpath/
        println("$ErrorActionPreference = \"Stop\"");
        println();
        println("function Download-File {");
        println("    $Id = $Args[0]");
        println("    $Url = \"$($URI)&id=$($Id)\"");
        println("    $Dst = \"$($DIR)\\$($Args[1])\"");
        println("    if (Test-Path -LiteralPath $Dst) {");
        println("        Write-Host \"'$($Dst)' already exists. Skipped.\"");
        println("        return");
        println("    }");
        println("    $DstDir = (Split-Path -LiteralPath $Dst)");
        println("    $DstFileName = (Split-Path -Leaf $Dst)");
        println("    $DstTmp = \"$($Dst).download\"");
        println("    if (-Not (Test-Path -LiteralPath $DstDir)) {");
        println("        New-Item -ItemType directory -Force -Path \"$($DstDir)\"");
        println("    }");
        println("    Write-Host \"downloading '$($Dst)'\"");
        println("    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true};(New-Object Net.WebClient).DownloadFile($Url, $DstTmp)");
        println("    Rename-Item -LiteralPath $DstTmp -NewName $DstFileName");
        println("}");
        println();
        println("If ( $Args.count -eq 0 ) {");
        println("    $DIR = (Get-Location)");
        println("} Else {");
        println("    $DIR = $Args[0]");
        println("}");
        println();
        println(String.format("Write-Host \"Mediaflux auth token expiry: %s\"", DateTime.string(tokenExpiry())));
        println();
        println(String.format("$URI = \"%s\"", servletURI()));
        flush();
    }

    @Override
    protected void writeTail() {
        flush();
    }

    @Override
    public void addAsset(String assetId, String dstPath) {
        println(String.format("Download-File %s \"%s\"", assetId, dstPath.replace('/', '\\')));
    }

    @Override
    public final TargetOS targetOS() {
        return TargetOS.WINDOWS;
    }

}
