package unimelb.mf.essentials.plugin.script.download;

import java.io.OutputStream;
import java.util.List;

import arc.mf.plugin.ServiceExecutor;
import unimelb.mf.essentials.plugin.script.ClientScriptWriter;

public abstract class AssetDownloadScriptWriter extends ClientScriptWriter {

    public static final String TOKEN_DOWNLOADER_ROLE = "unimelb:token-downloader";

    protected String where;
    protected List<String> ids;
    protected List<String> namespaces;

    protected AssetDownloadScriptWriter(OutputStream out, boolean closeOut, AssetDownloadScriptSettings settings)
            throws Throwable {
        super(out, closeOut, settings);
        this.where = settings.where();
        this.ids = settings.ids();
        this.namespaces = settings.namespaces();
    }

    protected abstract void process(ServiceExecutor executor) throws Throwable;

}
