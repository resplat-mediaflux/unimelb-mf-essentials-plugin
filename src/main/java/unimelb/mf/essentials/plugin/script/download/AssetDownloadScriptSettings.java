package unimelb.mf.essentials.plugin.script.download;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import unimelb.mf.essentials.plugin.script.ClientScriptSettings;

public class AssetDownloadScriptSettings extends ClientScriptSettings {

    private String _where;
    private List<String> _ids;
    private List<String> _namespaces;

    public String where() {
        return _where;
    }

    public ClientScriptSettings setWhere(String where) {
        _where = where;
        return this;
    }

    public List<String> ids() {
        return _ids;
    }

    public ClientScriptSettings setIds(Collection<String> ids) {
        if (_ids != null) {
            _ids.clear();
        }
        if (ids != null && !ids.isEmpty()) {
            _ids = new ArrayList<String>(ids);
        } else {
            _ids = null;
        }
        return this;
    }

    public List<String> namespaces() {
        return _namespaces;
    }

    public ClientScriptSettings setNamespaces(Collection<String> namespaces) {
        if (_namespaces != null) {
            _namespaces.clear();
        }
        if (namespaces != null && !namespaces.isEmpty()) {
            _namespaces = new ArrayList<String>(namespaces);
        } else {
            _namespaces = null;
        }
        return this;
    }
}
