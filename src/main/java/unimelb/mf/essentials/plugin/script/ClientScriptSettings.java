package unimelb.mf.essentials.plugin.script;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import unimelb.mf.essentials.plugin.util.ServerDetails;

public class ClientScriptSettings {

    private String _fileName;
    private ServerDetails _server;
    private String _token;
    private String _tokenApp;
    private Date _tokenExpiry;
    private boolean _autoFlush = true;
    private Map<String, Object> _args = null;

    public String fileName() {
        return _fileName;
    }

    public ClientScriptSettings setFileName(String fileName) {
        _fileName = fileName;
        return this;
    }

    public ServerDetails serverDetails() {
        return _server;
    }

    public ClientScriptSettings setServerDetails(ServerDetails server) {
        _server = server;
        return this;
    }

    public String token() {
        return _token;
    }

    public ClientScriptSettings setToken(String token) {
        _token = token;
        return this;
    }

    public String tokenApp() {
        return _tokenApp;
    }

    public ClientScriptSettings setTokenApp(String tokenApp) {
        _tokenApp = tokenApp;
        return this;
    }

    public Date tokenExpiry() {
        return _tokenExpiry;
    }

    public ClientScriptSettings setTokenExpiry(Date tokenExpiry) {
        _tokenExpiry = tokenExpiry;
        return this;
    }

    public boolean autoFlush() {
        return _autoFlush;
    }

    public ClientScriptSettings setAutoFlush(boolean autoFlush) {
        _autoFlush = autoFlush;
        return this;
    }

    public Map<String, Object> args() {
        return _args;
    }

    public ClientScriptSettings addArg(String name, Object value) {
        if (_args == null) {
            _args = new LinkedHashMap<String, Object>();
        }
        _args.put(name, value);
        return this;
    }

}
