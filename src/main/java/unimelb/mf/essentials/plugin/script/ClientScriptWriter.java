package unimelb.mf.essentials.plugin.script;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import unimelb.mf.essentials.plugin.util.ServerDetails;

public abstract class ClientScriptWriter extends PrintWriter {

    private boolean _closeOut = true;

    private ServerDetails _serverDetails;
    private String _token;
    private String _tokenApp;
    private Date _tokenExpiry;

    private boolean _autoFlush;

    private boolean _closed = false;

    private Map<String, Object> _args;

    protected ClientScriptWriter(OutputStream out, boolean closeOut, ClientScriptSettings settings) throws Throwable {
        super(new BufferedWriter(new OutputStreamWriter(out, "UTF-8")), settings.autoFlush());
        _closeOut = closeOut;
        _serverDetails = settings.serverDetails();
        _token = settings.token();
        _tokenApp = settings.tokenApp();
        _tokenExpiry = settings.tokenExpiry();
        _args = new LinkedHashMap<String, Object>();
        if (settings.args() != null) {
            _args.putAll(settings.args());
        }
        _autoFlush = settings.autoFlush();
        writeHead();
    }

    protected Map<String, Object> args() {
        return _args;
    }

    protected Object argValue(String argName) {
        return _args.get(argName);
    }

    protected void setArg(String argName, Object argValue) {
        _args.put(argName, argValue);
    }

    protected void initialize() {

    }

    public ServerDetails server() {
        return _serverDetails;
    }

    public String token() {
        return _token;
    }

    public String tokenApp() {
        return _tokenApp;
    }

    public Date tokenExpiry() {
        return _tokenExpiry;
    }

    @Override
    public void println() {
        super.write(lineSeparator());
        if (_autoFlush) {
            super.flush();
        }
    }

    public abstract TargetOS targetOS();

    public final String lineSeparator() {
        return targetOS().lineSeparator();
    }

    protected void writeHead() throws Throwable {

    }

    protected void writeTail() {

    }

    public void close() {
        if (!_closed) {
            try {
                writeTail();
            } finally {
                _closed = true;
                if (_closeOut) {
                    super.close();
                }
            }
        } else {
            if (_closeOut) {
                super.close();
            }
        }
    }
}
