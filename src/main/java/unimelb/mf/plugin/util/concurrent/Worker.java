package unimelb.mf.plugin.util.concurrent;

import arc.mf.plugin.PluginLog;

import java.util.concurrent.BlockingQueue;

public class Worker implements Runnable {

    private final BlockingQueue<Task> _taskQueue;
    private final PluginLog _log;
    private final String _context;
    private final Listener _listener;
    private Throwable _lastTaskError;

    public Worker(BlockingQueue<Task> taskQueue, PluginLog log, String context, Listener listener) {
        _taskQueue = taskQueue;
        _log = log;
        _context = context;
        _listener = listener;
    }

    @Override
    public void run() {
        try {
            if (_listener != null) {
                _listener.workerStarted(this);
            }
            while (!Thread.interrupted()) {
                Task currentTask = _taskQueue.take();
                if (currentTask.isPoison()) {
                    break;
                }
                if (_listener != null) {
                    _listener.taskStarted(currentTask);
                }
                try {
                    currentTask.execute();
                } catch (Throwable t) {
                    _lastTaskError = t;
                    if (_listener != null) {
                        _listener.taskFailed(currentTask, t);
                    }
                    break;
                }
                if (_listener != null) {
                    _listener.taskCompleted(currentTask);
                }
            }
            if (_listener != null) {
                _listener.workerFinished(this, _lastTaskError);
            }
        } catch (Throwable t) {
            logError(t);
            if (_listener != null) {
                _listener.workerFinished(this, t);
            }
        }
    }

    private void logError(Throwable t) {
        String message = _context == null ? t.getMessage() : (t.getMessage() + " context: " + _context);
        if (_log != null) {
            _log.add(PluginLog.ERROR, message, t);
        } else {
            PluginLog.log().add(PluginLog.ERROR, message, t);
        }
    }

    public interface Listener {
        default void taskStarted(Task task) {

        }

        void taskCompleted(Task task);

        void taskFailed(Task task, Throwable t);

        default void workerStarted(Worker worker) {

        }

        void workerFinished(Worker worker, Throwable t);
    }
}
