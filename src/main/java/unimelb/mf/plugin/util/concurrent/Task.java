package unimelb.mf.plugin.util.concurrent;

public interface Task {

    void execute() throws Throwable;

    default boolean isPoison() {
        return false;
    }
}
