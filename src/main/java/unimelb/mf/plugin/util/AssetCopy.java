package unimelb.mf.plugin.util;

import arc.mf.plugin.PluginLog;
import arc.mf.plugin.PluginTask;
import arc.mf.plugin.PluginThread;
import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import arc.xml.XmlWriter;
import unimelb.mf.plugin.util.collection.CollectionDetails;
import unimelb.mf.plugin.util.collection.CollectionType;
import unimelb.mf.plugin.util.concurrent.PoisonTask;
import unimelb.mf.plugin.util.concurrent.Task;
import unimelb.mf.plugin.util.concurrent.Worker;
import unimelb.mf.plugin.util.concurrent.Worker.Listener;
import unimelb.mf.plugin.util.entity.Entity;
import unimelb.mf.util.AssetPathUtils;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class AssetCopy {

    public static final int DROP_LEVELS_ALL = -1;
    public static final int DROP_LEVELS_NONE = 0;
    public static final int PAGE_SIZE = 500;
    public static final int VERSION_LATEST = 0;
    public static final int VERSION_ALL = -1;

    public static Result copy(ServiceExecutor executor, CollectionDetails srcDir, CollectionDetails dstParentDir,
                              Options options) throws Throwable {
        return copy(executor, srcDir.path(), srcDir.isAssetNamespace(), dstParentDir.path(),
                dstParentDir.isAssetNamespace(), options);
    }

    public static Result copy(ServiceExecutor executor, String srcDirPath, boolean srcDirIsNamespace,
                              String dstParentDirPath, boolean dstParentDirIsNamespace, Options options) throws Throwable {
        if (srcDirIsNamespace) {
            return copy(executor, srcDirPath, DROP_LEVELS_ALL, dstParentDirPath, dstParentDirIsNamespace, options);
        } else {
            return copy(executor, "path=" + srcDirPath, VERSION_LATEST, DROP_LEVELS_ALL, dstParentDirPath,
                    dstParentDirIsNamespace, options);
        }
    }

    /**
     * Copy an asset of the specified version to the destination directory(asset
     * namespace or collection asset).
     *
     * @param executor          The service executor.
     * @param srcAssetId        The input asset id.
     * @param srcAssetVersion   The asset version to copy. Set to 0 to copy the
     *                          latest version; Set to null to copy all versions.
     * @param dropLevels        The number of top levels of the asset path to drop
     *                          when generating the output asset path. If the asset
     *                          has path /a/b/c/d (4 levels) and dropLevels is 2 and
     *                          dstDirPath is /x/y then the copied asset path will
     *                          be /x/y/c/d. Use 0 to preserve the full input asset
     *                          path. The special value of -1 (the default) means
     *                          drop all leading levels so the new asset will just
     *                          be created directly in dstDirPath
     * @param dstDirPath        The destination directory path.
     * @param dstDirIsNamespace Is the destination directory an asset namespace? If
     *                          it is set to false, it indicates the destination
     *                          namespace is a collection asset.
     * @return the corresponding asset (or asset namespace).
     * @throws Throwable Exception is thrown if an error occurs.
     */
    public static Result copy(ServiceExecutor executor, String srcAssetId, int srcAssetVersion, int dropLevels,
                              String dstDirPath, boolean dstDirIsNamespace, Options options) throws Throwable {

        XmlDoc.Element ae = AssetUtils.getAssetById(executor, srcAssetId, Math.max(srcAssetVersion, 0));
        srcAssetId = ae.value("@id");
        String srcAssetPath = ae.value("path");
        String srcAssetName = ae.stringValue("name", "__copy_of_asset_" + srcAssetId + "__");
        boolean isCollection = ae.elementExists("collection");
        String dstPath;
        if (dropLevels == DROP_LEVELS_NONE) {
            dstPath = AssetPathUtils.join(dstDirPath, srcAssetPath);
        } else if (dropLevels > 0) {
            String relativePath = AssetPathUtils.dropTopLevels(srcAssetPath, dropLevels);
            if (relativePath != null) {
                dstPath = AssetPathUtils.join(dstDirPath, relativePath);
            } else {
                dstPath = AssetPathUtils.join(dstDirPath, srcAssetName);
            }
        } else {
            dstPath = AssetPathUtils.join(dstDirPath, srcAssetName);
        }
        final Entity srcEntity = new Entity(srcAssetId, srcAssetPath, false, isCollection);
        if (isCollection) {
            PluginTask.checkIfThreadTaskAborted();
            if (AssetNamespaceUtils.assetNamespaceExists(executor, dstPath)) {
                if (options.ifExists == IfExists.IGNORE) {
                    String dstAssetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, dstPath);
                    Entity dstEntity = new Entity(dstAssetNamespaceId, dstPath, true, false);
                    return new Result(srcEntity, dstEntity, false);
                } else if (options.ifExists == IfExists.RENAME) {
                    dstPath = rename(executor, dstPath, true);
                    AssetNamespaceUtils.createAssetNamespace(executor, dstPath, false, false);
                    String dstAssetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, dstPath);
                    copyContents(executor, srcAssetPath, false, dstPath, true, options.childrenVersion, options.addNote,
                            options.pageSize, options.nbWorkers);
                    Entity dstEntity = new Entity(dstAssetNamespaceId, dstPath, true, false);
                    return new Result(srcEntity, dstEntity, true);
                } else {
                    throw new Exception("Asset namespace already exists at: '" + dstPath + "'.");
                }
            } else if (AssetUtils.collectionAssetExists(dstPath, executor)) {
                if (options.ifExists == IfExists.IGNORE) {
                    String dstCollectionAssetId = AssetUtils.assetIdFromPath(dstPath, executor);
                    Entity dstEntity = new Entity(dstCollectionAssetId, dstPath, false, true);
                    return new Result(srcEntity, dstEntity, false);
                } else if (options.ifExists == IfExists.RENAME) {
                    dstPath = rename(executor, dstPath, false);
                    String dstCollectionAssetId = AssetUtils.createCollectionAsset(executor, dstPath, false, false);
                    copyContents(executor, srcAssetPath, false, dstPath, false, options.childrenVersion,
                            options.addNote, options.pageSize, options.nbWorkers);
                    Entity dstEntity = new Entity(dstCollectionAssetId, dstPath, false, true);
                    return new Result(srcEntity, dstEntity, true);
                } else {
                    throw new Exception("Asset namespace already exists at: '" + dstPath + "'.");
                }
            } else {
                CollectionType dstType = CollectionType.typeFor(dstPath, executor);
                if (dstType == CollectionType.ASSET_NAMESPACE) {
                    AssetNamespaceUtils.createAssetNamespace(executor, dstPath, false, false);
                    String dstAssetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, dstPath);
                    copyContents(executor, srcAssetPath, false, dstPath, true, options.childrenVersion, options.addNote,
                            options.pageSize, options.nbWorkers);
                    Entity dstEntity = new Entity(dstAssetNamespaceId, dstPath, true, false);
                    return new Result(srcEntity, dstEntity, true);
                } else {
                    String dstCollectionAssetId = AssetUtils.createCollectionAsset(executor, dstPath, false, false);
                    copyContents(executor, srcAssetPath, false, dstPath, false, options.childrenVersion,
                            options.addNote, options.pageSize, options.nbWorkers);
                    Entity dstEntity = new Entity(dstCollectionAssetId, dstPath, false, true);
                    return new Result(srcEntity, dstEntity, true);
                }
            }
        } else {
            boolean dstAssetExists = AssetUtils.assetExists(dstPath, executor);
            String dstAssetId;
            if (dstAssetExists) {
                if (options.ifExists == IfExists.IGNORE) {
                    dstAssetId = AssetUtils.assetIdFromPath(dstPath, executor);
                    Entity dstEntity = new Entity(dstAssetId, dstPath, false, false);
                    return new Result(srcEntity, dstEntity, false);
                } else if (options.ifExists == IfExists.RENAME) {
                    dstPath = rename(executor, dstPath, false);
                    // dstAssetExists = false;
                } else {
                    throw new Exception("Asset already exists at: '" + dstPath + "'.");
                }
            }
            if (srcAssetVersion == VERSION_ALL) {
                dstAssetId = copyGeneralAssetAllVersions(executor, srcAssetId, dstPath, dstDirIsNamespace,
                        options.addNote);
            } else if (srcAssetVersion >= 0) {
                PluginTask.checkIfThreadTaskAborted();
                dstAssetId = copyGeneralAssetSingleVersion(executor, srcAssetId, srcAssetVersion, "path=" + dstPath,
                        dstDirIsNamespace, options.addNote);
            } else {
                throw new Exception("Invalid asset version: " + srcAssetVersion);
            }
            Entity dstEntity = new Entity(dstAssetId, dstPath, false, false);
            return new Result(srcEntity, dstEntity, true);
        }
    }

    public static Result copy(ServiceExecutor executor, String srcAssetNamespacePath, int dropLevels, String dstDirPath,
                              boolean dstDirIsNamespace, Options options) throws Throwable {

        final Entity srcEntity = new Entity(AssetNamespaceUtils.getAssetNamespaceId(executor, srcAssetNamespacePath),
                srcAssetNamespacePath, true, false);
        String srcAssetNamespaceName = AssetPathUtils.getName(srcAssetNamespacePath);
        String dstPath;
        if (dropLevels == DROP_LEVELS_NONE) {
            dstPath = AssetPathUtils.join(dstDirPath, srcAssetNamespacePath);
        } else if (dropLevels > 0) {
            String relativePath = AssetPathUtils.dropTopLevels(srcAssetNamespacePath, dropLevels);
            if (relativePath != null) {
                dstPath = AssetPathUtils.join(dstDirPath, relativePath);
            } else {
                dstPath = AssetPathUtils.join(dstDirPath, srcAssetNamespaceName);
            }
        } else {
            dstPath = AssetPathUtils.join(dstDirPath, srcAssetNamespaceName);
        }

        PluginTask.checkIfThreadTaskAborted();
        if (AssetNamespaceUtils.assetNamespaceExists(executor, dstPath)) {
            if (options.ifExists == IfExists.IGNORE) {
                String dstAssetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, dstPath);
                Entity dstEntity = new Entity(dstAssetNamespaceId, dstPath, true, false);
                return new Result(srcEntity, dstEntity, false);
            } else if (options.ifExists == IfExists.RENAME) {
                dstPath = rename(executor, dstPath, true);
                AssetNamespaceUtils.createAssetNamespace(executor, dstPath, false, false);
                String dstAssetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, dstPath);
                copyContents(executor, srcAssetNamespacePath, true, dstPath, true, options.childrenVersion,
                        options.addNote, options.pageSize, options.nbWorkers);
                Entity dstEntity = new Entity(dstAssetNamespaceId, dstPath, true, false);
                return new Result(srcEntity, dstEntity, true);
            } else {
                throw new Exception("Asset namespace already exists at: '" + dstPath + "'.");
            }
        } else if (AssetUtils.collectionAssetExists(dstPath, executor)) {
            if (options.ifExists == IfExists.IGNORE) {
                String dstCollectionAssetId = AssetUtils.assetIdFromPath(dstPath, executor);
                Entity dstEntity = new Entity(dstCollectionAssetId, dstPath, false, true);
                return new Result(srcEntity, dstEntity, false);
            } else if (options.ifExists == IfExists.RENAME) {
                dstPath = rename(executor, dstPath, false);
                String dstCollectionAssetId = AssetUtils.createCollectionAsset(executor, dstPath, false, false);
                copyContents(executor, srcAssetNamespacePath, true, dstPath, false, options.childrenVersion,
                        options.addNote, options.pageSize, options.nbWorkers);
                Entity dstEntity = new Entity(dstCollectionAssetId, dstPath, false, true);
                return new Result(srcEntity, dstEntity, true);
            } else {
                throw new Exception("Asset namespace already exists at: '" + dstPath + "'.");
            }
        } else {
            CollectionType dstType = CollectionType.typeFor(dstPath, executor);
            if (dstType == CollectionType.ASSET_NAMESPACE) {
                AssetNamespaceUtils.createAssetNamespace(executor, dstPath, true, true);
                String dstAssetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, dstPath);
                copyContents(executor, srcAssetNamespacePath, true, dstPath, true, options.childrenVersion,
                        options.addNote, options.pageSize, options.nbWorkers);
                Entity dstEntity = new Entity(dstAssetNamespaceId, dstPath, true, false);
                return new Result(srcEntity, dstEntity, true);
            } else {
                String dstCollectionAssetId = AssetUtils.createCollectionAsset(executor, dstPath, true, true);
                copyContents(executor, srcAssetNamespacePath, true, dstPath, false, options.childrenVersion,
                        options.addNote, options.pageSize, options.nbWorkers);
                Entity dstEntity = new Entity(dstCollectionAssetId, dstPath, false, true);
                return new Result(srcEntity, dstEntity, true);
            }
        }
    }

    private static String rename(ServiceExecutor executor, String path, boolean isAssetNamespace) throws Throwable {
        String name = AssetPathUtils.getName(path);
        if (name == null) {
            throw new Exception("Cannot rename root path: '" + path + "'.");
        }
        String parentPath = AssetPathUtils.getParent(path);
        if (isAssetNamespace) {
            while (AssetNamespaceUtils.assetNamespaceExists(executor, path)) {
                name = generateName(name);
                path = AssetPathUtils.join(parentPath, name);
                PluginTask.checkIfThreadTaskAborted();
            }
        } else {
            while (AssetUtils.assetExists(path, executor)) {
                name = generateName(name);
                path = AssetPathUtils.join(parentPath, name);
                PluginTask.checkIfThreadTaskAborted();
            }
        }
        return path;
    }

    private static String generateName(String name) {
        StringBuilder sb = new StringBuilder();
        int n = 0;
        if (name.matches("^.*\\(\\d+\\)$")) {
            int idx = name.lastIndexOf('(');
            n = Integer.parseInt(name.substring(idx + 1, name.length() - 1));
            sb.append(name, 0, idx);
        } else {
            sb.append(name);
        }
        sb.append('(').append(n + 1).append(')');
        return sb.toString();
    }

    private static String copyGeneralAssetAllVersions(ServiceExecutor executor, String srcAssetId, String dstAssetPath,
                                                      boolean dstDirIsNamespace, boolean addNote) throws Throwable {
        String dstAssetId = null;
        List<Integer> assetVersions = AssetUtils.getVersions(executor, srcAssetId, true);
        for (Integer assetVersion : assetVersions) {
            PluginTask.checkIfThreadTaskAborted();
            dstAssetId = copyGeneralAssetSingleVersion(executor, srcAssetId, assetVersion,
                    dstAssetId == null ? "path=" + dstAssetPath : dstAssetId, dstDirIsNamespace, addNote);
        }
        return dstAssetId;
    }

    private static String copyGeneralAssetSingleVersion(ServiceExecutor executor, String srcAssetId,
                                                        int srcAssetVersion, String dstAssetId, boolean dstDirIsNamespace, boolean addNote) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("clone", new String[]{"content", "true", "meta", "true", "model", "true", "templates", "true",
                "version", Integer.toString(srcAssetVersion)}, srcAssetId);

        if (addNote) {
            dm.push("meta");
            dm.push("mf-note");
            dm.add("note", "Cloned from asset " + srcAssetId + " (version: " + srcAssetVersion + ")");
            dm.pop();
            dm.pop();
        }
        String dstDirType = dstDirIsNamespace ? "namespace" : "collection";
        dm.add("create", new String[]{"in", dstDirType}, true);
        dm.add("id", dstAssetId);
        return executor.execute("asset.set", dm.root()).value("id");
    }

    private static void copyContents(ServiceExecutor executor, String srcDirPath, boolean srcDirIsNamespace,
                                     String dstDirPath, boolean dstDirIsNamespace, int childrenVersion, boolean addNote, final int pageSize,
                                     final int nbWorkers) throws Throwable {
        BlockingQueue<Task> taskQueue = nbWorkers > 1 ? new ArrayBlockingQueue<>(1000) : null;
        final AtomicLong totalAssets = new AtomicLong(0);
        final AtomicLong nbCompletedTasks = new AtomicLong(0);
        final AtomicLong nbFailedTasks = new AtomicLong(0);
        final AtomicInteger nbFinishedWorkers = new AtomicInteger(0);
        final AtomicReference<Throwable> lastTaskError = new AtomicReference<>();
        final AtomicReference<String> currentDstAssetPath = new AtomicReference<>();

        boolean poisonTasksAdded = false;

        if (taskQueue != null) {
            final String srcDirType = srcDirIsNamespace ? "namespace" : "collection";
            final String dstDirType = dstDirIsNamespace ? "namespace" : "collection";
            final String context = "copy assets from " + srcDirType + ": '" + srcDirPath + "' to " + dstDirType + ": '"
                    + dstDirPath + "'";
            final PluginLog log = PluginLog.log();
            for (int i = 0; i < nbWorkers; i++) {
                Worker worker = new Worker(taskQueue, log, context, new Listener() {

                    @Override
                    public void taskStarted(Task task) {
                        if (task instanceof AssetCopyTask) {
                            currentDstAssetPath.set(((AssetCopyTask) task).dstAssetPath());
                        }
                    }

                    @Override
                    public void taskCompleted(Task task) {
                        nbCompletedTasks.getAndIncrement();
                    }

                    @Override
                    public void taskFailed(Task task, Throwable t) {
                        nbFailedTasks.getAndIncrement();
                        lastTaskError.set(t);
                    }

                    @Override
                    public void workerFinished(Worker worker, Throwable t) {
                        nbFinishedWorkers.getAndIncrement();
                        if (lastTaskError.get() == null) {
                            lastTaskError.set(t);
                        }
                        if (t != null) {
                            log.add(PluginLog.INFORMATION, Thread.currentThread().getName()
                                    + " finished with error: " + t.getMessage(), t);
                        } else {
                            log.add(PluginLog.INFORMATION, Thread.currentThread().getName()
                                    + " finished");
                        }
                    }
                });

                // execute in a different thread
                PluginThread.executeAsync("asset-copy-worker-" + i, worker);
            }
        }
        try {
            PluginTask.threadTaskBeginSetOf(-1);
            XmlDocMaker dm = new XmlDocMaker("args");
            if (srcDirIsNamespace) {
                dm.add("namespace", srcDirPath);
            } else {
                dm.add("collection", "path=" + srcDirPath);
                dm.add("where", "not (asset is collection)");
            }
            dm.add("action", "get-path");
            dm.add("as", "iterator");
            PluginTask.checkIfThreadTaskAborted();
            String iteratorId = executor.execute("asset.query", dm.root()).value("iterator");

            dm = new XmlDocMaker("args");
            dm.add("id", iteratorId);
            dm.add("size", pageSize);
            final XmlDoc.Element args = dm.root();
            boolean complete = false;

            try {
                while (!complete) {
                    PluginTask.checkIfThreadTaskAborted();
                    XmlDoc.Element re = executor.execute("asset.query.iterate", args);
                    complete = re.booleanValue("iterated/@complete");
                    List<XmlDoc.Element> pes = re.elements("path");
                    if (pes != null) {
                        totalAssets.getAndAdd(pes.size());
                        for (XmlDoc.Element pe : pes) {
                            PluginTask.checkIfThreadTaskAborted();

                            String path = pe.value();
                            String assetId = pe.value("@id");
                            String relativePath = AssetPathUtils.getRelativePath(path, srcDirPath);
                            String dstAssetPath = AssetPathUtils.join(dstDirPath, relativePath);
                            if (taskQueue != null) {
                                taskQueue.put(new AssetCopyTask(executor, assetId, childrenVersion == VERSION_ALL,
                                        dstAssetPath, dstDirIsNamespace, addNote));
                                // check if there is any failed task. If there is any, throw an exception to
                                // stop the process - poison tasks will be sent to the queue to stop all the
                                // workers.
                                if (lastTaskError.get() != null) {
                                    throw lastTaskError.get();
                                }
                            } else {
                                PluginTask.setCurrentThreadActivity("copying to: " + dstAssetPath);
                                if (childrenVersion == VERSION_ALL) {
                                    copyGeneralAssetAllVersions(executor, assetId, dstAssetPath, dstDirIsNamespace,
                                            addNote);
                                } else {
                                    PluginTask.checkIfThreadTaskAborted();
                                    copyGeneralAssetSingleVersion(executor, assetId, VERSION_LATEST,
                                            "path=" + dstAssetPath, dstDirIsNamespace, addNote);
                                }
                                nbCompletedTasks.getAndIncrement();
                            }
                            // report progress
                            PluginTask.threadTaskSetCompleted(nbCompletedTasks.get(), totalAssets.get());
                            String currentDstPath = currentDstAssetPath.get();
                            if (currentDstPath != null) {
                                PluginTask.setCurrentThreadActivity("copying to: " + currentDstPath);
                            }
                        }
                    }
                }
            } finally {
                AssetQueryUtils.destroyIterator(executor, iteratorId);
            }

            if (taskQueue != null) {
                // add poison tasks
                for (int i = 0; i < nbWorkers; i++) {
                    taskQueue.put(new PoisonTask());
                }
                poisonTasksAdded = true;

                // now wait for all the workers to finish
                while (!Thread.interrupted() && nbFinishedWorkers.get() < nbWorkers) {
                    PluginTask.checkIfThreadTaskAborted();
                    // check if there is any failed task. If there is any, throw the exception to
                    // stop the process - poison tasks will be sent to the queue to stop all the
                    // workers.
                    if (lastTaskError.get() != null) {
                        throw lastTaskError.get();
                    }
                    // report progress
                    PluginTask.threadTaskSetCompleted(nbCompletedTasks.get(), totalAssets.get());
                    String currentDstPath = currentDstAssetPath.get();
                    if (currentDstPath != null) {
                        PluginTask.setCurrentThreadActivity("copying to: " + currentDstPath);
                    }
                    Thread.sleep(100);
                }
                // report progress
                PluginTask.threadTaskSetCompleted(nbCompletedTasks.get(), totalAssets.get());
                PluginTask.clearCurrentThreadActivity();
            }
        } catch (Throwable t) {
            if (taskQueue != null && !poisonTasksAdded) {
                // add poison tasks on exception
                for (int i = 0; i < nbWorkers; i++) {
                    taskQueue.put(new PoisonTask());
                }
            }
            // report progress
            PluginTask.threadTaskSetCompleted(nbCompletedTasks.get(), totalAssets.get());
            PluginTask.clearCurrentThreadActivity();
            throw t;
        }

        if (srcDirIsNamespace) {
            copyEmptyAssetNamespaces(executor, srcDirPath, dstDirPath, dstDirIsNamespace);
        }
        copyEmptyCollectionAssets(executor, srcDirPath, srcDirIsNamespace, dstDirPath, dstDirIsNamespace);

    }

    private static void copyEmptyAssetNamespaces(ServiceExecutor executor, String srcAssetNamespacePath,
                                                 String dstDirPath, boolean dstDirIsNamespace) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", srcAssetNamespacePath);
        dm.add("leaf-only", "true");
        Collection<String> emptyNamespacePaths = executor.execute("asset.namespace.empty.list", dm.root())
                .values("namespace");
        if (emptyNamespacePaths != null) {
            for (String emptyNamespacePath : emptyNamespacePaths) {
                PluginTask.checkIfThreadTaskAborted();
                String emptyNamespaceRelativePath = AssetPathUtils.getRelativePath(emptyNamespacePath,
                        srcAssetNamespacePath);
                String dstPath = AssetPathUtils.join(dstDirPath, emptyNamespaceRelativePath);
                if (dstDirIsNamespace) {
                    AssetNamespaceUtils.createAssetNamespace(executor, dstPath, true, true);
                } else {
                    AssetUtils.createCollectionAsset(executor, dstPath, true, true);
                }
            }
        }
    }

    private static void copyEmptyCollectionAssets(ServiceExecutor executor, String srcDirPath,
                                                  boolean srcDirIsNamespace, String dstDirPath, boolean dstDirIsNamespace) throws Throwable {

        XmlDocMaker dm = new XmlDocMaker("args");
        if (srcDirIsNamespace) {
            dm.add("namespace", srcDirPath);
        } else {
            dm.add("collection", "path=" + srcDirPath);
        }
        dm.add("where", "asset is collection with no members");
        dm.add("action", "get-path");
        dm.add("as", "iterator");

        PluginTask.checkIfThreadTaskAborted();
        String iteratorId = executor.execute("asset.query", dm.root()).value("iterator");

        dm = new XmlDocMaker("args");
        dm.add("id", iteratorId);

        try {
            boolean complete = false;
            while (!complete) {
                PluginTask.checkIfThreadTaskAborted();
                XmlDoc.Element re = executor.execute("asset.query.iterate", dm.root());
                complete = re.booleanValue("iterated/@complete");
                Collection<String> empthCollectionAssetPaths = re.values("path");
                if (empthCollectionAssetPaths != null) {
                    for (String emptyCollectionAssetPath : empthCollectionAssetPaths) {
                        PluginTask.checkIfThreadTaskAborted();
                        String emptyCollectionAssetRelativePath = AssetPathUtils
                                .getRelativePath(emptyCollectionAssetPath, srcDirPath);
                        String dstPath = AssetPathUtils.join(dstDirPath, emptyCollectionAssetRelativePath);
                        if (dstDirIsNamespace) {
                            AssetNamespaceUtils.createAssetNamespace(executor, dstPath, true, true);
                        } else {
                            AssetUtils.createCollectionAsset(executor, dstPath, true, true);
                        }
                    }
                }
            }
        } catch (Throwable t) {
            executor.execute("asset.query.iterator.destroy", "<args><id>" + iteratorId + "</id></args>", null, null);
        }
    }

    public enum IfExists {
        ERROR, IGNORE, RENAME;

        public static String[] stringValues() {
            IfExists[] vs = values();
            String[] svs = new String[vs.length];
            for (int i = 0; i < vs.length; i++) {
                svs[i] = vs[i].toString();
            }
            return svs;
        }

        public static IfExists fromString(String sv, IfExists defaultValue) {
            if (sv != null) {
                IfExists[] vs = values();
                for (IfExists v : vs) {
                    if (v.toString().equals(sv)) {
                        return v;
                    }
                }
            }
            return defaultValue;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

    public static class Result {

        public final Entity from;
        public final Entity to;
        public final boolean createdOrUpdated;

        public Result(Entity from, Entity to, boolean createdOrUpdated) {
            this.from = from;
            this.to = to;
            this.createdOrUpdated = createdOrUpdated;
        }

        public void save(XmlWriter w) throws Throwable {
            w.push("copy", new String[]{"created-or-updated", Boolean.toString(this.createdOrUpdated)});
            w.add("from", new String[]{"type", from.typeName(), "id", from.id()}, from.path());
            w.add("to", new String[]{"type", to.typeName(), "id", to.id()}, to.path());
            w.pop();
        }
    }

    public static class Options {
        public final IfExists ifExists;
        public final int childrenVersion;
        public final boolean addNote;
        public final int pageSize;
        public final int nbWorkers;

        public Options(int nbWorkers, IfExists ifExists, int childrenVersion, boolean addNote, int pageSize) {
            this.nbWorkers = nbWorkers;
            this.ifExists = ifExists;
            this.childrenVersion = childrenVersion;
            this.addNote = addNote;
            this.pageSize = pageSize;
        }

        public Options(int nbWorkers, IfExists ifExists, int childrenVersion, boolean addNote) {
            this(nbWorkers, ifExists, childrenVersion, addNote, PAGE_SIZE);
        }

        public Options() {
            this(1, IfExists.IGNORE, VERSION_LATEST, true, PAGE_SIZE);
        }
    }

    private static class AssetCopyTask implements unimelb.mf.plugin.util.concurrent.Task {
        private final ServiceExecutor _executor;
        private final String _srcAssetId;
        private final boolean _allVersions;
        private final String _dstAssetPath;
        private final boolean _dstParentIsNamespace;
        private final boolean _addNote;

        private AssetCopyTask(ServiceExecutor executor, String srcAssetId, boolean allVersions, String dstAssetPath,
                              boolean dstParentIsNamespace, boolean addNote) {
            _executor = executor;
            _srcAssetId = srcAssetId;
            _allVersions = allVersions;
            _dstAssetPath = dstAssetPath;
            _dstParentIsNamespace = dstParentIsNamespace;
            _addNote = addNote;
        }

        @Override
        public void execute() throws Throwable {
            try {
                if (_allVersions) {
                    copyGeneralAssetAllVersions(_executor, _srcAssetId, _dstAssetPath, _dstParentIsNamespace, _addNote);
                } else {
                    PluginTask.checkIfThreadTaskAborted();
                    copyGeneralAssetSingleVersion(_executor, _srcAssetId, VERSION_LATEST, "path=" + _dstAssetPath,
                            _dstParentIsNamespace, _addNote);
                }
            } catch (Throwable t) {
                throw new Exception(
                        t.getMessage() + " Context: copy asset " + _srcAssetId + " to '" + _dstAssetPath + "'", t);
            }
        }

        public String dstAssetPath() {
            return _dstAssetPath;
        }
    }

}
