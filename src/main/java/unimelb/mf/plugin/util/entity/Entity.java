package unimelb.mf.plugin.util.entity;

public class Entity {

    private String _id;
    private String _path;
    private final boolean _isAssetNamespace;
    private final boolean _isCollectionAsset;

    public Entity(String id, String path, boolean isAssetNamespace, boolean isCollectionAsset) {
        _id = id;
        _path = path;
        _isAssetNamespace = isAssetNamespace;
        _isCollectionAsset = isCollectionAsset;
    }

    public String id() {
        return _id;
    }

    public String path() {
        return _path;
    }

    public boolean isCollectionAsset() {
        return _isCollectionAsset;
    }

    public boolean isAssetNamespace() {
        return _isAssetNamespace;
    }

    public boolean isGeneralAsset() {
        return !isCollectionAsset() && !isAssetNamespace();
    }

    public String typeName() {
        if (isAssetNamespace()) {
            return "namespace";
        } else if (isCollectionAsset()) {
            return "collection";
        } else {
            return "asset";
        }
    }

}
