package unimelb.mf.plugin.util;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;

public class AssetQueryUtils {

    public static void destroyIterator(ServiceExecutor executor, String iteratorId) throws Throwable {
        destroyIterator(executor, iteratorId, true);
    }

    public static void destroyIterator(ServiceExecutor executor, String iteratorId, boolean ignoreMissing)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", iteratorId);
        dm.add("ignore-missing", ignoreMissing);
        executor.execute("asset.query.iterator.destroy", dm.root());
    }
}
