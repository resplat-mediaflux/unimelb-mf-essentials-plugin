package unimelb.mf.plugin.util;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class UserUtils {

    public static boolean hasSystemAdministratorRole(ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("role", new String[] { "type", "role" }, "system-administrator");
        return executor.execute("actor.self.have", dm.root()).booleanValue("role");
    }

	/**
	 * Does the user actually exist ?
	 */
	public static Boolean exists (ServiceExecutor executor, String authority, String domain,
			String user) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		if (authority!=null) {
			dm.add("authority", authority);
		}
		dm.add("domain", domain);
		dm.add("user", user);
		XmlDoc.Element r = executor.execute("user.exists", dm.root());
		return r.booleanValue("exists");
	}

	public static Boolean isAuthDomainEnabled (ServiceExecutor executor, String domain) throws Throwable {
		XmlDocMaker dm = new XmlDocMaker("args");
		dm.add("domain", domain);
		return executor.execute("authentication.domain.enabled", dm.root()).booleanValue("enabled");
	}

	
}
