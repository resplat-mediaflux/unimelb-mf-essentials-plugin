package unimelb.mf.plugin.util;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;

public class AssetNamespaceUtils {

    public static void createAssetNamespace(ServiceExecutor executor, String assetNamespacePath, boolean createAll,
            boolean ignoreIfExists) throws Throwable {
        if (ignoreIfExists) {
            if (assetNamespaceExists(executor, assetNamespacePath)) {
                return;
            }
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", new String[] { "all", Boolean.toString(createAll) }, assetNamespacePath);
        executor.execute("asset.namespace.create", dm.root());
    }

    public static XmlDoc.Element describeAssetNamespace(ServiceExecutor executor, String assetNamespacePath)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNamespacePath);
        return executor.execute("asset.namespace.describe", dm.root()).element("namespace");
    }

    public static boolean assetNamespaceExists(ServiceExecutor executor, String assetNamespacePath) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNamespacePath);
        return executor.execute("asset.namespace.exists", dm.root()).booleanValue("exists");
    }

    public static void move(ServiceExecutor executor, String srcNamespacePath, String dstParentNamespacePath)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", srcNamespacePath);
        dm.add("to", dstParentNamespacePath);
        executor.execute("asset.namespace.move", dm.root());
    }

    public static void destroyAssetNamespace(ServiceExecutor executor, String assetNamespacePath) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNamespacePath);
        executor.execute("asset.namespace.destroy", dm.root());
    }

    public static String getAssetNamespaceId(ServiceExecutor executor, String assetNamespacePath) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", assetNamespacePath);
        return executor.execute("asset.namespace.identifier.get", dm.root()).value("id");
    }

}
