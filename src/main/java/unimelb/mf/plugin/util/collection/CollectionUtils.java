package unimelb.mf.plugin.util.collection;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import unimelb.mf.plugin.util.AssetNamespaceUtils;
import unimelb.mf.plugin.util.AssetQueryUtils;
import unimelb.mf.plugin.util.AssetUtils;
import unimelb.mf.util.AssetPathUtils;

public class CollectionUtils {

    public static boolean collectionExists(ServiceExecutor executor, String dirPath) throws Throwable {
        return AssetUtils.collectionAssetExists(dirPath, executor)
                || AssetNamespaceUtils.assetNamespaceExists(executor, dirPath);
    }

    public static SimpleEntry<String, CollectionType> createCollection(ServiceExecutor executor, String path,
            boolean createParents, boolean ignoreIfExists) throws Throwable {
        if (!createParents) {
            String directParent = AssetPathUtils.getParent(path);
            if (!collectionExists(executor, directParent)) {
                throw new Exception("parent collection: '" + directParent + "' not found.");
            }
        }
        CollectionType type = CollectionType.typeFor(path, executor);
        if (type == CollectionType.COLLECTION_ASSET) {
            String collectionAssetId = AssetUtils.createCollectionAsset(executor, path, createParents, ignoreIfExists);
            return new SimpleEntry<String, CollectionType>(collectionAssetId, CollectionType.COLLECTION_ASSET);
        } else if (type == CollectionType.ASSET_NAMESPACE) {
            AssetNamespaceUtils.createAssetNamespace(executor, path, createParents, ignoreIfExists);
            String assetNamespaceId = AssetNamespaceUtils.getAssetNamespaceId(executor, path);
            return new SimpleEntry<String, CollectionType>(assetNamespaceId, CollectionType.COLLECTION_ASSET);
        } else {
            throw new Exception("Unsupported conllection type: " + type);
        }
    }

    public long totalContentSize(ServiceExecutor executor, String collectionPath) throws Throwable {
        CollectionDetails collection = CollectionDetails.get(executor, collectionPath);
        if (collection.exists()) {
            return collection.totalContentSize(executor);
        }
        throw new Exception("collection: '" + collectionPath + "' does not exist");
    }

    public static void move(ServiceExecutor executor, String srcCollectionPath, String dstParentCollectionPath)
            throws Throwable {
        move(executor, CollectionDetails.get(executor, srcCollectionPath),
                CollectionDetails.get(executor, dstParentCollectionPath));
    }

    public static void move(ServiceExecutor executor, CollectionDetails srcCollection,
            CollectionDetails dstParentCollection) throws Throwable {
        if (srcCollection.type() == CollectionType.COLLECTION_ASSET) {
            if (dstParentCollection.type() == CollectionType.COLLECTION_ASSET) {
                AssetUtils.move(executor, "path=" + srcCollection.path(), "path=" + dstParentCollection.path());
            } else {
                moveCollectionToNamespace(srcCollection.path(), dstParentCollection.path(), executor);
            }
        } else {
            if (dstParentCollection.type() == CollectionType.COLLECTION_ASSET) {
                moveNamespaceToCollection(srcCollection.path(), dstParentCollection.path(), executor);
            } else {
                AssetNamespaceUtils.move(executor, srcCollection.path(), dstParentCollection.path());
            }
        }
    }

    private static void moveCollectionToNamespace(String srcCollectionAssetPath, String dstParentNamespacePath,
            ServiceExecutor executor) throws Throwable {
        String srcCollectionAssetName = AssetPathUtils.getName(srcCollectionAssetPath);
        String dstNamespacePath = AssetPathUtils.join(dstParentNamespacePath, srcCollectionAssetName);
        AssetNamespaceUtils.createAssetNamespace(executor, dstNamespacePath, false, true);

        // move non-collection assets
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("collection", "path=" + srcCollectionAssetPath);
        dm.add("where", "asset in collection '" + srcCollectionAssetPath + "'");
        dm.add("where", "not (asset is collection)");
        dm.add("action", "pipe");
        dm.add("pipe-nb-threads", 4);
        dm.push("service", new String[] { "name", "asset.move" });
        dm.add("namespace", dstNamespacePath);
        dm.pop();
        executor.execute("asset.query", dm.root());

        // copy sub-collections
        dm = new XmlDocMaker("args");
        dm.add("collection", "path=" + srcCollectionAssetPath);
        dm.add("where", "asset in collection '" + srcCollectionAssetPath + "'");
        dm.add("where", "asset is collection");
        dm.add("action", "get-path");
        dm.add("as", "iterator");
        String iteratorId = executor.execute("asset.query", dm.root()).value("iterator");
        try {
            dm = new XmlDocMaker("args");
            dm.add("id", iteratorId);
            dm.add("size", 1000);

            boolean complete = false;
            while (!complete) {
                XmlDoc.Element re = executor.execute("asset.query.iterate", dm.root());
                complete = re.booleanValue("iterated/@complete");
                Collection<String> subCollectionAssetPaths = re.values("path");
                if (subCollectionAssetPaths != null) {
                    for (String subCollectionAssetPath : subCollectionAssetPaths) {
                        moveCollectionToNamespace(subCollectionAssetPath, dstNamespacePath, executor);
                    }
                }
            }
        } catch (Throwable t) {
            AssetQueryUtils.destroyIterator(executor, iteratorId);
        }

        // delete the remaining source collection assets
        AssetUtils.destroyAsset(executor, "path=" + srcCollectionAssetPath, true);
    }

    private static void moveNamespaceToCollection(String srcNamespacePath, String dstParentCollectionAssetPath,
            ServiceExecutor executor) throws Throwable {
        String srcNamespaceName = AssetPathUtils.getName(srcNamespacePath);
        String dstCollectionAssetPath = AssetPathUtils.join(dstParentCollectionAssetPath, srcNamespaceName);
        AssetUtils.createCollectionAsset(executor, dstCollectionAssetPath, false, true);

        // move assets
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("namespace", srcNamespacePath);
        dm.add("where", "namespace='" + srcNamespacePath + "'");
        dm.add("action", "pipe");
        dm.add("pipe-nb-threads", 4);
        dm.push("service", new String[] { "name", "asset.move" });
        dm.add("to", "path=" + dstCollectionAssetPath);
        dm.pop();
        executor.execute("asset.query", dm.root());

        // copy sub-namespaces
        dm = new XmlDocMaker("args");
        dm.add("namespace", srcNamespacePath);
        dm.add("assets", false);
        XmlDoc.Element re = executor.execute("asset.namespace.list", dm.root());
        Collection<String> subNamespaceNames = re.values("namespace/namespace");
        if (subNamespaceNames != null) {
            for (String subNamespaceName : subNamespaceNames) {
                String subNamespacePath = AssetPathUtils.join(srcNamespacePath, subNamespaceName);
                moveNamespaceToCollection(subNamespacePath, dstCollectionAssetPath, executor);
            }
        }

        // destroy source namespaces
        AssetNamespaceUtils.destroyAssetNamespace(executor, srcNamespacePath);
    }

}
