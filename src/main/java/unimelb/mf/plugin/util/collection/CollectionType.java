package unimelb.mf.plugin.util.collection;

import java.io.FileNotFoundException;

import arc.mf.plugin.ServiceExecutor;
import unimelb.mf.plugin.util.AssetNamespaceUtils;
import unimelb.mf.plugin.util.AssetUtils;
import unimelb.mf.util.AssetPathUtils;

public enum CollectionType {

    ASSET_NAMESPACE("namespace"), COLLECTION_ASSET("collection");

    public final String shortName;

    CollectionType(String shortName) {
        this.shortName = shortName;
    }

    public String toString() {
        return this.shortName;
    }

    public static String[] shortNames() {
        return new String[] { COLLECTION_ASSET.shortName, ASSET_NAMESPACE.shortName };
    }

    public static CollectionType fromShortName(String shortName) {
        CollectionType[] vs = values();
        for (CollectionType v : vs) {
            if (v.shortName.equalsIgnoreCase(shortName)) {
                return v;
            }
        }
        return null;
    }

    /**
     * Get the type of existing collection (could be collection asset or asset
     * namespace). If the collection asset or asset namespace does not exist at the
     * specified path, an Exception will be thrown.
     *
     * @param path     The path to the collection asset or asset namespace.
     * @param executor The service executor
     * @return the collection type
     * @throws Throwable Exception is thrown if the asset or asset namespace doesn't
     *                   exist at the specified path.
     */
    public static CollectionType typeOf(String path, ServiceExecutor executor) throws Throwable {
        if (AssetUtils.collectionAssetExists(path, executor)) {
            return CollectionType.COLLECTION_ASSET;
        } else if (AssetNamespaceUtils.assetNamespaceExists(executor, path)) {
            return CollectionType.ASSET_NAMESPACE;
        } else {
            throw new FileNotFoundException("Asset or asset namespace does not exist at path: " + path);
        }
    }

    /**
     * Detect the type for a collection. It could be non-existent. If the collection
     * does not exist in Mediaflux, try using/inheriting the type of its nearest
     * parent.
     *
     * @param path     The directory path.
     * @param executor The service executor
     * @return The collection type
     * @throws Throwable exceptions
     */
    public static CollectionType typeFor(String path, ServiceExecutor executor) throws Throwable {
        if ("/".equals(path)) {
            return CollectionType.ASSET_NAMESPACE;
        }
        if (AssetUtils.collectionAssetExists(path, executor)) {
            return CollectionType.COLLECTION_ASSET;
        } else if (AssetNamespaceUtils.assetNamespaceExists(executor, path)) {
            return CollectionType.ASSET_NAMESPACE;
        } else {
            String parentPath = AssetPathUtils.getParent(path);
            return typeFor(parentPath, executor);
        }
    }
}
