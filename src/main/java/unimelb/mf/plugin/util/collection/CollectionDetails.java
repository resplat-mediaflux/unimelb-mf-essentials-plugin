package unimelb.mf.plugin.util.collection;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDocMaker;
import unimelb.mf.plugin.util.AssetNamespaceUtils;
import unimelb.mf.plugin.util.AssetUtils;
import unimelb.mf.util.AssetPathUtils;

public class CollectionDetails {

    private final String _path;
    private final CollectionType _type;
    private final boolean _exists;

    public CollectionDetails(String path, CollectionType type, boolean exists) {
        _path = path;
        _type = type;
        _exists = exists;
    }

    public final String path() {
        return _path;
    }

    public final String name() {
        return AssetPathUtils.getName(_path);
    }

    public final CollectionType type() {
        return _type;
    }

    public final boolean exists() {
        return _exists;
    }

    public final boolean isAssetNamespace() {
        return _type == CollectionType.ASSET_NAMESPACE;
    }

    public final boolean isCollectionAsset() {
        return _type == CollectionType.COLLECTION_ASSET;
    }

    public long totalContentSize(ServiceExecutor executor) throws Throwable {
        if (_exists) {
            XmlDocMaker dm = new XmlDocMaker("args");
            if (_type == CollectionType.COLLECTION_ASSET) {
                dm.add("collection", "path=" + _path);
            } else {
                dm.add("namespace", _path);
            }
            dm.add("action", "sum");
            dm.add("xpath", "content/size");
            return executor.execute("asset.query", dm.root()).longValue("value", 0);
        }
        return 0;
    }

    public CollectionDetails createIfNotExist(ServiceExecutor executor) throws Throwable {
        if (_exists) {
            return this;
        } else {
            if (_type == CollectionType.ASSET_NAMESPACE) {
                AssetNamespaceUtils.createAssetNamespace(executor, _path, true, true);
                return new CollectionDetails(_path, CollectionType.ASSET_NAMESPACE, true);
            } else {
                AssetUtils.createCollectionAsset(executor, _path, true, true);
                return new CollectionDetails(_path, CollectionType.COLLECTION_ASSET, true);
            }
        }
    }

    public static CollectionDetails get(ServiceExecutor executor, String collectionPath) throws Throwable {
        return get(executor, collectionPath, null);
    }

    public static CollectionDetails get(ServiceExecutor executor, String collectionPath, CollectionType type)
            throws Throwable {
        if (type != null) {
            if (type == CollectionType.COLLECTION_ASSET) {
                return new CollectionDetails(collectionPath, CollectionType.COLLECTION_ASSET,
                        AssetUtils.collectionAssetExists(collectionPath, executor));
            } else {
                return new CollectionDetails(collectionPath, CollectionType.ASSET_NAMESPACE,
                        AssetNamespaceUtils.assetNamespaceExists(executor, collectionPath));
            }
        } else {
            if (AssetUtils.collectionAssetExists(collectionPath, executor)) {
                return new CollectionDetails(collectionPath, CollectionType.COLLECTION_ASSET, true);
            } else if (AssetNamespaceUtils.assetNamespaceExists(executor, collectionPath)) {
                return new CollectionDetails(collectionPath, CollectionType.ASSET_NAMESPACE, true);
            } else {
                return new CollectionDetails(collectionPath, CollectionType.typeFor(collectionPath, executor), false);
            }
        }
    }

    public static CollectionDetails get(ServiceExecutor executor, String... components) throws Throwable {
        String path = AssetPathUtils.join(AssetPathUtils.join(components));
        return get(executor, path);
    }

    public static CollectionDetails create(ServiceExecutor executor, String collectionPath, boolean createParents)
            throws Throwable {
        String parentPath = AssetPathUtils.getParent(collectionPath);
        if (!createParents && !CollectionUtils.collectionExists(executor, parentPath)) {
            throw new Exception("Cannot create collection. Parent collection: '" + parentPath + "' does not exist.");
        }
        if (CollectionUtils.collectionExists(executor, collectionPath)) {
            return get(executor, collectionPath);
        }
        CollectionType type = CollectionType.typeFor(parentPath, executor);
        if (type == CollectionType.COLLECTION_ASSET) {
            AssetUtils.createCollectionAsset(executor, collectionPath, createParents, true);
        } else {
            AssetNamespaceUtils.createAssetNamespace(executor, collectionPath, createParents, true);
        }
        return new CollectionDetails(collectionPath, type, true);
    }

}
