package unimelb.mf.plugin.util;

import java.util.List;

import arc.mf.plugin.ServiceExecutor;
import arc.xml.XmlDoc;
import arc.xml.XmlDocMaker;
import unimelb.mf.plugin.util.collection.CollectionType;
import unimelb.mf.util.AssetPathUtils;

public class AssetUtils {

    public static void destroyAsset(ServiceExecutor executor, String assetId, boolean members) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        dm.add("members", members);
        executor.execute("asset.destroy", dm.root());
    }

    public static String assetPathFromId(ServiceExecutor executor, String assetId) throws Throwable {
        return getAssetById(executor, assetId).value("path");
    }

    public static XmlDoc.Element getAssetById(ServiceExecutor executor, String assetId) throws Throwable {
        return getAssetById(executor, assetId, null);
    }

    public static XmlDoc.Element getAssetById(ServiceExecutor executor, String assetId, Integer version)
            throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", new String[] { "version", version == null ? null : Integer.toString(version) }, assetId);
        return executor.execute("asset.get", dm.root()).element("asset");
    }

    public static String assetIdFromPath(String assetPath, ServiceExecutor executor) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", "path=" + assetPath);
        return executor.execute("asset.identifier.get", dm.root()).value("id");
    }

    public static String createCollectionAsset(ServiceExecutor executor, String assetPath, boolean createParents,
            boolean ignoreIfExists) throws Throwable {
        if (ignoreIfExists) {
            if (assetExists(assetPath, executor)) {
                XmlDoc.Element ae = getAssetByPath(assetPath, executor);
                boolean isCollection = ae.elementExists("collection");
                if (isCollection) {
                    return ae.value("@id");
                }
            }
        }
        XmlDocMaker dm = new XmlDocMaker("args");
        String parentPath = AssetPathUtils.getParent(assetPath);
        if (AssetNamespaceUtils.assetNamespaceExists(executor, parentPath)) {
            dm.add("namespace", parentPath);
        } else if (AssetUtils.collectionAssetExists(parentPath, executor)) {
            dm.add("pid", "path=" + parentPath);
        } else {
            CollectionType type = CollectionType.typeFor(parentPath, executor);
            if (type == CollectionType.ASSET_NAMESPACE) {
                dm.add("namespace", new String[] { "create", Boolean.toString(createParents) }, parentPath);
            } else {
                dm.add("pid", new String[] { "create", Boolean.toString(createParents) }, "path=" + parentPath);
            }
        }
        dm.add("name", AssetPathUtils.getName(assetPath));
        dm.add("collection", new String[] { "unique-name-index", "true", "level-zero-root",
                Boolean.toString("/".equals(parentPath)) }, true);
        XmlDoc.Element ae = executor.execute("asset.create", dm.root());
        return ae.value("id");
    }

    public static boolean assetExists(String assetPath, ServiceExecutor executor) throws Throwable {
        return assetExists(executor, "path=" + assetPath);
    }

    public static XmlDoc.Element getAssetByPath(String assetPath, ServiceExecutor executor) throws Throwable {
        return getAssetById(executor, "path=" + assetPath);
    }

    public static boolean assetExists(ServiceExecutor executor, String assetId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", assetId);
        return executor.execute("asset.exists", dm.root()).booleanValue("exists");
    }

    public static boolean collectionAssetExists(String assetPath, ServiceExecutor executor) throws Throwable {
        return assetExists(assetPath, executor) && isCollectionAsset(assetPath, executor);
    }

    public static boolean collectionAssetExists(ServiceExecutor executor, String assetId) throws Throwable {
        return assetExists(executor, assetId) && isCollectionAsset(executor, assetId);
    }

    public static boolean isCollectionAsset(String assetPath, ServiceExecutor executor) throws Throwable {
        return isCollectionAsset(executor, "path=" + assetPath);
    }

    public static boolean isCollectionAsset(ServiceExecutor executor, String assetId) throws Throwable {
        XmlDoc.Element ae = getAssetById(executor, assetId);
        return ae.elementExists("collection");
    }

    public static void move(ServiceExecutor executor, String srcAssetId, String dstCollectionAssetId) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("id", srcAssetId);
        dm.add("to", dstCollectionAssetId);
        executor.execute("asset.move", dm.root());
    }

    public static List<Integer> getVersions(ServiceExecutor executor, String assetId, boolean ascend) throws Throwable {
        XmlDocMaker dm = new XmlDocMaker("args");
        dm.add("sort", ascend ? "asc" : "desc");
        dm.add("id", assetId);
        return executor.execute("asset.versions", dm.root()).intValues("asset/version/@n");
    }
}
