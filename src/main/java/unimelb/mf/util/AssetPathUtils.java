package unimelb.mf.util;

import java.util.Arrays;
import java.util.regex.Pattern;

public class AssetPathUtils {

    public static String getName(String path) {
        if ("/".equals(path)) {
            return null;
        }
        path = trimTrailingSlash(path);
        int idx = path.lastIndexOf('/');
        if (idx == -1) {
            return path;
        }
        return path.substring(idx + 1);
    }

    public static String trimTrailingSlash(String path) {
        if ("/".equals(path)) {
            return path;
        }
        while (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        return path;
    }

    public static String getParent(String path) {
        if ("/".equals(path)) {
            return null;
        }
        path = trimTrailingSlash(path);
        int idx = path.lastIndexOf('/');
        if (idx < 0) {
            return null;
        } else if (idx == 0) {
            return "/";
        } else {
            return path.substring(0, idx);
        }
    }

    public static String join(String... paths) {
        if (paths == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < paths.length; i++) {
            String path = paths[i];
            if (path == null || path.isEmpty()) {
                continue;
            }
            if (i == 0) {
                if (!"/".equals(path) && path.endsWith("/")) {
                    path = trimTrailingSlash(path);
                }
            } else {
                path = trimSlash(path);
            }
            if (path != null && !path.isEmpty()) {
                if (i > 0) {
                    sb.append("/");
                }
                sb.append(path);
            }
        }
        return sb.toString();
    }

    public static String trimSlash(String path) {
        return trimTrailingSlash(trimLeadingSlash(path));
    }

    public static String trimLeadingSlash(String path) {
        if ("/".equals(path)) {
            return path;
        }
        while (path.startsWith("/")) {
            path = path.substring(1);
        }
        return path;
    }

    public static String dropTopLevels(String path, int nLevels) {
        if (nLevels < 0) {
            throw new AssertionError("The number of levels must be positive.");
        } else if (nLevels > 0) {
            if (path == null || path.isEmpty() || "/".equals(path)) {
                return null;
            }
            path = trimLeadingSlash(path);
            String[] components = path.split(Pattern.quote("/"));
            if (nLevels >= components.length) {
                return null;
            } else {
                String[] keepComponents = Arrays.copyOfRange(components, nLevels, components.length);
                return join(keepComponents);
            }
        } else {
            return path;
        }
    }

    public static int getDepth(String path) {
        if (path == null || path.isEmpty() || "/".equals(path)) {
            return 0;
        }
        path = trimLeadingSlash(path);
        String[] components = path.split(Pattern.quote("/"));
        return components.length;
    }

    public static String getRelativePath(String path, String basePath) throws Throwable {
        String path1 = trimSlash(path);
        String basePath1 = trimSlash(basePath);
        if (path1.equals(basePath1)) {
            throw new Exception("Cannot construct relative two equal paths.");
        } else if (path1.startsWith(basePath1)) {
            return path1.substring(basePath1.length() + 1);
        } else {
            throw new Exception("Cannot construct relative path. '" + path + "' is not a descendant of base path: '" + basePath + "'");
        }
    }
}
