package unimelb.utils;

import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * IP address calculator:
 * http://jodies.de/ipcalc
 * <p>
 * How to calculate net mask, min/max IP in subnet:
 * https://www.baeldung.com/cs/get-ip-range-from-subnet-mask
 */
public class IPV4Utils {

    public static final String REGEX_IPV4 = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    public static final String REGEX_IPV4_CIDR = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])/(?:[0-9]|[12][0-9]|3[012])$";

    public static boolean isValidIP(String ip) {
        return ip != null && ip.matches(REGEX_IPV4);
    }

    public static boolean isValidCIDR(String cidr) {
        return cidr != null && cidr.matches(REGEX_IPV4_CIDR);
    }

    public static int getNetMask(String cidr) {
        String[] parts = cidr.split("/");
        int prefix = parts.length < 2 ? 0 : Integer.parseInt(parts[1]);
        return 0xffffffff << (32 - prefix);
    }

    public static String getNetMaskString(String cidr) {
        return toString(getNetMask(cidr));
    }

    public static byte[] getNetMaskBytes(String subnet) {
        int mask = getNetMask(subnet);
        return new byte[]{(byte) (mask >>> 24), (byte) (mask >> 16 & 0xff), (byte) (mask >> 8 & 0xff),
                (byte) (mask & 0xff)};
    }

    public static byte[] toBytes(String ip) throws UnknownHostException {
        return InetAddress.getByName(ip).getAddress();
    }


    public static String toString(int ip) {
        return String.format("%d.%d.%d.%d", ip >>> 24, ip >> 16 & 0xff, ip >> 8 & 0xff, ip & 0xff);
    }

    public static int parseIP(String ip) throws UnknownHostException {
        InetAddress addr = InetAddress.getByName(ip);
        byte[] octets = addr.getAddress();
        int result = 0;
        for (byte octet : octets) {
            result <<= 8;
            result |= octet & 0xff;
        }
        return result;
    }

    public static int[] parseCIDR(String cidr) throws UnknownHostException {
        String[] parts = cidr.split("/");
        int ip = parseIP(parts[0]);
        int prefix = parts.length < 2 ? 0 : Integer.parseInt(parts[1]);
        int mask = 0xffffffff << (32 - prefix);
        int min = ip & mask & 0xffffffff;
        int max = ip | ~mask & 0xffffffff;
        return new int[]{min, max};
    }

    public static int getMinIP(String cidr) throws UnknownHostException {
        return parseCIDR(cidr)[0];
    }

    public static int getMaxIP(String cidr) throws UnknownHostException {
        return parseCIDR(cidr)[1];
    }

    public static boolean isInRange(String ip, String cidr) throws UnknownHostException {
        int[] range = parseCIDR(cidr);
        int maxIp = range[0];
        int minIp = range[1];
        int theIp = parseIP(ip);
        return Integer.compareUnsigned(minIp, theIp) <= 0 && Integer.compareUnsigned(theIp, maxIp) <= 0;
    }

    public static void printCIDR(String cidr, PrintStream ps) throws UnknownHostException {
        String[] parts = cidr.split("/");
        int ip = parseIP(parts[0]);
        int prefix = parts.length < 2 ? 0 : Integer.parseInt(parts[1]);
        int subnetMask = 0xffffffff << (32 - prefix);
        int wildcardMask = ~subnetMask;
        int min = ip & subnetMask & 0xffffffff;
        int max = ip | wildcardMask & 0xffffffff;

        ps.println("CIDR: " + toString(min) + "/" + prefix);
        ps.println("Number of mask bits: " + prefix);
        ps.println("Subnet mask: " + toString(subnetMask));
        ps.println("Wildcard mask: " + toString(wildcardMask));
        ps.println("Network address: " + toString(min));
        ps.println("Broadcast address: " + toString(max));
        int numberOfIpAddresses = (int) (Integer.toUnsignedLong(max) - Integer.toUnsignedLong(min) + 1);
        ps.println("Number of IP addresses: " + numberOfIpAddresses);
        int numberOfUsableIpAddresses = numberOfIpAddresses - 2;
        ps.println("Number of usable IP addresses: " + numberOfUsableIpAddresses);
    }

    public static void main(String[] args) throws Throwable {
        String cidr = "121.200.6.230/30";
        printCIDR(cidr, System.out);
    }
}
