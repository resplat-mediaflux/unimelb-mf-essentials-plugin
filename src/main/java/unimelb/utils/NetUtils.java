package unimelb.utils;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class NetUtils {

	public static final int LDAP_PORT = 389;
	public static final int LDAPS_PORT = 636;
	public static final int GC_PORT = 3268;
	public static final int GCS_PORT = 3269;

	public static boolean isPortListening(String host, int port, int timeout) throws Throwable {
		try (Socket s = new Socket()) {
			s.connect(new InetSocketAddress(host, port), timeout);
			return true;
		} catch (Throwable e) {
			return false;
		}
	}

	public static Attribute getLdapSRVRecord(String domainName) throws Throwable {
		return getSRVRecord("ldap", "tcp", domainName);
	}

	public static List<SRVRecordEntry> getLdapSRVRecordEntries(String domainName) throws Throwable {
		return getSRVRecordEntries("ldap", "tcp", domainName);
	}

	public static Attribute getGcSRVRecord(String domainName) throws Throwable {
		return getSRVRecord("gc", "tcp", domainName);
	}

	public static List<SRVRecordEntry> getGcSRVRecordEntries(String domainName) throws Throwable {
		return getSRVRecordEntries("gc", "tcp", domainName);
	}

	public static Attribute getKerberosSRVRecord(String domainName) throws Throwable {
		return getSRVRecord("kerberos", "tcp", domainName);
	}

	public static List<SRVRecordEntry> getKerberosSRVRecordEntries(String domainName) throws Throwable {
		return getSRVRecordEntries("kerberos", "tcp", domainName);
	}

	public static List<SRVRecordEntry> getSRVRecordEntries(String serviceType, String protocol, String domainName)
			throws Throwable {
		Attribute attr = getSRVRecord(serviceType, protocol, domainName);
		List<SRVRecordEntry> entries = new ArrayList<SRVRecordEntry>(attr.size());
		for (int i = 0; i < attr.size(); i++) {
			entries.add(new SRVRecordEntry(String.valueOf(attr.get(i))));
		}
		return entries;
	}

	public static Attribute getSRVRecord(String serviceType, String protocol, String domainName) throws Throwable {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
		env.put("java.naming.provider.url", "dns:");
		DirContext ctx = new InitialDirContext(env);
		Attributes attributes = ctx.getAttributes("_" + serviceType + "._" + protocol + "." + domainName,
				new String[] { "SRV" });
		return attributes.get("SRV");
	}

	public static class SRVRecordEntry implements Comparable<SRVRecordEntry> {
		public final int priority;
		public final int weight;
		public final int port;
		public final String host;

		SRVRecordEntry(String s) {
			String[] parts = s.split("\\ +");
			this.priority = Integer.parseInt(parts[0]);
			this.weight = Integer.parseInt(parts[1]);
			this.port = Integer.parseInt(parts[2]);
			String host = parts[3].trim();
			while (host.endsWith(".")) {
				host = host.substring(0, host.length() - 1);
			}
			this.host = host;
		}

		@Override
		public int compareTo(SRVRecordEntry e) {
			if (this.priority < e.priority) {
				return -1;
			}
			if (this.priority > e.priority) {
				return 1;
			}
			if (this.weight > e.weight) {
				return -1;
			}
			if (this.weight < e.weight) {
				return 1;
			}
			return 0;
		}
	}

	public static List<String> getAliveLdapHosts(String domainName, int port, int portTestTimeout) throws Throwable {

		List<SRVRecordEntry> entries = NetUtils.getLdapSRVRecordEntries(domainName);
		if (entries != null && !entries.isEmpty()) {
			Collections.sort(entries);
			List<String> hosts = new ArrayList<String>();
			for (SRVRecordEntry e : entries) {
				if (e.port == port) {
					hosts.add(e.host);
				} else {
					if (NetUtils.isPortListening(e.host, port, portTestTimeout)) {
						hosts.add(e.host);
					}
				}
			}
			if (!hosts.isEmpty()) {
				return hosts;
			}
		}
		return null;
	}

	public static List<String> getAliveLdapHosts(String domainName, int port) throws Throwable {
		return getAliveLdapHosts(domainName, port, 100);
	}

	public static List<String> getAliveLdapHosts(String domainName) throws Throwable {
		return getAliveLdapHosts(domainName, LDAP_PORT, 100);
	}
	
	public static List<String> getAliveGcHosts(String domainName, int port, int portTestTimeout) throws Throwable {

		List<SRVRecordEntry> entries = NetUtils.getGcSRVRecordEntries(domainName);
		if (entries != null && !entries.isEmpty()) {
			Collections.sort(entries);
			List<String> hosts = new ArrayList<String>();
			for (SRVRecordEntry e : entries) {
				if (e.port == port) {
					hosts.add(e.host);
				} else {
					if (NetUtils.isPortListening(e.host, port, portTestTimeout)) {
						hosts.add(e.host);
					}
				}
			}
			if (!hosts.isEmpty()) {
				return hosts;
			}
		}
		return null;
	}

	public static List<String> getAliveGcHosts(String domainName, int port) throws Throwable {
		return getAliveGcHosts(domainName, port, 100);
	}

	public static List<String> getAliveGcHosts(String domainName) throws Throwable {
		return getAliveGcHosts(domainName, LDAP_PORT, 100);
	}

	public static void main(String[] args) throws Throwable {
		System.out.println("LDAP:");
		List<String> hosts = getAliveLdapHosts("unimelb.edu.au");
		for (String host : hosts) {
			System.out.println(host);
		}
		System.out.println("GC:");
		hosts = getAliveGcHosts("unimelb.edu.au");
		for (String host : hosts) {
			System.out.println(host);
		}
	}

}
