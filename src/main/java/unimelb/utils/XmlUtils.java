package unimelb.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Collections;
import java.util.List;

import arc.streams.StreamCopy;
import arc.xml.XmlDoc;

public class XmlUtils {

	public static void saveIndentedText(XmlDoc.Element e, StringBuilder sb, int indentStart, int indentSize) {
		sb.append(String.join("", Collections.nCopies(indentStart, " "))).append(":").append(e.name());
		if (e.hasAttributes()) {
			List<XmlDoc.Attribute> attrs = e.attributes();
			for (XmlDoc.Attribute attr : attrs) {
				sb.append(" -").append(attr.name()).append(" ").append(attr.value());
			}
		}
		if (e.value() != null) {
			sb.append(" ").append(e.value());
		}
		sb.append("\n");
		if (e.hasSubElements()) {
			List<XmlDoc.Element> ses = e.elements();
			for (XmlDoc.Element se : ses) {
				saveIndentedText(se, sb, indentStart + indentSize, indentSize);
			}
		}
	}

	public static void saveIndentedText(XmlDoc.Element e, boolean self, StringBuilder sb, int indentStart,
			int indentSize) {
		if (self) {
			saveIndentedText(e, sb, indentStart, indentSize);
		} else {
			if (e.hasSubElements()) {
				List<XmlDoc.Element> ses = e.elements();
				for (XmlDoc.Element se : ses) {
					saveIndentedText(se, sb, indentStart, indentSize);
				}
			}
		}
	}

	public static String toIndentedText(XmlDoc.Element e, boolean self, int indentStart, int indentSize) {
		StringBuilder sb = new StringBuilder();
		saveIndentedText(e, self, sb, indentStart, indentSize);
		return sb.toString();
	}

	public static String toIndentedText(XmlDoc.Element e, boolean self, int indentSize) {
		return toIndentedText(e, self, 0, indentSize);
	}

	public static String toIndentedText(XmlDoc.Element e, boolean self) {
		return toIndentedText(e, self, 0, 4);
	}

	public static String toIndentedText(XmlDoc.Element e) {
		return toIndentedText(e, true, 0, 4);
	}

	public static String serviceCallToIndentedText(String serviceName, XmlDoc.Element args) throws Throwable {
		StringBuilder sb = new StringBuilder(serviceName).append("\n");
		XmlUtils.saveIndentedText(args, false, sb, 4, 4);
		return sb.toString();
	}
	
	
	public static void writeServerSideXMLFile(XmlDoc.Element xe, File file) throws Throwable {
		String str = xe.toString();
		ByteArrayInputStream bis = new ByteArrayInputStream(str.getBytes());
		StreamCopy.copy(bis, file);
	}

}
