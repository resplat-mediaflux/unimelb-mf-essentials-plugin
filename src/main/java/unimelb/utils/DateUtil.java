package unimelb.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;



public class DateUtil {

	/**
	 * Difference of dates in days
	 * 
	 * @param date2 - date1
	 */
	public static long dateDifferenceInDays (Date date2, Date date1) throws Throwable {
		long diff = date2.getTime()-date1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Difference of dates in hours
	 * 
	 * @param date2 - date1
	 */
	public static long dateDifferenceInHours (Date date2, Date date1) throws Throwable {
		long diff = date2.getTime()-date1.getTime();
		return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
	}

	
	/*
	 * Convert date to MF format dd-MMM-yyyy HH:mm:ss (time part optional)
	 * 
	 */
	public static String formatDate (java.util.Date date, Boolean withTime, Boolean setTimeToNull) throws Throwable {
		// Format the new date in MF date format\
		if (withTime) {
			if (setTimeToNull) {
				return formatDate(date, "dd-MMM-yyyy 00:00:00");
			} else {
				return formatDate(date, "dd-MMM-yyyy HH:mm:ss");
			}
		} else {
			return formatDate(date, "dd-MMM-yyyy");
		}
	}

	/*
	 * Convert date to MF format dd-MMM-yyyy HH:mm:ss
	 * 
	 */
	public static String formatDate (java.util.Date date, String pattern) throws Throwable {
		// Format the new date in MF date format
		SimpleDateFormat formatterOut = new SimpleDateFormat (pattern);
		return formatterOut.format(date);
	}

	/**
	 * Convert date string between given  patterns
	 * 
	 * @param dateIn
	 * @param patternIn
	 * @param patternOut
	 * @return
	 * @throws Throwable
	 */
	public static String convertDateString  (String dateIn, String patternIn, String patternOut) throws Throwable  { 
		//
		SimpleDateFormat formatterIn = new SimpleDateFormat (patternIn);

		try {
			Date date = formatterIn.parse(dateIn);
			SimpleDateFormat formatterOut = new SimpleDateFormat (patternOut);
			return formatterOut.format(date);
		} catch (Throwable ex) {
			throw new RuntimeException ("The date string " + dateIn + " is not in an expected format");
		}
	}

	/**
	 * Convert date string with pattern to date
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 * @throws Throwable
	 */
	public static Date dateFromString (String date, String pattern) throws Throwable {
		SimpleDateFormat formatter = new SimpleDateFormat (pattern);

		try {
			return formatter.parse(date);
		} catch (Throwable ex) {
			throw new RuntimeException ("The date string " + date + " is not in an expected format");
		}
	}

	/**
	 * Get today's date in various formats 
	 * style=0 -> ddMMyyyy
	 * style=1 -> YYYY-MM-dd
	 * style=2 -> dd-MMM-YYYY
	 * style=3 -> dd-MMM-YYYY HH:mm:ss
	 * style=4 -> dd-MMM-YYYY_HH:mm:ss
	 */
	public static String  todaysDate(int style) throws Throwable {
		Date date = new Date();
		SimpleDateFormat formatterOut = null;
		//
		if (style==0) {
			formatterOut = new SimpleDateFormat ("ddMMMyyyy");
		} else if (style==1) {
			formatterOut = new SimpleDateFormat ("yyyy-MM-dd");
		} else if (style==2) {
			formatterOut = new SimpleDateFormat ("dd-MMM-yyyy");
		} else if (style==3) {
			formatterOut = new SimpleDateFormat ("dd-MMM-yyyy HH:mm:ss");
		} else if (style==4) {
			formatterOut = new SimpleDateFormat ("dd-MMM-yyyy_HH:mm:ss");
		} else {
			throw new RuntimeException("Invalid date style");
		}
		return formatterOut.format(date);
	}

	/**
	 * Get date in days (i.e. 365*year + day_of_year)
	 * 
	 * @param date
	 * @return
	 * @throws Throwable
	 */
	public static Integer dateInDays (Date date) throws Throwable {
		if (date==null) return null;
		//
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int day = c.get(Calendar.DAY_OF_YEAR);
		int year = c.get(Calendar.YEAR);
		//
		Integer d = year*365 + day;		

		return d;
	}


	/**
	 * Get today's time in format dd-MMM-yyyy-HH:mm:ss
	 */
	public static String  todaysTime () throws Throwable {
		return todaysTime (0);
	}

	
	/**
	 * Get today's date/time in format 
	 * style=0 -> DD-MMM-YYYY-hh:mm:ss
	 * style=1 -> YYMMDDhhmmss.SSS
	 * style=2 -> YYYYMMDD-hhmmss
	 * style=3 -> DD-MMM-YYYY hh:mm:ss

	 */
	public static String  todaysTime (int style) throws Throwable {
		Date date = new Date();
		SimpleDateFormat formatterOut = null;
		if (style==0) {
			formatterOut = new SimpleDateFormat ("dd-MMM-yyyy-HH:mm:ss");
		} else if (style==1) {
			formatterOut = new SimpleDateFormat ("yyMMddHHmmssSSS");
		} else if (style==2) {
			formatterOut = new SimpleDateFormat ("yyyyMMdd-HHmmss");
		} else if (style==3) {
				formatterOut = new SimpleDateFormat ("dd-MMM-yyyy HH:mm:ss");
		} else {
			throw new RuntimeException("Invalid date/time style");
		}
		return formatterOut.format(date);
	}

	
	

	/**
	 * Compare two date of birth dates.  Ignore any part of the date apart from YYYY-MM-DD
	 * 
	 * @param dob1
	 * @param dob2
	 * @return
	 * @throws Throwable
	 */
	public static boolean areDatesOfBirthEqual (Date dob1, Date dob2) throws Throwable {
		String d1 = formatDate (dob1, "yyyy-MM-dd");
		String d2 = formatDate (dob2, "yyyy-MM-dd");
		return (d1.equals(d2));
	}
	
	public static Date addDays (Date date, Integer days) throws Throwable {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, days); 
		return c.getTime();

	}

	public static Date addMinutes (Date date, Integer mins) throws Throwable {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MINUTE, mins);
		return c.getTime();
	}

}
