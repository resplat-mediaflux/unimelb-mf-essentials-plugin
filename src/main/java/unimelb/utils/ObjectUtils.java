package unimelb.utils;

public class ObjectUtils {

    public static boolean equals(Object a, Object b) {
        if (a != null && b != null) {
            return a.equals(b);
        }
        if (a == null && b == null) {
            return true;
        } else {
            return false;
        }
    }
}
