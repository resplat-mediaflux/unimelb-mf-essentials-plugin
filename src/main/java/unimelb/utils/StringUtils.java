package unimelb.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringUtils {

    public static String trimPrefix(String str, String prefix, boolean repeat) {
        String r = str;
        if (repeat) {
            while (r.startsWith(prefix)) {
                r = r.substring(prefix.length());
            }
        } else {
            if (r.startsWith(prefix)) {
                r = r.substring(prefix.length());
            }
        }
        return r;
    }

    public static String trimSuffix(String str, String suffix, boolean repeat) {
        String r = str;
        if (repeat) {
            while (r.endsWith(suffix)) {
                r = r.substring(0, r.length() - suffix.length());
            }
        } else {
            if (r.endsWith(suffix)) {
                r = r.substring(0, r.length() - suffix.length());
            }
        }
        return r;
    }

    public static String stringOf(char c, int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(c);
        }
        return sb.toString();
    }

    public static String join(Collection<String> strs, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String str : strs) {
            append(sb, delimiter, str);
        }
        return sb.toString();
    }

    public static void append(StringBuilder sb, String delimiter, String str) {
        if (sb.length() > 0) {
            sb.append(delimiter);
        }
        sb.append(str);
    }

    public static String repeat(String s, int n) {
        return String.join("", Collections.nCopies(n, s));
    }

    public static void main(String[] args) {
        // System.out.println(stringOf('1', 10));
    }
    
    /**
     * This function sorts numeric strings into ascending order.
     * 
     * @param numbers
     * @return
     * @throws Throwable
     */
	public static List<String> sortNumbersAscending (Collection<String> numbers) throws Throwable {
		if (numbers == null || numbers.isEmpty()) {
			return null;
		}
		List<String> list = new ArrayList<String>(numbers);
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String v1, String v2) {

				Integer i1 = Integer.parseInt(v1);
				Integer i2 = Integer.parseInt(v2);
				if (i1>i2) {
					return 1;
				} else if (i1.equals(i2)) {
					return 0;
				} else {
					return -1;
				}			

			}
		});
		return list;
	}


    public static String escapeHtml(String source) {
        if (source == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            switch (c) {
                // Special characters for HTML
                case '\u0026':
                    sb.append("&amp;");
                    break;
                case '\u003C':
                    sb.append("&lt;");
                    break;
                case '\u003E':
                    sb.append("&gt;");
                    break;
                case '\u0022':
                    sb.append("&quot;");
                    break;

                case '\u0152':
                    sb.append("&OElig;");
                    break;
                case '\u0153':
                    sb.append("&oelig;");
                    break;
                case '\u0160':
                    sb.append("&Scaron;");
                    break;
                case '\u0161':
                    sb.append("&scaron;");
                    break;
                case '\u0178':
                    sb.append("&Yuml;");
                    break;
                case '\u02C6':
                    sb.append("&circ;");
                    break;
                case '\u02DC':
                    sb.append("&tilde;");
                    break;
                case '\u2002':
                    sb.append("&ensp;");
                    break;
                case '\u2003':
                    sb.append("&emsp;");
                    break;
                case '\u2009':
                    sb.append("&thinsp;");
                    break;
                case '\u200C':
                    sb.append("&zwnj;");
                    break;
                case '\u200D':
                    sb.append("&zwj;");
                    break;
                case '\u200E':
                    sb.append("&lrm;");
                    break;
                case '\u200F':
                    sb.append("&rlm;");
                    break;
                case '\u2013':
                    sb.append("&ndash;");
                    break;
                case '\u2014':
                    sb.append("&mdash;");
                    break;
                case '\u2018':
                    sb.append("&lsquo;");
                    break;
                case '\u2019':
                    sb.append("&rsquo;");
                    break;
                case '\u201A':
                    sb.append("&sbquo;");
                    break;
                case '\u201C':
                    sb.append("&ldquo;");
                    break;
                case '\u201D':
                    sb.append("&rdquo;");
                    break;
                case '\u201E':
                    sb.append("&bdquo;");
                    break;
                case '\u2020':
                    sb.append("&dagger;");
                    break;
                case '\u2021':
                    sb.append("&Dagger;");
                    break;
                case '\u2030':
                    sb.append("&permil;");
                    break;
                case '\u2039':
                    sb.append("&lsaquo;");
                    break;
                case '\u203A':
                    sb.append("&rsaquo;");
                    break;
                case '\u20AC':
                    sb.append("&euro;");
                    break;

                // Character entity references for ISO 8859-1 characters
                case '\u00A0':
                    sb.append("&nbsp;");
                    break;
                case '\u00A1':
                    sb.append("&iexcl;");
                    break;
                case '\u00A2':
                    sb.append("&cent;");
                    break;
                case '\u00A3':
                    sb.append("&pound;");
                    break;
                case '\u00A4':
                    sb.append("&curren;");
                    break;
                case '\u00A5':
                    sb.append("&yen;");
                    break;
                case '\u00A6':
                    sb.append("&brvbar;");
                    break;
                case '\u00A7':
                    sb.append("&sect;");
                    break;
                case '\u00A8':
                    sb.append("&uml;");
                    break;
                case '\u00A9':
                    sb.append("&copy;");
                    break;
                case '\u00AA':
                    sb.append("&ordf;");
                    break;
                case '\u00AB':
                    sb.append("&laquo;");
                    break;
                case '\u00AC':
                    sb.append("&not;");
                    break;
                case '\u00AD':
                    sb.append("&shy;");
                    break;
                case '\u00AE':
                    sb.append("&reg;");
                    break;
                case '\u00AF':
                    sb.append("&macr;");
                    break;
                case '\u00B0':
                    sb.append("&deg;");
                    break;
                case '\u00B1':
                    sb.append("&plusmn;");
                    break;
                case '\u00B2':
                    sb.append("&sup2;");
                    break;
                case '\u00B3':
                    sb.append("&sup3;");
                    break;
                case '\u00B4':
                    sb.append("&acute;");
                    break;
                case '\u00B5':
                    sb.append("&micro;");
                    break;
                case '\u00B6':
                    sb.append("&para;");
                    break;
                case '\u00B7':
                    sb.append("&middot;");
                    break;
                case '\u00B8':
                    sb.append("&cedil;");
                    break;
                case '\u00B9':
                    sb.append("&sup1;");
                    break;
                case '\u00BA':
                    sb.append("&ordm;");
                    break;
                case '\u00BB':
                    sb.append("&raquo;");
                    break;
                case '\u00BC':
                    sb.append("&frac14;");
                    break;
                case '\u00BD':
                    sb.append("&frac12;");
                    break;
                case '\u00BE':
                    sb.append("&frac34;");
                    break;
                case '\u00BF':
                    sb.append("&iquest;");
                    break;
                case '\u00C0':
                    sb.append("&Agrave;");
                    break;
                case '\u00C1':
                    sb.append("&Aacute;");
                    break;
                case '\u00C2':
                    sb.append("&Acirc;");
                    break;
                case '\u00C3':
                    sb.append("&Atilde;");
                    break;
                case '\u00C4':
                    sb.append("&Auml;");
                    break;
                case '\u00C5':
                    sb.append("&Aring;");
                    break;
                case '\u00C6':
                    sb.append("&AElig;");
                    break;
                case '\u00C7':
                    sb.append("&Ccedil;");
                    break;
                case '\u00C8':
                    sb.append("&Egrave;");
                    break;
                case '\u00C9':
                    sb.append("&Eacute;");
                    break;
                case '\u00CA':
                    sb.append("&Ecirc;");
                    break;
                case '\u00CB':
                    sb.append("&Euml;");
                    break;
                case '\u00CC':
                    sb.append("&Igrave;");
                    break;
                case '\u00CD':
                    sb.append("&Iacute;");
                    break;
                case '\u00CE':
                    sb.append("&Icirc;");
                    break;
                case '\u00CF':
                    sb.append("&Iuml;");
                    break;
                case '\u00D0':
                    sb.append("&ETH;");
                    break;
                case '\u00D1':
                    sb.append("&Ntilde;");
                    break;
                case '\u00D2':
                    sb.append("&Ograve;");
                    break;
                case '\u00D3':
                    sb.append("&Oacute;");
                    break;
                case '\u00D4':
                    sb.append("&Ocirc;");
                    break;
                case '\u00D5':
                    sb.append("&Otilde;");
                    break;
                case '\u00D6':
                    sb.append("&Ouml;");
                    break;
                case '\u00D7':
                    sb.append("&times;");
                    break;
                case '\u00D8':
                    sb.append("&Oslash;");
                    break;
                case '\u00D9':
                    sb.append("&Ugrave;");
                    break;
                case '\u00DA':
                    sb.append("&Uacute;");
                    break;
                case '\u00DB':
                    sb.append("&Ucirc;");
                    break;
                case '\u00DC':
                    sb.append("&Uuml;");
                    break;
                case '\u00DD':
                    sb.append("&Yacute;");
                    break;
                case '\u00DE':
                    sb.append("&THORN;");
                    break;
                case '\u00DF':
                    sb.append("&szlig;");
                    break;
                case '\u00E0':
                    sb.append("&agrave;");
                    break;
                case '\u00E1':
                    sb.append("&aacute;");
                    break;
                case '\u00E2':
                    sb.append("&acirc;");
                    break;
                case '\u00E3':
                    sb.append("&atilde;");
                    break;
                case '\u00E4':
                    sb.append("&auml;");
                    break;
                case '\u00E5':
                    sb.append("&aring;");
                    break;
                case '\u00E6':
                    sb.append("&aelig;");
                    break;
                case '\u00E7':
                    sb.append("&ccedil;");
                    break;
                case '\u00E8':
                    sb.append("&egrave;");
                    break;
                case '\u00E9':
                    sb.append("&eacute;");
                    break;
                case '\u00EA':
                    sb.append("&ecirc;");
                    break;
                case '\u00EB':
                    sb.append("&euml;");
                    break;
                case '\u00EC':
                    sb.append("&igrave;");
                    break;
                case '\u00ED':
                    sb.append("&iacute;");
                    break;
                case '\u00EE':
                    sb.append("&icirc;");
                    break;
                case '\u00EF':
                    sb.append("&iuml;");
                    break;
                case '\u00F0':
                    sb.append("&eth;");
                    break;
                case '\u00F1':
                    sb.append("&ntilde;");
                    break;
                case '\u00F2':
                    sb.append("&ograve;");
                    break;
                case '\u00F3':
                    sb.append("&oacute;");
                    break;
                case '\u00F4':
                    sb.append("&ocirc;");
                    break;
                case '\u00F5':
                    sb.append("&otilde;");
                    break;
                case '\u00F6':
                    sb.append("&ouml;");
                    break;
                case '\u00F7':
                    sb.append("&divide;");
                    break;
                case '\u00F8':
                    sb.append("&oslash;");
                    break;
                case '\u00F9':
                    sb.append("&ugrave;");
                    break;
                case '\u00FA':
                    sb.append("&uacute;");
                    break;
                case '\u00FB':
                    sb.append("&ucirc;");
                    break;
                case '\u00FC':
                    sb.append("&uuml;");
                    break;
                case '\u00FD':
                    sb.append("&yacute;");
                    break;
                case '\u00FE':
                    sb.append("&thorn;");
                    break;
                case '\u00FF':
                    sb.append("&yuml;");
                    break;

                // Mathematical, Greek and Symbolic characters for HTML
                case '\u0192':
                    sb.append("&fnof;");
                    break;
                case '\u0391':
                    sb.append("&Alpha;");
                    break;
                case '\u0392':
                    sb.append("&Beta;");
                    break;
                case '\u0393':
                    sb.append("&Gamma;");
                    break;
                case '\u0394':
                    sb.append("&Delta;");
                    break;
                case '\u0395':
                    sb.append("&Epsilon;");
                    break;
                case '\u0396':
                    sb.append("&Zeta;");
                    break;
                case '\u0397':
                    sb.append("&Eta;");
                    break;
                case '\u0398':
                    sb.append("&Theta;");
                    break;
                case '\u0399':
                    sb.append("&Iota;");
                    break;
                case '\u039A':
                    sb.append("&Kappa;");
                    break;
                case '\u039B':
                    sb.append("&Lambda;");
                    break;
                case '\u039C':
                    sb.append("&Mu;");
                    break;
                case '\u039D':
                    sb.append("&Nu;");
                    break;
                case '\u039E':
                    sb.append("&Xi;");
                    break;
                case '\u039F':
                    sb.append("&Omicron;");
                    break;
                case '\u03A0':
                    sb.append("&Pi;");
                    break;
                case '\u03A1':
                    sb.append("&Rho;");
                    break;
                case '\u03A3':
                    sb.append("&Sigma;");
                    break;
                case '\u03A4':
                    sb.append("&Tau;");
                    break;
                case '\u03A5':
                    sb.append("&Upsilon;");
                    break;
                case '\u03A6':
                    sb.append("&Phi;");
                    break;
                case '\u03A7':
                    sb.append("&Chi;");
                    break;
                case '\u03A8':
                    sb.append("&Psi;");
                    break;
                case '\u03A9':
                    sb.append("&Omega;");
                    break;
                case '\u03B1':
                    sb.append("&alpha;");
                    break;
                case '\u03B2':
                    sb.append("&beta;");
                    break;
                case '\u03B3':
                    sb.append("&gamma;");
                    break;
                case '\u03B4':
                    sb.append("&delta;");
                    break;
                case '\u03B5':
                    sb.append("&epsilon;");
                    break;
                case '\u03B6':
                    sb.append("&zeta;");
                    break;
                case '\u03B7':
                    sb.append("&eta;");
                    break;
                case '\u03B8':
                    sb.append("&theta;");
                    break;
                case '\u03B9':
                    sb.append("&iota;");
                    break;
                case '\u03BA':
                    sb.append("&kappa;");
                    break;
                case '\u03BB':
                    sb.append("&lambda;");
                    break;
                case '\u03BC':
                    sb.append("&mu;");
                    break;
                case '\u03BD':
                    sb.append("&nu;");
                    break;
                case '\u03BE':
                    sb.append("&xi;");
                    break;
                case '\u03BF':
                    sb.append("&omicron;");
                    break;
                case '\u03C0':
                    sb.append("&pi;");
                    break;
                case '\u03C1':
                    sb.append("&rho;");
                    break;
                case '\u03C2':
                    sb.append("&sigmaf;");
                    break;
                case '\u03C3':
                    sb.append("&sigma;");
                    break;
                case '\u03C4':
                    sb.append("&tau;");
                    break;
                case '\u03C5':
                    sb.append("&upsilon;");
                    break;
                case '\u03C6':
                    sb.append("&phi;");
                    break;
                case '\u03C7':
                    sb.append("&chi;");
                    break;
                case '\u03C8':
                    sb.append("&psi;");
                    break;
                case '\u03C9':
                    sb.append("&omega;");
                    break;
                case '\u03D1':
                    sb.append("&thetasym;");
                    break;
                case '\u03D2':
                    sb.append("&upsih;");
                    break;
                case '\u03D6':
                    sb.append("&piv;");
                    break;
                case '\u2022':
                    sb.append("&bull;");
                    break;
                case '\u2026':
                    sb.append("&hellip;");
                    break;
                case '\u2032':
                    sb.append("&prime;");
                    break;
                case '\u2033':
                    sb.append("&Prime;");
                    break;
                case '\u203E':
                    sb.append("&oline;");
                    break;
                case '\u2044':
                    sb.append("&frasl;");
                    break;
                case '\u2118':
                    sb.append("&weierp;");
                    break;
                case '\u2111':
                    sb.append("&image;");
                    break;
                case '\u211C':
                    sb.append("&real;");
                    break;
                case '\u2122':
                    sb.append("&trade;");
                    break;
                case '\u2135':
                    sb.append("&alefsym;");
                    break;
                case '\u2190':
                    sb.append("&larr;");
                    break;
                case '\u2191':
                    sb.append("&uarr;");
                    break;
                case '\u2192':
                    sb.append("&rarr;");
                    break;
                case '\u2193':
                    sb.append("&darr;");
                    break;
                case '\u2194':
                    sb.append("&harr;");
                    break;
                case '\u21B5':
                    sb.append("&crarr;");
                    break;
                case '\u21D0':
                    sb.append("&lArr;");
                    break;
                case '\u21D1':
                    sb.append("&uArr;");
                    break;
                case '\u21D2':
                    sb.append("&rArr;");
                    break;
                case '\u21D3':
                    sb.append("&dArr;");
                    break;
                case '\u21D4':
                    sb.append("&hArr;");
                    break;
                case '\u2200':
                    sb.append("&forall;");
                    break;
                case '\u2202':
                    sb.append("&part;");
                    break;
                case '\u2203':
                    sb.append("&exist;");
                    break;
                case '\u2205':
                    sb.append("&empty;");
                    break;
                case '\u2207':
                    sb.append("&nabla;");
                    break;
                case '\u2208':
                    sb.append("&isin;");
                    break;
                case '\u2209':
                    sb.append("&notin;");
                    break;
                case '\u220B':
                    sb.append("&ni;");
                    break;
                case '\u220F':
                    sb.append("&prod;");
                    break;
                case '\u2211':
                    sb.append("&sum;");
                    break;
                case '\u2212':
                    sb.append("&minus;");
                    break;
                case '\u2217':
                    sb.append("&lowast;");
                    break;
                case '\u221A':
                    sb.append("&radic;");
                    break;
                case '\u221D':
                    sb.append("&prop;");
                    break;
                case '\u221E':
                    sb.append("&infin;");
                    break;
                case '\u2220':
                    sb.append("&ang;");
                    break;
                case '\u2227':
                    sb.append("&and;");
                    break;
                case '\u2228':
                    sb.append("&or;");
                    break;
                case '\u2229':
                    sb.append("&cap;");
                    break;
                case '\u222A':
                    sb.append("&cup;");
                    break;
                case '\u222B':
                    sb.append("&int;");
                    break;
                case '\u2234':
                    sb.append("&there4;");
                    break;
                case '\u223C':
                    sb.append("&sim;");
                    break;
                case '\u2245':
                    sb.append("&cong;");
                    break;
                case '\u2248':
                    sb.append("&asymp;");
                    break;
                case '\u2260':
                    sb.append("&ne;");
                    break;
                case '\u2261':
                    sb.append("&equiv;");
                    break;
                case '\u2264':
                    sb.append("&le;");
                    break;
                case '\u2265':
                    sb.append("&ge;");
                    break;
                case '\u2282':
                    sb.append("&sub;");
                    break;
                case '\u2283':
                    sb.append("&sup;");
                    break;
                case '\u2284':
                    sb.append("&nsub;");
                    break;
                case '\u2286':
                    sb.append("&sube;");
                    break;
                case '\u2287':
                    sb.append("&supe;");
                    break;
                case '\u2295':
                    sb.append("&oplus;");
                    break;
                case '\u2297':
                    sb.append("&otimes;");
                    break;
                case '\u22A5':
                    sb.append("&perp;");
                    break;
                case '\u22C5':
                    sb.append("&sdot;");
                    break;
                case '\u2308':
                    sb.append("&lceil;");
                    break;
                case '\u2309':
                    sb.append("&rceil;");
                    break;
                case '\u230A':
                    sb.append("&lfloor;");
                    break;
                case '\u230B':
                    sb.append("&rfloor;");
                    break;
                case '\u2329':
                    sb.append("&lang;");
                    break;
                case '\u232A':
                    sb.append("&rang;");
                    break;
                case '\u25CA':
                    sb.append("&loz;");
                    break;
                case '\u2660':
                    sb.append("&spades;");
                    break;
                case '\u2663':
                    sb.append("&clubs;");
                    break;
                case '\u2665':
                    sb.append("&hearts;");
                    break;
                case '\u2666':
                    sb.append("&diams;");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }
}
