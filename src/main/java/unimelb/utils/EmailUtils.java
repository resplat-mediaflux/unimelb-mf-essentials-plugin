package unimelb.utils;

import java.util.regex.Pattern;

public class EmailUtils {

    public static final Pattern REGEX_EMAIL_ADDRESS = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);

    public static boolean isValidEmailAddress(String emailAddr) {
        return REGEX_EMAIL_ADDRESS.matcher(emailAddr).find();
    }
}
