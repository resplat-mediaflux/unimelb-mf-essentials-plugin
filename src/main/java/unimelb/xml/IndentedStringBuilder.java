package unimelb.xml;

public class IndentedStringBuilder extends IndentedWriter {

	private StringBuilder _sb;

	private int _maxLength = -1;

	public IndentedStringBuilder(int indentSize, int maxLength) {
		this(new StringBuilder(), 0, indentSize, maxLength);
	}

	public IndentedStringBuilder(StringBuilder sb, int indentSize, int maxLength) {
		this(sb, 0, indentSize, maxLength);
	}

	public IndentedStringBuilder(StringBuilder sb, int indentLevel, int indentSize, int maxLength) {
		super(indentLevel, indentSize);
		_sb = sb;
		_maxLength = maxLength;
	}

	@Override
	public void add(String element) throws Throwable {
		if (_maxLength > 0 && _sb.length() > _maxLength) {
			// @formatter:off
			// System.err.println(String.format("Maximum string length limit(%d) exceeded.",
			// _maxLength));
			// @formatter:on
			return;
		}
		super.add(element);
	}

	@Override
	public void appValue(String indentedText) throws Throwable {
		if (_maxLength > 0 && _sb.length() > _maxLength) {
			// @formatter:off
			// System.err.println(String.format("Maximum string length limit(%d) exceeded.",
			// _maxLength));
			// @formatter:on
			return;
		}
		super.appValue(indentedText);
	}

	public void push(String e, String[] attrs, boolean cr) throws Throwable {
		if (_maxLength < 0 || _sb.length() <= _maxLength) {
			super.push(e, attrs, false);
			if (cr) {
				write("\n");
			}
		}
		if (cr) {
			incIndentLevel();
		}
	}

	@Override
	public void push(String e, String[] attrs) throws Throwable {
		push(e, attrs, true);
	}

	@Override
	public void push(String element, String[] attributes, final String value) throws Throwable {
		if (_maxLength < 0 || _sb.length() <= _maxLength) {
			push(element, attributes, false);
			setValue(value);
			write("\n");
		}
		incIndentLevel();
	}

	@Override
	public void push(String element, final String[] attributes, final Object value) throws Throwable {
		if (_maxLength < 0 || _sb.length() <= _maxLength) {
			push(element, attributes, false);
			setValue(value == null ? null : value.toString());
			write("\n");
		}
		incIndentLevel();
	}

	@Override
	public void setValue(String value, boolean safe) throws Throwable {
		if (_maxLength < 0 || _sb.length() <= _maxLength) {
			super.setValue(value, safe);
		}
	}

	public int length() {
		return _sb.length();
	}

	@Override
	public String toString() {
		return _sb.toString();
	}

	public boolean reachedMaxLength() {
		if (_maxLength < 0) {
			return false;
		} else {
			return _sb.length() > _maxLength;
		}
	}

	@Override
	protected void write(String s) {
		_sb.append(s);
	}

	// @formatter:off
//    public static void main(String[] args) throws Throwable {
//        XmlStringWriter w = new XmlStringWriter();
//        IndentStringBuilder sb = new IndentStringBuilder(4, -1);
//        CollectionXmlWriter dm = new CollectionXmlWriter(w, sb);
//        dm.push("asset", new String[] { "id", "789", "reindexed", "false" });
//        dm.push("index", new String[] { "type", "type" });
//        dm.add("keys", 21);
//        dm.add("values", 0);
//        dm.push("errors");
//        dm.add("destroyed", 0);
//        dm.add("extras", 0);
//        dm.add("missing", 1);
//        dm.add("total", 1);
//        dm.pop();
//        dm.pop();
//        dm.add("reindexed", false);
//        dm.pop();
//        System.out.println(w.document());
//        System.out.println(sb.toString());
//    }
	// @formatter:on

}
