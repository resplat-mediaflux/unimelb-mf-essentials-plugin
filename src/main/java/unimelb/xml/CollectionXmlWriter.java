package unimelb.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arc.xml.XmlWriter;

public class CollectionXmlWriter extends XmlWriter {

    private List<XmlWriter> _ws;

    public CollectionXmlWriter(XmlWriter... ws) {
        _ws = new ArrayList<XmlWriter>();
        if (ws != null) {
            for (XmlWriter w : ws) {
                _ws.add(w);
            }
        }
    }

    public CollectionXmlWriter(Collection<XmlWriter> ws) {
        _ws = new ArrayList<XmlWriter>(ws);
    }

    @Override
    public void add(String e) throws Throwable {
        for (XmlWriter w : _ws) {
            w.add(e);
        }
    }

    @Override
    public void appValue(String value) throws Throwable {
        for (XmlWriter w : _ws) {
            w.appValue(value);
        }
    }

    @Override
    public void pop() throws Throwable {
        for (XmlWriter w : _ws) {
            w.pop();
        }
    }

    @Override
    public void popAll() throws Throwable {
        for (XmlWriter w : _ws) {
            w.popAll();
        }
    }

    @Override
    public void push(String e, String[] attrs) throws Throwable {
        for (XmlWriter w : _ws) {
            w.push(e, attrs);
        }
    }

    @Override
    public void push(String element, String[] attributes, final String value) throws Throwable {
        for (XmlWriter w : _ws) {
            if (w instanceof IndentedWriter) {
                ((IndentedWriter) w).push(element, attributes, value);
            } else {
                w.push(element, attributes, value);
            }
        }
    }

    @Override
    public void push(String element, final String[] attributes, final Object value) throws Throwable {
        for (XmlWriter w : _ws) {
            if (w instanceof IndentedWriter) {
                ((IndentedWriter) w).push(element, attributes, value);
            } else {
                w.push(element, attributes, value);
            }
        }
    }

    @Override
    public void setValue(String e, boolean attrs) throws Throwable {
        for (XmlWriter w : _ws) {
            w.setValue(e, attrs);
        }
    }

    @Override
    public String streamMimeType() {
        if (_ws.size() == 1) {
            return _ws.get(0).streamMimeType();
        }
        return null;
    }

}
