package unimelb.xml;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class IndentedTextWriter extends IndentedWriter implements Closeable {

    private PrintWriter _w;

    public IndentedTextWriter(Writer w) {
        this(w, DEFAULT_IDENT_SIZE);
    }

    public IndentedTextWriter(Writer w, int indentSize) {
        super(indentSize);
        _w = (w instanceof PrintWriter) ? ((PrintWriter) w) : new PrintWriter(new BufferedWriter(w));
    }

    public IndentedTextWriter(File f) throws IOException {
        this(f, DEFAULT_IDENT_SIZE);
    }

    public IndentedTextWriter(File f, int indentSize) throws IOException {
        this(new PrintWriter(new BufferedWriter(new FileWriter(f))), indentSize);
    }

    public IndentedTextWriter(OutputStream os) throws IOException {
        this(os, DEFAULT_IDENT_SIZE);
    }

    public IndentedTextWriter(OutputStream os, int indentSize) throws IOException {
        this(new PrintWriter(new BufferedWriter(new OutputStreamWriter(os))), indentSize);
    }

    @Override
    public void pop() throws Throwable {
        super.pop();
        _w.flush();
    }

    public void flush() {
        _w.flush();
    }

    public void close() {
        try {
            super.close();
        } catch (Throwable t) {
            t.printStackTrace();
            throw new RuntimeException(t);
        } finally {
            _w.close();
        }
    }

    @Override
    protected void write(String s) {
        _w.print(s);
    }

}
