package unimelb.xml;

import arc.xml.XmlDoc;

public class XPathUtils {

    public static boolean isAttributePath(String path) {
        if (path != null) {
            int idx = path.lastIndexOf('@');
            if (idx == 0) {
                return true;
            } else if (idx > 0) {
                return path.charAt(idx - 1) == '/';
            }
        }
        return false;
    }

    public static boolean nodeExists(XmlDoc.Element e, String path) throws Throwable {
        if (isAttributePath(path)) {
            return e.value(path) != null;
        } else {
            return e.elementExists(path);
        }
    }

    public static void main(String[] args) {
        String path1 = "@c";
        String path2 = "a/b/@c";
        String path3 = "a[@b='1']/c";
        String path4 = "a[@b='1']/c[@d='2']";
        System.out.println(isAttributePath(path1));
        System.out.println(isAttributePath(path2));
        System.out.println(isAttributePath(path3));
        System.out.println(isAttributePath(path4));

    }
}
