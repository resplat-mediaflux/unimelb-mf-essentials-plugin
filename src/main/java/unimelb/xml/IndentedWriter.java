package unimelb.xml;

import arc.xml.XmlSafe;
import arc.xml.XmlWriter;

public abstract class IndentedWriter extends XmlWriter {

	public static final int DEFAULT_IDENT_SIZE = 4;

	private int _indentLevel = 0;
	private int _indentSize = DEFAULT_IDENT_SIZE;

	protected IndentedWriter(int indentSize) {
		this(0, indentSize);
	}

	protected IndentedWriter(int indentLevel, int indentSize) {
		_indentLevel = indentLevel;
		_indentSize = indentSize;
	}

	protected abstract void write(String s);

	private void printIndent() {
		for (int i = 0; i < _indentLevel; i++) {
			for (int j = 0; j < _indentSize; j++) {
				write(" ");
			}
		}
	}

	private void printAttrs(String[] attrs) {
		if (attrs != null) {
			for (int i = 0; i < attrs.length; i++) {
				if (i % 2 == 0) {
					write(" -" + attrs[i]);
				} else {
					write(" " + attrs[i]);
				}
			}
		}
	}

	@Override
	public void add(String element) throws Throwable {
		printIndent();
		write(element + "\n");
	}

	@Override
	public void appValue(String indentedText) throws Throwable {
		write(indentedText);
		// throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public void pop() throws Throwable {
		decIndentLevel();
	}

	@Override
	public void popAll() throws Throwable {
		_indentLevel = 0;
	}

	protected void push(String e, String[] attrs, boolean cr) throws Throwable {
		printIndent();
		write(":" + e);
		printAttrs(attrs);
		if (cr) {
			write("\n");
			incIndentLevel();
		}
	}

	@Override
	public void push(String e, String[] attrs) throws Throwable {
		push(e, attrs, true);
	}

	@Override
	public void push(String element, String[] attributes, final String value) throws Throwable {
		push(element, attributes, false);
		setValue(value);
		write("\n");
		incIndentLevel();
	}

	@Override
	public void push(String element, final String[] attributes, final Object value) throws Throwable {
		push(element, attributes, false);
		setValue(value == null ? null : value.toString());
		write("\n");
		incIndentLevel();
	}

	protected void incIndentLevel() {
		_indentLevel++;
	}

	protected void decIndentLevel() {
		_indentLevel--;
	}

	@Override
	public void setValue(String value, boolean safe) throws Throwable {
		if (value != null) {
			write(" ");
			if (safe) {
				write(XmlSafe.safeValue(value));
			} else {
				write(value);
			}
		}
	}

	@Override
	public String streamMimeType() {
		return "plain/text";
	}
}
