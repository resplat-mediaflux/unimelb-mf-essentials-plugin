#!/bin/bash

TOKEN=@@TOKEN@@
DMDIR=${HOME}/.Arcitecta/DataMover
DMCFG=${DMDIR}/settings.xml

if [[ ! -d ${DMDIR} ]]; then
    echo -n "creating directory: ${DMDIR} ... "
    mkdir -p ${DMDIR}
    echo "done."
fi

if [[ ! -f ${DMCFG} ]]; then
    echo -n "creating Mediaflux Data Mover configuration: ${DMCFG} ... "
    echo "<settings><token>${TOKEN}</token></settings>" > ${DMCFG}
    echo "done."
    exit 0
fi

SETTINGS=$(cat $DMCFG)
if [[ $SETTINGS =~ ${TOKEN} ]]; then
    echo "token: ${TOKEN} already exists in Mediaflux Data Mover configuration file: ${DMCFG}"
    exit 0
elif [[ $SETTINGS =~ "</token>" ]]; then
    SETTINGS=${SETTINGS/<token>[a-zA-Z0-9@]*<\/token>/<token>${TOKEN}<\/token>}
elif [[ $SETTINGS =~ "<token/>" ]]; then
    SETTINGS=${SETTINGS/<token\/>/<token>${TOKEN}<\/token>}
else
    SETTINGS=${SETTINGS/<\/settings>/<token>${TOKEN}<\/token><\/settings>}
fi

TIMESTAMP=$(date +%Y%m%d%H%M%S)
mv ${DMCFG} ${DMCFG}.${TIMESTAMP}

echo -n "installing token: ${TOKEN} into Mediaflux Data Mover config file: ${DMCFG} ... "
echo ${SETTINGS} > ${DMCFG}
echo "done."