@echo off

set TOKEN=@@TOKEN@@
set DMDIR=%USERPROFILE%\.Arcitecta\DataMover
set DMCFG=%DMDIR%\settings.xml

mkdir "%DMDIR%" 2>NUL

if not exist %DMCFG% (
    echo | set /p MSG=creating Mediaflux Data Mover configuration file: %DMCFG% ... 
    echo ^<settings^>^<token^>%TOKEN%^</token^>^</settings^> > %DMCFG%
    echo done.
) else (
    powershell -NoProfile -Command "[xml]$doc = Get-Content %DMCFG%; $te = $doc.DocumentElement.SelectSingleNode(\"token\"); if (-not $te) { $te = $doc.CreateElement(\"token\"); $doc.DocumentElement.AppendChild($te); }; if ( $te.InnerText -ne \"%TOKEN%\") { $te.InnerText = \"%TOKEN%\" ; Write-Host -NoNewline updating Mediaflux Data Mover configuration file: %DMCFG% ... ; Rename-Item \"%DMCFG%\" settings.xml.$(Get-Date -Format yyyyMMddHHmmss); $doc.Save(\"%DMCFG%\"); Write-Host ' done.' } else { Write-Host token: '%TOKEN%' already exists in %DMCFG% No change. };"
)

powershell -NoProfile -Command "Add-Type -AssemblyName PresentationCore,PresentationFramework; [System.Windows.MessageBox]::Show('Token: %TOKEN% has been installed into Mediaflux Data Mover configuration file: %DMCFG%. To load the token, please start or restart Mediaflux Data Mover.', 'Mediaflux Data Mover Token Installer', [System.Windows.MessageBoxButton]::Ok, [System.Windows.MessageBoxImage]::Information)"