# unimelb-mf-essentials-plugin

## Eclipse

* Install the latest eclipse from the [eclipse website](https://www.eclipse.org/downloads/packages/).  Choose the "Eclipse IDE for Java Developers".
* Download the last version of [Open JDK 8](https://adoptium.net/) and install
* Launch eclipse, and create a new workspace to use with JDK 8
* Window -> Preferences -> Java -> Installed JREs
  * Add -> Standard VM
  * Browse to the directory where you installed JDK 8
  * Select the checkbox to denote this is the default JRE for new projects
  * Make sure you don't have any newer versions of Oracle Java 1.8 in this list, as it will be used in preference
* Import project unimelb-mf-essentials-plugin
  * File -> Import -> Existing Maven Projects
  * Navigate to the checked out this git repo and click Finish
* Build
  * Right click on the project root, and Run As -> Maven install.
  * You will find the outputs in the `target` subdirectory of the git repo

## Visual Studio Code

* Install the latest Visual Studio Code.  This can be installed from the Snap Store on Ubuntu
* Make sure you have Java installed (version > 11)
* Install the Java Extension Pack
* Install any other Extension packs you might like e.g. vscodevim
* Import the various git repos into your project

## Maven on the command line

Set JAVA_HOME environment variable and make sure the correct java is in the PATH, then run maven:

    export JAVA_HOME="/opt/jdk8u332-b09"
    export PATH="$JAVA_HOME/bin:$PATH"
    cd <location of git repo>
    mvn clean package

The outputs will be in the `target` directory.
